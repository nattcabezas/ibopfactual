<div class="idsSources view">
<h2><?php echo __('Ids Source'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($idsSource['IdsSource']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($idsSource['IdsSource']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ids Source'), array('action' => 'edit', $idsSource['IdsSource']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ids Source'), array('action' => 'delete', $idsSource['IdsSource']['id']), array(), __('Are you sure you want to delete # %s?', $idsSource['IdsSource']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ids Sources'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ids Source'), array('action' => 'add')); ?> </li>
	</ul>
</div>
