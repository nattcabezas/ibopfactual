<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php echo isset($pageTitle) ? $pageTitle : 'Keepers of the Lineage'; ?></title>
                <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
                
		<?php
                    echo $this->Html->meta('icon') . "\n";

                    echo $this->Html->css('bootstrap.min') . "\n";
                    echo $this->Html->css('jquery.fancybox') . "\n";
                    echo $this->Html->css('font-awesome/css/font-awesome') . "\n";
                    echo $this->Html->css('custom') . "\n";
                    echo $this->Html->css('ng_responsive_tables'); 
                    echo $this->Html->css('responsive-tabs'); 
                    echo $this->Html->css('animateCustom'); 
                    echo $this->Html->css('animations');
                    echo $this->Html->css('pgwslideshow.css');

                    echo $this->Html->script('jquery-1.11.0.min') . "\n"; 
                    echo $this->Html->script('bootstrap.min') . "\n";
                    echo $this->Html->script('jquery.fancybox') . "\n";
                    echo $this->Html->script('ng_responsive_tables') . "\n";
                    echo $this->Html->script('responsive-tabs') . "\n"; 
                    echo $this->Html->script('custom') . "\n";
                    echo $this->Html->script('pgwslideshow.min.js');
                    
                    echo $this->element('meta-tags') . "\n";
		?>
	</head>
        <body>
                <div class="" role="navigation">

                    <div class="row-menutop">

                        <div class="col-lg-12">  
                            <?php echo $this->element('main-menu'); ?>
                        </div>

                    </div>
                </div>

                <div id="hidden-search">
                    <div class="container">
                        <?php echo $this->element('hidden-search'); ?>
                    </div>
                </div>
                
            
                <?php echo $this->fetch('content'); ?>
            
                <footer>
                    <div class="copyright">
                        <div class="container">
                            <div class="col-lg-10">
                                <p>
                                    IBOP Factual. International Brotherhood of Prizefighters. <?php echo date('Y');?>. All rights reserved.
                                    <a href="#"><?php echo __('Home', true);?> </a>   |                                    
                                    <a href="<?php echo $this->Html->url('/Page/Terms_of_service') ?>"><?php echo __('Terms of service', true);?></a>  |   
                                    <a href="<?php echo $this->Html->url('/Page/Privacy_Policy') ?>"><?php echo __('Privacy Policy', true);?> </a>   |                                      
                                    <a href="#"><?php echo __('Contact us', true);?></a>
                                </p>
                            </div>
                            <div>
                                <div class="col-lg-2">
                                    <a href="" class="pull-right social-button">
                                        <i class="fa fa-facebook-square fa-2x"></i>
                                    </a>
                                    <a href="" class="pull-right social-button">
                                        <i class="fa fa-twitter-square fa-2x"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            
	</body>
</html>