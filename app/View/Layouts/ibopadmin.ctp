<!DOCTYPE html>
<html>
    <head>
	<?php echo $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Keepers of the Lineage</title>
	<?php
            echo $this->Html->meta('icon');
                
                
            echo $this->Html->css('bootstrap-admin');
            echo $this->Html->css('abop-admin');
            echo $this->Html->css('font-awesome-admin/css/font-awesome.css');
            echo $this->Html->css('summernote');
            echo $this->Html->css('jquery-ui-1.10.3.custom');
            echo $this->Html->css('jquery-ui-1.10.3.theme');
            echo $this->Html->css('jquery.ui.1.10.3.ie');
            echo $this->Html->css('bootstrap-dialog');
            echo $this->Html->css('jquery.fancybox');

            echo $this->Html->script('jquery-1.11.0.min');
            echo $this->Html->script('jquery-ui-1.10.3.custom.min');
            echo $this->Html->script('bootstrap');
            echo $this->Html->script('summernote.min');
            echo $this->Html->script('admin.general');
            echo $this->Html->script('bootstrap-dialog');
            echo $this->Html->script('jquery.fancybox');    
	?>
    </head>
    <body>
        <div id="wrapper">
            
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'index'));?>">
                        <img src="<?php echo $this->Html->url('/img/ibop_logo_backend.svg');?>" class="logo-layout">
                    </a>
                </div>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <?php echo $this->element('menu'); ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right navbar-user">
                        <li class="dropdown user-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-language"></i>
                                <?php echo __('language', true);?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'changeLanguage/eng'));?>">
                                        <img src="<?php echo $this->html->url('/img/flags/GBR.png') ?>">
                                        <?php echo __('English', true);?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'changeLanguage/spa'));?>">
                                        <img src="<?php echo $this->html->url('/img/flags/ESP.png') ?>">
                                        <?php echo __('Spanish', true);?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown user-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user['User']['name']?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'profile'));?>">
                                        <i class="fa fa-user"></i> <?php echo __('Profile', true);?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Validation', 'action' => 'logout'));?>">
                                        <i class="fa fa-power-off"></i> <?php echo __('Log Out', true);?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
                
            </nav>
            
            <div id="page-wrapper">
                
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Session->flash();  ?>
                    </div>
                </div>
                
                <?php echo $this->fetch('content'); ?>
            </div>
            
        </div>
        <?php echo $this->element('javascripts'); ?>
    </body>
</html>