<!DOCTYPE html>
<html>
    <head>
	<?php echo $this->Html->charset(); ?>
	<title>Keepers of the Lineage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap-admin');
		echo $this->Html->css('login');
		echo $this->Html->css('abop-admin');
                
        echo $this->Html->script('jquery-1.10.2.min');
        echo $this->Html->script('bootstrap');
	?>
    </head>
    <body>
        <div id="fullscreen_bg" class="fullscreen_bg"/>

            <div class="container">
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
    </body>
</html>