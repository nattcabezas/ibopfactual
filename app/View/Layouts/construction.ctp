<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php echo isset($pageTitle) ? $pageTitle : 'Keepers of the Lineage'; ?></title>
                <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
                
		<?php
                    echo $this->Html->meta('icon') . "\n";

                    echo $this->Html->css('bootstrap.min') . "\n";
                    echo $this->Html->css('404') . "\n";
		?>
	</head>
        <body>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <img src="<?php echo $this->Html->url('/img/ibop_logo_construction.svg'); ?>" style="margin-left: auto; margin-right: auto; display: block; margin-top: 5em;" class=" img-responsive" alt="ibop_logo">
                            </div>
                            <div class="error-template">
                                <h1>
                                    Oops!</h1>
                                <h2>
                                    We're working here</h2>
                                <div class="error-details">
                                    This area is in construction. Sorry for the inconvenience.
                                </div>
                                <div class="error-actions">
                                    <a href="<?php echo $this->Html->url('/');?>" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                                        Take Me Home </a><a href="<?php echo $this->Html->url(array('controller' => 'Contacts'));?>" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
	</body>
</html>