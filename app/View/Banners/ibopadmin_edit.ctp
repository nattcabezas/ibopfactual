<?php
echo $this->Html->css('fileinput');
echo $this->Html->script('fileinput');
?>
<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Banners', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-picture-o"></i> 
                    <?php echo __('Banners', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
    </div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-picture-o"></i> <?php echo __('edit banner', true);?></h3>
            </div>
                <div class="panel-body">
                <?php echo $this->Form->create('Banner', array('type' => 'file')); ?>
                    
                    <?php echo $this->Form->input('id');?>
                    <div class="form-group">
                        <label><?php echo __('Tittle:', true);?></label>                                                   
                        <?php echo $this->Form->input('tittle', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Image', true);?></label>
                        <?php echo $this->Form->input('img_file', array('type' => 'file', 'label' => false, 'div' => false, 'class' => 'form-control file-input')); ?>
                        <?php echo $this->Form->input('img', array('type' => 'hidden')); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Link', true);?></label>
                        <?php echo $this->Form->input('link', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control'))?>                        
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Content:', true);?></label>
                        <?php echo $this->Form->input('content', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Type:', true);?></label>
                        <div class="form-group">
                            <?php 
                                $optionsType = array(
                                    '1'  => 'Home Page Top', 
                                    '2'  => 'Home Page Footer',
                                    '3'  => 'Search Persons', 
                                    '4'  => 'Detail Persons',
                                    '5'  => 'Search Fights', 
                                    '6'  => 'Detail Fights',
                                    '7'  => 'Search Events', 
                                    '8'  => 'Detail Events'
                                );
                            ?>
                            <?php echo $this->Form->input('type', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $optionsType, 'empty' => array('0' => 'select type of banner'))); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Date:', true);?></label>
                        <?php echo $this->Form->input('date', array('label' => false, 'div' => false, 'class' => 'form-control form-date', 'type'=> 'text')); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Date end:', true);?></label>
                        <?php echo $this->Form->input('end_date', array('label' => false, 'div' => false, 'class' => 'form-control form-date', 'type' =>'text')); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Status:', true);?></label>
                        <div class="form-group">
                            <?php 
                                $options = array(
                                    '0'  => 'Hide', 
                                    '1'  => 'Visible'
                                    
                                );
                            ?>
                            <?php echo $this->Form->input('status', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->button(__('Edit Banner'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
		</div>
	</div>
</div>
<script>
  $(document).ready(function() {      
        <?php if ($this->request->data('Banner.img') != null) { ?>
            $('.file-input').fileinput({
                showUpload: false,
                initialPreview: ["<img src='<?php echo $this->Html->url('/files/banners/' . $this->request->data('Banner.img')); ?>' class='file-preview-image'>"],
                initialCaption: "<?php echo $this->request->data('Banner.img'); ?>"
            });
        <?php } else { ?>
            $('.file-input').fileinput({showUpload: false});
        <?php } ?>
        
        $('#BannerImgFile').change(function() {
            $('#BannerImg').val($('.file-caption-name').html());
        });
        $('.fileinput-remove-button').click(function(event) {
            $('#BannerImg').val("");
        });
        $('.fileinput-remove').click(function() {
            $('#BannerImg').val("");
        });
    });
</script>