<div class="container search-container">
    <div class="row">
        <h1>Thanks!!!</h1>
        <hr>
        <div class="bs-callout-info">
            <p>Thank you for registering with IBOPFACTUAL.  Please check your e-mail and follow the instructions to complete your registration..</p>
        </div>
    </div>
</div>