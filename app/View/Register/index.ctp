<?php 
    echo $this->Html->script('jquery.plugin');
    echo $this->Html->script('jquery.realperson');
?>
<div class="container search-container">

    <div class="row">
        
            <?php echo $this->Form->create('PageUser'); ?>
        
                <h2><?php echo __('Please Sign Up', true);?></h2>
                
                <hr class="colorgraph">
                
                <?php echo $this->Session->flash();  ?>
                
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <?php echo $this->Form->input('name', array('class' => 'form-control input-lg', 'label' => false, 'div' => 'form-group', 'tabindex' => '1', 'placeholder' => 'First Name', 'required' => true)); ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <?php echo $this->Form->input('last_name', array('class' => 'form-control input-lg', 'label' => false, 'div' => 'form-group', 'tabindex' => '2', 'placeholder' => 'Last Name', 'required' => true))?>
                    </div>
                </div>
                
                <?php echo $this->Form->input('username', array('class' => 'form-control input-lg', 'label' => false, 'div' => 'form-group', 'tabindex' => '3', 'placeholder' => 'User Name', 'required' => true)); ?>
                
                <?php echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control input-lg', 'label' => false, 'div' => 'form-group', 'tabindex' => '4', 'placeholder' => 'Email Address', 'required' => true)); ?>
                
                <div class="row">
                    <?php 
                        if(isset($passwordError)){
                            $classPasswordError = 'has-error';
                        } else {
                            $classPasswordError = '';
                        }
                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control input-lg ' . $classPasswordError, 'label' => false, 'div' => 'form-group', 'tabindex' => '5', 'placeholder' => 'Password', 'required' => true)); ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <?php echo $this->Form->input('confirm_password', array('type' => 'password', 'class' => 'form-control input-lg ' . $classPasswordError, 'label' => false, 'div' => 'form-group', 'tabindex' => '6', 'placeholder' => 'Confirm Password', 'required' => true)); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6" style="text-align: right;">
                        <label class="input-lg">
                            What is the result of 
                            <?php 
                                echo $firstNumber;
                                if($operador == 0){
                                    echo ' + ';
                                } else {
                                    echo ' - ';
                                }
                                echo $secondNumber;
                            ?>
                            =
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <?php
                            if(isset($capchaError)){
                                $classCapchaError = 'has-error';
                            } else {
                                $classCapchaError = '';
                            }
                        ?>
                        <?php echo $this->Form->input('capchaResult', array('type' => 'hidden', 'value' => $result))?>
                        <?php echo $this->Form->input('capcha', array('class' => 'form-control input-lg', 'label' => false, 'div' => 'form-group ' . $classCapchaError , 'tabindex' => '7', 'placeholder' => 'What is the result?', 'id' => 'capcha', 'required' => true)); ?>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-xs-6 col-sm-5 col-md-5">
                        <span class="button-checkbox">
                            <button type="button" class="btn" data-color="info" tabindex="8">I Agree</button>
                            <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1" required="required">
                        </span>
                    </div>
                    <div class="col-xs-6 col-sm-7 col-md-7">
                        <?php echo __('By clicking', true); ?>
                        <strong class="label label-primary"><?php echo __('Register', true);?></strong>, 
                        <?php echo __('you agree to the', true);?>
                        <a href="#" data-toggle="modal" data-target="#t_and_c_m"><?php echo __('Terms and Conditions', true);?></a> 
                        <?php echo __('set out by this site, including our Cookie Use.', true);?>
                    </div>
                </div>

                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-12 col-md-6"><input type="submit" value="Register" class="btn btn-primary btn-block btn-lg" tabindex="9"></div>
                    <div class="col-xs-12 col-md-6"><a href="<?php echo $this->Html->url(array('controller' => 'Register', 'action' => 'login'));?>" class="btn btn-success btn-block btn-lg" id="sign-in">Sign In</a></div>
                </div>
            <?php echo $this->Form->end(); ?>
       
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                </div>
                <div class="modal-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>

<script type="text/javascript">
    $(function() {
        
    });
</script>