<div class="container search-container">
    <div class="row">
        
        <h2><?php echo __('Login', true);?></h2>
        
        <hr class="colorgraph">
        
        <?php echo $this->Session->flash();  ?>
        
        <?php echo $this->Form->create('PageUser')?>
            <fieldset>
                <div class="form-group input-group">
                    <span class="input-group-addon">@</span>
                    <?php echo $this->Form->input('email', array('class' => 'form-control input-lg', 'div' => false, 'label' => false, 'placeholder' => 'Email Address')); ?>
                </div>
                
                <div class="form-group input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-unlock-alt"></i>
                    </span>
                    <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control input-lg', 'div' => false, 'label' => false, 'placeholder' => 'Password')); ?>
                </div>
                
                <span class="button-checkbox">
                    <button type="button" class="btn" data-color="info"><?php echo __('Remember Me', true);?></button>
                    <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
                    <a href="<?php echo $this->Html->url(array('controller' => 'Register', 'action' => 'forgotPassword'));?>" class="btn btn-link pull-right"><?php echo __('Forgot Password?', true);?></a>
                </span>
                
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign In">
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="<?php echo $this->Html->url(array('controller' => 'Register', 'action' => 'index'));?>" class="btn btn-lg btn-primary btn-block"><?php echo __('Register', true);?></a>
                    </div>
                </div>
            </fieldset>
        <?php echo $this->Form->end();  ?>
    </div>
</div>