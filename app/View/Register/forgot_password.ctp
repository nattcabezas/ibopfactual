<div class="container search-container">
    <div class="row">
        <?php echo $this->Form->create('forgotPassword'); ?>
            <h2><?php echo __('Recover Account', true);?></h2>
            <hr class="colorgraph">
            <?php echo $this->Session->flash();  ?>
            
            <?php echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control input-lg', 'label' => false, 'div' => 'form-group', 'tabindex' => '3', 'placeholder' => 'Email', 'required' => true)); ?>
            
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6" style="text-align: right;">
                    <label class="input-lg">
                        What is the result of 
                        <?php 
                            echo $firstNumber;
                            if($operador == 0){
                                echo ' + ';
                            } else {
                                echo ' - ';
                            }
                            echo $secondNumber;
                        ?>
                        =
                    </label>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <?php
                        if(isset($capchaError)){
                            $classCapchaError = 'has-error';
                        } else {
                            $classCapchaError = '';
                        }
                    ?>
                    <?php echo $this->Form->input('capchaResult', array('type' => 'hidden', 'value' => $result))?>
                    <?php echo $this->Form->input('capcha', array('class' => 'form-control input-lg', 'label' => false, 'div' => 'form-group ' . $classCapchaError , 'tabindex' => '7', 'placeholder' => 'What is the result?', 'id' => 'capcha', 'required' => true)); ?>
                </div>
            </div>  
            
            <hr class="colorgraph">
            
            <div class="row">
                <div class="col-xs-12 col-md-6"><input type="submit" value="<?php echo __('Recover Account', true)?>" class="btn btn-primary btn-block btn-lg" tabindex="9"></div>
                <div class="col-xs-12 col-md-6"><a href="<?php echo $this->Html->url(array('controller' => 'Register', 'action' => 'login'));?>" class="btn btn-success btn-block btn-lg" id="sign-in">Sign In</a></div>
            </div>
            
        <?php echo $this->Form->end(); ?>
    </div>
</div>