<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Page User', true); ?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-user"></i> <?php echo __('Page User', true) ?></li>
        </ol>
    </div>
</div>
<!--<div class="row">    
    <div class="col-lg-6">        
<?php echo $this->Form->create('searchContacts', array('url' => array('controller' => 'Contacts', 'action' => 'searchContacts'))); ?>
            <div class="col-lg-10">
<?php echo $this->Form->input('keywork', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
            </div>
            <div class="col-lg-2">
<?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
<?php echo $this->Form->end(); ?>
    </div>        
</div>-->

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true); ?></th>
                        <th><?php echo __('User Name', true); ?></th>
                        <th><?php echo __('status', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pageUser as $user) { ?>
                        <tr class="">
                            <td><?php echo $user['PageUser']['name'] ?> <?php echo $user['PageUser']['last_name'] ?></td>
                            <td><?php echo $user['PageUser']['username'] ?></td>
                            <td>
                                <?php
                                if ($user['PageUser']['status'] == 1) {
                                    echo __('Active', true);
                                } else {
                                    echo __('Look', true);
                                }
                                ?>
                            </td>
                            <td>
                    <center>
                        <?php echo $this->Form->create('PageUser', array('class' => 'form-inline', 'id' => 'user-fomr-' . $user['PageUser']['id'])); ?>
                        <div class="form-group">
                            <label><?php echo __('Type of User', true); ?></label>
                            <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $user['PageUser']['id'])) ?>
                            <?php echo $this->Form->input('type', array('class' => 'form-control page-user-select', 'label' => false, 'div' => false, 'options' => array(1 => 'Normal', 2 => 'Premium', 3 => 'V.I.P'), 'default' => $user['PageUser']['type'])); ?>
                            
                          
                           <a href="<?php echo $this->Html->url(array('action' => 'deleteUser/'. $user['PageUser']['id'])); ?>" class="btn btn-danger btn-delete">
                                <i class="fa fa-trash-o"></i>
                            </a>

                        </div>
                        <?php echo $this->Form->end(); ?>
                    </center>
                    </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.page-user-select').change(function () {
            var urlLoad = "<?php echo $this->Html->url(array('controller' => 'PageUsers', 'action' => 'saveType')); ?>";
            var formData = $(this).parent().parent();
            ///console.log(formData);
            $.post(urlLoad, formData.serialize());
        });
    });
</script>