<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Types of Fights', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-bars"></i> <?php echo __('Types of Fights', true)?></li>
        </ol>
    </div>
    
    <div class="col-lg-2 col-md-offset-10">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Types of Fights', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($types as $type){ ?>
                        <tr>
                            <td><?php echo $type['Type']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($type['Type']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>