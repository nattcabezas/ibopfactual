<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Categories', true); ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'Articles','action' => 'index')); ?>">
                    <i class="fa fa-file"></i> 
                    <?php echo __('Articles', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Categories', true); ?>
            </li>
           
        </ol>
    </div>    
    <div class="col-lg-2 col-md-offset-10"> 
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Category', true); ?>
        </a>      
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true); ?></th>
                        <th><?php echo __('Description', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($categories as $category) { ?>
                  
                        <tr>
                            <td><?php echo $category['Category']['name'] ?></td>
                            <td><?php echo $category['Category']['description'] ?></td>
                            <td>
                    <center>
                        <div class="btn-group">
                            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($category['Category']['id']))); ?>" class="btn btn-primary">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <!--<a href="<?php echo $this->Html->url(array('action' => 'delete/' .  base64_encode($category['Category']['id'])));?>" class="btn btn-danger btn-delete">
                                <i class="fa fa-trash"></i>
                            </a> -->
                        </div>
                    </center>
                    </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php
        echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->numbers(array(
            'before' => '',
            'after' => '',
            'separator' => '',
            'tag' => 'li',
            'currentClass' => 'active',
            'currentTag' => 'a',
        ));
        echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));
        ?>
    </ul>
</div>