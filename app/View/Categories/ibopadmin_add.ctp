<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Categories', true); ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-file"></i> 
                    <?php echo __('Categories', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true); ?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-file"></i> <?php echo __('add category', true); ?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('Category'); ?>

                <div class="form-group">
                    <label><?php echo __('Name:', true); ?></label>
                    <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Description:', true); ?></label>
                    <?php echo $this->Form->input('description', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->button(__('Add Category'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>     