<?php echo $this->element('carousel'); ?>

<div class="col-lg-12">
    <?php echo $this->element('quote-carousel'); ?>
</div>
    <?php echo $this->element('banners', array('thisBanner' => $topBanner)); ?>
 <div class="container">
    <div class="fighters-row">
        <div class="events-cell">

            <ul id="myTab" class="nav nav-tabs responsive">
                <li class="active">
                    <a href="#upcoming_events" class="tab_tittle" data-toggle="tab"><span class="glyphicon glyphicon-exclamation-sign" style="color:#ccc"></span><?php echo __(' Upcoming events', true);?></a>
                </li>
                <li>
                    <a href="#past_events" class="tab_tittle" data-toggle="tab"><span class="glyphicon glyphicon-file" style="color:#ccc"></span><?php echo __(' Past events', true);?></a>
                </li>
            </ul>
    
            <div id="myTabContent" class="tab-content responsive">

                <div class="tab-pane fade in active " id="upcoming_events">

                    <div >
                        <?php foreach ($upcomingEvents as $event){ ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 event-cover">
                                    <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Event']['id'], $event['Event']['name']));?>">
                                        <?php if(count($event['EventsImage']) > 0){?>
                                        <img src="<?php echo $this->Html->url('/files/img/' . $event['EventsImage'][0]['Images']['url']);?>" class="img-responsive img-thumbnail event_img" alt="boxingEvent" ">
                                        <?php } else { ?>
                                            <img src="<?php echo $this->Html->url('/img/defaults/generic_event.jpg')?>" class="img-responsive img-thumbnail event_img" alt="boxingEvent">
                                        <?php } ?>
                                    </a>
                                
                                    <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Event']['id'], $event['Event']['name']));?>" >
                                        <p class="event_fline"> <?php echo $event['Event']['name']?></p>
                                    </a> 
                                        <p><?php echo $event['Event']['description']?></p> 
                                    <?php if($event['Venues']['locations_id'] != null){ ?>
                                        <p><?php echo $event['Venues']['Locations']['State']['name']?>, <?php echo $event['Venues']['Locations']['Countries']['name']?></p>
                                    <?php } ?>
                                </div>
                        <?php } ?>
                    </div>


                </div>

                <div class="tab-pane fade" id="past_events">
                    
                    <div >
                        <?php foreach ($pastEvents as $event){ ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 event-cover" >
                                    <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Event']['id'], $event['Event']['name']));?>">
                                        <?php if(count($event['EventsImage']) > 0){?>
                                        <img src="<?php echo $this->Html->url('/files/img/' . $event['EventsImage'][0]['Images']['url']);?>" class=" img-responsive img-thumbnail event_img" alt="boxingEvent">
                                        <?php } else { ?>
                                            <img src="<?php echo $this->Html->url('/img/defaults/generic_event.jpg')?>" class="img-responsive img-thumbnail event_img" alt="boxingEvent">
                                        <?php } ?>
                                    </a>
                                
                                    <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Event']['id'], $event['Event']['name']));?>">
                                        <p class="event_fline"><?php echo $event['Event']['name']?></p>
                                    </a> 
                                    <p><?php echo $event['Event']['description']?></p> 
                                    <?php if($event['Venues']['locations_id'] != null){ ?>
                                        <p><?php echo $event['Venues']['Locations']['State']['name']?>, <?php echo $event['Venues']['Locations']['Countries']['name']?></p>
                                    <?php } ?>
                            </div>
                                <?php } ?>
                    </div>
                        
                </div>

            </div>

            </div>

        </div>
    </div>
</div>    

<div class="fights-row">
     <div class="container">
    <div class="container-pad" id="fights-listings">

        <!-- Begin Fights-->
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="fights-row-tittle">
                <span class="h1"><?php echo __('Featured Fights'); ?></span> <hr>
            </div>  
            <div class=""> 
                <?php foreach ($featuredFights as $fight) { ?>
                <div class="versus-cell " >
                        <div class="text-center">
                            <div class="col-lg-12 clearfix ">
                                <h4 class="">
                                    <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($fight['Fights']['id'], $fight['Fights']['title'])); ?>" target="_parent"><?php echo $fight['Fights']['title'] ?></a>
                                </h4>
                                
                                <div class="col-lg-12 clearfix">   

                                    <div class="col-lg-12">
                                        <h5 class="fnt-smaller fnt-lighter fnt-arial">
                                            <a href='<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($fight['Event']['id'], $fight['Event']['name'])); ?>'><?php echo $fight['Event']['name']; ?></a>
                                        </h5>
                                    </div>

                                </div>
                            </div>
                            
                            <div class="row">
                                
                                    <div class="col-lg-3 col-lg-offset-2 col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">
                                        <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['Corners'][0]['Identities']['id'], $fight['Corners'][0]['Identities']['name'] . ' ' . $fight['Corners'][0]['Identities']['last_name'])); ?>" class="text-center">
                                            <?php if ($fight['Corners'][0]['Image']['url'] != null) { ?>
                                                <img src="<?php echo $this->Html->url('/files/img/' . $fight['Corners'][0]['Image']['url']); ?>" alt="<?php echo $fight['Corners'][0]['Image']['title']; ?>" class="img img-responsive img-fighter">
                                            <?php } else { ?>
                                                <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" alt="" class="img img-responsive img-fighter">
                                            <?php } ?>
                                            <?php echo $fight['Corners'][0]['Identities']['name'] . ' ' . $fight['Corners'][0]['Identities']['last_name'] ?>
                                        </a>
                                    </div>
                                    <div class="img_vs col-lg-2 col-md-1 col-sm-1">
                                        <img src="<?php echo $this->Html->url('/img/vs-04.png') ?>" alt="" class="img img-responsive img-versus" >
                                    </div>

                                    <div class="col-lg-3 col-md-4 col-sm-4">
                                        <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['Corners'][1]['Identities']['id'], $fight['Corners'][1]['Identities']['name'] . ' ' . $fight['Corners'][1]['Identities']['last_name'])); ?>" class="">
                                            <?php if ($fight['Corners'][1]['Image']['url'] != null) { ?>
                                                <img src="<?php echo $this->Html->url('/files/img/' . $fight['Corners'][1]['Image']['url']); ?>" alt="<?php echo $fight['Corners'][1]['Image']['title']; ?>" class="img img-responsive img-fighter">
                                            <?php } else { ?>
                                                <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" alt="" class="img img-responsive img-fighter">
                                            <?php } ?>
                                            <?php echo $fight['Corners'][1]['Identities']['name'] . ' ' . $fight['Corners'][1]['Identities']['last_name'] ?>
                                        </a>
                                    </div>
                                
                            </div>
                            
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
<!-- Begin events-->
            <div class="col-lg-6 col-md-6 col-sm-6">  
                <div class="fights-row-tittle">
                <span class="h1"><?php echo __('Featured Events');?></span> <hr>
                </div> 
                 <div class=""> 
                <!-- Begin Event-->
                <?php foreach ($featuredEvent as $event){ ?>
                    <div class="versus-cell fights-listing">
                        <div class="media">
                            <a class="pull-left" href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Events']['id'], $event['Events']['name']));?>" target="_parent">
                                <?php if(count($event['Image']) > 0){ ?>
                                    <img src="<?php echo $this->Html->url('/files/img/' . $event['Image']['url']);?>" alt="<?php echo $event['Image']['title']; ?>" class="img img-responsive">
                                <?php } else { ?>
                                    <img src="<?php echo $this->Html->url('/img/defaults/generic_event.jpg')?>" alt="" class="img img-responsive">
                                <?php } ?>
                            </a>

                            <div class="clearfix visible-sm"></div>

                            <div class="media-body fnt-smaller">
                                <a href="#" target="_parent"></a>

                                <h4 class="media-heading" >
                                    <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Events']['id'], $event['Events']['name']));?>"><?php echo $event['Events']['name']?></a> 
                                </h4>

                                <p class="clr-aaa" style="margin-top: 0.3em">
                                    <?php echo $event['Location']['State']?>, <?php echo $event['Location']['country']?>
                                    <img src="<?php echo $this->Html->url('/img/flags/64/' . $event['Location']['flag']);?>" alt="<?php echo $event['Location']['country']?>" class="pull-right img img-responsive" width="32px"/> 
                                </p>

                                <p class="hidden-xs p-color">
                                    <?php 
                                        if($event['Events']['description'] != null){
                                            echo substr($event['Events']['description'], 0, 250) . '...'; 
                                        }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- End Event--> 
            </div>
            
        </div><!-- End row -->
    
    </div><!-- End container -->
    </div>
</div><!-- End row -->

<?php 
  echo $this->element('banners', array('thisBanner' => $footerBanner));   
?>


<div class="container">
    <div class="articles-cell">
        
        <?php foreach ($feedArticles as $article) { ?>
        
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div>
                <h3 class="text-center">
                    <a href="<?php echo $article['link']?>" target="_blank"><?php echo $article['title']?></a>
                </h3>
                <!--<img src="http://placehold.it/260x174/999999/cccccc" class="img-responsive img-thumbnail " width="600" >
                <p class="text-center">
                    Irish featherweight Conor McGregor on Saturday had an entire nation walking with him. Since McGregor last stepped in the cage, he had become a living sound bite and the most hyped fighter...
                </p>-->
                <div class="feed-conted">
                    <?php echo $article['description']?>
                </div>
            </div>
        </div>
        <?php } ?>
        
    </div>
</div>

<?php echo $this->Html->script('home'); ?>



    <script type="text/javascript">
  (function($) {
      fakewaffle.responsiveTabs(['xs', 'sm']);
  })(jQuery);
</script>
