<style type="text/css">
	.body-panel{
		display: none;
	}
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Fights', true);?> - <?php echo $titleName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-shield"></i> 
                    <?php echo __('Fights', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            
            <a href="<?php echo $this->Html->url('/Fight/'.$this->getUrlPerson->getUrl($id,$titleName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
                <i class="fa fa-desktop"></i>
            </a>
            
            <?php if($user['Role']['manage'] == 1 ){ ?>
               
                <?php $locked = $fight['Fight']['locked'] == 1 ? '0' : '1'; ?>
                <a href="<?php echo $this->Html->url(array('action' => 'lock/' . base64_encode($id) . '/' . $locked)); ?>" class="btn btn-primary tooltip-button">
                    <?php if( $fight['Fight']['locked'] == 1){ ?>
                        <i class="fa fa-lock"></i>
                    <?php }else{ ?>
                        <i class="fa fa-unlock-alt"></i>
                    <?php }?>
                </a>
            <?php } ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($id))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($id))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($id))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Odds', 'action' => 'index/' . base64_encode($id))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Odds'); ?>">
                <i class="fa fa-usd"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'titles/' . base64_encode($id))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Titles'); ?>">
                <i class="fa fa-bookmark"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($id))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>
</div>

<div class="row">
	<div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="#" class="display-panel" target-display="#basic-panel"><i class="fa fa-shield"></i> <?php echo __('Edit Fight', true);?></a></h3>
                </div>
                <div class="panel-body body-panel" id="basic-panel">
                    <?php echo $this->Form->create('Fight'); ?>
                        
                        <?php echo $this->Form->input('id'); ?>
                        <div class="form-group">
                                <label><?php echo __('Source', true);?></label>
                                <?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'select source'), 'required' => false , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Title', true);?></label>
                                <?php echo $this->Form->input('title', array('label' => false, 'div' => false, 'class' => 'form-control', 'required' => true, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>
                        
                        <div class="form-group">
                                <label><?php echo __('Event', true);?></label>
                                <?php echo $this->Form->input('events_label', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true, 'value' => $this->request->data('Event.name'), 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                                <?php echo $this->Form->input('events_id', array('type' => 'hidden')); ?>
                        </div>
                    
                        <div class="form-group">
                                <label><?php echo __('Description', true);?></label>
                                <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Weight', true);?></label>
                                <?php echo $this->Form->input('weights_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'select weight'), 'required' => false, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Scheduled Rounds', true);?></label>
                                <?php echo $this->Form->input('schedule_rounds', array('label' => false, 'div' => false, 'class' => 'form-control', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Time per Round (-1 means timeless round)', true);?></label>
                                <br/>
                                <label>
                                     <?php echo $this->Form->checkbox('no_time_checkbox'); ?>
                                 </label>
                                <?php echo __('Round with no time', true);?>
                                
                                
                                <?php echo $this->Form->input('time_per_round', array('label' => false, 'div' => false, 'class' => 'form-control', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Boxed Rounds', true);?></label>
                                <?php echo $this->Form->input('boxed_roundas', array('label' => false, 'div' => false, 'class' => 'form-control', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>
                        
                        <div class="form-group">
                                <label><?php echo __('Via', true);?></label>
                                <?php 
                                    $options = array(
                                        'KO'            => 'KO', 
                                        'TKO'           => 'TKO', 
                                        'TD'            => 'TD', 
                                        'MD'            => 'MD', 
                                        'SD'            => 'SD', 
                                        'D-PTS'         => 'D-PTS', 
                                        'DRAW'          => 'DRAW', 
                                        'D-NWS'         => 'D-NWS', 
                                        'NWS'           => 'NWS', 
                                        'RTD'           => 'RTD', 
                                        'PTS'           => 'PTS', 
                                        'MAJORITY DRAW' => 'MAJORITY DRAW', 
                                        'FOUL'          => 'FOUL', 
                                        'DQ'            => 'DQ', 
                                        'SUBMISSION'    => 'SUBMISSION', 
                                        'STOPPAGE'      => 'STOPPAGE', 
                                        'NC'            => 'NC',
                                        'ND'            => 'ND',
                                        'Split Draw'    => 'Split Draw',
                                        'UD'            => 'UD',
                                        'scheduled'     => 'scheduled', 
                                        'EXHIBITION'    => 'EXHIBITION', 
                                        'AMATEUR'       => 'AMATEUR'
                                    );
                                ?>
                                <?php echo $this->Form->input('via', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => array('' => 'select via'), 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>
                        
                        <div class="form-group" id="submission_comments" style="display: <?php echo $this->request->data('Fight.via') == 'SUBMISSION' ? 'block;' : 'none;'; ?>;">
                                <label><?php echo __('Submission Comments', true);?></label>
                                <?php 
                                    
                                    $options = array();
                                    foreach ($submissions as $submission)
                                    {
                                        $options[$submission['Submission']['name']] = $submission['Submission']['name'];
                                    }
                                ?>
                                <?php echo $this->Form->input('submission_comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => array('' => 'Select Submission Comment'), 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>
                    
                        <div class="form-group">
                                <label><?php echo __('Winner', true);?></label>
                                <?php $options = array(1 => 'Corner 1', 2 => 'Corner 2', 3 => 'Draw', 4 => 'No Contest');?>
                                <?php echo $this->Form->input('winner', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => 'select winner', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>
                    
                        <!--<div class="form-group">
                                <label><?php //echo __('', true);?></label>
                                <?php //echo $this->Form->input('final_figths_id', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>-->

                        <div class="form-group">
                                <label><?php echo __('Rules', true);?></label>
                                <?php echo $this->Form->input('rules_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'select rules'), 'required' => false, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Time', true);?></label>
                                <?php echo $this->Form->input('time', array('label' => false, 'div' => false, 'class' => 'form-control', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Notes', true);?></label>
                                <?php echo $this->Form->input('notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Boxrec Notes', true);?></label>
                                <?php echo $this->Form->input('boxrec_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Other Notes ', true);?></label>
                                <?php echo $this->Form->input('other_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
                        </div>

                        <!--<div class="form-group">
                                <label><?php echo __('Winer', true);?></label>
                                <?php echo $this->Form->input('winner', array('label' => false, 'div' => false, 'class' => 'form-control', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>-->

                        <div class="form-group">
                                <label><?php echo __('Types', true);?></label>
                                <?php echo $this->Form->input('types_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                        </div>

                        <div class="checkbox">
                                <label>
                                        <?php echo $this->Form->input('is_competitor', array('type' => 'checkbox', 'label' => false, 'div' => false, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                                        <?php echo __('is competitor', true);?>
                                </label>
                        </div>

                        <div class="form-group">
                            <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                                <?php echo $this->Form->button(__('Edit Fight'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                            <?php } ?>
                        </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
	</div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#coner-1"><i class="fa fa-shield"></i> <?php echo __('Corner 1', true);?></a></h3>
            </div>
            <div class="panel-body body-panel" id="coner-1">
                <?php echo $this->Form->create('FightIdentity', array('url' => array('controller' => 'Fights', 'action' => 'saveCorner'))); ?>
                
                    <?php
                        
                        if( ($corner1['Identities']['name'] != "") && ($corner1['Identities']['last_name'] != "") ){
                            $corner1name = $corner1['Identities']['name'].' '.$corner1['Identities']['last_name'];
                        } else if($corner1['Identities']['name'] != ""){
                            $corner1name = $corner1['Identities']['name'];
                        } else if($corner1['Identities']['last_name'] != ""){
                            $corner1name = $corner1['Identities']['last_name'];
                        } else {
                            $corner1name = "";
                        }
                        
                        echo $this->Form->input('id', array('type' => 'hidden', 'value' => $corner1['FightIdentity']['id'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true));
                        echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $id, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true));
                        echo $this->Form->input('corner', array('type' => 'hidden', 'value' => 1, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true));
                    ?>
                
                    <div class="form-group">
                        <label><?php echo __('Fighter', true);?></label>
                        <?php echo $this->Form->input('identities_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1IdentitiesLabel', 'required' => true, 'value' => $corner1name, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'id' => 'Corner1IdentitiesId', 'value' => $corner1['FightIdentity']['identities_id']))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Corner color', true);?></label>
                        <?php echo $this->Form->input('corner_color', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1CornerColor', 'value' => $corner1['FightIdentity']['corner_color'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Weight ', true);?></label>
                        <?php echo $this->Form->input('weigth', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1Weigth', 'value' => $corner1['FightIdentity']['weigth'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Final Weight', true);?></label>
                        <?php echo $this->Form->input('final_weigth', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1FinalWeigth', 'value' => $corner1['FightIdentity']['final_weigth'],'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Shorts', true);?></label>
                        <?php echo $this->Form->input('shorts', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1Shorts', 'value' => $corner1['FightIdentity']['shorts'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Robe', true);?></label>
                        <?php echo $this->Form->input('robe', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1Robe', 'value' => $corner1['FightIdentity']['robe'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Robe Says', true);?></label>
                        <?php echo $this->Form->input('robe_says', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1RobeSays', 'value' => $corner1['FightIdentity']['robe_says'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Purse Amount', true);?></label>
                        <?php echo $this->Form->input('purse_amount', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1PurseAmount', 'value' => $corner1['FightIdentity']['purse_amount'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div> 
                    <div class="form-group">
                        <label><?php echo __('Purse Comments', true);?></label>
                        <?php echo $this->Form->input('purse_comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner1PurseComments', 'value' => $corner1['FightIdentity']['purse_comments'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div> 
                    <div class="form-group">
                        <label><?php echo __('Death in ring?', true);?></label>
                        <?php echo $this->Form->input('death_ring', array('label' => false, 'div' => false, 'id' => 'Corner1DeathRing', 'class' => 'form-control', 'options' => array('no' => 'no', 'yes' => 'yes'), 'empty' => array(0 => 'select option'), 'default' => $corner1['FightIdentity']['death_ring'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Nickname Used', true);?></label>
                        <?php echo $this->Form->input('comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'textarea', 'value' => $corner1['FightIdentity']['comments'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <!--<div class="checkbox">
                        <label>
                            <?php echo $this->Form->input('winner', array('type' => 'checkbox', 'label' => false, 'div' => false, 'checked' => $corner1['FightIdentity']['winner'],'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                            <?php echo __('is Winner', true);?>
                        </label>
                    </div>-->
                    <div class="form-group">
                        <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                            <?php echo $this->Form->button(__('save corner'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        <?php } ?>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#coner-2"><i class="fa fa-shield"></i> <?php echo __('Corner 2', true);?></a></h3>
            </div>
            <div class="panel-body body-panel" id="coner-2">
                <?php echo $this->Form->create('FightIdentity', array('url' => array('controller' => 'Fights', 'action' => 'saveCorner'))); ?>
                
                    <?php
                    
                        if( ($corner2['Identities']['name'] != "") && ($corner2['Identities']['last_name'] != "") ){
                            $corner2name = $corner2['Identities']['name'].' '.$corner2['Identities']['last_name'];
                        } else if($corner2['Identities']['name'] != ""){
                            $corner2name = $corner2['Identities']['name'];
                        } else if($corner2['Identities']['last_name'] != ""){
                            $corner2name = $corner2['Identities']['last_name'];
                        } else {
                            $corner2name = "";
                        }
                        
                        echo $this->Form->input('id', array('type' => 'hidden', 'value' => $corner2['FightIdentity']['id']));
                        echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $id));
                        echo $this->Form->input('corner', array('type' => 'hidden', 'value' => 2));
                    ?>
                
                    <div class="form-group">
                        <label><?php echo __('Fighter', true);?></label>
                        <?php echo $this->Form->input('identities_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2IdentitiesLabel', 'required' => true, 'value' => $corner2name, 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'id' => 'Corner2IdentitiesId', 'value' => $corner2['FightIdentity']['identities_id']))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Corner color', true);?></label>
                        <?php echo $this->Form->input('corner_color', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2CornerColor', 'value' => $corner2['FightIdentity']['corner_color'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Weight', true);?></label>
                        <?php echo $this->Form->input('weigth', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2Weigth', 'value' => $corner2['FightIdentity']['weigth'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Final Weight', true);?></label>
                        <?php echo $this->Form->input('final_weigth', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2FinalWeigth', 'value' => $corner2['FightIdentity']['final_weigth'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Shorts', true);?></label>
                        <?php echo $this->Form->input('shorts', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2Shorts', 'value' => $corner2['FightIdentity']['shorts'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Robe', true);?></label>
                        <?php echo $this->Form->input('robe', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2Robe', 'value' => $corner2['FightIdentity']['robe'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Robe Says', true);?></label>
                        <?php echo $this->Form->input('robe_says', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2RobeSays', 'value' => $corner2['FightIdentity']['robe_says'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Purse Amount', true);?></label>
                        <?php echo $this->Form->input('purse_amount', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2PurseAmount', 'value' => $corner2['FightIdentity']['purse_amount'],'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div> 
                    <div class="form-group">
                        <label><?php echo __('Purse Comments', true);?></label>
                        <?php echo $this->Form->input('purse_comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'Corner2PurseComments', 'value' => $corner2['FightIdentity']['purse_comments'],'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div> 
                    <div class="form-group">
                        <label><?php echo __('Death in ring?', true);?></label>
                        <?php echo $this->Form->input('death_ring', array('label' => false, 'div' => false, 'id' => 'Corner2DeathRing', 'class' => 'form-control', 'options' => array('no' => 'no', 'yes' => 'yes'), 'empty' => array(0 => 'select option'), 'default' => $corner2['FightIdentity']['death_ring'],'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Nickname Used', true);?></label>
                        <?php echo $this->Form->input('comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'textarea', 'value' => $corner2['FightIdentity']['comments'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                    </div>
                    <!--<div class="checkbox">
                        <label>
                            <?php echo $this->Form->input('winner', array('type' => 'checkbox', 'label' => false, 'div' => false, 'checked' => $corner2['FightIdentity']['winner'], 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                            <?php echo __('is Winner', true);?>
                        </label>
                    </div>-->
                    <div class="form-group">
                        <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                            <?php echo $this->Form->button(__('save corner'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        <?php } ?>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#other-information"><i class="fa fa-shield"></i> <?php echo __('Other Information', true);?></a></h3>
            </div>
            <div class="panel-body body-panel" id="other-information">
                <?php echo $this->Form->create('FightDatum', array('url' => array('controller' => 'Fights', 'action' => 'saveFightDatum'))); ?>
                
                    <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => isset($fightsData['FightDatum']['id']) ? $fightsData['FightDatum']['id'] : ''));?>
                <?php echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $id));?>
                    <div class="form-group">
                        <label><?php echo __('Referee', true);?></label>
                        <?php echo $this->Form->input('referee_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => isset($fightsData['Referee']['name']) ? $fightsData['Referee']['name'].' '.$fightsData['Referee']['last_name']  : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('referee', array('type' => 'hidden', 'value' => isset($fightsData['Referee']['id']) ? $fightsData['Referee']['id'] : ''))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Ring announcer', true);?></label>
                        <?php echo $this->Form->input('ring_announcer_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => isset($fightsData['ring_announcer']['name']) ? $fightsData['ring_announcer']['name'].' '.$fightsData['ring_announcer']['last_name'] : '', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('ring_announcer', array('type' => 'hidden', 'value' => isset($fightsData['ring_announcer']['id']) ? $fightsData['ring_announcer']['id'] : ''))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Judge 1', true);?></label>
                        <?php echo $this->Form->input('judge_1_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => isset($fightsData['judge_1']['name']) ? $fightsData['judge_1']['name'].' '.$fightsData['judge_1']['last_name'] : '', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('judge_1', array('type' => 'hidden', 'value' => isset($fightsData['judge_1']['id']) ? $fightsData['judge_1']['id'] : ''))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Judge 2', true);?></label>
                        <?php echo $this->Form->input('judge_2_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => isset($fightsData['judge_2']['name']) ? $fightsData['judge_2']['name'].' '.$fightsData['judge_2']['last_name'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('judge_2', array('type' => 'hidden', 'value' => isset($fightsData['judge_2']['id']) ? $fightsData['judge_2']['id'] : ''))?>

                    </div>
                    <div class="form-group">
                        <label><?php echo __('Judge 3', true);?></label>
                        <?php echo $this->Form->input('judge_3_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => isset($fightsData['judge_3']['name']) ? $fightsData['judge_3']['name'].' '.$fightsData['judge_3']['last_name'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('judge_3', array('type' => 'hidden', 'value' => isset($fightsData['judge_3']['id']) ? $fightsData['judge_3']['id'] : ''))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Score Judge 1', true);?></label>
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('score1_judge_1', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Score 1'), 'value' => isset($fightsData['FightDatum']['score1_judge_1']) ? $fightsData['FightDatum']['score1_judge_1'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                            </div>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('score2_judge_1', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Score 2'), 'value' => isset($fightsData['FightDatum']['score2_judge_1']) ? $fightsData['FightDatum']['score2_judge_1'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Score Judge 2', true);?></label>
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('score1_judge_2', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Score 1'), 'value' => isset($fightsData['FightDatum']['score1_judge_2']) ? $fightsData['FightDatum']['score1_judge_2'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                            </div>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('score2_judge_2', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Score 2'), 'value' => isset($fightsData['FightDatum']['score2_judge_2']) ? $fightsData['FightDatum']['score2_judge_2'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Score Judge 3', true);?></label>
                        <div class="row">
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('score1_judge_3', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Score 1'), 'value' => isset($fightsData['FightDatum']['score1_judge_3']) ? $fightsData['FightDatum']['score1_judge_3'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                            </div>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('score2_judge_3', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Score 2'), 'value' => isset($fightsData['FightDatum']['score2_judge_3']) ? $fightsData['FightDatum']['score2_judge_3'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Title Supervisor', true);?></label>
                        <?php echo $this->Form->input('inspector_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => isset($fightsData['inspector']['name']) ? $fightsData['inspector']['name'].' '.$fightsData['inspector']['last_name'] : '' , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('inspector', array('type' => 'hidden', 'value' => isset($fightsData['inspector']['id']) ? $fightsData['inspector']['id'] : ''))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Timekeeper', true);?></label>
                        <?php echo $this->Form->input('timekeeper_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => isset($fightsData['timekeeper']['name']) ? $fightsData['timekeeper']['name'].''.$fightsData['timekeeper']['last_name'] : '', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true))?>
                        <?php echo $this->Form->input('timekeeper', array('type' => 'hidden', 'value' => isset($fightsData['timekeeper']['id']) ? $fightsData['timekeeper']['id'] : ''))?>
                    </div>
                    <div class="form-group">
                        <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                            <?php echo $this->Form->button(__('save information'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        <?php } ?>
                    </div>
                
                <?php echo $this->Form->end(); ?>
                
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#corner1-jobs"><i class="fa fa-shield"></i> <?php echo __('Corner 1 Jobs');?></a></h3>
            </div>
            <div class="panel-body body-panel" id="corner1-jobs">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->create('FighterJob', array('url' => array('controller' => 'Fights', 'action' => 'saveJobs')));?>
                            <?php
                                echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $id));
                                echo $this->Form->input('corner', array('type' => 'hidden', 'value' => '1'));
                            ?>
                            <div class="form-group">
                                <label><?php echo __('Person:', true);?></label>
                                <?php echo $this->Form->input('identities_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'required' => true, 'id' => 'Corner1JobIdentitiesLabel', 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'id' => 'Corner1JobIdentities'))?>
                            </div>
                            <div class="form-group">
                                <label><?php echo __('Jobs:', true);?></label>
                                <?php echo $this->Form->input('jobs_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $jobs, 'empty' => array(0 => 'select job') , 'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo __('Description:');?></label>
                                <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control'))?>
                            </div>
                            <div class="form-group">
                                <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                                <?php } ?>
                            </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-striped tablesorter">
                                <thead>
                                    <tr>
                                        <th><?php echo __('Job', true);?></th>
                                        <th><?php echo __('Name', true);?></th>
                                        <th>&emsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($jobsCorner1 as $job){ ?>
                                        <tr>
                                            <th><?php echo $job['Jobs']['name']?></th>
                                            <th><?php echo $job['Identities']['name']?> <?php echo $job['Identities']['last_name']?></th>
                                            <th>
                                                <div class="btn-group">
                                                    <?php 
                                                        echo $this->Form->create('FighterJob', array('url' => array('controller' => 'Fights', 'action' => 'deleteJobs')));
                                                        echo $this->Form->input('id', array('type' => 'hidden', 'value' => $job['FighterJob']['id']));
                                                        echo $this->Form->input('idFight', array('type' => 'hidden', 'value' => $id));
                                                        if( $fight['Fight']['locked'] == 0 ){ 
                                                            echo $this->Form->button('<i class="fa fa-trash-o"></i>', array('class' => 'btn btn-primary', 'type' => 'submit')); 
                                                        }
                                                        echo $this->Form->end();
                                                    ?>
                                                </div>
                                            </th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#corner2-jobs"><i class="fa fa-shield"></i> <?php echo __('Corner 2 Jobs');?></a></h3>
            </div>
            <div class="panel-body body-panel" id="corner2-jobs">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->create('FighterJob', array('url' => array('controller' => 'Fights', 'action' => 'saveJobs')));?>
                            <?php
                                echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $id));
                                echo $this->Form->input('corner', array('type' => 'hidden', 'value' => '2'));
                            ?>
                            <div class="form-group">
                                <label><?php echo __('Person:', true);?></label>
                                <?php echo $this->Form->input('identities_label', array('label' => false, 'div' => false, 'class' => 'form-control', 'required' => true, 'id' => 'Corner2JobIdentitiesLabel',  'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'id' => 'Corner2JobIdentities'))?>
                            </div>
                            <div class="form-group">
                                <label><?php echo __('Jobs:', true);?></label>
                                <?php echo $this->Form->input('jobs_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $jobs, 'empty' => array(0 => 'select job'),  'readonly' => $fight['Fight']['locked'] == 0 ? false : true)); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo __('Description:');?></label>
                                <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control'))?>
                            </div>
                            <div class="form-group">
                                <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                                <?php } ?>
                            </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-striped tablesorter">
                                <thead>
                                    <tr>
                                        <th><?php echo __('Job', true);?></th>
                                        <th><?php echo __('Name', true);?></th>
                                        <th>&emsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($jobsCorner2 as $job){ ?>
                                        <tr>
                                            <th><?php echo $job['Jobs']['name']?></th>
                                            <th><?php echo $job['Identities']['name']?> <?php echo $job['Identities']['last_name']?></th>
                                            <th>
                                                <div class="btn-group">
                                                    <?php 
                                                        echo $this->Form->create('FighterJob', array('url' => array('controller' => 'Fights', 'action' => 'deleteJobs')));
                                                        echo $this->Form->input('id', array('type' => 'hidden', 'value' => $job['FighterJob']['id']));
                                                        echo $this->Form->input('idFight', array('type' => 'hidden', 'value' => $id));
                                                        if( $fight['Fight']['locked'] == 0 ){ 
                                                            echo $this->Form->button('<i class="fa fa-trash-o"></i>', array('class' => 'btn btn-primary', 'type' => 'submit')); 
                                                        }
                                                        echo $this->Form->end();
                                                    ?>
                                                </div>
                                            </th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-4"></div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        
        /***************/
        /*
         * This code fills the corner name space so the operator
         * dont have to fill it
         */
        var checkCorner1 = "<?php echo $corner1['FightIdentity']['identities_id'];?>";
        var checkCorner2 = "<?php echo $corner2['FightIdentity']['identities_id'];?>";
        var fightTitle = "<?php echo $titleName?>";
        
        if (checkCorner1=="")
        {
            var fighter1 = fightTitle.substr(0, fightTitle.indexOf(' vs')); 
            var loadFighter1 = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(fighter1);
            $.post(loadFighter1, function (data) {
                var json = JSON.parse(data);
                $('#Corner1IdentitiesLabel').val(json[0].label);
                $('#Corner1IdentitiesId').val(json[0].id);
            });
        }
        
        if (checkCorner2=="")
        {
            var fighter2 = fightTitle.substr(fightTitle.indexOf("vs ")+3);
            fighter2 = fighter2.trim();        
            var loadFighter2 = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(fighter2);        
            $.post(loadFighter2, function (data) {
                var json = JSON.parse(data);
                $('#Corner2IdentitiesLabel').val(json[0].label);
                $('#Corner2IdentitiesId').val(json[0].id);
            });
        }
        /***************/
        
        $('#FightNoTimeCheckbox').change(function(){
            if($(this).is(':checked')){
                $("#FightTimePerRound").prop("readonly", true);
                $("#FightTimePerRound").val(-1);
            } else {
                $("#FightTimePerRound").prop("readonly", false);
                $("#FightTimePerRound").val(1);
            }
        });
        
        
        $('.display-panel').click(function(event){
            event.preventDefault();
            var panel = $(this).attr('target-display');
            if( $(panel).is(':visible') ){  
                    $(panel).slideUp(1000);
            } else {
                    $(panel).slideDown(1000);
            }
            return false;
        });
        
        $('#Corner1IdentitiesLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#Corner1IdentitiesLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#Corner1IdentitiesLabel').val(ui.item.label);
                $('#Corner1IdentitiesId').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#Corner2IdentitiesLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#Corner2IdentitiesLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#Corner2IdentitiesLabel').val(ui.item.label);
                $('#Corner2IdentitiesId').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightDatumRefereeLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#FightDatumRefereeLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightDatumRefereeLabel').val(ui.item.label);
                $('#FightDatumReferee').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightDatumRingAnnouncerLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#FightDatumRingAnnouncerLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightDatumRingAnnouncerLabel').val(ui.item.label);
                $('#FightDatumRingAnnouncer').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightDatumJudge1Label').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#FightDatumJudge1Label').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightDatumJudge1Label').val(ui.item.label);
                $('#FightDatumJudge1').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightDatumJudge2Label').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#FightDatumJudge2Label').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightDatumJudge2Label').val(ui.item.label);
                $('#FightDatumJudge2').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightDatumJudge3Label').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#FightDatumJudge3Label').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightDatumJudge3Label').val(ui.item.label);
                $('#FightDatumJudge3').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightDatumInspectorLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#FightDatumInspectorLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightDatumInspectorLabel').val(ui.item.label);
                $('#FightDatumInspector').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightDatumTimekeeperLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#FightDatumTimekeeperLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightDatumTimekeeperLabel').val(ui.item.label);
                $('#FightDatumTimekeeper').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#Corner1JobIdentitiesLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#Corner1JobIdentitiesLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#Corner1JobIdentitiesLabel').val(ui.item.label);
                $('#Corner1JobIdentities').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#Corner2JobIdentitiesLabel').autocomplete({
            minLength: 2,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui){
                $('#Corner2JobIdentitiesLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#Corner2JobIdentitiesLabel').val(ui.item.label);
                $('#Corner2JobIdentities').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
        };
        
        $('#FightEventsLabel').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getEvents'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('#FightEventsLabel').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                $('#FightEventsLabel').val(ui.item.label);
                $('#FightEventsId').val(ui.item.id);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $('#FightVia').change(function(){
           if($(this).val() === 'SUBMISSION'){
               $('#submission_comments').fadeIn();
           }else if($(this).val() === 'STOPPAGE'){
               $('#submission_comments').fadeIn();
           }else{
               $('#submission_comments').fadeOut();
           }
        });
        
    });
</script>
