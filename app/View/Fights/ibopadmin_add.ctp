<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Fights', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-shield"></i> 
                    <?php echo __('Fights', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
</div>

<div class="row">
	<div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-shield"></i> <?php echo __('add Fight', true);?></h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create('Fight'); ?>

                        <div class="form-group">
                                <label><?php echo __('Source', true);?></label>
                                <?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'select source'), 'required' => true)); ?>
                        </div>

                        <div class="form-group">
                                <label id="labelTitle"><?php echo __('Title', true);?></label>
                                
                                <input type="text" id="corner1" required="true" class="form-control input-sm search-identity" placeholder="<?php echo __('Corner 1'); ?>">
                                <?php echo $this->Form->input('corner1_id', array('type' => 'hidden')); ?>
                                <label><?php echo __('VS.', true);?></label>
                                <input type="text" id="corner2" required="true" class="form-control input-sm search-identity" placeholder="<?php echo __('Corner 2'); ?>">
                                <?php echo $this->Form->input('corner2_id', array('type' => 'hidden')); ?>
                                
                                <?php echo $this->Form->input('title', array('label' => false, 'div' => false, 'class' => 'form-control', 'required' => true , 'id' => 'inputTitle' , 'type' => 'hidden')); ?>
                        </div>
                    
                        <div class="form-group">
                                <label><?php echo __('Event', true);?></label>
                                <?php echo $this->Form->input('events_label', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)); ?>
                                <?php echo $this->Form->input('events_id', array('type' => 'hidden')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Description', true);?></label>
                                <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Weight', true);?></label>
                                <?php echo $this->Form->input('weights_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'select weight'), 'required' => false)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Schedule Rounds', true);?></label>
                                <?php echo $this->Form->input('schedule_rounds', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Time per Round', true);?></label>
                                 
                                <br/>
                                <label>
                                     <?php echo $this->Form->checkbox('no_time_checkbox'); ?>
                                 </label>
                                <?php echo __('Round with no time', true);?>
                                
                                <?php echo $this->Form->input('time_per_round', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Boxed rounds', true);?></label>
                                <?php echo $this->Form->input('boxed_roundas', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Via', true);?></label>
                                <?php 
                                    $options = array(
                                        'KO'            => 'KO', 
                                        'TKO'           => 'TKO', 
                                        'TD'            => 'TD', 
                                        'MD'            => 'MD', 
                                        'SD'            => 'SD', 
                                        'D-PTS'         => 'D-PTS', 
                                        'DRAW'          => 'DRAW', 
                                        'D-NWS'         => 'D-NWS', 
                                        'NWS'           => 'NWS', 
                                        'RTD'           => 'RTD', 
                                        'PTS'           => 'PTS', 
                                        'MAJORITY DRAW' => 'MAJORITY DRAW', 
                                        'FOUL'          => 'FOUL', 
                                        'DQ'            => 'DQ', 
                                        'SUBMISSION'    => 'SUBMISSION', 
                                        'STOPPAGE'      => 'STOPPAGE', 
                                        'NC'            => 'NC',
                                        'ND'            => 'ND',
                                        'Split Draw'    => 'Split Draw',
                                        'UD'            => 'UD',
                                        'scheduled'     => 'scheduled', 
                                        'EXHIBITION'    => 'EXHIBITION', 
                                        'AMATEUR'       => 'AMATEUR'
                                    );
                                ?>
                                <?php echo $this->Form->input('via', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => array('' => 'select via'))); ?>
                        </div>
                   
                        <div class="form-group" id="submission_comments" style="display: none;">
                                <label><?php echo __('Submission Comments', true);?></label>
                                
                                <?php 
                                    $options = array();
                                    foreach ($submissions as $submission)
                                    {
                                        $options[$submission['Submission']['name']] = $submission['Submission']['name'];
                                    }
                                ?>
                                <?php echo $this->Form->input('submission_comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => array('' => 'Select Submission Comment'))); ?>
                        </div>
                    
                        <div class="form-group">
                                <label><?php echo __('Winner', true);?></label>
                                <?php $options = array(1 => 'Corner 1', 2 => 'Corner 2', 3 => 'Draw', 4 => 'No Contest');?>
                                <?php echo $this->Form->input('winner', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => 'select winner', 'required' => true)); ?>
                        </div>
                    
                        
                        <!--<div class="form-group">
                                <label><?php //echo __('', true);?></label>
                                <?php //echo $this->Form->input('final_figths_id', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>-->

                        <div class="form-group">
                                <label><?php echo __('Rules', true);?></label>
                                <?php echo $this->Form->input('rules_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'select rules'), 'required' => false)); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Time', true);?></label>
                                <?php echo $this->Form->input('time', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Notes', true);?></label>
                                <?php echo $this->Form->input('notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Boxrec Notes', true);?></label>
                                <?php echo $this->Form->input('boxrec_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
                        </div>

                        <div class="form-group">
                                <label><?php echo __('Other Notes ', true);?></label>
                                <?php echo $this->Form->input('other_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
                        </div>

                        <!--<div class="form-group">
                                <label><?php //echo __('Winer', true);?></label>
                                <?php // echo $this->Form->input('winner', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>-->

                        <div class="form-group">
                                <label><?php echo __('Types', true);?></label>
                                <?php echo $this->Form->input('types_id', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>

                        <div class="checkbox">
                                <label>
                                        <?php echo $this->Form->input('is_competitor', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                                        <?php echo __('is competitor', true);?>
                                </label>
                        </div>

                        <div class="form-group">
                            <?php echo $this->Form->button(__('Add Fight'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('#corner1').autocomplete({
            minLength: 3,
            source: function(request, response) {
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data) {
                    response(data);
                });
            },
            select: function( event, ui ) {
                $('#corner1').val(ui.item.label);
                $('#FightCorner1Id').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
        };
        
        $('#corner2').autocomplete({
            minLength: 3,
            source: function(request, response) {
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data) {
                    response(data);
                });
            },
            select: function( event, ui ) {
                $('#corner2').val(ui.item.label);
                $('#FightCorner2Id').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
        };
        
        $('.search-identity').focusout(function(){
             $("#labelTitle").text("Title " + $("#corner1").val().substring(0,$("#corner1").val().indexOf('(')) + " vs " + $("#corner2").val().substring(0,$("#corner2").val().indexOf('(')));
             $("#inputTitle").val($("#corner1").val().substring(0,$("#corner1").val().indexOf('(')) + "vs " + $("#corner2").val().substring(0,$("#corner2").val().indexOf('(')));
        });
        
        $('#FightNoTimeCheckbox').change(function(){
            if($(this).is(':checked')){
                $("#FightTimePerRound").prop("readonly", true);
            } else {
                $("#FightTimePerRound").prop("readonly", false);
            }
        });
                
        $('#FightEventsLabel').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getEvents'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('#FightEventsLabel').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                $('#FightEventsLabel').val(ui.item.label);
                $('#FightEventsId').val(ui.item.id);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $('#FightVia').change(function(){
           if($(this).val() === 'SUBMISSION'){
               $('#submission_comments').fadeIn();
           }else if($(this).val() === 'STOPPAGE'){
               $('#submission_comments').fadeIn();
           }else{
               $('#submission_comments').fadeOut();
           }
        });
        
    });
</script>