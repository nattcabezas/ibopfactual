<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Fights', true);?> - <?php echo $titleName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-shield"></i> 
                    <?php echo __('Fights', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Images', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url('/Fight/'.$this->getUrlPerson->getUrl($idFights,$titleName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
                <i class="fa fa-desktop"></i>
            </a>
            
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Odds', 'action' => 'index/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('odds'); ?>">
                <i class="fa fa-usd"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'titles/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Titles'); ?>">
                <i class="fa fa-bookmark"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>    
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#"><?php echo __('All Images', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'searchImages/' . base64_encode($idFights)));?>"><?php echo __('Search Images', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'fightsImages/' . base64_encode($idFights)));?>"><?php echo __('Fight Images', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <?php foreach($images as $image) { ?>
        <div class="col-sm-6 col-md-4" style="margin-bottom: 20px;">
            <div class="thumbnail">
                <img src="<?php echo $this->Html->url('/files/img/' . $image['Image']['url']);?>" style="height: 200px;">
                <div class="caption">
                    
                    <div class="form-conted" id="form-<?php echo $image['Image']['id']?>" style="display: none;">
                        
                    </div>
                    
                    <?php echo $this->Form->create('FightsImage');?>
                    <?php echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $idFights)); ?>
                    <?php echo $this->Form->input('images_id', array('type' => 'hidden', 'value' => $image['Image']['id']));?>
                    
                      <div id="button-<?php echo $image['Image']['id']?>" class="btn-group" style="left: 30%">
                            <a class="btn btn-info display-form" target-form="form-<?php echo $image['Image']['id']?>" id-image="<?php echo $image['Image']['id']?>">
                                <i class="fa fa-pencil"></i> <?php echo __('Edit Image', true);?>
                            </a>
                          
                          
                    <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                       <?php echo $this->Form->button(__('Add Image'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    <?php } ?>
                    
                      </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php
        if( count($images) == 0 ){
            echo '<div class="col-md-12"><h3>' . __('no results found', true) . '</h3></div>';
        }
    ?>
</div><br>

<div class="row">
    <div class="col-md-3">
        <?php if( $page != 1){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idFights) . '/' . ($page - 1)));?>" class="btn btn-primary"><?php echo __('Previous', true);?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if( count($images) > 0 ){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idFights) . '/' . ($page + 1)));?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true);?></a>
        <?php } ?>
    </div>
    
</div>


<script type="text/javascript">
    
    $(document).ready(function() {
        
        $('.display-form').click(function(event) {
            /*event.preventDefault();
            var elementDisplay = '#' + $(this).attr('target-form');
            $(this).parent().fadeOut(function(){
                $(elementDisplay).slideDown();
            });*/
            var idImage = $(this).attr('id-image');
            var urlForm = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'formImage'));?>/" + idImage;
            $.post(urlForm, function(data){
                $('#form-' + idImage).html(data);
                $('#form-' + idImage).toggle('slow');
                $('#button-' + idImage).fadeOut();
            });
        });
        
    });
    
</script>
