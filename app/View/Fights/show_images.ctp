<section class="row">
    
    <script language="javascript">
        $(document).ready(function() {
            $('.pgwSlideshow').pgwSlideshow();
        });
    </script>

    <ul class="pgwSlideshow">
        <?php foreach ($images as $image){ ?>
            <li>
                <a href="#">
                    <img alt="<?php echo $image['Images']['title']?>" title="<?php echo $image['Images']['title']?>" src="<?php echo $this->Html->url('/files/img/' . $image['Images']['url']); ?>" />
                </a>
            </li>
        <?php } ?>
    </ul>

</section>