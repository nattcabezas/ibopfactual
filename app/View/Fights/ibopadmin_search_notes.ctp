<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Fights', true);?> - <?php echo $titleName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-shield"></i> 
                    <?php echo __('Fights', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Notes', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Odds', 'action' => 'index/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('odds'); ?>">
                <i class="fa fa-usd"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'titles/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Titles'); ?>">
                <i class="fa fa-bookmark"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>    
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idFights)));?>"><?php echo __('All Notes', true);?></a></li>
            <li class="active"><a href="#"><?php echo __('Search Notes', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'fightNotes/' . base64_encode($idFights)));?>"><?php echo __('Fight Notes', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group input-group">
            <input type="text" class="form-control" id="search-keyword" placeholder="<?php echo __('keyword', true);?>">
            <span class="input-group-btn">
                <button class="btn btn-primary" id="search-images" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="table-responsive">
        <table class="table table-striped table-hover table-striped tablesorter">
            <thead>
                <tr>
                    <th><?php echo __('Notas', true);?></th>
                    <th>&emsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($notes as $note) { ?>
                    <tr>
                        
                        <td>
                            <a href="#" id="note-link-<?php echo $note['Note']['id']?>"><?php echo $note['Note']['title']?></a>
                                <div style="display: none;">
                                   <div id="note-modal-<?php echo $note['Note']['id']?>">
                                       <p>
                                           <?php echo $note['Note']['note']?>
                                       </p>
                                   </div>
                               </div>
                        </td>
                        <td>
                            <?php echo $this->Form->create('FightsNote');?>
                            <?php echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $idFights)); ?>
                            <?php echo $this->Form->input('notes_id', array('type' => 'hidden', 'value' => $note['Note']['id'])); ?>
                            <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                                <?php echo $this->Form->button(__('Add Note'), array('class' => 'btn btn-primary btn-group-justified', 'type' => 'submit')); ?>
                            <?php }  ?>
                            <?php echo $this->Form->end(); ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php
        if( count($notes) == 0 ){
            echo '<div class="col-md-12"><h3>' . __('no results found', true) . '</h3></div>';
        }
    ?>
</div>

<div class="row">
    <div class="col-md-3">
        <?php if( $page != 1){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'searchNotes/' . base64_encode($idFights) . '/' . base64_encode($keywordData) . '/' .  ($page - 1)));?>" class="btn btn-primary"><?php echo __('Previous', true);?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if( count($notes) > 0 ){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'searchNotes/' . base64_encode($idFights) . '/' . base64_encode($keywordData) . '/' . ($page + 1)));?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true);?></a>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#search-images').click(function(event) {
            var keyword     = Base64.encode($('#search-keyword').val());
            var loadsearch  = "<?php echo $this->Html->url(array('action' => 'searchNotes/' . base64_encode($idFights)))?>/";
            window.location.replace(loadsearch + keyword);
        });
        
        var modalID = "";
        <?php
            foreach ($notes as $note){
                $modalIDLink  = 'note-link-' . $note['Note']['id'];
                $modalID  = 'note-modal-' . $note['Note']['id'];

                echo "$( \"#". $modalIDLink . "\" ).click(function () {
                        $( \"#". $modalID . "\" ).dialog({
                            modal: true,
                            buttons: {
                              Ok: function() {
                                $( this ).dialog(\"close\" );
                              }
                            }
                        });


                    });\n";
                    
            }
        ?>
        
        
    });
</script>