<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Fights', true);?> - <?php echo $titleName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-shield"></i> 
                    <?php echo __('Fights', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Titles', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url('/Fight/'.$this->getUrlPerson->getUrl($idFights,$titleName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
                <i class="fa fa-desktop"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Odds', 'action' => 'index/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('odds'); ?>">
                <i class="fa fa-usd"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'titles/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Titles'); ?>">
                <i class="fa fa-bookmark"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php if( $fight['Fight']['locked'] == 0 ){ ?> 
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo __('Add Title', true);?></h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create('FightsTitle'); ?>

                        <?php echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $idFights))?>
                        <div class="form-group">
                            <label><?php echo __('Title', true);?></label>
                            <?php echo $this->Form->input('titles_label', array('type' => 'text', 'div' => false, 'class' => 'form-control', 'label' => false))?>
                            <?php echo $this->Form->input('titles_id', array('type' => 'hidden'))?>
                        </div>

                        <div class="form-group">
                            <label><?php echo __('Title Issued', true);?></label>
                            <?php echo $this->Form->input('title_issued', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        </div>

                        <div class="form-group">
                            <label><?php echo __('Previously Owned', true);?></label>
                            <?php echo $this->Form->input('previously_owned', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $fighters, 'empty' => array(null => 'Vacant'), 'required' => false)); ?>
                        </div>

                        <div class="checkbox">
                            <label>
                                    <strong><?php echo __('Interim');?></strong>
                                    <?php echo $this->Form->input('interim', array('type' => 'checkbox', 'label' => false, 'div' => false, 'val' => 1)); ?>
                            </label>
                        </div>

                        <div class="form-group">                        
                                <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>                     
                        </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true);?></th>
                        <th><?php echo __('Title Issued', true);?></th>
                        <th><?php echo __('Previously Owned', true);?></th>
                        <th><?php echo __('Is Interim', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($titles as $title){ ?>
                        <tr>
                            <td><?php echo $title['Titles']['name']?> [<strong><?php echo $title['Titles']['description']?></strong>]</td>
                            <td><?php echo $title['FightsTitle']['title_issued']?></td>
                            <td>
                                <?php 
                                    if($title['Identity']['name'] != null ){ 
                                        echo $title['Identity']['name'] . ' ' . $title['Identity']['last_name'];
                                    } else {
                                        echo 'Vacant';
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($title['FightsTitle']['interim']){
                                        echo __('Yes', true);
                                    } else {
                                        echo __('No', true);
                                    }
                                ?>
                            </td>
                            <td>
                                <?php if( $fight['Fight']['locked'] == 0 ){ ?>
                                    <a href="<?php echo $this->Html->url(array('action' => 'titleDelete/' . base64_encode($idFights). '/' . base64_encode($title['FightsTitle']['id'])))?>" class="btn btn-primary"><i class="fa fa-trash-o"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        
        $('#FightsTitleTitlesLabel').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadTitles = "<?php echo $this->Html->url(array('controller' => 'Titles', 'action' => 'getTitle'))?>/" + request.term;
                $.getJSON(loadTitles, function(data){
                    response(data);
                }).fail(function(jqXHR, textStatus, error) {
                    console.log(error);
                    console.log(textStatus);
                    console.log(jqXHR);
                });
            },
            focus: function(event, ui){
                $('#FightsTitleTitlesLabel').val(ui.item.label);
                return false;
            },
            select: function(event, ui){
                $('#FightsTitleTitlesLabel').val(ui.item.label);
                $('#FightsTitleTitlesId').val(ui.item.id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function( ul, item ){
            
            if (item.description!="")
            {
                return $( "<li>" )
                .append( "<a>" + item.label + " [<strong>" + item.description + "</strong>]</a>" )
                .appendTo( ul );
            }
            else
            {
                return $( "<li>" )
                    .append( "<a>" + item.label + " [<strong>" + "No Description" + "</strong>]</a>" )
                    .appendTo( ul ); 
            }
            
        };
        
    });
</script>