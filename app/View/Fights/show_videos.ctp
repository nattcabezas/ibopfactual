<?php 
    echo $this->Html->css('video-js');
    //echo $this->Html->css('moo');
    echo $this->Html->css('video-playlist');
    echo $this->Html->script('video');
    echo $this->Html->script('videojs-playlists');
?>
<section class="row">
    <div class="col-sm-12">
        
        <div class="video-holder centered" style="width: 100%;">
            <video id="video" class="video-js vjs-default-skin vjs-big-play-centered"
                   controls preload="auto"
                   data-setup=''
                   poster="">
            </video>
            <div class="playlist-components" style="width: 49%;">
                <div class="playlist" style="width: 100%;">
                    <ul style="width: 100%"></ul> 
               </div>
                <div class="button-holder">
                    <img id="prev" alt="Previous video" src="<?php echo $this->Html->url('/img/Previous.png')?>">
                    <img id="next" alt="Next video" src="<?php echo $this->Html->url('/img/Next.png')?>">
                </div>
            </div>
        </div>
        <hr>
    </div>
</section>
<script type="text/javascript">
    
    (function($){
        
        var videos = [
            <?php foreach ($videos as $video) { ?>
                    {
                        src: [
                            "<?php echo $video['Videos']['mp4']?>"
                        ],
                        poster: "<?php echo $this->Html->url('/img/defaults/videoGeneric-03.png');?>",
                        title: "<?php echo $video['Videos']['title']?>"
                    },
            <?php }?>
        ];


    var demoModule = {
    init : function(){
      this.els = {};
      this.cacheElements();
      this.initVideo();
      this.createListOfVideos();
      this.bindEvents();
      this.overwriteConsole();
    },
    overwriteConsole : function(){
      console._log = console.log;
      console.log = this.log;
    },
    log : function(string){
      demoModule.els.log.append('<p>' + string + '</p>');
      console._log(string);
    },
    cacheElements : function(){
      this.els.$playlist = $('div.playlist > ul');
      this.els.$next = $('#next');
      this.els.$prev = $('#prev');
      this.els.log = $('div.panels > pre');
    },
    initVideo : function(){
      this.player = videojs('video');
      this.player.playList(videos);
    },
    createListOfVideos : function(){
      var html = '';
      for (var i = 0, len = this.player.pl.videos.length; i < len; i++){
        html += '<li data-videoplaylist="'+ i +'">'+
                  '<span class="number">' + (i + 1) + '</span>'+
                  '<span class="poster"><img src="'+ videos[i].poster +'"></span>' +
                  '<span class="title">'+ videos[i].title +'</span>' +
                '</li>';
      }
      this.els.$playlist.empty().html(html);
      this.updateActiveVideo();
    },
    updateActiveVideo : function(){
      var activeIndex = this.player.pl.current;

      this.els.$playlist.find('li').removeClass('active');
      this.els.$playlist.find('li[data-videoplaylist="' + activeIndex +'"]').addClass('active');
    },
    bindEvents : function(){
      var self = this;
      this.els.$playlist.find('li').on('click', $.proxy(this.selectVideo,this));
      this.els.$next.on('click', $.proxy(this.nextOrPrev,this));
      this.els.$prev.on('click', $.proxy(this.nextOrPrev,this));
      this.player.on('next', function(e){
        console.log('Next video');
        self.updateActiveVideo.apply(self);
      });
      this.player.on('prev', function(e){
        console.log('Previous video');
        self.updateActiveVideo.apply(self);
      });
      this.player.on('lastVideoEnded', function(e){
        console.log('Last video has finished');
      });
    },
    nextOrPrev : function(e){
      var clicked = $(e.target);
      this.player[clicked.attr('id')]();
    },
    selectVideo : function(e){
      var clicked = e.target.nodeName === 'LI' ? $(e.target) : $(e.target).closest('li');

      if (!clicked.hasClass('active')){
        console.log('Selecting video');
        var videoIndex = clicked.data('videoplaylist');
        this.player.playList(videoIndex);
        this.updateActiveVideo();
      }
    }
  };

  demoModule.init();
  
  $('#video').attr('style', 'width: 50%; height: 357px;');
  
})(jQuery);
    
</script>