<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Fights', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-shield"></i> <?php echo __('Fights', true)?></li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-6">
        <div class="input-group">
            <input type="text" class="form-control search-identity" placeholder="<?php echo __('Search');?>">
            <span class="input-group-btn"><a class="btn btn-primary"><i class="fa fa-search"></i></a></span>
        </div>
        
    </div>
    
    <a href="<?php echo $this->Html->url(array('action' => 'addMultiple')); ?>" class="btn btn-primary">
        <?php echo __('Add Multiple Fights', true);?>
    </a>
        
    <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
        <?php echo __('Add Fight', true);?>
    </a>
        
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fights as $fight) { ?>
                        <tr>
                            <td><?php echo $fight['Fight']['title']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($fight['Fight']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('add images');?>">
                                            <i class="fa fa-file-image-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($fight['Fight']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('add videos');?>">
                                            <i class="fa fa-file-video-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($fight['Fight']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('add notes');?>">
                                            <i class="fa fa-file-text-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Odds', 'action' => 'index/' . base64_encode($fight['Fight']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('add odds');?>">
                                            <i class="fa fa-usd"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'titles/' . base64_encode($fight['Fight']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('add title');?>">
                                            <i class="fa fa-bookmark"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($fight['Fight']['id'], $fight['Fight']['title'])); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Fight details');?>">
                                            <i class="fa fa-users"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($fight['Fight']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit');?>">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.search-identity').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'getFights'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('.search-identity').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                var editIdentity  = "<?php echo $this->Html->url(array('action' => 'edit'))?>/" + Base64.encode(ui.item.id);;
                window.location.replace(editIdentity);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
    });
</script>