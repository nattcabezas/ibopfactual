<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="container">
                <div class="row mobile-social-share">
                    <div class="col-md-9">
                        <h1><?php echo $fight['Fight']['title']?></h1>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">

                <?php if (isset($fight['Event']['Venues']['name'])) { ?>
                    <h3 class="text-center link-fight-detail">
                        <a href="<?php echo $this->Html->url('/Venue/' . $this->getUrlPerson->getUrl($fight['Event']['Venues']['id'], $fight['Event']['Venues']['name'])); ?>">
                            <i class="fa fa-building-o "></i>
                            <?php echo $fight['Event']['Venues']['name']; ?>
                        </a>
                    </h3>
                <?php } ?>
                <h5 class="text-center link-fight-detail">
                    <i class="fa fa-calendar-o "></i>
                    <?php
                    $elementosFechaEvento = explode("-", $fight['Event']['date']);

                    $mesVacio = 0;
                    $diaVacio = 0;

                    if (strcmp($elementosFechaEvento[1], "00") == 0) {
                        $mesVacio = 1;
                    }
                    if (strcmp($elementosFechaEvento[2], "00") == 0) {
                        $diaVacio = 1;
                    }

                    if ($mesVacio == 1) {
                        echo ($elementosFechaEvento[0] . " month and day unknown");
                    }

                    if ($diaVacio == 1 && $mesVacio == 0) {
                        echo ($elementosFechaEvento[0] . "-" . $elementosFechaEvento[1] . " day unknown");
                    }

                    if ($diaVacio == 0 && $mesVacio == 0) {
                        echo date('F jS, Y', strtotime($fight['Event']['date']));
                    }
                    ?>
                </h5>

                <?php if ((isset($location['Countries']['name'])) && ($location['Countries']['name'] != null)) { ?>
                    <h5 class="text-center link-fight-detail">
                        <i class="fa fa-globe "></i> 
                        <?php
                        if ((isset($location['Cities']['name'])) && ($location['Cities']['name'] != null)) {
                            echo $location['Cities']['name'] . ', ';
                        }
                        if ((isset($location['States']['name'])) && ($location['States']['name'] != null)) {
                            echo $location['States']['name'] . ', ';
                        }
                        echo $location['Countries']['name'];
                        ?>
                    </h5>
                <?php } ?>
            </div>


            <div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">

                <?php if ((isset($fight['FightIdentity'][0]['Identities']['name'])) && ($fight['FightIdentity'][0]['Identities']['name'] != null) && ($fight['FightIdentity'][0]['Identities']['name'] != 'unknown')) { ?>
                    <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][0]['Identities']['id'], $fight['FightIdentity'][0]['Identities']['name'] . ' ' . $fight['FightIdentity'][0]['Identities']['last_name'])); ?>">
                        <?php if ((isset($images['corner1']['Images']['url'])) && ($images['corner1']['Images']['url'] != null)) { ?>
                            <img src="<?php echo $this->Html->url('/files/img/' . $images['corner1']['Images']['url']); ?>" class="img_fight_fighter" width="250" >  
                        <?php } else { ?>
                            <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" class="img_fight_fighter">
                        <?php } ?>
                        <h4 class="text-center link-fight">
                            <i class="fa fa-user "></i> 
                            <?php echo $fight['FightIdentity'][0]['Identities']['name'] . ' ' . $fight['FightIdentity'][0]['Identities']['last_name']; ?>
                        </h4>
                    </a>
                <?php } else { ?>
                    <a>
                        <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" class="img_fight_fighter">
                        <h4 class="text-center link-fight">
                            <i class="fa fa-user "></i> <?php echo __('unknown', true); ?>
                        </h4>
                    </a>
                <?php } ?>

            </div>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <img src="<?php echo $this->Html->url('/img/vs-04.png') ?>" class="img img-responsive img-versus" width="80" >
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">

                <?php if ((isset($fight['FightIdentity'][1]['Identities']['name'])) && ($fight['FightIdentity'][1]['Identities']['name'] != null) && ($fight['FightIdentity'][1]['Identities']['name'] != 'unknown')) { ?>
                    <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][1]['Identities']['id'], $fight['FightIdentity'][1]['Identities']['name'] . ' ' . $fight['FightIdentity'][1]['Identities']['last_name'])); ?>">
                        <?php if ((isset($images['corner2']['Images']['url'])) && ($images['corner2']['Images']['url'] != null)) { ?>
                            <img src="<?php echo $this->Html->url('/files/img/' . $images['corner2']['Images']['url']); ?>" class="img_fight_fighter" width="250" >  
                        <?php } else { ?>
                            <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" class="img_fight_fighter" width="150" >
                        <?php } ?>
                        <h4 class="text-center link-fight">
                            <i class="fa fa-user "></i> 
                            <?php echo $fight['FightIdentity'][1]['Identities']['name'] . ' ' . $fight['FightIdentity'][1]['Identities']['last_name']; ?>
                        </h4>
                    </a>
                <?php } else { ?>
                    <a>
                        <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" class="img_fight_fighter">
                        <h4 class="text-center link-fight">
                            <i class="fa fa-user "></i> <?php echo __('unknown', true); ?>
                        </h4>
                    </a>
                <?php } ?>


            </div>



        </div>
    </div>



    <div class="fighters-details">

        <ul id="myTab" class="nav nav-tabs responsive">
            <li class="active">
                <a href="#general-panel" class="h4" data-toggle="tab"><?php echo __('Fight details', true); ?></a>
            </li>

            <?php if ($countImages > 0) { ?>
                <li>
                    <a href="#photos-panel" class="h4 load-images" data-toggle="tab"><?php echo __('Photos', true); ?></a>
                </li>
            <?php } ?>

            <?php if ($countVideos > 0) { ?>
                <li>
                    <a href="#videos-panel" class="h4 load-videos" data-toggle="tab"><?php echo __('Videos', true); ?></a>
                </li>
            <?php } ?>
        </ul>

        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="general-panel">
                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td>
                                        <span class=""><?php echo __('Official result', true); ?> </span>
                                    </td>
                                    <td>
                                        <strong>
                                            <?php
                                            echo '(' . $fight['Fight']['via'] . ') ';
                                            if ($fight['Fight']['winner'] == '1') {
                                                if ((isset($fight['FightIdentity'][0]['Identities']['name'])) && ($fight['FightIdentity'][0]['Identities']['name'] != null) && ($fight['FightIdentity'][0]['Identities']['name'] != 'unknown')) {
                                                    echo $fight['FightIdentity'][0]['Identities']['name'] . ' ' . $fight['FightIdentity'][0]['Identities']['last_name'];
                                                }
                                                echo ' ' . __('defeated', true) . ' ';
                                                if ((isset($fight['FightIdentity'][1]['Identities']['name'])) && ($fight['FightIdentity'][1]['Identities']['name'] != null) && ($fight['FightIdentity'][1]['Identities']['name'] != 'unknown')) {
                                                    echo $fight['FightIdentity'][1]['Identities']['name'] . ' ' . $fight['FightIdentity'][1]['Identities']['last_name'];
                                                }
                                            } else if ($fight['Fight']['winner'] == '2') {
                                                if ((isset($fight['FightIdentity'][1]['Identities']['name'])) && ($fight['FightIdentity'][1]['Identities']['name'] != null) && ($fight['FightIdentity'][1]['Identities']['name'] != 'unknown')) {
                                                    echo $fight['FightIdentity'][1]['Identities']['name'] . ' ' . $fight['FightIdentity'][1]['Identities']['last_name'];
                                                }
                                                echo ' ' . __('defeated', true) . ' ';
                                                if ((isset($fight['FightIdentity'][0]['Identities']['name'])) && ($fight['FightIdentity'][0]['Identities']['name'] != null) && ($fight['FightIdentity'][0]['Identities']['name'] != 'unknown')) {
                                                    echo $fight['FightIdentity'][0]['Identities']['name'] . ' ' . $fight['FightIdentity'][0]['Identities']['last_name'];
                                                }
                                            } else if ($fight['Fight']['winner'] == '3') {
                                                if ((isset($fight['FightIdentity'][0]['Identities']['name'])) && ($fight['FightIdentity'][0]['Identities']['name'] != null) && ($fight['FightIdentity'][0]['Identities']['name'] != 'unknown')) {
                                                    echo $fight['FightIdentity'][0]['Identities']['name'] . ' ' . $fight['FightIdentity'][0]['Identities']['last_name'];
                                                }
                                                echo ' ' . __('draw with', true) . ' ';
                                                if ((isset($fight['FightIdentity'][1]['Identities']['name'])) && ($fight['FightIdentity'][1]['Identities']['name'] != null) && ($fight['FightIdentity'][1]['Identities']['name'] != 'unknown')) {
                                                    echo $fight['FightIdentity'][1]['Identities']['name'] . ' ' . $fight['FightIdentity'][1]['Identities']['last_name'];
                                                }
                                            } else {
                                                echo __('No Contest');
                                            }
                                            echo ' ' . __('in') . ' ';
                                            echo $fight['Fight']['boxed_roundas'] . ' ';
                                            echo $fight['Fight']['boxed_roundas'] > 1 ? __('rounds', true) : __('round', true);
                                            ?>

                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span><?php echo __('Weight class:', true); ?></span>
                                    </td>
                                    <td>
                                        <strong><?php echo $fight['Weights']['name'] ?></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span><?php echo __('Scheduled rounds:', true); ?></span>
                                    </td>
                                    <td>
                                        <strong>
                                            <?php
                                            echo $fight['Fight']['schedule_rounds'];
                                            echo 'x';
                                            if ($fight['Fight']['time_per_round'] != "") {
                                                echo $fight['Fight']['time_per_round'];
                                            } else {
                                                if ($fight['Fight']['types_id'] == 1) {
                                                    echo '3';
                                                } else if ($fight['Fight']['types_id'] == 2) {
                                                    echo '5';
                                                }
                                            }
                                            ?>
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span><?php echo __('Time of match', true); ?>:</span>
                                    </td>
                                    <td>
                                        <strong><?php echo $fight['Fight']['time'] ?></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class=""><?php echo __('Event:', true); ?></span>
                                    </td>
                                    <td class="link-fight-detail">
                                        <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($fight['Event']['id'], $fight['Event']['name'])); ?>">
                                            <i class="fa fa-calendar-o "></i> <?php echo $fight['Event']['name']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class=""><?php echo __('Type of Fight:', true); ?></span>
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ($fight['Fight']['types_id'] == 1) { ?>
                                            <strong class=""> <?php echo __('Boxing') ?></strong>
                                        <?php } ?>
                                        <?php if ($fight['Fight']['types_id'] == 2) { ?>
                                            <strong class=""> <?php echo __('MMA') ?></strong>
                                        <?php } ?>  
                                        <?php if ($fight['Fight']['types_id'] == 3) { ?>
                                            <strong class=""> <?php echo __('ADCC Rules (Grappling)') ?></strong>
                                        <?php } ?>    
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <span class=""><?php echo __('Titles:', true); ?></span>
                                        <td data-content="<?php echo __('Titles:', true); ?>">  
                                            <strong>
                                                <?php
                                                $numNickname = 0;
                                                foreach ($fightTitle as $title) {
                                                    if ($numNickname > 0) {
                                                        echo ', ';
                                                    }
                                                    echo ($title ['Titles']['name']);
                                                    $numNickname ++;
                                                }
                                                ?>
                                                
                                            </strong>
                                        </td>
                          
                                </tr>

                            </tbody>

                        </table>

                        <hr><br>

                        <table class="table table_basic responsive">
                            <tbody>
                                <tr>
                                    <td>
                                        <span><?php echo __('Referee: ', true); ?></span>
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ((isset($fightData['Referee']['id'])) && ($fightData['Referee']['id'] != null) && ($fightData['Referee']['name'] != null) && ($fightData['Referee']['name'] != "unknown")) { ?>
                                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fightData['Referee']['id'], $fightData['Referee']['name'] . ' ' . $fightData['Referee']['last_name'])); ?>">
                                                <i class="fa fa-user"></i> <?php echo $fightData['Referee']['name'] . ' ' . $fightData['Referee']['last_name'] ?>
                                            </a>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <span><?php echo __('Ring announcer:', true); ?></span> 
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ((isset($fightData['ring_announcer']['id'])) && ($fightData['ring_announcer']['id'] != null) && ($fightData['ring_announcer']['name'] != null) && ($fightData['ring_announcer']['name'] != "unknown")) { ?>
                                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fightData['ring_announcer']['id'], $fightData['ring_announcer']['name'] . ' ' . $fightData['ring_announcer']['last_name'])); ?>">
                                                <i class="fa fa-user"></i> <?php echo $fightData['ring_announcer']['name'] . ' ' . $fightData['ring_announcer']['last_name'] ?>
                                            </a>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span><?php echo __('Inspector: ', true); ?></span>
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ((isset($fightData['inspector']['id'])) && ($fightData['inspector']['id'] != null) && ($fightData['inspector']['name'] != null) && ($fightData['inspector']['name'] != "unknown")) { ?>


                                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fightData['inspector']['id'], $fightData['Referee']['name'] . ' ' . $fightData['inspector']['last_name'])); ?>">
                                                <i class="fa fa-user"></i> <?php echo $fightData['inspector']['name'] . ' ' . $fightData['inspector']['last_name'] ?>
                                            </a>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <span><?php echo __('Timekeeper:', true); ?></span> 
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ((isset($fightData['timekeeper']['id'])) && ($fightData['timekeeper']['id'] != null) && ($fightData['timekeeper']['name'] != null) && ($fightData['timekeeper']['name'] != "unknown")) { ?>
                                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fightData['timekeeper']['id'], $fightData['timekeeper']['name'] . ' ' . $fightData['timekeeper']['last_name'])); ?>">
                                                <i class="fa fa-user"></i> <?php echo $fightData['timekeeper']['name'] . ' ' . $fightData['timekeeper']['last_name'] ?>
                                            </a>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span><?php echo __('Judge 1:', true); ?></span>
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ((isset($fightData['judge_1']['id'])) && ($fightData['judge_1']['id'] != null) && ($fightData['judge_1']['name'] != null) && ($fightData['judge_1']['name'] != "unknown")) { ?>
                                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fightData['judge_1']['id'], $fightData['judge_1']['name'] . ' ' . $fightData['judge_1']['last_name'])); ?>">
                                                <i class="fa fa-user"></i> <?php echo $fightData['judge_1']['name'] . ' ' . $fightData['judge_1']['last_name'] ?>
                                            </a>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td class="">
                                        <span><?php echo __('Score', true); ?></span>
                                    </td>
                                    <td class="">
                                        <?php if (isset($fightData['FightDatum'])) { ?>
                                            <strong><?php echo $fightData['FightDatum']['score1_judge_1'] ?> - <?php echo $fightData['FightDatum']['score2_judge_1'] ?></strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span><?php echo __('Judge 2:', true); ?></span>
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ((isset($fightData['judge_2']['id'])) && ($fightData['judge_2']['id'] != null) && ($fightData['judge_2']['name'] != null) && ($fightData['judge_2']['name'] != "unknown")) { ?>
                                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fightData['judge_2']['id'], $fightData['judge_2']['name'] . ' ' . $fightData['judge_2']['last_name'])); ?>">
                                                <i class="fa fa-user"></i> <?php echo $fightData['judge_2']['name'] . ' ' . $fightData['judge_2']['last_name'] ?>
                                            </a>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td class="">
                                        <span><?php echo __('Score', true); ?></span>
                                    </td>
                                    <td class="">
                                        <?php if (isset($fightData['FightDatum'])) { ?>
                                            <strong><?php echo $fightData['FightDatum']['score1_judge_2'] ?> - <?php echo $fightData['FightDatum']['score2_judge_2'] ?></strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span><?php echo __('Judge 3:', true); ?></span>
                                    </td>
                                    <td class="link-fight-detail">
                                        <?php if ((isset($fightData['judge_3']['id'])) && ($fightData['judge_3']['id'] != null) && ($fightData['judge_3']['name'] != null) && ($fightData['judge_3']['name'] != "unknown")) { ?>
                                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fightData['judge_3']['id'], $fightData['judge_3']['name'] . ' ' . $fightData['judge_3']['last_name'])); ?>">
                                                <i class="fa fa-user"></i> <?php echo $fightData['judge_3']['name'] . ' ' . $fightData['judge_3']['last_name'] ?>
                                            </a>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td class="">
                                        <span><?php echo __('Score', true); ?></span>
                                    </td>
                                    <td class="">
                                        <?php if (isset($fightData['FightDatum'])) { ?>
                                            <strong><?php echo $fightData['FightDatum']['score1_judge_3'] ?> - <?php echo $fightData['FightDatum']['score2_judge_3'] ?></strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <hr><br>

                        <table   class="table table_basic">


                            <tr>
                                <td>
                                    <span>&emsp;</span>
                                </td>
                                <td>
                                    <strong><?php echo __('Corner 1'); ?></strong>
                                </td>
                                <td>
                                    <strong><?php echo __('Corner 2'); ?></strong>
                                </td>
                            </tr>



                            <tr>
                                <td>
                                    <span><?php echo __('Fighter:'); ?></span>
                                </td>
                                <td>
                                    <?php if ((isset($cornerData1['Identities']['id'])) && ($cornerData1['Identities']['id']) != null && ($cornerData1['Identities']['name'])) { ?>
                                        <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($cornerData1['Identities']['id'], $cornerData1['Identities']['name'] . ' ' . $cornerData1['Identities']['last_name'])); ?>">
                                            <i class="fa fa-user"></i> <?php echo $cornerData1['Identities']['name'] . ' ' . $cornerData1['Identities']['last_name'] ?>
                                        </a>
                                    <?php } else { ?>
                                        <strong> - </strong>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if ((isset($cornerData2['Identities']['id']) ) && ($cornerData2['Identities']['id']) != null && ($cornerData2['Identities']['name'])) { ?>
                                        <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($cornerData2['Identities']['id'], $cornerData2['Identities']['name'] . ' ' . $cornerData2['Identities']['last_name'])); ?>">
                                            <i class="fa fa-user"></i> <?php echo $cornerData2['Identities']['name'] . ' ' . $cornerData2['Identities']['last_name'] ?>
                                        </a>
                                    <?php } else { ?>
                                        <strong> - </strong>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['corner_color'] != null || ($cornerData2['FightIdentity']['corner_color'] != null))) {
                                ?>

                                <tr>

                                    <td>
                                        <span><?php echo __('Corner color:'); ?></span>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['corner_color'])) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['corner_color'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['corner_color']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['corner_color'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>


                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['weigth'] != null || ($cornerData2['FightIdentity']['weigth'] != null))) {
                                ?>   
                                <tr>
                                    <td>
                                        <span><?php echo __('Weight:'); ?></span>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['weigth'])) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['weigth'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id'])) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['weigth'])) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['weigth'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>


                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['final_weigth'] != null || ($cornerData2['FightIdentity']['final_weigth'] != null))) {
                                ?>   
                                <tr>
                                    <td>
                                        <span><?php echo __('Final Weight: '); ?></span>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['final_weigth']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['final_weigth'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['final_weigth']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['final_weigth'] ?>
                                            </p>
                                        <?php } elseif ((isset($cornerData2['FightIdentity']['id']) != null)) { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>

                                </tr>
                            <?php } ?>


                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['shorts'] != null || ($cornerData2['FightIdentity']['shorts'] != null))) {
                                ?>  
                                <tr>
                                    <td>
                                        <span><?php echo __('Shorts:'); ?></span>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['shorts']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['shorts'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['shorts']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['shorts'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>


                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['robe'] != null || ($cornerData2['FightIdentity']['robe'] != null))) {
                                ?>  
                                <tr>
                                    <td>
                                        <span><?php echo __('Robe:'); ?></span>
                                    </td> 
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['robe']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['robe'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['robe']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['robe'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['robe_says'] != null || ($cornerData2['FightIdentity']['robe_says'] != null))) {
                                ?>  
                                <tr>
                                    <td>
                                        <span><?php echo __('Robe Says:'); ?></span>
                                    </td> 
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['robe_says']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['robe_says'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['robe_says']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['robe_says'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['purse_amount'] != null || ($cornerData2['FightIdentity']['purse_amount'] != null))) {
                                ?>  
                                <tr>
                                    <td>
                                        <span><?php echo __('Purse Amount:'); ?></span>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['purse_amount']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['purse_amount'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['purse_amount']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['robe_says'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['purse_comments'] != null || ($cornerData2['FightIdentity']['purse_comments'] != null))) {
                                ?>  
                                <tr>
                                    <td>
                                        <span><?php echo __('Purse Comments:'); ?></span>
                                    </td> 
                                    <td>
                                        <?php if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['purse_comments']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['purse_comments'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['purse_comments']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['purse_comments'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php if ((($cornerData1['FightIdentity']['death_ring'] != 0) || ($cornerData1['FightIdentity']['death_ring'] == 'yes')) || (($cornerData2['FightIdentity']['death_ring'] != 0) || ($cornerData2['FightIdentity']['death_ring'] == 'yes'))) { ?> 
                                <tr>
                                    <td>
                                        <span><?php echo __('Death in ring?'); ?></span>
                                    </td> 
                                    <td>
                                        <?php
                                        if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['death_ring']) == 'yes' ||
                                                ($cornerData1['FightIdentity']['death_ring']) == 'no')) {
                                            ?>
                                            <p>
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['death_ring'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
    <?php } ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['death_ring']) == 'yes' ||
                                                ($cornerData2['FightIdentity']['death_ring']) == 'no')) {
                                            ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['death_ring'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>

<?php if ((isset($cornerData1['FightIdentity']['id'])) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['comments'] != null || ($cornerData2['FightIdentity']['comments'] != null))) {
    ?> 
                                <tr>
                                    <td>
                                        <span><?php echo __('Nickname Used:'); ?></span>
                                    </td> 
                                    <td>
    <?php if ((isset($cornerData1['FightIdentity']['id']) && ($cornerData1['FightIdentity']['id']) != null && ($cornerData1['FightIdentity']['comments']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData1['FightIdentity']['comments'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                        <?php } ?>
                                    </td>
                                    <td>
    <?php if ((isset($cornerData2['FightIdentity']['id']) && ($cornerData2['FightIdentity']['id']) != null && ($cornerData2['FightIdentity']['comments']))) { ?>
                                            <p >
                                                <i class="fa fa"></i> <?php echo $cornerData2['FightIdentity']['comments'] ?>
                                            </p>
                                        <?php } else { ?>
                                            <strong> - </strong>
                                <?php } ?>
                                    </td>
                                </tr>
<?php } ?>

                        </table>



                            <?php if ($jobsCorner1) { ?>
                                <?php if ($jobsCorner2) { ?>
                                <div class="col-lg-6">
                                    <?php } else { ?>
                                    <div class="col-lg-6 col-md-offset-3">
    <?php } ?>
                                    <h3><center><?php echo __('Jobs Corner 1') ?></center></h3>
                                    <table class="table table_basic">
                                        <tr>
                                            <td><?php echo __('Job') ?></td> 
                                            <td><?php echo __('Name') ?></td>
                                        </tr>
    <?php foreach ($jobsCorner1 as $job) { ?>
                                            <tr>
                                                <td><?php echo $job['Jobs']['name'] ?></td> 
                                                <td>
                                                    <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($job['Identities']['id'], $job['Identities']['name'] . ' ' . $job['Identities']['last_name'])); ?>">
        <?php echo $job['Identities']['name'] . ' ' . $job['Identities']['last_name'] ?>
                                                    </a>
                                                </td>
                                            </tr>
    <?php } ?>
                                    </table>

                                </div>
                            <?php } ?>


                                <?php if ($jobsCorner2) { ?>
                                    <?php if ($jobsCorner1) { ?>
                                    <div class="col-lg-6">
                                        <?php } else { ?>
                                        <div class="col-lg-6 col-md-offset-3">
    <?php } ?>
                                        <h3><center><?php echo __('Jobs Corner 2') ?></center></h3>
                                        <table class="table table_basic">
                                            <tr>
                                                <td><?php echo __('Job') ?></td> 
                                                <td><?php echo __('Name') ?></td>
                                            </tr>
    <?php foreach ($jobsCorner2 as $job) { ?>
                                                <tr>
                                                    <td><?php echo $job['Jobs']['name'] ?></td> 
                                                    <td>
                                                        <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($job['Identities']['id'], $job['Identities']['name'] . ' ' . $job['Identities']['last_name'])); ?>">
        <?php echo $job['Identities']['name'] . ' ' . $job['Identities']['last_name'] ?>
                                                        </a>
                                                    </td>
                                                </tr>
                                    <?php } ?>
                                        </table>
                                    </div>
<?php } ?>





                            </div>

                        </div>
                    </div>



<?php if ($countImages > 0) { ?>         
                        <div class="tab-pane fade" id="photos-panel">
                            <div class="col-lg-12" id="load-img"><br>
                                <center>
                                    <i class="fa fa-refresh fa-spin fa-3x"></i>
                                </center>
                            </div>
                        </div>
                    <?php } ?> 

<?php if ($countVideos > 0) { ?>
                        <div class="tab-pane fade" id="videos-panel">
                            <div class="col-lg-12" id="load-video"><br>
                                <center>
                                    <i class="fa fa-refresh fa-spin fa-3x"></i>
                                </center>
                            </div>
                        </div>
<?php } ?> 

                </div>
                <div class="clear"></div>
            </div>

        </div>      
        <div class='footer_separator'></div>            

        <script type="text/javascript">

            (function ($) {
                fakewaffle.responsiveTabs(['xs', 'sm']);
            })(jQuery);

            $(document).ready(function () {

                var loadImages = true;
                $('.load-images').click(function () {
                    if (loadImages) {
                        var loadPageImages = "<?php echo $this->Html->url(array('action' => 'showImages/' . $id)); ?>";
                        $.post(loadPageImages, function (data) {
                            $('#load-img').html(data);
                            loadPageImages = false;
                        });
                    }
                });

                var loadVideos = true;
                $('.load-videos').click(function () {
                    if (loadVideos) {
                        var loadPageVideos = "<?php echo $this->Html->url(array('action' => 'showVideos/' . $id)); ?>";
                        $.post(loadPageVideos, function (data) {
                            $('#load-video').html(data);
                            loadVideos = false;
                        });
                    }
                });

            });
        </script>
