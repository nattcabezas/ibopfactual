<?php foreach ($fights as $fight){ ?>
    <tr>
        <td>
            <?php
                $fullName1  = isset($fight['FightIdentity'][0]['Identities']['name']) ? $fight['FightIdentity'][0]['Identities']['name'] . ' ' : '';
                $fullName1 .= isset($fight['FightIdentity'][0]['Identities']['last_name']) ? $fight['FightIdentity'][0]['Identities']['last_name'] : '';
                if(isset($fight['FightIdentity'][0])){
                    $personUrl1 =  $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][0]['Identities']['id'], $fullName1));
                } else {
                    $personUrl1 = "#";
                }
            ?>
            <a href="<?php echo  $personUrl1; ?>">
                <?php 
                    if( (isset($fight['FightIdentity'][0]['Identities']['name'])) && ($fight['FightIdentity'][0]['Identities']['name'] != "") ){
                        echo $fight['FightIdentity'][0]['Identities']['name'] . ' ';
                    }
                    if( (isset($fight['FightIdentity'][0]['Identities']['last_name'])) && ($fight['FightIdentity'][0]['Identities']['last_name'] != "") ){
                        echo $fight['FightIdentity'][0]['Identities']['last_name'];
                    }
                ?>
            </a>
        </td>
        <td><?php echo __('Vs');?></td>
        <td>
            <?php
                $fullName2  = isset($fight['FightIdentity'][1]['Identities']['name']) ? $fight['FightIdentity'][1]['Identities']['name'] . ' ' : '';
                $fullName2 .= isset($fight['FightIdentity'][1]['Identities']['last_name']) ? $fight['FightIdentity'][1]['Identities']['last_name'] : '';
                if(isset($fight['FightIdentity'][1])){
                    $personUrl2 =  $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][1]['Identities']['id'], $fullName2));
                } else {
                    $personUrl2 = "#";
                }
            ?>
            <a href="<?php echo $personUrl2; ?>">
                <?php 
                    if( (isset($fight['FightIdentity'][1]['Identities']['name'])) && ($fight['FightIdentity'][1]['Identities']['name'] != "") ){
                        echo $fight['FightIdentity'][1]['Identities']['name'] . ' ';
                    }
                    if( (isset($fight['FightIdentity'][1]['Identities']['last_name'])) && ($fight['FightIdentity'][1]['Identities']['last_name'] != "") ){
                        echo $fight['FightIdentity'][1]['Identities']['last_name'];
                    }
                ?>
            </a>
        </td>
        <td>
            <?php
                if( (isset($fight['Weights']['name'])) && ($fight['Weights']['name'] != "")){
                    echo $fight['Weights']['name'];
                }
            ?>
        </td>
        <td>
            <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($fight['Event']['id'], $fight['Event']['name'])); ?>">
                <?php
                    if( (isset($fight['Event']['name'])) && ($fight['Event']['name'] != "")){
                        echo $fight['Event']['name'];
                    }
                ?>
            </a>
        </td>
        <td>
            <?php
                if( (isset($fight['Locations']['Countries']['name'])) && ($fight['Locations']['Countries']['name'] != "")){
                    echo '<img src="' . $this->Html->url('/img/flags/' . $fight['Locations']['Countries']['flag']) . '"> ';
                }
                if( (isset($fight['Locations']['Cities']['name'])) && ($fight['Locations']['Cities']['name'] != "")){
                    echo $fight['Locations']['Cities']['name'].', ';
                }
                if( (isset($fight['Locations']['States']['name'])) && ($fight['Locations']['States']['name'] != "")){
                    echo $fight['Locations']['States']['name'].', ';
                }
                if( (isset($fight['Locations']['Countries']['name'])) && ($fight['Locations']['Countries']['name'] != "")){
                    echo $fight['Locations']['Countries']['name'];
                }
            ?>
        </td>
        <td>
            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($fight['Fight']['id'], $fight['Fight']['title'])); ?>">
                <i class="fa fa-boxers fa-2x"></i>
            </a>
        </td>
    </tr>
<?php } 