<div class="container search-container">
    <div class="row">
        <div class="col-lg-12">
            <form action="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'search'));?>" class="search-person-form" role="form" method="post">  
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Keyword" class="form-control" name="Keyword-fight" value="<?php echo $keyword; ?>">
                            <label for="date" class="glyphicon glyphicon-search" rel="tooltip" title="date"></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
        </div>
    </div>
    
    <?php echo $this->element('banners', array('thisBanner' => $detailBanner)); ?>
    
    <table id="" class="responsive responsive-table table_basic ">
        <thead>
        <tr>
            <th colspan="3" style="text-align: center;"><?php echo __('Fighters', true); ?></th>
            <th><?php echo __('Weight Class', true); ?></th>
            <th><?php echo __('Event', true); ?></th>
            <th><?php echo __('Location', true); ?></th>
            <th><?php echo __('Detail', true); ?></th>
        </tr>
        </thead>
        <tbody class="fights-data table_strips">
            <?php foreach ($fights as $fight){ ?>
            <tr>
                <td data-content="">
                    <?php
                        $fullName1  = isset($fight['FightIdentity'][0]['Identities']['name']) ? $fight['FightIdentity'][0]['Identities']['name'] . ' ' : '';
                        $fullName1 .= isset($fight['FightIdentity'][0]['Identities']['last_name']) ? $fight['FightIdentity'][0]['Identities']['last_name'] : '';
                        if(isset($fight['FightIdentity'][0])){
                            $personUrl1 =  $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][0]['Identities']['id'], $fullName1));
                        } else {
                            $personUrl1 = "#";
                        }
                    ?>
                    <a href="<?php echo  $personUrl1; ?>">
                        <?php 
                            if( (isset($fight['FightIdentity'][0]['Identities']['name'])) && ($fight['FightIdentity'][0]['Identities']['name'] != "") ){
                                echo $fight['FightIdentity'][0]['Identities']['name'] . ' ';
                            }
                            if( (isset($fight['FightIdentity'][0]['Identities']['last_name'])) && ($fight['FightIdentity'][0]['Identities']['last_name'] != "") ){
                                echo $fight['FightIdentity'][0]['Identities']['last_name'];
                            }
                        ?>
                    </a>
                </td>
                <td data-content="<?php echo __('Fighters', true);?>"><?php echo __('Vs');?></td>
                <td data-content="">
                    <?php
                        $fullName2  = isset($fight['FightIdentity'][1]['Identities']['name']) ? $fight['FightIdentity'][1]['Identities']['name'] . ' ' : '';
                        $fullName2 .= isset($fight['FightIdentity'][1]['Identities']['last_name']) ? $fight['FightIdentity'][1]['Identities']['last_name'] : '';
                        if(isset($fight['FightIdentity'][1])){
                            $personUrl2 =  $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][1]['Identities']['id'], $fullName2));
                        } else {
                            $personUrl2 = "#";
                        }
                    ?>
                    <a href="<?php echo $personUrl2; ?>">
                        <?php 
                            if( (isset($fight['FightIdentity'][1]['Identities']['name'])) && ($fight['FightIdentity'][1]['Identities']['name'] != "") ){
                                echo $fight['FightIdentity'][1]['Identities']['name'] . ' ';
                            }
                            if( (isset($fight['FightIdentity'][1]['Identities']['last_name'])) && ($fight['FightIdentity'][1]['Identities']['last_name'] != "") ){
                                echo $fight['FightIdentity'][1]['Identities']['last_name'];
                            }
                        ?>
                    </a>
                </td>
                <td data-content="<?php echo __('Weight Class', true);?>">
                    <?php
                        if( (isset($fight['Weights']['name'])) && ($fight['Weights']['name'] != "")){
                            echo $fight['Weights']['name'];
                        }
                    ?>
                </td>
                <td data-content="<?php echo __('Event', true);?>">
                    <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($fight['Event']['id'], $fight['Event']['name'])); ?>">
                        <?php
                            if( (isset($fight['Event']['name'])) && ($fight['Event']['name'] != "")){
                                echo $fight['Event']['name'];
                            }
                        ?>
                    </a>
                </td>
                <td data-content="<?php echo __('Location', true);?>">
                    <?php
                        if( (isset($fight['Locations']['Countries']['name'])) && ($fight['Locations']['Countries']['name'] != "")){
                            echo '<img src="' . $this->Html->url('/img/flags/' . $fight['Locations']['Countries']['flag']) . '"> ';
                        }
                        if( (isset($fight['Locations']['Cities']['name'])) && ($fight['Locations']['Cities']['name'] != "")){
                            echo $fight['Locations']['Cities']['name'].', ';
                        }
                        if( (isset($fight['Locations']['States']['name'])) && ($fight['Locations']['States']['name'] != "")){
                            echo $fight['Locations']['States']['name'].', ';
                        }
                        if( (isset($fight['Locations']['Countries']['name'])) && ($fight['Locations']['Countries']['name'] != "")){
                            echo $fight['Locations']['Countries']['name'];
                        }
                    ?>
                </td>
                <td data-content="<?php echo __('Detail', true);?>">
                    <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($fight['Fight']['id'], $fight['Fight']['title'])); ?>">
                        <i class="fa fa-boxers fa-2x"></i>
                    </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    
</div>
<script type="text/javascript">
    
    $(document).ready(function() {
        var numPage = 2;
        $(window).scroll(function(){
            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100){
                var loadPage = "<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'searchPage/' . $keywordSearch));?>/" + numPage + "/" + <?php echo $secondQuery?>;
                numPage++;
                $.post(loadPage, function(data){
                    $('.fights-data').append(data);
                });
            }
        });
        
        /*$('.responsive-table').ngResponsiveTables({
            smallPaddingCharNo: 13,
            mediumPaddingCharNo: 18,
            largePaddingCharNo: 30
        });*/
    });
    
</script>