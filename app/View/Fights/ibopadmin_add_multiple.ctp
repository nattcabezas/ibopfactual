<?php
    //debug($sources);
?>
    <div>
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo __('Add multiple fights', true);?></h1>
        </div>
        <button id="add_field_button" class="btn btn-primary">Add More Fields</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="button_create_event">Create event with date-location</button>
        <button id="remove_last_button" class="btn btn-danger">Remove last row</button>       
        
    </div>
    
    <div class="pre-scrollable" style="max-height: 500px;">
        <?php echo $this->Form->create('ibopadmin_addMultiple'); ?>
        <table style="width:100%" class="input_fields_wrap" id="fight_table">
          <tr>
            <th>Row</th>
            <th>Source</th>
            <th>Fighter 1</th>
            <th>Weight 1</th> 
            <th>Fighter 2</th>
            <th>Weight 2</th> 
            <th>Winner</th> 
            <th>Via</th> 
            <th>Time</th> 
            <th>Boxed rounds</th>
            <th>Scheduled rounds</th>
            <th>Event</th> 
          </tr>
          
          <tr id="fight0">
            <td>1</td>
            <td>
                <?php /*echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'Source'), 'required' => true, 'name' => 'sources[0]'));*/ ?>
                <select name="sources[0]" required>
                    <option value="">Select source</option>
                    <option value="1">BOXREC</option>
                    <option value="2">IBOP</option>
                    <option value="3">FF</option>
                    <option value="4">SHERDOG</option>
                    <option value="5">OTHER</option>
                    <option value="6">CYBERBOXINGZONE</option>
                    <option value="7">MIXEDMARTIALARTS.COM</option>
                    <option value="8">ADCOMBAT.COM</option>
                    <option value="9">WIKIPEDIA.ORG</option>
                    <option value="10">RoadFC.com</option>
                    <option value="11">Boxing Commission</option>
                    <option value="12">NEWSPAPERS</option>
                    <option value="13">The RING (Record Book)</option>
                </select>
            </td>
            <td><input class="search-identity" placeholder="Fighter 1" type="text" name="fighter1[0]" required="true"></td>
            <input type="hidden" name="idFighter1[0]">
            <td><input type="text" placeholder="Weight 1" class="form-control input-sm" name="weight1[0]"></td> 
            <td><input placeholder="Fighter 2" class="search-identity" type="text" name="fighter2[0]" required="true"></td>
            <input type="hidden" name="idFighter2[0]">
            <td><input type="text" placeholder="Weight 2" class="form-control input-sm" name="weight2[0]"></td> 
            <td>
                <select name="winner[0]">
                    <option value="1">Fighter 1</option>
                    <option value="2">Fighter 2</option>
                    <option value="3">Draw</option>
                </select>
            </td> 
            
            <!--<input type="text" name="via[0]">-->
            <td>
                <select name="via[0]">
                    <option value="KO">KO</option>
                    <option value="TKO">TKO</option>
                    <option value="TD">TD</option>
                    <option value="MD">MD</option>
                    <option value="SD">SD</option>
                    <option value="D-PTS">D-PTS</option>
                    <option value="DRAW">DRAW</option>
                    <option value="D-NWS">D-NWS</option>
                    <option value="NWS">NWS</option>
                    <option value="RTD">RTD</option>
                    <option value="PTS">PTS</option>
                    <option value="MAJORITY DRAW">MAJORITY DRAW</option>
                    <option value="FOUL">FOUL</option>
                    <option value="DQ">DQ</option>
                    <option value="SUBMISSION">SUBMISSION</option>
                    <option value="STOPPAGE">STOPPAGE</option>
                    <option value="NC">NC</option>
                    <option value="ND">ND</option>
                    <option value="Split Draw">Split Draw</option>
                    <option value="UD">UD</option>
                    <option value="scheduled">scheduled</option>
                    <option value="EXHIBITION">EXHIBITION</option>
                    <option value="AMATEUR">AMATEUR</option>
                </select>
            </td> 
            <td><input type="datetime" value="00:00" name="time[0]"></td>
            <td><input placeholder="Boxed rounds" type="text" class="form-control input-sm" name="boxed_rounds[0]"></td>
            <td><input type="text" placeholder="Scheduled rounds" class="form-control input-sm" name="scheduled_rounds[0]"></td>            
            <td><input type="text" placeholder="Event" name="event[0]" required="true"></td>
            <input type="hidden" name="idEvent[0]">
            <input type="hidden" name="countryEvent[0]">
            <input type="hidden" name="stateEvent[0]">
            <input type="hidden" name="cityEvent[0]">
            
            <input type="hidden" name="eventDate[0]">
        </tr>
        
        </table>
        
        <?php echo $this->Form->end(__('Create fights'));?>
    </div>
        
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">New message</h4>
          </div>
          <div class="modal-body">
            <form>
                
              <div class="form-group">
                <input type="checkbox" name="search_venue" value="1"> Search location by venue<br>
                <input type="text" placeholder="Venue" name="venue" id="EventVenuesLabel" style="width:250px" required="true">
                <input type="hidden" id="EventVenuesId">
              </div>
                
              <div class="form-group">
                <label for="recipient-name" class="control-label">Date:</label>
                <input type="text" class="form-control" id="event-date">
              </div>
              <div class="form-group">
                  <label for="message-text" class="control-label">Location:</label>
                    <div class="panel-body">
                        <?php echo $this->Form->create('Location', array('url' => array('controller' => 'Venues', 'action' => 'assign_to_event'))); ?>
                            <div class="col-lg-3">
                                <div class="form-group input-group">
                                    <span class="input-group-addon" id="flag-contry" style="height: 34px;">&nbsp;&nbsp;&nbsp;</span>
                                     <?php echo $this->Form->input('countries_text', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Country', true))); ?>
                                     <?php echo $this->Form->input('countries_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            
                            <div class="col-lg-3">
                                <div class="form-group input-group">
                                    <div id="states-contend">
                                        <select class="form-control">
                                            <option><?php echo __('select state', true);?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group input-group">
                                    <div id="cities-contend">
                                        <select class="form-control">
                                            <option><?php echo __('select city', true);?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                                <?php echo $this->Form->end(); ?>
                    </div>
                    
              </div>
              <div class="form-group">
                <label for="message-text" class="control-label">Apply to row#:</label>
                <select id="rowNumber">
                    <option value="1">1</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <label style="float:left;color:#00FF00;font-style:italic;line-height: 34px;font-size:large" id="notification_banner"></label>
            
            <button type="button" class="btn btn-primary" id="save_button">Save event</button>
            <button type="button" class="btn btn-primary" id="clear_button">Clear values</button>
            <button type="button" class="btn btn-danger" id="close_button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

<script type="text/javascript">
    
    /*    
     * Take a date and return a string with the date
     * on written form
    */
    function returnDateString(theDate)
    {
        var m_names = new Array("January", "February", "March", 
            "April", "May", "June", "July", "August", "September", 
            "October", "November", "December");

        /*
         *position 0 = year 
         * position 1 = month
         * position 2 = day
         */
        var arrayDates = theDate.split("-");
        var selectedMonth = "";
        
        if (arrayDates[1] != "10" && arrayDates[1] != "11" && arrayDates[1] != "12")
        {
            selectedMonth = arrayDates[1].substring(1);
            selectedMonth = parseInt(selectedMonth)-1;
        }
        if (arrayDates[1]=="10")
        {
            selectedMonth = 9;
        }
        if (arrayDates[1]=="11")
        {
            selectedMonth = 10;
        }
        if (arrayDates[1]=="12")
        {
            selectedMonth = 11;
        }
                
        if (arrayDates[2] == "01" || arrayDates[2] == "21" || arrayDates[2] == "31")
        {
            sup = "st";
        }
        else if (arrayDates[2] == "02" || arrayDates[2] == "22")
        {
            sup = "nd";
        }
        else if (arrayDates[2] == "03" || arrayDates[2] == "23")
        {
            sup = "rd";
        }
        else
        {
            sup = "th";
        }
                        
        var finalDate = m_names[selectedMonth]+" "+parseInt(arrayDates[2])+sup+", "+arrayDates[0];
        return finalDate;
        
    }

    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Add event')
        
            $('#event-date').datepicker({
                dateFormat: 'yy-mm-dd',
                yearRange: '1780:2016',
                changeMonth: true,
                changeYear: true,
                altField: "#idTourDateDetailsHidden",
                altFormat: "yy-mm-dd"
            });
    })
        
    $("[name='event[0]']").autocomplete({
        minLength: 3,
        source: function(request, response) {
            var loadEvents = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getEvents'));?>/" + request.term;
            $.getJSON(loadEvents, function(data) {
                response(data);
            });
        },
        select: function( event, ui ) {
                $("[name='event[0]']").val(ui.item.label);
                $("[name='idEvent[0]']").val(ui.item.id);
                return false;
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        $("[name='idEvent[0]']").val(item.id);
        return $("<li>")
            .append("<a>" + item.label + "</a>")
                .appendTo(ul);           
    };
    
    $("[name='fighter1[0]']").autocomplete({       
        minLength: 3,
        source: function(request, response) {
            var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
            $.getJSON(loadIdentities, function(data) {
                response(data);
            });
        },
        select: function( event, ui ) {
                $("[name='fighter1[0]']").val(ui.item.label);
                $("[name='idFighter1[0]']").val(ui.item.id);
                return false;
        } 
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
         return $("<li>")
            .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };

    $("[name='fighter2[0]']").autocomplete({
        minLength: 3,
        source: function(request, response) {
            var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
            $.getJSON(loadIdentities, function(data) {
                response(data);
            });
        },
        select: function( event, ui ) {
                $("[name='fighter2[0]']").val(ui.item.label);
                $("[name='idFighter2[0]']").val(ui.item.id);
                return false;
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
         $("[name='idFighter2[0]']").val(item.id);
         return $("<li>")
            .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };

$(document).ready(function() {
    $("[name='venue']").hide();

    var showVenue = function() {
        if ( $("[name='venue']").is(":visible") )
        {
            $("[name='venue']").hide();
        }
        else
        {
            $("[name='venue']").show();
            
        }
    };
    $("[name='search_venue']").on( "click", showVenue );
    
    
    $(window).scroll(function() {
        $('#header').css('top', $(this).scrollTop() + "px");
    });
    
    $("#remove_last_button").click(function(e){
        if (x==1)
        {
            alert("Cannot remove only row");
        }
        else
        {
            var valueToRemove = x -1;
            $("[name='fighter1["+valueToRemove+"]']").remove();
            $("[name='idFighter1["+valueToRemove+"]']").remove();
            $("[name='weight1["+valueToRemove+"]']").remove();
            $("[name='fighter2["+valueToRemove+"]']").remove();
            $("[name='idFighter2["+valueToRemove+"]']").remove();
            $("[name='weight2["+valueToRemove+"]']").remove();
            $("[name='winner["+valueToRemove+"]']").remove();
            $("[name='via["+valueToRemove+"]']").remove();
            $("[name='time["+valueToRemove+"]']").remove();
            $("[name='boxed_rounds["+valueToRemove+"]']").remove();
            $("[name='scheduled_rounds["+valueToRemove+"]']").remove();
            $("[name='event["+valueToRemove+"]']").remove();
            $("[name='idEvent["+valueToRemove+"]']").remove();
            $("[name='eventDate["+valueToRemove+"]']").remove();
            $("[name='row["+valueToRemove+"]']").remove();
            $("[name='sources["+valueToRemove+"]']").remove();
            x = x-1;
            
            $("select[id=rowNumber] option:last").remove();
        }
    });
    
    $("#button_create_event").click(function(e){
        e.preventDefault();
    });
    
    $("#clear_button").click(function(e){
        clearValues();
    });
    
    $("#close_button").click(function(e){
        clearValues();        
    });
    
    function clearValues()
    {
        $("[name='search_venue']").attr('checked', false); // Unchecks it
        $("[name='venue']").hide();
        $("#event-date").val("");
        $("#LocationCountriesText").val("");
        $("#EventVenuesLabel").val("");
        $("#LocationStatesId").empty();
        $("#LocationStatesId").hide();
        $("#LocationCitiesId").empty();
        $("#LocationCitiesId").hide();
        $("#flag-contry img:last-child").remove();
        $("#notification_banner").empty();
    }
    
    $("#save_button").click(function(e){
        e.preventDefault();
        
        var errors = 0; //if errors is cero, put new event on row, otherwise show error
        var errorMessage = "";
        
        if ($("#event-date").val()=="")
        {
            errorMessage = "Please enter a date";
            errors = 1;
        }
        else
        {
            var fechaNormal = $("#event-date").val();
            var fecha = returnDateString($("#event-date").val());    
        }
        
        var separatorLocations = "";
        
        var locationStates = $("#LocationStatesId option:selected").text();
        if (locationStates=="Select State")
        {
            locationStates = "";
        }
        else
        {
            separatorLocations = ", ";
        }
        
        var locationCities = $("#LocationCitiesId option:selected").text()
        if (locationCities=="Select City")
        {
            locationCities = ""
            separatorLocations = "";
        }
        
        var location = "";
        if ($('#LocationCountriesId').val()==240)
        {
            if ($("[name='venue']").is(":visible"))
            {
                location = $('#EventVenuesLabel').val();
            }
            else
            {
                location = locationCities+separatorLocations+locationStates;
            }
        }
        else
        {
            if ($("[name='venue']").is(":visible"))
            {
                location = $('#EventVenuesLabel').val();
            }
            else
            {
                location = locationCities+separatorLocations+$("#LocationCountriesText").val();    
            }
        }
        
        if (location==", ")
        {
            errorMessage = "Please specify location";
            errors = 1;
        }
        
        if (errors == 0)
        {
            $("#notification_banner").css('color', '#00FF00');
            var currentRow = (parseInt($("#rowNumber option:selected").text()));
            if (x!=1)
            {
                if (currentRow!=1)
                {
                    currentRow--;
                    $("#notification_banner").text("Event assigned to row "+(currentRow+1));
                    $("#event-"+currentRow).val(fecha+" - "+location);    
                    $("[name='idEvent["+ currentRow +"]']").val("-1");
                    $("[name='eventDate["+currentRow+"]']").val(fechaNormal);

                    $("[name='countryEvent["+currentRow+"]']").val($('#LocationCountriesId').val());
                    $("[name='stateEvent["+currentRow+"]']").val($('#LocationStatesId').val());
                    $("[name='cityEvent["+currentRow+"]']").val($('#LocationCitiesId').val());

                }
                else
                {
                    $("[name='event[0]']").val(fecha+" - "+location);
                    $("#notification_banner").text("Event assigned to row 1");
                    $("[name='idEvent[0]']").val("-1");
                    $("[name='eventDate[0]']").val(fechaNormal);

                    $("[name='countryEvent[0]']").val($('#LocationCountriesId').val());
                    $("[name='stateEvent[0]']").val($('#LocationStatesId').val());
                    $("[name='cityEvent[0]']").val($('#LocationCitiesId').val());

                }

            }
            else
            {
                $("#notification_banner").text("Event assigned to row 1");
                $("[name='event[0]']").val(fecha+" - "+location);
                $("[name='idEvent[0]']").val("-1");
                $("[name='eventDate[0]']").val(fechaNormal);

                $("[name='countryEvent[0]']").val($('#LocationCountriesId').val());
                $("[name='stateEvent[0]']").val($('#LocationStatesId').val());
                $("[name='cityEvent[0]']").val($('#LocationCitiesId').val());

            }
        }
        else
        {
            $("#notification_banner").css('color', 'red');
            $("#notification_banner").text(errorMessage);
        }

    });
    
    $('#EventVenuesLabel').autocomplete({
       minLength: 3,
       appendTo: "#exampleModal",
       source: function(request, response){
         var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'getVenue'));?>/" + request.term;
            $.getJSON(loadIdentities, function(data){
                response(data);
            });
       },
       focus: function( event, ui ) {
        $('#EventVenuesLabel').val(ui.item.label);
        return false;
       },
       select: function( event, ui ) {
                $('#EventVenuesLabel').val(ui.item.label);
                $('#EventVenuesId').val(ui.item.id);
                
                console.log('El id del venue es '+ $('#EventVenuesId').val() + " city " +ui.item.City+ " cityID "+ui.item.CityID +" state "+ ui.item.State + " stateID="+ ui.item.StateID +" country "+ui.item.Country);
                var loadCountries = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(ui.item.Country);
                
                $.getJSON(loadCountries, function(data){                   
                    $('#flag-contry').html("<img src='<?php echo $this->html->url('/img/flags')?>/" + data[0].flag + "'>");
                    $("#LocationCountriesText").val(data[0].label);
                    $('#LocationCountriesId').val(data[0].id);
                    $('#cities-contend').html("");
                                        
                    var loadStates = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getStates'));?>";
                    $.post(loadStates, {countries_id: data[0].id}, function(data){
                        $('#states-contend').html(data);
                        $("#LocationStatesId").val(ui.item.StateID);
                        var loadCities = "<?php echo $this->Html->url(array('controller' => 'Events','action' => 'getCities'));?>";
                        $.post(loadCities, {states_id: ui.item.StateID}, function(data){
                                $('#cities-contend').html(data);
                                $("#LocationCitiesId").val(ui.item.CityID);                                
                            });
                    });
                    
                });
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + " - " + item.City + " " + item.State + ", " + item.Country + "</a>" )
            .appendTo( ul );
    };
    
    $("#LocationCountriesText").autocomplete({
        minLength: 0,
        appendTo: "#exampleModal",
	source: function(request, response){
            var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(request.term);
            $.getJSON(loadIdentities, function(data){
                response(data);
            });
        },
	focus: function( event, ui ) {
            $("#LocationCountriesText").val(ui.item.label);
	    return false;
	},
	select: function( event, ui ) {
            $("#LocationCountriesText" ).val(ui.item.label);
	    $('#flag-contry').html("<img src='<?php echo $this->html->url('/img/flags')?>/" + ui.item.flag + "'>");
	    $('#LocationCountriesId').val(ui.item.id);            
	    $('#cities-contend').html("");
            var loadStates = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getStates'));?>";
	    $.post(loadStates, {countries_id: ui.item.id}, function(data){
                $('#states-contend').html(data);
	        $('#LocationStatesId').change(function(){
                var loadCities = "<?php echo $this->Html->url(array('controller' => 'Events','action' => 'getCities'));?>";
                    $.post(loadCities, {states_id: $(this).val()}, function(data){
                        $('#cities-contend').html(data);
                    });
	        });
	    });
	    return false;
	}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
                .append( "<a><img src='<?php echo $this->html->url('/img/flags')?>/" + item.flag + "'> " + item.label + "</a>" )
	        .appendTo( ul );
	};
    
    /*
    * 
    This code allows to add unlimited fights as requested
     **/
    /*-------------------------------*/
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    
    $(add_button).click(function(e){ //on add input button click
        rowNumber.add(new Option(x+1, x+1));
        
        e.preventDefault();        
        $("#fight_table").find('tbody')
            .append($('<tr>')
                .append($('<td>')
                    .append('<p name="row['+x+']">'+ (x+1) + '</p>' )
                )
                .append($('<td>')
                    .append('<select name="sources[' +x+ ']" required><option value="">Select source</option><option value="1">BOXREC</option><option value="2">IBOP</option><option value="3">FF</option><option value="4">SHERDOG</option><option value="5">OTHER</option><option value="6">CYBERBOXINGZONE</option><option value="7">MIXEDMARTIALARTS.COM</option><option value="8">ADCOMBAT.COM</option><option value="9">WIKIPEDIA.ORG</option><option value="10">RoadFC.com</option><option value="11">Boxing Commission</option><option value="12">NEWSPAPERS</option><option value="13">The RING (Record Book)</option></select>')
                )
                .append($('<td>')
                    .append( '<input id="fighter1-'+x+'" class="search-identity" placeholder="Fighter 1" type="text" name="fighter1[' +x+ ']" required="true"/><input type="hidden" name="idFighter1['+x+']">' )
                )
                .append($('<td>')
                    .append( '<input type="text" class="form-control input-sm" placeholder="Weight 1" name="weight1[' +x+ ']"/>' )
                )
                .append($('<td>')
                    .append( '<input id="fighter2-'+x+'" class="search-identity"  type="text" placeholder="Fighter 2" name="fighter2[' +x+ ']" required="true"/><input type="hidden" name="idFighter2['+x+']">' )
                )
                .append($('<td>')
                    .append( '<input type="text" class="form-control input-sm" placeholder="Weight 2" name="weight2[' +x+ ']"/>' )
                )
                .append($('<td>')
                    .append('<select name="winner[' +x+ ']"><option value="1">Fighter 1</option><option value="2">Fighter 2</option><option value="3">Draw</option></select>')
                )
                .append($('<td>')
                    .append('<select name="via[' +x+ ']"><option value="KO">KO</option><option value="TKO">TKO</option><option value="TD">TD</option><option value="MD">MD</option><option value="SD">SD</option><option value="D-PTS">D-PTS</option><option value="DRAW">DRAW</option><option value="D-NWS">D-NWS</option><option value="NWS">NWS</option><option value="RTD">RTD</option><option value="PTS">PTS</option><option value="MAJORITY DRAW">MAJORITY DRAW</option><option value="FOUL">FOUL</option><option value="DQ">DQ</option><option value="SUBMISSION">SUBMISSION</option><option value="STOPPAGE">STOPPAGE</option><option value="NC">NC</option><option value="ND">ND</option><option value="Split Draw">Split Draw</option><option value="UD">UD</option><option value="scheduled">scheduled</option><option value="EXHIBITION">EXHIBITION</option><option value="AMATEUR">AMATEUR</option></select>')
                )
                .append($('<td>')
                    .append( '<input type="datetime" value="00:00" name="time[' +x+ ']"/>' )
                )
                .append($('<td>')
                    .append( '<input type="text" class="form-control input-sm" placeholder="Boxed rounds" name="boxed_rounds[' +x+ ']"/>' )
                )
                .append($('<td>')
                    .append( '<input type="text" class="form-control input-sm" placeholder="Scheduled rounds" name="scheduled_rounds[' +x+ ']"/>' )
                )
                .append($('<td>')
                    .append( '<input type="text" name="event['+x+']" id="event-'+x+'" placeholder="Event" required="true"/><input type="hidden" name="idEvent['+x+']"><input type="hidden" name="eventDate['+x+']"><input type="hidden" name="countryEvent['+x+']"><input type="hidden" name="stateEvent['+x+']"><input type="hidden" name="cityEvent['+x+']">' )
                )
            );
            /*********************/
            $("#fighter1-"+x).autocomplete({
                minLength: 3,
                source: function(request, response) {
                    var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
                    $.getJSON(loadIdentities, function(data) {
                        response(data);
                    });
                },
                select: function( event, ui ) {
                        var nameFighter1 = $(this).attr('name');
                        var actualX = nameFighter1.substring(nameFighter1.lastIndexOf("[")+1,nameFighter1.lastIndexOf("]"));                      
                        $("[name='" + $(this).attr('name')+"']").val(ui.item.label);
                        $("[name='idFighter1[" +(actualX)+ "]']").val(ui.item.id);
                        return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {              
                return $("<li>")
                    .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
            };
            
            $("#fighter2-"+x).autocomplete({
                minLength: 3,
                source: function(request, response) {
                    var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
                    $.getJSON(loadIdentities, function(data) {
                        response(data);
                    });
                },
                select: function( event, ui ) {
                        var nameFighter2 = $(this).attr('name');                        
                        var actualX = nameFighter2.substring(nameFighter2.lastIndexOf("[")+1,nameFighter2.lastIndexOf("]"));                        
                        $("[name='" + $(this).attr('name')+"']").val(ui.item.label);
                        $("[name='idFighter2[" +(actualX)+ "]']").val(ui.item.id);
                        return false;
                } 
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                return $("<li>")
                    .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
            };
            
            $("#event-"+x).autocomplete({
                minLength: 3,
                source: function(request, response) {
                    var loadEvents = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getEvents'));?>/" + request.term;
                    $.getJSON(loadEvents, function(data) {
                        response(data);
                    });
                },
                select: function( event, ui ) {
                        var eventName = $(this).attr('name');                        
                        var actualX = eventName.substring(eventName.lastIndexOf("[")+1,eventName.lastIndexOf("]"));
                        $("[name='" + $(this).attr('name')+"']").val(ui.item.label);
                        $("[name='idEvent[" +(actualX)+ "]']").val(ui.item.id);
                        return false;
                } 
            }).data("ui-autocomplete")._renderItem = function(ul, item) {              
                return $("<li>")
                    .append("<a>" + item.label + "</a>")
                        .appendTo(ul);           
            };
            x++;
    });    
});
</script>