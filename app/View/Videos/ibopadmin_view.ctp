<div class="videos view">
<h2><?php echo __('Video'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($video['Video']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($video['Video']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Description'); ?></dt>
		<dd>
			<?php echo h($video['Video']['short_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($video['Video']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('M4v'); ?></dt>
		<dd>
			<?php echo h($video['Video']['m4v']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ogv'); ?></dt>
		<dd>
			<?php echo h($video['Video']['ogv']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Webm'); ?></dt>
		<dd>
			<?php echo h($video['Video']['webm']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mp4'); ?></dt>
		<dd>
			<?php echo h($video['Video']['mp4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Flv'); ?></dt>
		<dd>
			<?php echo h($video['Video']['flv']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ibop Owned'); ?></dt>
		<dd>
			<?php echo h($video['Video']['ibop_owned']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Video'), array('action' => 'edit', $video['Video']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Video'), array('action' => 'delete', $video['Video']['id']), array(), __('Are you sure you want to delete # %s?', $video['Video']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Videos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Video'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Events'), array('controller' => 'events', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Event'), array('controller' => 'events', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fights'), array('controller' => 'fights', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fight'), array('controller' => 'fights', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Identities'), array('controller' => 'identities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Identity'), array('controller' => 'identities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Venues'), array('controller' => 'venues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Venue'), array('controller' => 'venues', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Events'); ?></h3>
	<?php if (!empty($video['Event'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Venues Id'); ?></th>
		<th><?php echo __('Locations Id'); ?></th>
		<th><?php echo __('Total Attendance'); ?></th>
		<th><?php echo __('Paid Attendance'); ?></th>
		<th><?php echo __('Broad Cast'); ?></th>
		<th><?php echo __('Overseeing Body'); ?></th>
		<th><?php echo __('Matchmaker'); ?></th>
		<th><?php echo __('Inspector'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Boxrec Notes'); ?></th>
		<th><?php echo __('Other Notes'); ?></th>
		<th><?php echo __('Broad Cast Comments'); ?></th>
		<th><?php echo __('Start Time'); ?></th>
		<th><?php echo __('Actual Start Time'); ?></th>
		<th><?php echo __('Main Event Time'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($video['Event'] as $event): ?>
		<tr>
			<td><?php echo $event['id']; ?></td>
			<td><?php echo $event['name']; ?></td>
			<td><?php echo $event['description']; ?></td>
			<td><?php echo $event['date']; ?></td>
			<td><?php echo $event['venues_id']; ?></td>
			<td><?php echo $event['locations_id']; ?></td>
			<td><?php echo $event['total_attendance']; ?></td>
			<td><?php echo $event['paid_attendance']; ?></td>
			<td><?php echo $event['broad_cast']; ?></td>
			<td><?php echo $event['overseeing_body']; ?></td>
			<td><?php echo $event['matchmaker']; ?></td>
			<td><?php echo $event['inspector']; ?></td>
			<td><?php echo $event['notes']; ?></td>
			<td><?php echo $event['boxrec_notes']; ?></td>
			<td><?php echo $event['other_notes']; ?></td>
			<td><?php echo $event['broad_cast_comments']; ?></td>
			<td><?php echo $event['start_time']; ?></td>
			<td><?php echo $event['actual_start_time']; ?></td>
			<td><?php echo $event['main_event_time']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'events', 'action' => 'view', $event['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'events', 'action' => 'edit', $event['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'events', 'action' => 'delete', $event['id']), array(), __('Are you sure you want to delete # %s?', $event['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Event'), array('controller' => 'events', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Fights'); ?></h3>
	<?php if (!empty($video['Fight'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sources Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Weights Id'); ?></th>
		<th><?php echo __('Schedule Rounds'); ?></th>
		<th><?php echo __('Time Per Round'); ?></th>
		<th><?php echo __('Boxed Roundas'); ?></th>
		<th><?php echo __('Final Figths Id'); ?></th>
		<th><?php echo __('Rules Id'); ?></th>
		<th><?php echo __('Time'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Boxrec Notes'); ?></th>
		<th><?php echo __('Other Notes'); ?></th>
		<th><?php echo __('Winner'); ?></th>
		<th><?php echo __('Types Id'); ?></th>
		<th><?php echo __('Is Competitor'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($video['Fight'] as $fight): ?>
		<tr>
			<td><?php echo $fight['id']; ?></td>
			<td><?php echo $fight['sources_id']; ?></td>
			<td><?php echo $fight['title']; ?></td>
			<td><?php echo $fight['description']; ?></td>
			<td><?php echo $fight['weights_id']; ?></td>
			<td><?php echo $fight['schedule_rounds']; ?></td>
			<td><?php echo $fight['time_per_round']; ?></td>
			<td><?php echo $fight['boxed_roundas']; ?></td>
			<td><?php echo $fight['final_figths_id']; ?></td>
			<td><?php echo $fight['rules_id']; ?></td>
			<td><?php echo $fight['time']; ?></td>
			<td><?php echo $fight['notes']; ?></td>
			<td><?php echo $fight['boxrec_notes']; ?></td>
			<td><?php echo $fight['other_notes']; ?></td>
			<td><?php echo $fight['winner']; ?></td>
			<td><?php echo $fight['types_id']; ?></td>
			<td><?php echo $fight['is_competitor']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'fights', 'action' => 'view', $fight['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'fights', 'action' => 'edit', $fight['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'fights', 'action' => 'delete', $fight['id']), array(), __('Are you sure you want to delete # %s?', $fight['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Fight'), array('controller' => 'fights', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Identities'); ?></h3>
	<?php if (!empty($video['Identity'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Birth Name'); ?></th>
		<th><?php echo __('Height'); ?></th>
		<th><?php echo __('Reach'); ?></th>
		<th><?php echo __('Gender'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($video['Identity'] as $identity): ?>
		<tr>
			<td><?php echo $identity['id']; ?></td>
			<td><?php echo $identity['name']; ?></td>
			<td><?php echo $identity['last_name']; ?></td>
			<td><?php echo $identity['birth_name']; ?></td>
			<td><?php echo $identity['height']; ?></td>
			<td><?php echo $identity['reach']; ?></td>
			<td><?php echo $identity['gender']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'identities', 'action' => 'view', $identity['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'identities', 'action' => 'edit', $identity['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'identities', 'action' => 'delete', $identity['id']), array(), __('Are you sure you want to delete # %s?', $identity['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Identity'), array('controller' => 'identities', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Venues'); ?></h3>
	<?php if (!empty($video['Venue'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sources Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Locations Id'); ?></th>
		<th><?php echo __('Capacity'); ?></th>
		<th><?php echo __('Owner'); ?></th>
		<th><?php echo __('Address 1'); ?></th>
		<th><?php echo __('Address 2'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Box Office Phone'); ?></th>
		<th><?php echo __('Commente'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($video['Venue'] as $venue): ?>
		<tr>
			<td><?php echo $venue['id']; ?></td>
			<td><?php echo $venue['sources_id']; ?></td>
			<td><?php echo $venue['name']; ?></td>
			<td><?php echo $venue['description']; ?></td>
			<td><?php echo $venue['locations_id']; ?></td>
			<td><?php echo $venue['capacity']; ?></td>
			<td><?php echo $venue['owner']; ?></td>
			<td><?php echo $venue['address_1']; ?></td>
			<td><?php echo $venue['address_2']; ?></td>
			<td><?php echo $venue['phone']; ?></td>
			<td><?php echo $venue['box_office_phone']; ?></td>
			<td><?php echo $venue['commente']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'venues', 'action' => 'view', $venue['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'venues', 'action' => 'edit', $venue['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'venues', 'action' => 'delete', $venue['id']), array(), __('Are you sure you want to delete # %s?', $venue['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Venue'), array('controller' => 'venues', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
