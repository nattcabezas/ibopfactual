<?php
echo $this->Html->css('fileinput');
echo $this->Html->script('fileinput');
?>
<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Video', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-file-video-o"></i> 
                    <?php echo __('Videos', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-file-video-o"></i> <?php echo __('add video', true);?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('Video', array('type'=>'file')); ?>

                    <div class="form-group">
                        <label><?php echo __('Type:', true);?></label>
                        <?php echo $this->Form->input('image_types_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $types)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Title:', true);?></label>
                        <?php echo $this->Form->input('title', array('label' => false, 'div' => false,'required' => true, 'class' => 'form-control')); ?>
                    </div>
                
                    <div class="form-group">
                        <?php
                            $languageOptions = array(
                                'English' => 'English',
                                'Spanish' => 'Spanish',
                                'Other' => 'Other'
                            );
                        ?>
                        <label><?php echo __('Language:', true);?></label>
                        <?php echo $this->Form->input('language', array('label' => false, 'div' => false,'required' => true,'options' => $languageOptions, 'class' => 'form-control','empty' => array(null => 'Select language'), 'required' => true)); ?>
                    </div>
                

                    <div class="form-group">
                        <label><?php echo __('Short description:', true);?></label>
                        <?php echo $this->Form->input('short_description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Description:', true);?></label>
                        <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('ogv:', true);?></label>
                        <?php echo $this->Form->input('ogv', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('webm:', true);?></label>
                        <?php echo $this->Form->input('webm', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('mp4:', true);?></label>
                        <?php echo $this->Form->input('mp4', array('label' => false, 'div' => false, 'required' => true, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('flv:', true);?></label>
                        <?php echo $this->Form->input('flv', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Image', true);?></label>
                        <?php echo $this->Form->input('img_file', array('type' => 'file', 'label' => false, 'div' => false, 'class' => 'form-control file-input')); ?>
                        <?php echo $this->Form->input('img', array('type' => 'hidden')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Source 1');?></label>
                        <?php echo $this->Form->input('source_1', array('label' => false, 'div' => false, 'class' => 'form-control'))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Source 2');?></label>
                        <?php echo $this->Form->input('source_2', array('label' => false, 'div' => false, 'class' => 'form-control'))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Donated by');?></label>
                        <?php echo $this->Form->input('donated_by', array('label' => false, 'div' => false, 'class' => 'form-control'))?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Private');?></label>
                        <?php echo $this->Form->input('private', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => array(1 => 'Public', 2 => 'Private', 3 => 'V.I.P')))?>
                    </div>

                    <div class="checkbox">
                        <label>
                            <?php echo $this->Form->input('ibop_owned', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => 1)); ?>
                            <?php echo __('ibop owned', true);?>
                        </label>
                    </div>

                    <div class="form-group">
                        <?php echo $this->Form->button(__('Add Video'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<script>
  $(document).ready(function() {
        $('.file-input').fileinput({showUpload: false});
        $('#VideoImgFile').change(function() {
            $('#VideoImg').val($('.file-caption-name').html());
        });
        $('.fileinput-remove-button').click(function(event) {
            $('#VideoImg').val("");
        });
        $('.fileinput-remove').click(function() {
            $('#VideoImg').val("");
        });
    });
</script>