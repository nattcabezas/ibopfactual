<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Rules', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-book"></i> 
                    <?php echo __('Rules', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-book"></i> <?php echo __('add Rules', true);?></h3>
            </div>
            <div class="panel-body">
				<?php echo $this->Form->create('Rule'); ?>
					
					<?php echo $this->Form->input('id'); ?>
					<div class="form-group">
						<label><?php echo __('Title:', true);?></label>
						<?php echo $this->Form->input('title', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
					</div>
					
					<div class="form-group">
						<label><?php echo __('Description:', true);?></label>
						<?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
                		<?php echo $this->Form->button(__('Add Title'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                	</div>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
