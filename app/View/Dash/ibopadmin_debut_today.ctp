<table class="table table-striped table-hover table-striped tablesorter">
    <thead>
        <tr>
            <th><?php echo __('Name', true);?></th>            
            <th><?php echo __('Debut Date', true);?></th>
        </tr>
    </thead>
    <tbody>        
        <?php foreach ($debutToday as $debut) { ?>
            <tr>
                <td>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($debut['identities']['identity_id']))); ?>">
                        <?php echo $debut['identities']['name'] . " " .  $debut['identities']['last_name']  ?>                            
                    </a>
                </td>               
                 <td>
                    <?php echo $debut['0']['debut_date'] ?>                     
                </td>
            </tr>
        <?php } ?>    
    </tbody>
</table>
 