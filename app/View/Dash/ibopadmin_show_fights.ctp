<div class="table-responsive" id="fight-today-table">
    <table class="table table-striped table-hover table-striped tablesorter">
        <thead>
            <tr>
                <th><?php echo __('Fight', true);?></th>
                <th><?php echo __('Event', true);?></th>
                <th><?php echo __('Date', true);?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($fights as $fight){ ?>
                <tr>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights a day like today']['fight_id']))); ?>">
                            <?php echo $fight['Fights a day like today']['fight_title']; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'edit/' . base64_encode($fight['Fights a day like today']['event_id']))); ?>">
                            <?php echo $fight['Fights a day like today']['event_name']; ?>
                        </a>
                    </td>
                    <td><?php echo $fight['Fights a day like today']['date']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>