<div class="table-responsive" id="debut-today-table">
    <table class="table table-striped table-hover table-striped tablesorter">
        <thead>
            <tr>
                <th><?php echo __('Name', true);?></th>
                <th><?php echo __('Debut Date', true);?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($debut as $identity){ ?>
                <tr>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($identity['Boxers debut a day like today']['identity_id'])));?>">
                            <?php echo $identity['Boxers debut a day like today']['name'] . ' ' . $identity['Boxers debut a day like today']['last_name']?>
                        </a>
                    </td>
                    <td><?php echo $identity['Boxers debut a day like today']['debut_date']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
         $(document).ready(function() {
            $('.search-debut-date').click(function(event){
                event.preventDefault();
                var loadPage = "<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'debutToday'));?>";
                $('#spin-debut').fadeIn();
                $.post(loadPage, {date: $('#date-input4').val()}, function(data){
                    $('#debut-today-table').html(data);
                    $('#spin-debut').fadeOut();
                });
                
            });
        });    
</script>