<div class="row">
	<div class="col-lg-12">
        <h1><?php echo __('Profile', true);?></h1>
        
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo __('edit user', true);?></h3>
            </div>
            <div class="panel-body">
            	<?php echo $this->Form->create('User', array('role' => 'form')); ?>

                    <?php echo $this->Form->input('id', array('value' => $userData['User']['id']));?>

					<div class="form-group">   
                        <label><?php echo __('Username:', true);?></label>
                        <?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'div' => false, 'required' => true, 'value' => $userData['User']['username']))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Full Name:', true);?></label>
                        <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'div' => false, 'required' => true, 'value' => $userData['User']['name']))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('E-mail:', true);?></label>
                        <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'div' => false, 'required' => true, 'value' => $userData['User']['email']))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Password:', true);?></label>
                        <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'div' => false))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Confirm Password:', true);?></label>
                        <?php echo $this->Form->input('confirm-password', array('type' => 'password', 'class' => 'form-control', 'label' => false, 'div' => false))?>
                    </div>
					
					<div class="form-group">
                        <?php echo $this->Form->button(__('Save User'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    </div>

            	<?php echo $this->Form->end(); ?>
            </div>
    	</div>
    </div>
</div>