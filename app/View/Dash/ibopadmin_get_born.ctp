<table class="table table-striped table-hover table-striped tablesorter">
    <thead>
        <tr>
            <th><?php echo __('Name', true);?></th>
            <th><?php echo __('Birth Date', true);?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($bornToday as $born) { ?>
            <tr>
                <td>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($born['Identities']['id']))); ?>">
                        <?php echo $born['Identities']['name'] . ' '  . $born['Identities']['last_name'] ?>                            
                    </a>
                    
                </td>
                <td>
                    <?php echo $born['IdentityBirth']['date'] ?>
                </td>                        
            </tr>
        <?php } ?>    
    </tbody>
</table>
 