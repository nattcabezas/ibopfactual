<table class="table table-striped table-hover table-striped tablesorter">
    <thead>
        <tr>
            <th><?php echo __('Fight', true);?></th>
            <th><?php echo __('Event', true);?></th>
            <th><?php echo __('Date', true);?></th>
        </tr>
    </thead>
    <tbody>
        
        <?php foreach ($fightToday as $fight) { ?>
            <tr>
                <td>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fight']['id']))); ?>">
                        <?php echo $fight['Fight']['title']   ?>                            
                    </a>               
                </td>
                <td>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'edit/' . base64_encode($fight['Event']['id']))); ?>">
                        <?php echo $fight['Event']['name'] ?>
                    </a>
                </td>
                 <td>
                    <?php echo $fight['Event']['date'] ?>
                </td>
            </tr>
        <?php } ?>    
    </tbody>
</table>
 