<div class="table-responsive" id="death-today-table">
    <table class="table table-striped table-hover table-striped tablesorter">
        <thead>
            <tr>
                <th><?php echo __('Name', true);?></th>
                <th><?php echo __('Death Date', true);?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($die as $identity){ ?>
                <tr>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($identity['Boxers die a day like today']['id'])));?>">
                            <?php echo $identity['Boxers die a day like today']['name'] . ' ' . $identity['Boxers die a day like today']['last_name']?>
                        </a>
                    </td>
                    <td><?php echo $identity['Boxers die a day like today']['death_day']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
