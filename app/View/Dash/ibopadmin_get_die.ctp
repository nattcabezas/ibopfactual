<table class="table table-striped table-hover table-striped tablesorter">
    <thead>
        <tr>
            <th><?php echo __('Name', true);?></th>
            <th><?php echo __('Death Date', true);?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dieToday as $die) { ?>
            <tr>
                <td>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($die['Identities']['id']))); ?>">
                        <?php echo $die['Identities']['name'] . ' '  . $die['Identities']['last_name'] ?>                            
                    </a>
                    
                </td>
                <td>
                    <?php echo $die['IdentityDeath']['date'] ?>
                </td>                        
            </tr>
        <?php } ?>    
    </tbody>
</table>
 