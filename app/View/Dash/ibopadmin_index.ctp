<?php 
    echo $this->Html->css('morris');
    echo $this->Html->script('raphael-min');
    echo $this->Html->script('morris.min');
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Dashboard', true); ?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard', true) ?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-group fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo number_format($countIdentity, 0, '.', ' '); ?></div>
                        <div><?php echo __('Persons in the system');?></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'index'));?>">
                <div class="panel-footer">
                    <span class="pull-left"><?php echo __('Go To Persons')?></span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shield fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo number_format($countFight, 0, '.', ' '); ?></div>
                        <div><?php echo __('Fights in the system');?></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'index'));?>">
                <div class="panel-footer">
                    <span class="pull-left"><?php echo __('Go to Fights')?></span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-calendar fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo number_format($countEvent, 0, '.', ' '); ?></div>
                        <div><?php echo __('Events in the system');?></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'index'));?>">
                <div class="panel-footer">
                    <span class="pull-left"><?php echo __('Go to Events')?></span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-building-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo number_format($countVenue, 0, '.', ' '); ?></div>
                        <div><?php echo __('Venues in the system');?></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'index'));?>">
                <div class="panel-footer">
                    <span class="pull-left"><?php echo __('Go to Venues')?></span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    
</div>

<div class="row">
    <div class="col-lg-4">
        <h3><?php echo __('Gender Data');?></h3>
        <div id="graph-gender"></div>
    </div>
    <div class="col-lg-offset-4 col-lg-4">
        <h3><?php echo __('Types of Fights data');?></h3>
        <div id="graph-types"></div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-lg-12">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#persons-born" data-toggle="tab"><?php echo __('Born This Day');?></a>
            </li>
            <li>
                <a href="#persons-debut" data-toggle="tab" id="load-debut"><?php echo __('Today’s Debut');?></a>
            </li>
            <li>
                <a href="#persons-die" data-toggle="tab" id="load-die"><?php echo __('Died This Day');?></a>
            </li>
            <li>
                <a href="#fights-today" data-toggle="tab" id="load-fights"><?php echo __('Today’s Fights');?></a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="persons-born">
                
                <div class="row" style="padding-top: 25px;">
                    <div class="col-lg-12">

                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-4">
                                <?php echo $this->Form->input('date-input', array('type' => 'text', 'class' => 'form-control form-date', 'div' => false, 'label' => false, 'placeholder' => 'search date', 'readonly' => true))?>
                            </div>
                            <div class="col-sm-2">
                                <a href="" class="btn btn-primary search-date"><i class="fa fa-search"></i> search</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="table-responsive" id="born-today-table">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Name', true);?></th>
                                <th><?php echo __('Birth Date', true);?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($bornToday as $identity){ ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($identity['Boxers born a day like today']['id'])));?>">
                                            <?php echo $identity['Boxers born a day like today']['name'] . ' ' . $identity['Boxers born a day like today']['last_name']?>
                                        </a>
                                    </td>
                                    <td><?php echo $identity['Boxers born a day like today']['birth_date']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="persons-debut">
                <div class="row" style="padding-top: 25px;">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-4">
                                <?php echo $this->Form->input('date-input4', array('type' => 'text', 'class' => 'form-control form-date', 'div' => false, 'label' => false, 'placeholder' => 'search date', 'readonly' => true))?>
                            </div>
                            <div class="col-sm-2">
                                <a href="" class="btn btn-primary search-debut-date"><i class="fa fa-search"></i> search</a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <center>
                    <i class="fa fa-refresh fa-spin fa-3x" id="spin-debut"></i>
                </center>
            </div>
            <div class="tab-pane fade" id="persons-die">
                <div class="row" style="padding-top: 25px;">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-4">
                                <?php echo $this->Form->input('date-input2', array('type' => 'text', 'class' => 'form-control form-date', 'div' => false, 'label' => false, 'placeholder' => 'search date', 'readonly' => true))?>
                            </div>
                            <div class="col-sm-2">
                                <a href="" class="btn btn-primary search-death-date"><i class="fa fa-search"></i> search</a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <center>
                    <i class="fa fa-refresh fa-spin fa-3x" id="spin-die"></i>
                </center>
            </div>
            <div class="tab-pane fade" id="fights-today">
                <div class="row" style="padding-top: 25px;">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-4">
                                <?php echo $this->Form->input('date-input3', array('type' => 'text', 'class' => 'form-control form-date', 'div' => false, 'label' => false, 'placeholder' => 'search date', 'readonly' => true))?>
                            </div>
                            <div class="col-sm-2">
                                <a href="" class="btn btn-primary search-fight-date"><i class="fa fa-search"></i> search</a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <center>
                    <i class="fa fa-refresh fa-spin fa-3x" id="spin-fight"></i>
                </center>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Use Morris.Bar
    <?php 
        $percentMale    = ($maleIdentities * 100)/$countIdentity;
        $percentFemale  = ($femaleIdentities * 100)/$countIdentity;
    ?>
    Morris.Donut({
        element: 'graph-gender',
        data: [
            {
                value: <?php echo $percentMale; ?>, 
                label: '<?php echo __('Male', true);?>', 
                formatted: '<?php echo __('Total male %s - %s%', number_format($maleIdentities, 0, '.', ' '), round($percentMale, 1));?>'
            },
            {
                value: <?php echo $percentFemale; ?>, 
                label: '<?php echo __('Female', true);?>', 
                formatted: '<?php echo __('Total female %s - %s%', number_format($femaleIdentities, 0, '.', ' '), round($percentFemale, 1));?>'
            }
        ],
        labelColor: '#0B62A4',
        formatter: function (x, data) { return data.formatted; }
    });
    
    Morris.Donut({
        element: 'graph-types',
        data:[
            <?php foreach ($typesData as $types){ ?>
                <?php
                    $percentType = ($types['count'] * 100)/$countFight;
                ?>
                {
                    value: <?php echo $percentType; ?>,
                    label: '<?php echo $types['name']?>',
                    formatted: '<?php echo __('Total %s %s - %s%', $types['name'], number_format($types['count'], 0, '.', ' '), round($percentType, 1));?>'
                },
            <?php } ?>
        ],
        labelColor: '#92231F',
        colors:[
            '#92231F',
            '#DF3530'
        ],
        formatter: function (x, data) { return data.formatted; }
    });
    
    $(document).ready(function() {
    
        var showDebut = true;
        $('#load-debut').click(function(){
            if(showDebut){
                var loadDebut = "<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'showDebut'));?>";
                $.post(loadDebut, function(data){
                    $('#spin-debut').fadeOut();
                    $('#persons-debut').append(data);
                });
                showDebut = false;
            }
        });
        
        var showDie = true;
        $('#load-die').click(function(){
            if(showDie){
                var loadDie = "<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'showDie'));?>";
                $.post(loadDie, function(data){
                    $('#spin-die').fadeOut();
                    $('#persons-die').append(data);
                });
                showDie = false;
            }
        });
        
        var showFights = true;
        $('#load-fights').click(function(){
            var loadFights = "<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'showFights'));?>";
            $.post(loadFights, function(data){
                $('#spin-fight').fadeOut();               
                $('#fights-today').append(data);
                
            });
            showFights = false;
        });
        
        $('.search-date').click(function(event){
            
            event.preventDefault();
            var loadPage = "<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'getBorn'));?>";
            $.post(loadPage, {date: $('#date-input').val()}, function(data){
                $('#born-today-table').html(data);
            });
                    
            
        });
         $('.search-death-date').click(function(event){
                event.preventDefault();
                var loadPage = "<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'getDie'));?>";
                $.post(loadPage, {date: $('#date-input2').val()}, function(data){
                    $('#death-today-table').html(data);
                });


        });
        $('.search-fight-date').click(function(event){
                event.preventDefault();
                var loadPage = "<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'fightToday'));?>";
                $.post(loadPage, {date: $('#date-input3').val()}, function(data){
                    $('#fight-today-table').html(data);
                });


        });
        
         
    
    });
    
</script>