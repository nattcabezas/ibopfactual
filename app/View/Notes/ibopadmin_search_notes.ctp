<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Notes', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-file-o"></i> <?php echo __('Notes', true)?></li>
        </ol>
    </div>
</div>

<div class="row">
    
    <div class="col-lg-6">
        <?php echo $this->Form->create('searchNotes', array('url' => array('controller' => 'Notes', 'action' => 'searchNotes'))); ?>
            <div class="col-lg-10">
                <?php echo $this->Form->input('keywork', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
            </div>
            <div class="col-lg-2">
                <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
        <?php echo $this->Form->end(); ?>
    </div>
    
    <div class="col-lg-2 col-md-offset-4">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Notes', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($notes as $note){ ?>
                        <tr>
                            <td><?php echo $note['Note']['title']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($note['Note']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        
                                        <a href="<?php echo $this->Html->url(array('action' => 'assign/' . base64_encode($note['Note']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        
                                        <a href="<?php echo $this->Html->url(array('action' => 'delete/' . base64_encode($note['Note']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>