<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Notes', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-file-o"></i> 
                    <?php echo __('Notes', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
    </div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-file-o"></i> <?php echo __('add note', true);?></h3>
            </div>
            <div class="panel-body">

            	<?php echo $this->Form->create('Note'); ?>
				
					<?php echo $this->Form->input('id');?>
					<div class="form-group">
						<label><?php echo __('Title:', true);?></label>
						<?php echo $this->Form->input('title', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Note:', true);?></label>
						<?php echo $this->Form->input('note', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Autor:', true);?></label>
						<?php echo $this->Form->input('autor_label', array('label' => false, 'div' => false, 'class' => 'form-control input-autocomplete input-identity', 'add-to' => '#NoteAutor', 'value' => $note['Identity']['name'].' '.$note['Identity']['last_name'])); ?>
						<?php echo $this->Form->input('autor', array('type' => 'hidden', 'value' => $note['Identity']['id'])); ?>
					</div>
                
                                        <div class="form-group">
                                            <?php echo $this->Form->input('creation_date', array('label' => 'Creation date:' ,'type' => 'text', 'class' => "calendar")); ?>
					</div>
                
					
					<div class="form-group">
                		<?php echo $this->Form->button(__('save Note'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                	</div>

				<?php echo $this->Form->end(); ?>

            </div>
		</div>
	</div>
</div>

  <script>
  $(function() {
    $( ".calendar" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true
    });
  });
  </script>