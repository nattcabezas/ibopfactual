<div class="notes view">
<h2><?php echo __('Note'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($note['Note']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($note['Note']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($note['Note']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Autor'); ?></dt>
		<dd>
			<?php echo h($note['Note']['autor']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Note'), array('action' => 'edit', $note['Note']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Note'), array('action' => 'delete', $note['Note']['id']), array(), __('Are you sure you want to delete # %s?', $note['Note']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Notes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Note'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fights'), array('controller' => 'fights', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fight'), array('controller' => 'fights', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Fights'); ?></h3>
	<?php if (!empty($note['Fight'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sources Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Weights Id'); ?></th>
		<th><?php echo __('Schedule Rounds'); ?></th>
		<th><?php echo __('Time Per Round'); ?></th>
		<th><?php echo __('Boxed Roundas'); ?></th>
		<th><?php echo __('Final Figths Id'); ?></th>
		<th><?php echo __('Rules Id'); ?></th>
		<th><?php echo __('Time'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Boxrec Notes'); ?></th>
		<th><?php echo __('Other Notes'); ?></th>
		<th><?php echo __('Winner'); ?></th>
		<th><?php echo __('Types Id'); ?></th>
		<th><?php echo __('Is Competitor'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($note['Fight'] as $fight): ?>
		<tr>
			<td><?php echo $fight['id']; ?></td>
			<td><?php echo $fight['sources_id']; ?></td>
			<td><?php echo $fight['title']; ?></td>
			<td><?php echo $fight['description']; ?></td>
			<td><?php echo $fight['weights_id']; ?></td>
			<td><?php echo $fight['schedule_rounds']; ?></td>
			<td><?php echo $fight['time_per_round']; ?></td>
			<td><?php echo $fight['boxed_roundas']; ?></td>
			<td><?php echo $fight['final_figths_id']; ?></td>
			<td><?php echo $fight['rules_id']; ?></td>
			<td><?php echo $fight['time']; ?></td>
			<td><?php echo $fight['notes']; ?></td>
			<td><?php echo $fight['boxrec_notes']; ?></td>
			<td><?php echo $fight['other_notes']; ?></td>
			<td><?php echo $fight['winner']; ?></td>
			<td><?php echo $fight['types_id']; ?></td>
			<td><?php echo $fight['is_competitor']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'fights', 'action' => 'view', $fight['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'fights', 'action' => 'edit', $fight['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'fights', 'action' => 'delete', $fight['id']), array(), __('Are you sure you want to delete # %s?', $fight['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Fight'), array('controller' => 'fights', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
