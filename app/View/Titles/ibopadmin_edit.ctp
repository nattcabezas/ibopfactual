<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Titles', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-bookmark"></i> 
                    <?php echo __('Titles', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
        <div class="btn-group pull-right" style="padding-bottom: 10px;">          
            <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idTitle) )); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view'); ?>">
                <i class="fa fa-eye"></i>
            </a>           
        </div>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bookmark"></i> <?php echo __('edit title', true);?></h3>
            </div>
            <div class="panel-body">
				<?php echo $this->Form->create('Title'); ?>
					
					<?php echo $this->Form->input('id');?>
					<div class="form-group">
						<label><?php echo __('Title:', true);?></label>
						<?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
					</div>
					
					<div class="form-group">
						<label><?php echo __('Description:', true);?></label>
						<?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text')); ?>
					</div>
                                        
                                        <div class="form-group">
                                            <label><?php echo __('Type:', true); ?></label>
                                            <?php echo $this->Form->input('title_category', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $titlesCategory) ); ?>
                                        </div>
                                            

					<div class="form-group">
                		<?php echo $this->Form->button(__('save Title'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                	</div>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
