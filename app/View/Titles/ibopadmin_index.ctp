<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Title', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-bookmark"></i> <?php echo __('Title', true)?></li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-6">
        <div class="input-group">
            <input type="text" class="form-control search-identity" placeholder="<?php echo __('Search');?>">
            <span class="input-group-btn"><a class="btn btn-primary"><i class="fa fa-search"></i></a></span>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-offset-4">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Title', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($titles as $title){ ?>
                        <tr>
                            <td><?php echo $title['Title']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($title['Title']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($title['Title']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.search-identity').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Titles', 'action' => 'getTitle'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('.search-identity').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                var editIdentity  = "<?php echo $this->Html->url(array('action' => 'edit'))?>/" + Base64.encode(ui.item.id);;
                window.location.replace(editIdentity);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + " [<strong>" + item.description + "</strong>]</a>" )
            .appendTo( ul );
        };
        
    });
</script>