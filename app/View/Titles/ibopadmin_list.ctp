
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Title', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-bookmark"></i> <?php echo __('Title', true)?></li>
        </ol>
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($fights[0]['Titles']['id']) )); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                
                <i class="fa fa-edit"></i>
            </a>
        </div>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fights as $fight){ ?>
                        <tr>
                            <td>
                                <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights']['id'])));?>" >
                                    <?php echo $fight['Fights']['title']?>  
                                </a>
                            </td>                           
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>