
<li class="<?php echo $thisController == 'Dash' ? 'active' : ''; ?>">
    <a href="<?php echo $this->Html->url(array('controller' => 'Dash', 'action' => 'index')); ?>">
        <i class="fa fa-dashboard"></i> 
        <?php echo __('Dashboard', true); ?>
    </a>
</li>

<?php if( $user['Role']['users'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'User' ? 'active' : ''; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => 'User', 'action' => 'index')); ?>">
            <i class="fa fa-user"></i> 
            <?php echo __('Users', true); ?>
        </a>
    </li>
<?php } ?>

<?php if( $user['Role']['persons'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'Identities' ? 'active' : ''; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'index'));?>">
            <i class="fa fa-group"></i> 
            <?php echo __('Persons', true); ?>
        </a>
    </li>
<?php } ?>

<?php if( $user['Role']['figth'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'Fights' ? 'active' : ''; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'index'));?>">
            <i class="fa fa-shield"></i> 
            <?php echo __('Fights', true); ?>
        </a>
    </li>
<?php } ?>

<?php if( $user['Role']['events'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'Events' ? 'active' : ''; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'index'));?>">
            <i class="fa fa-calendar"></i> 
            <?php echo __('Events', true); ?>
        </a>
    </li>
<?php } ?>

<?php if( $user['Role']['venues'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'Venues' ? 'active' : ''; ?>">
        <a href="<?php echo $this->html->url(array('controller' => 'Venues', 'action' => 'index')); ?>">
            <i class="fa fa-building-o"></i>
            <?php echo __('Venues', true); ?>
        </a>
    </li>
<?php } ?>

<?php if( $user['Role']['images'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'Images' ? 'active' : ''; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'index'));?>">
            <i class="fa fa-file-photo-o"></i>
            <?php echo __('Images', true); ?>
        </a>
    </li>
<?php } ?>

<?php if( $user['Role']['videos'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'Videos' ? 'active' : ''; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => 'Videos', 'action' => 'index'));?>">
            <i class="fa fa-file-video-o"></i> 
            <?php echo __('Videos', true); ?>
        </a>
    </li>
<?php } ?>

<?php if( $user['Role']['notes'] == 1 ) { ?>
    <li class="<?php echo $thisController == 'Notes' ? 'active' : ''; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => 'Notes', 'action' => 'index'))?>">
            <i class="fa fa-file-o"></i> 
            <?php echo __('Notes', true); ?>
        </a>
    </li>
<?php } ?>

<?php
    if(
        ($thisController == 'Weights') ||
        ($thisController == 'Titles') ||
        ($thisController == 'Types') ||
        ($thisController == 'Sources')
    ){
        $manageMenuClass = 'active';
    } else {
        $manageMenuClass = "";
    }
?>
<?php if( $user['Role']['manage'] == 1 ) { ?>
    <li class="dropdown <?php echo $manageMenuClass; ?>">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> <?php echo __('Manage Data', true)?> <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'Weights', 'action' => 'index'));?>">
                    <?php echo __('Weight Class', true);?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'Titles', 'action' => 'index'));?>">
                    <?php echo __('Titles', true);?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'Types', 'action' => 'index'));?>">
                    <?php echo __('Types of fights', true);?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'Rules', 'action' => 'index'));?>">
                    <?php echo __('Rules', true);?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'Sources', 'action' => 'index')); ?>">
                    <?php echo __('Sources', true);?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'SourceOdds', 'action' => 'index')); ?>">
                    <?php echo __('Sources ODDS', true);?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'IdsSources', 'action' => 'index')); ?>">
                    <?php echo __('IDs Types', true);?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'Jobs', 'action' => 'index')); ?>">
                    <?php echo __('Jobs', true);?>
                </a>
            </li>
        </ul>
    </li>
<?php
    if(
        ($thisController == 'Articles') ||
        ($thisController == 'ImageTypes') ||
        ($thisController == 'Featured') 
    ){
        $manageSiteMenuClass = 'active';
    } else {
        $manageSiteMenuClass = "";
    }
?>
    <li class="dropdown <?php echo $manageSiteMenuClass; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> <?php echo __('Manage Site', true)?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index')); ?>">
                        <?php echo __('Articles', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Banners', 'action' => 'index')); ?>">
                        <?php echo __('Banners', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Contacts', 'action' => 'index')); ?>">
                        <?php echo __('Contacts', true);?>
                    </a>
                </li>
                 <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Pages', 'action' => 'index')); ?>">
                        <?php echo __('Pages', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Quotes', 'action' => 'index')); ?>">
                        <?php echo __('Quotes', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Clean', 'action' => 'index')); ?>">
                        <?php echo __('Clean', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'ImageTypes', 'action' => 'index')); ?>">
                        <?php echo __('Types of Image', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'PageUsers', 'action' => 'index'));?>">
                        <?php echo __('Page Users', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Featured', 'action' => 'featuredPersons')); ?>">
                        <?php echo __('Site Featured', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Logs', 'action' => 'index')); ?>">
                        <?php echo __('Logs', true);?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Backup', 'action' => 'index')); ?>">
                        <?php echo __('Backup Data Base', true);?>
                    </a>
                </li>
            </ul>
    </li>
<?php }