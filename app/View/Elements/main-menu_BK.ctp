<div class="container">
    <!-- <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="burger-menu">Menu</span> 
    </button>-->
    
    <a id="simple-menu" href="#sidr" class="hidden-lg hidden-md hidden-sm"><span class="fa fa-arrow-left arrow white menu_devices"> Menu</span></a>
    <div id="sidr">
        <!-- Your content -->
        <a href="<?php echo $this->Html->url('/'); ?>" class="link-logo">
            
            <img src="<?php echo $this->Html->url('/img/ibop_logo.svg'); ?>" class="menu_logo img-responsive" alt="ibop_logo">
        </a>
        
        <ul class="">
            <li class="mainmenu-item "> 
               <a href="<?php echo $this->Html->url('/'); ?>">
                    <span class="fa fa-home"></span>  <?php echo __('Home', true); ?>
                </a>
            </li> 

            <li class="mainmenu-item ">
                <a href="#" class="menu-display" display-target="#search-panel-fighter_responsive">
                    <span class="fa fa-gloves"></span>  <?php echo __('Fighters', true); ?>
                </a>
            </li>


            <div class="search-div collapse" id="search-panel-fighter_responsive">
                <div class="row ">
                     <form action="<?php echo $this->Html->url(array('controller' => 'Persons', 'action' => 'search'));?>" role="form" id="person-search" method="post" accept-charset="utf-8">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Name" class="form-control" id="keyword-person" name="name">
                            <label for="name" class="glyphicon glyphicon-search " rel="tooltip" title="name"></label>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Last Name" class="form-control" id="keyword-person" name="last-name">
                            <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified button_menu" id="persons-button" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
                    
                </div>
            </div>


            <li class="mainmenu-item">
                <a href="#" class="menu-display" display-target="#search-panel-fights_responsive">
                    <span class="fa fa-boxing-ring" ></span>  <?php echo __('Fights', true); ?> 
                </a>
            </li>

            <div class="search-div collapse" id="search-panel-fights_responsive">
                <div class="row" >
                    
            <form action="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'search'));?>" role="form" id="fight-search" method="post">  
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Keyword" class="form-control" id="Keyword-fight" name="Keyword-fight">
                            <label for="date" class="glyphicon glyphicon-search" rel="tooltip" title="date"></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified button_menu" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
                 
                </div>
            </div>

            <li class="mainmenu-item ">
                <a href="#" class="menu-display" display-target="#search-panel-events_responsive">
                    <span class="fa fa-champ-cup"></span>  <?php echo __('Events', true); ?> 
                </a>
            </li>

            <div class="search-div collapse" id="search-panel-events_responsive">
                <div class="row" >
                    <form action="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'search'));?>" role="form" id="event-search" method="post">  
                <div class="col-md-8">

                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="search by date" class="form-control date-search" id="date" name="date-event">
                            <label for="date" class="glyphicon glyphicon-search" rel="tooltip" title="date"></label>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
                </div>
            </div>

            <li class="mainmenu-item ">
                <a href="<?php echo $this->Html->url(array('controller' => 'Articles')); ?>">
                    <span class="fa fa-book"></span>  <?php echo __('Articles', true); ?> 
                </a>
            </li>

            <li class="mainmenu-item ">
                <a href="<?php echo $this->Html->url(array('controller' => 'contacts')) ?>">
                    <span class="fa fa-envelope"></span>  <?php echo __('Contact', true); ?>
                </a>
            </li>
        </ul>
</div>
    
   
    
    <div class="collapse navbar-collapse menu_top_bar" id="collapse">
<a href="<?php echo $this->Html->url('/');?>" class="link-logo">
    <img src="<?php echo $this->Html->url('/img/ibop_logo.svg');?>" class="menu_logo img-responsive" alt="ibop_logo">
                </a>
        <ul class="nav navbar-nav menu-top navbar-right">
           

            <li class="mainmenu-item ">
                <a href="<?php echo $this->Html->url('/');?>">
                    <span class="fa fa-home"></span>  <?php echo __('Home', true);?>
                </a>
            </li> 
            
            <li class="mainmenu-item ">
                <a href="#" class="menu-display" display-target="#search-panel-fighter">
                    <span class="fa fa-gloves"></span>  <?php echo __('Fighters', true);?>
                </a>
            </li>
            
             <li class="mainmenu-item">
                <a href="#" class="menu-display" display-target="#search-panel-fights">
                    <span class="fa fa-boxing-ring" ></span>  <?php echo __('Fights', true);?> 
                </a>
            </li>
            
             <li class="mainmenu-item ">
                <a href="#" class="menu-display" display-target="#search-panel-events">
                    <span class="fa fa-champ-cup"></span>  <?php echo __('Events', true);?> 
                </a>
            </li>
            
            <li class="mainmenu-item ">
                 
                 <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index'));?>">
                    <span class="fa fa-book"></span>  <?php echo __('Articles', true);?> 
                </a>
            </li>

            <li class="mainmenu-item ">
                <a href="<?php echo $this->Html->url(array('controller' => 'contacts')) ?>">
                    <span class="fa fa-envelope"></span>  <?php echo __('Contact', true);?>
                </a>
            </li>
        </ul>
    </div>
</div>


<script>
$(document).ready(function() {
  $('#simple-menu').sidr({
      displace: false
  });
});
</script>
