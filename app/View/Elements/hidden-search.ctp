<?php 
    echo $this->Html->css('datepicker-bootstrap');
    echo $this->Html->script('bootstrap-datepicker');
?>
<div class="row">
    
    <div class="search-div collapse" id="search-panel-fighter">
        <div class="search-div-mobile" >
            <h4><?php echo __('Search for persons', true);?></h4>
            
            <form action="<?php echo $this->Html->url(array('controller' => 'Persons', 'action' => 'search'));?>" role="form" id="person-search" method="post" accept-charset="utf-8">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Name" class="form-control" id="keyword-person" name="name">
                            <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Last Name" class="form-control" id="keyword-person" name="last-name">
                            <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified" id="persons-button" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
        </div>
    </div>
                
    <div class="search-div collapse" id="search-panel-fights">
        
        <div class="search-div-mobile" >
            <h4><?php echo __('Search for fights', true);?></h4>
            <form action="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'search'));?>" role="form" id="fight-search" method="post">  
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Keyword" class="form-control" id="Keyword-fight" name="Keyword-fight">
                            <label for="date" class="glyphicon glyphicon-search" rel="tooltip" title="date"></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
        </div>
    </div>
                
    <div class="search-div collapse" id="search-panel-events">

        <div class="search-div-mobile" >
            <h4><?php echo __('Search for events', true);?></h4>
            <form action="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'search'));?>" role="form" id="event-search" method="post">  
                <div class="col-md-8">

                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="search by date" class="form-control date-search" id="date" name="date-event">
                            <label for="date" class="glyphicon glyphicon-search" rel="tooltip" title="date"></label>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.date-search').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>
