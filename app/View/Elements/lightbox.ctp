<div class="col-lg-2 col-md-4 col-sm-5">
    <div id="search">
        <button type="button" class="close">&times;</button>
        <form>
            <div class="container " >
                <div class="search-lightbox">
                    <h2 style="color: #bbb"><?php echo __('Search for persons, date, venue or event', true);?></h2>
                    <div class="col-lg-1"></div> 
                    
                    <div class="col-lg-5">
                        <div class="form-group">
                            <div class="icon-addon addon-lg">
                                <input type="text" placeholder="Person" class="form-control" id="name">
                                <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="icon-addon addon-lg">
                                <input type="date" placeholder="Date mm/dd/yyyy" class="form-control" id="date">
                                <label for="date" class="glyphicon glyphicon-search" rel="tooltip" title="date"></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        <div class="form-group">
                            <div class="icon-addon addon-lg">
                                <input type="text" placeholder="Venue" class="form-control" id="venue">
                                <label for="venue" class="glyphicon glyphicon-search" rel="tooltip" title="venue"></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="icon-addon addon-lg">
                                <input type="text" placeholder="Event" class="form-control" id="event">
                                <label for="venue" class="glyphicon glyphicon-search" rel="tooltip" title="venue"></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-1"></div>

                </div>
            
                <div class="search-submit text-center">
                    <a class="h3" href="#search"><span class="fa fa-search "></span> <?php echo __('Search', true);?></a>
                </div>    
            </div>
        </form>
    </div>
</div>
            
           