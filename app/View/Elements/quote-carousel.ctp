<div class="container">
  <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#quote-carousel" data-slide-to="1"></li>
          <li data-target="#quote-carousel" data-slide-to="2"></li>
          <li data-target="#quote-carousel" data-slide-to="3"></li>
          <li data-target="#quote-carousel" data-slide-to="4"></li>
        </ol>
        
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
        
          <!-- Quote 1 -->
          <?php $firstItem = true; ?>
          <?php //debug($randomQuotes);?>
          <?php foreach ($randomQuotes as $randomQuote){ ?> 
          <div class="item <?php echo $firstItem ? 'active' : ''; ?>" >
            <blockquote>
              <div class="row">
                <div class="col-sm-3 text-center">
                    <?php if( ($randomQuote['Quote']['img'] != null) ){ ?>
                        <img class="img-circle" src="<?php echo $this->Html->url('/files/quotes/' . $randomQuote['Quote']['img']); ?>" style="width: 100px;height:100px;">
                    <?php } else if( (isset($randomQuote['Identity']['Images'])) && ($randomQuote['Identity']['Images'] != null) ){ ?>
                        <img class="img-circle" src="<?php echo $this->Html->url('/files/img/' . $randomQuote['Identity']['Images']); ?>" style="width: 100px;height:100px;">
                    <?php } else { ?>
                        <img class="img-circle" src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg'); ?>" style="width: 100px;height:100px;">
                    <?php } ?>
                </div>
                <div class="col-sm-9">
                    
                  <p><i class="fa fa-quote-left greyedout"></i> <?php echo $randomQuote['Quote']['quote'] ?> <i class="fa fa-quote-right greyedout"></i></p>
                  <small>
                      <?php 
                        if( ($randomQuote['Identity']['name'] != null) ){ 
                                echo $randomQuote['Identity']['name'] . " " .$randomQuote['Identity']['last_name'] ;
                        }else{
                              echo  $randomQuote['Quote']['quotes_by'];
                        }
                        if($randomQuote['Quote']['note'] != null){
                            echo $randomQuote['Quote']['note'];
                        }
                      ?> 
                  </small>
                </div>
              </div>
            </blockquote>
          </div>
          <?php $firstItem = false;?>
          <?php } ?>
        </div>
        
        <!-- Carousel Buttons Next/Prev -->
        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
      </div>                          
    </div>
  </div>
</div>