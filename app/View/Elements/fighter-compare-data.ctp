<center>
    <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fighterData['Identity']['id'], $fighterData['Identity']['name'].' '.$fighterData['Identity']['last_name']))?> ">
        <h1><?php echo $fighterData['Identity']['name'] ?> <?php echo $fighterData['Identity']['last_name'] ?></h1>
    </a>
    

    <?php
        if ((isset($fighterPhoto['Images']['url'])) && ($fighterPhoto['Images']['url'] != null)) {
            $imageShare = $this->Html->url('/files/img/' . $fighterPhoto['Images']['url']);
        } else {
            $imageShare = $this->Html->url('/img/defaults/generic_fighter.jpg');
        }
    ?>
    <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fighterData['Identity']['id'], $fighterData['Identity']['name'].' '.$fighterData['Identity']['last_name']))?> ">
        <img src="<?php echo  $imageShare?>" class="img-responsive" style="height: 250px" > 
    </a>
</center>

<hr>
<div style="height: 150px;">
    <table class="responsive responsive-table table_basic_generaldetails" >
        <tbody>
            <tr>
                <td class="hidden-1025"><?php echo __('Name:', true); ?></td>
                <td data-content="<?php echo __('Name:', true); ?>">
                    <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fighterData['Identity']['id'], $fighterData['Identity']['name'].' '.$fighterData['Identity']['last_name']))?> ">
                        <strong><?php echo $fighterData['Identity']['name'] ?> <?php echo $fighterData['Identity']['last_name'] ?></strong>
                    </a>
                </td>
            </tr>
            <?php if( $fighterData['Identity']['height'] != null) { ?>
                <tr>
                    <td class="hidden-1025"><?php echo __('Height:', true); ?></td>
                    <td data-content="<?php echo __('Height:', true); ?>">
                        <strong><?php echo $fighterData['Identity']['height'] ?></strong>
                    </td>
                </tr>
            <?php } ?>
            <?php if( $fighterData['Identity']['reach'] != null) { ?>
                <tr>
                    <td class="hidden-1025"><?php echo __('Reach:', true); ?></td>
                    <td data-content="<?php echo __('Reach:', true); ?>">
                        <strong><?php echo $fighterData['Identity']['reach'] ?></strong>
                    </td>
                </tr>
            <?php } ?>
            <?php if( $fighterData['Identity']['stance'] != null) { ?>
                <tr>
                    <td class="hidden-1025"><?php echo __('Stance:', true); ?></td>
                    <td data-content="<?php echo __('Stance:', true); ?>">
                        <strong><?php echo $fighterData['Identity']['stance'] ?></strong>
                    </td>
                </tr>
            <?php } ?>
            <?php if( $fighterData['Identity']['gender'] != 'D') { ?>
                <tr>
                    <td class="hidden-1025"><?php echo __('Gender:', true); ?></td>
                    <td data-content="<?php echo __('Gender:', true); ?>">
                        <strong><?php echo $fighterData['Identity']['gender'] == 'F' ? 'Female' : 'Male' ?></strong>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<hr>

<div class="fighters-details">
    <ul id="myTab" class="nav nav-tabs responsive">
        <?php if($fighterRecord['boxing']['is']>0){?>
            <li class="active">
                <a href="#boxing-panel-<?php echo $fighterData['Identity']['id'] ?>" class="h4" data-toggle="pill"><?php echo __('Boxing Record', true); ?></a>
            </li>
        <?php } ?>
            
        <?php if($fighterRecord['mma']['is']>0 && $fighterRecord['boxing']['is'] > 0 ){?>
            <li class="">
                <a href="#mma-panel-<?php echo $fighterData['Identity']['id'] ?>" class="h4" data-toggle="pill"><?php echo __('MMA Record', true); ?></a>
            </li>
        <?php } ?>
            
        <?php if ($fighterRecord['mma']['is']>0 && $fighterRecord['boxing']['is'] == 0  )  { ?>
            <li class="active">
                <a href="#mma-panel-<?php echo $fighterData['Identity']['id'] ?>" class="h4" data-toggle="pill"><?php echo __('MMA Record', true); ?></a>
            </li>
        <?php  }?>
    </ul>
    <div id="myTabContent" class="tab-content">
        <?php if($fighterRecord['boxing']['is']>0){?>
            <div class="tab-pane fade in active" id="boxing-panel-<?php echo $fighterData['Identity']['id'] ?>">
            
                <table class="table table-condensed">               
                    <tbody>
                        <tr>
                            <td><span class="won"><?php echo __('Won', true); ?>: </span></td>
                            <td>
                                <strong>
                                    <?php
                                    echo $fighterRecord['boxing']['won'];
                                    if (($fighterRecord['boxing']['win_ko'] > 0) || ($fighterRecord['boxing']['won_nws'] > 0)) {
                                        echo ' (';
                                        if ($fighterRecord['boxing']['win_ko'] > 0) {
                                            echo __('KO', true) . ' ' . $fighterRecord['boxing']['win_ko'];
                                            if ($fighterRecord['boxing']['won_nws'] > 0) {
                                                echo ', ';
                                            }
                                        }
                                        if ($fighterRecord['boxing']['won_nws'] > 0) {
                                            echo __('NWS') . ' ' . $fighterRecord['boxing']['won_nws'];
                                        }
                                        echo ')';
                                    }
                                    ?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="lost"><?php echo __('Lost', true); ?>: </span>
                            <td>
                                <strong>
                                    <?php
                                    echo $fighterRecord['boxing']['lost'];
                                    if (($fighterRecord['boxing']['lost_ko'] > 0) || ($fighterRecord['boxing']['lost_nws'] > 0)) {
                                        echo ' (';
                                        if ($fighterRecord['boxing']['lost_ko'] > 0) {
                                            echo __('KO', true) . ' ' . $fighterRecord['boxing']['lost_ko'];
                                            if ($fighterRecord['boxing']['lost_nws'] > 0) {
                                                echo ', ';
                                            }
                                        }
                                        if ($fighterRecord['boxing']['lost_nws'] > 0) {
                                            echo __('NWS') . ' ' . $fighterRecord['boxing']['lost_nws'];
                                        }
                                        echo ')';
                                    }
                                    ?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="draw"><?php echo __('Draw', true); ?>:</span></td>
                            <td>
                                <strong>
                                    <?php
                                    echo $fighterRecord['boxing']['draw'];
                                    if ($fighterRecord['boxing']['draw_nws'] > 0) {
                                        echo '(' . __('D - NWS') . ' ' . $fighterRecord['boxing']['draw_nws'] . ')';
                                    }
                                    ?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="won"><?php echo __('Box rounds fought', true); ?>: </span></td>
                            <td><strong><?php echo $fighterRecord['boxing']['rounds'] ?></strong></td>
                        </tr>
                        <tr>
                            <td><span class="won"><?php echo __('Box KO pct', true); ?>:</span></td>
                            <td>
                                <strong>
                                    <?php echo round(($fighterRecord['boxing']['win_perc'] / $fighterRecord['boxing']['total']) * 100, 2) . '%'; ?>
                                </strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            <?php if ($fighterRecord['mma']['is']>0) {?>
                <div class="tab-pane fade" id="mma-panel-<?php echo $fighterData['Identity']['id'] ?>">
                    <table class="table table-condensed">

                        <tbody>
                            <tr>
                                <td><span class="won"><?php echo __('Won', true); ?>: </span></td>
                                <td>
                                    <strong>
                                        <?php
                                        echo $fighterRecord['mma']['won'];
                                        if ($fighterRecord['mma']['win_ko'] > 0) {
                                            echo '(' . __('KO', true) . ' ' . $fighterRecord['mma']['win_ko'] . ')';
                                        }
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="lost"><?php echo __('Lost', true); ?>: </span>
                                <td>
                                    <strong>
                                        <?php
                                        echo $fighterRecord['mma']['lost'];
                                        if ($fighterRecord['mma']['lost_ko'] > 0) {
                                            echo '(' . __('KO', true) . ' ' . $fighterRecord['mma']['lost_ko'] . ')';
                                        }
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="draw"><?php echo __('Draw', true); ?>: </span></td>
                                <td>
                                    <strong><?php echo $fighterRecord['mma']['draw']; ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="won"><?php echo __('MMA rounds fought', true); ?>:</span></td>
                                <td>
                                    <strong><?php echo $fighterRecord['mma']['rounds']; ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="won"><?php echo __('MMA KO pct', true); ?>:</span></td>
                                <td>
                                    <strong>
                                        <?php
                                        echo round(($fighterRecord['mma']['win_ko'] / $fighterRecord['mma']['is']) * 100, 2) . '%';
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                
                </div>
            <?php } ?>
        <?php } ?>
        <?php if ($fighterRecord['mma']['is']>0 && $fighterRecord['boxing']['is'] == 0  )  { ?>
            <div class="tab-pane fade in active" id="mma-panel-<?php echo $fighterData['Identity']['id'] ?>">
                <table class="table table-condensed">

                        <tbody>
                            <tr>
                                <td><span class="won"><?php echo __('Won', true); ?>: </span></td>
                                <td>
                                    <strong>
                                        <?php
                                        echo $fighterRecord['mma']['won'];
                                        if ($fighterRecord['mma']['win_ko'] > 0) {
                                            echo '(' . __('KO', true) . ' ' . $fighterRecord['mma']['win_ko'] . ')';
                                        }
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="lost"><?php echo __('Lost', true); ?>: </span>
                                <td>
                                    <strong>
                                        <?php
                                        echo $fighterRecord['mma']['lost'];
                                        if ($fighterRecord['mma']['lost_ko'] > 0) {
                                            echo '(' . __('KO', true) . ' ' . $fighterRecord['mma']['lost_ko'] . ')';
                                        }
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="draw"><?php echo __('Draw', true); ?>: </span></td>
                                <td>
                                    <strong><?php echo $fighterRecord['mma']['draw']; ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="won"><?php echo __('MMA rounds fought', true); ?>:</span></td>
                                <td>
                                    <strong><?php echo $fighterRecord['mma']['rounds']; ?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="won"><?php echo __('MMA KO pct', true); ?>:</span></td>
                                <td>
                                    <strong>
                                        <?php
                                        echo round(($fighterRecord['mma']['win_ko'] / $fighterRecord['mma']['is']) * 100, 2) . '%';
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            
            </div>
         <?php } ?>
        
        
        
    </div>
</div>