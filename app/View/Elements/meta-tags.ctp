<?php
    if(isset($pageTitle) ){
        echo '<meta name="title" content="' . $pageTitle . '">' . "\n";
    }
    if(isset($pageDescription) ){
        echo '<meta name="description" content="' . $pageDescription . '">' . "\n";
    }
    
    /***** facebook and twitter detail person  ******/
    if($thisController == 'Persons' && $thisAction == 'index'){
        if(isset($pageTitle) ){
            echo '<meta property="og:title" content="' . $pageTitle . '">' . "\n";
        }
        if($pageImage != 0){
            echo '<meta property="og:image" content="' . $this->Html->url('/files/img/' . $pageImage) . '">' . "\n";
        } else {
            echo '<meta property="og:image" content="' . $this->Html->url('/img/defaults/generic_fighter.jpg') . '">' . "\n";
        }
        if(isset($pageDescription) ){
            echo '<meta property="og:description" content="' . $pageDescription . '">' . "\n";
        }
        
        echo '<meta name="twitter:card" content="summary">' . "\n";
        echo '<meta name="twitter:site" content="@ibopfactual">' . "\n";
        if($pageImage != 0){
            echo '<meta name="twitter:image" content="' . $this->Html->url('/files/img/' . $pageImage) . '">' . "\n";
        } else {
            echo '<meta name="twitter:image" content="' . $this->Html->url('/img/defaults/generic_fighter.jpg') . '">' . "\n";
        }
        if(isset($pageDescription) ){
            echo '<meta name="twitter:description" content="' . $pageDescription . '">' . "\n";
        }
        
    }