<script type="text/javascript">
	$(document).ready(function() {

            if ($('.input-identity').length){
                $('.input-identity').autocomplete({
                    minLength: 3,
                    source: function(request, response){
                        var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity'));?>/" + Base64.encode(request.term);
                        $.getJSON(loadIdentities, function(data){
                            response(data);
                        });
                    },
                    focus: function( event, ui ) {
                        $('#' + $(this).attr('id')).val(ui.item.label);
                        return false;
                    },
                    select: function( event, ui ) {
                        $('#' + $(this).attr('id')).val(ui.item.label);
                        $($(this).attr('add-to')).val(ui.item.id);
                        return false;
                    }
                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                        return $( "<li>" )
                        .append( "<a>" + item.label + "</a>" )
                        .appendTo( ul );
                };
            }
            
            $('.btn-delete').click(function(event){
                event.preventDefault();
                var url = $(this).attr('href');
                BootstrapDialog.show({
                    title: '<?php echo __('Alert!');?>',
                    message: '<center><h3><?php echo __('Are you sure you want to delete the item?');?></h3><h5><?php echo __('In the process you can delete related items');?></h5></center>',
                    buttons: [
                        {
                            label: '<?php echo __('Cancel');?>',
                            action: function(dialog){
                                dialog.close();
                            }
                        },
                        {
                            label: '<?php echo __('Delete');?>',
                            icon: 'fa fa-trash',
                            cssClass: 'btn btn-danger',
                            action: function(dialog){
                                dialog.close();
                                window.location.href = url;
                            }
                        }
                    ]
                });
            });

	});
</script>