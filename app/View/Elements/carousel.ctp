
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        
        <div class="carousel-inner">
            
            
            <?php $firstItem = true; ?>
            <?php foreach ($articles as $article) { ?>
            
               
                    <div class="item<?php echo $firstItem ? ' active' : ''; ?>">
                        
                        <div class="carousel-bg hidden-md hidden-sm hidden-xs">
                            <?php if($article['Article']['img'] != null) { ?>
                                <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380);?>" class="img-responsive" style="width: 100%">
                            <?php } else { ?>
                                <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg')?>" class="img-responsive" style="width: 100%">
                            <?php } ?>
                        </div>
                        
                        <div class="container">
                            <?php if($article['Article']['img'] != null) { ?>
                                <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380);?>" class="img-responsive ">
                            <?php } else { ?>
                                <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg')?>" class="img-responsive" >
                            <?php } ?>
                        
                        </div>
                            <div class="carousel-caption container">
                                <h1>
                                    <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Category']['name'], $article['Article']['id'], $article['Article']['title']));?>"><?php echo $article['Article']['title']?></a>
                                </h1>
                                <p><?php echo substr(strip_tags($article['Article']['contend']), 0, 250);?>...</p>
                            </div>
                        
                    </div><!-- End Item -->
                
                <?php $firstItem = false;?>
            <?php } ?>
            <!-- End Item -->
        </div>
             
        <!-- End Carousel Inner -->
        <div class="carousel-nav home-carousel">
            <ul class="nav nav-pills nav-justified container ">
                <?php $firstItem = true; ?>
                <?php $dataSlide = 0; ?>
                <?php foreach ($articles as $article) { ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $dataSlide?>" class="<?php echo $firstItem ? 'active' : ''; ?>">
                        <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Article']['id'], $article['Article']['title']));?>">
                            <?php echo $article['Article']['title']?>
                        </a>
                    </li><hr class="border-bottom hidden-lg hidden-md hidden-sm">
                    <?php $firstItem = false;?>
                    <?php $dataSlide++; ?>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!-- End Carousel -->
