<div id="socialHolder" class="">
    <div id="socialShare" class="btn-group share-group">
        <a data-toggle="dropdown" class="btn btn-info">
            <i class="fa fa-share-alt"></i>
        </a>
        <button href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle share">
                <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-doni">
            <li>
                <a data-original-title="Twitter" rel="tooltip"  href="#" class="btn btn-twitter btn-share" data-placement="left">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li>
                <a data-original-title="Facebook" rel="tooltip"  href="#" class="btn btn-facebook btn-share" data-placement="left">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>					
            <li> 
                <a data-original-title="Google+" rel="tooltip"  href="#" class="btn btn-google btn-share" data-placement="left">
                    <i class="fa fa-google-plus"></i>
                </a>
            </li>
            <li>
                <a data-original-title="LinkedIn" rel="tooltip"  href="#" class="btn btn-linkedin btn-share" data-placement="left">
                    <i class="fa fa-linkedin"></i>
                </a>
            </li>
            <li>
                <a data-original-title="Pinterest" rel="tooltip" href="#" class="btn btn-pinterest btn-share" data-placement="left">
                    <i class="fa fa-pinterest"></i>
                </a>
            </li>
            <!--<li>
                <a  data-original-title="Email" rel="tooltip" href="#" class="btn btn-mail" data-placement="left" data-toggle="modal" data-target="#share-email">
                    <i class="fa fa-envelope"></i>
                </a>
            </li>-->
        </ul>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="share-email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-share').click(function(){
            var typeShare   = $(this).attr('data-original-title');
            var urlShare    = "";
            var windowSize  = "";
            var shareTitle  = "<?php echo $shareTitle; ?>";
            var x           = screen.width/2 - 700/2;
            var y           = screen.height/2 - 450/2;
            
            if(typeShare == 'Twitter'){
                urlShare    = "https://twitter.com/intent/tweet?text=" + shareTitle + "&via=ibopfactual&url=" + this.href + "&original_referer=" + this.href;
                windowSize  = "width=800, height=300, top=" + y + ", left=" + x + ", directories=no";
            } else if(typeShare == 'Facebook'){
                urlShare    = "https://www.facebook.com/sharer/sharer.php?";
                urlShare    += "u=" + this.href;
                windowSize  = "width=800, height=300, top=" + y + ", left=" + x + ", directories=no";
            } else if(typeShare == 'Google+'){
                urlShare    = "https://plus.google.com/u/0/share?url=";
                urlShare    += this.href;
                windowSize  = "width=500, height=500, top=" + y + ", left=" + x + ", directories=no";
            } else if(typeShare == 'LinkedIn'){
                urlShare    = "https://www.linkedin.com/shareArticle?url=";
                urlShare    += this.href;
                windowSize  = "width=500, height=500, top=" + y + ", left=" + x + ", directories=no";
            } else if(typeShare == 'Pinterest'){
                urlShare    = "http://www.pinterest.com/pin/create/button/?url=" + this.href;
                windowSize  = "width=500, height=500, top=" + y + ", left=" + x + ", directories=no";
            }
            window.open(urlShare,  "_blank", windowSize);
        });
    });
</script>
