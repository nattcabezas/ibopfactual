<div class="container">
    
    <!-- Start Mobile Burguer Menu-->
    <a href="<?php echo $this->Html->url('/');?>" id="logo-top-mobile" class="link-logo-mobile">
                <img src="<?php echo $this->Html->url('/img/ibop_logo.svg');?>" class="menu_logo img-responsive" alt="ibop_logo">
            </a>
    
        <button type="button" class="navbar-toggle" id="burger" >
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
        </button>
    
    <div id="doni-side-menu" class="">
        <div class="menu_top_bar" id="collapse">
           
            <ul class="nav navbar-nav menu-top navbar-right" id="principal-menu">
                <li class="mainmenu-item-mobile ">
                    <a href="<?php echo $this->Html->url('/');?>">
                        <span class="fa fa-home fa-2x"></span> <br> <?php echo __('Home', true);?>
                    </a>
                </li> 

                <li class="mainmenu-item-mobile ">
                    <a href="#" class="menu-display" display-target="#search-panel-fighter">
                        <span class="fa fa-gloves fa-2x"></span> <br> <?php echo __('Fighters', true);?>
                    </a>
                </li>

                 <li class="mainmenu-item-mobile">
                    <a href="#" class="menu-display" display-target="#search-panel-fights">
                        <span class="fa fa-boxing-ring fa-2x"></span> <br> <?php echo __('Fights', true);?> 
                    </a>
                </li>

                 <li class="mainmenu-item-mobile ">
                    <a href="#" class="menu-display" display-target="#search-panel-events">
                        <span class="fa fa-champ-cup fa-2x"></span> <br> <?php echo __('Events', true);?> 
                    </a>
                </li>

                <li class="mainmenu-item-mobile ">

                     <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index'));?>">
                        <span class="fa fa-book fa-2x"></span> <br> <?php echo __('Articles', true);?> 
                    </a>
                </li>

                <li class="mainmenu-item-mobile ">
                    <a href="<?php echo $this->Html->url(array('controller' => 'contacts')) ?>">
                        <span class="fa fa-envelope fa-2x"></span> <br>  <?php echo __('Contact', true);?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    
   <!--End Mobile Burguer Menu-->
    
    <div class="collapse navbar-collapse menu_top_bar" id="">
            <a href="<?php echo $this->Html->url('/');?>" class="link-logo">
                <img src="<?php echo $this->Html->url('/img/ibop_logo.svg');?>" class="menu_logo img-responsive" alt="ibop_logo">
            </a>
            <ul class="nav navbar-nav menu-top navbar-right" id="principal-menu">
                <li class="mainmenu-item ">
                    <a href="<?php echo $this->Html->url('/');?>">
                        <span class="fa fa-home"></span>  <?php echo __('Home', true);?>
                    </a>
                </li> 

                <li class="mainmenu-item ">
                    <a href="#" class="menu-display" display-target="#search-panel-fighter">
                        <span class="fa fa-gloves"></span>  <?php echo __('Fighters', true);?>
                    </a>
                </li>

                 <li class="mainmenu-item">
                    <a href="#" class="menu-display" display-target="#search-panel-fights">
                        <span class="fa fa-boxing-ring" ></span>  <?php echo __('Fights', true);?> 
                    </a>
                </li>

                 <li class="mainmenu-item ">
                    <a href="#" class="menu-display" display-target="#search-panel-events">
                        <span class="fa fa-champ-cup"></span>  <?php echo __('Events', true);?> 
                    </a>
                </li>

                <li class="mainmenu-item ">

                     <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index'));?>">
                        <span class="fa fa-book"></span>  <?php echo __('Articles', true);?> 
                    </a>
                </li>

                <li class="mainmenu-item ">
                    <a href="<?php echo $this->Html->url(array('controller' => 'contacts')) ?>">
                        <span class="fa fa-envelope"></span>  <?php echo __('Contact', true);?>
                    </a>
                </li>
                <?php if((!isset($userPage)) || ($userPage == null)){ ?>
                    <li class="mainmenu-item ">
                        <a href="#formLogin" class="login-form" data-toggle="modal" data-target="#formLogin">
                            <span class="fa fa-user"></span>  <?php echo __('Login', true);?>
                        </a>
                    </li>
                <?php } else {?>
                    <li class="mainmenu-item dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            <span class="fa fa-user"></span>  <?php echo $userPage['PageUser']['username']?>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="">
                                    <i class="fa fa-user"></i>
                                    <?php echo __('Profile', true);?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->Html->url(array('controller' => 'Register', 'action' => 'logout'));?>">
                                    <i class="fa fa-power-off"></i>
                                    <?php echo __('Log Out', true)?>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
    </div>
</div>

<div class="modal fade" id="formLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo __('Close', true);?></span></button>
                <h2 class="modal-title" id="myModalLabel"><?php echo __('Please Sign In', true);?></h2>
            </div>
            <div class="modal-body" id="title-content">
                <?php echo $this->Form->create('PageUser', array('url' => array('controller' => 'Register', 'action' => 'login')))?>
                    <fieldset>
                        <div class="form-group input-group">
                            <span class="input-group-addon">@</span>
                            <?php echo $this->Form->input('email', array('class' => 'form-control input-lg', 'div' => false, 'label' => false, 'placeholder' => 'Email Address')); ?>
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-unlock-alt"></i>
                            </span>
                            <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control input-lg', 'div' => false, 'label' => false, 'placeholder' => 'Password')); ?>
                        </div>

                        <span class="button-checkbox">
                            <button type="button" class="btn" data-color="info"><?php echo __('Remember Me', true);?></button>
                            <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
                            <a href="<?php echo $this->Html->url(array('controller' => 'Register', 'action' => 'forgotPassword'));?>" class="btn btn-link pull-right"><?php echo __('Forgot Password?', true);?></a>
                        </span>

                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign In">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <a href="<?php echo $this->Html->url(array('controller' => 'Register', 'action' => 'index'));?>" class="btn btn-lg btn-primary btn-block"><?php echo __('Register', true);?></a>
                            </div>
                        </div>
                    </fieldset>
                <?php echo $this->Form->end();  ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('.button-checkbox').each(function(){
                    var $widget = $(this),
                            $button = $widget.find('button'),
                            $checkbox = $widget.find('input:checkbox'),
                            color = $button.data('color'),
                            settings = {
                                            on: {
                                                    icon: 'glyphicon glyphicon-check'
                                            },
                                            off: {
                                                    icon: 'glyphicon glyphicon-unchecked'
                                            }
                            };

                    $button.on('click', function () {
                            $checkbox.prop('checked', !$checkbox.is(':checked'));
                            $checkbox.triggerHandler('change');
                            updateDisplay();
                    });

                    $checkbox.on('change', function () {
                            updateDisplay();
                    });

                    function updateDisplay() {
                            var isChecked = $checkbox.is(':checked');
                            // Set the button's state
                            $button.data('state', (isChecked) ? "on" : "off");

                            // Set the button's icon
                            $button.find('.state-icon')
                                    .removeClass()
                                    .addClass('state-icon ' + settings[$button.data('state')].icon);

                            // Update the button's color
                            if (isChecked) {
                                    $button
                                            .removeClass('btn-default')
                                            .addClass('btn-' + color + ' active');
                            }
                            else
                            {
                                    $button
                                            .removeClass('btn-' + color + ' active')
                                            .addClass('btn-default');
                            }
                    }
                    function init() {
                            updateDisplay();
                            // Inject the icon if applicable
                            if ($button.find('.state-icon').length == 0) {
                                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                            }
                    }
                    init();
            });
    });
</script>