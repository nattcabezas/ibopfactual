<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Backup Data Base', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-database"></i></i> <?php echo __('Backup Data Base ', true)?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-primary btn-group-justified" href="<?php echo $this->Html->url(array('controller' => 'User', 'action' => 'mysqlDump'))?>"><i class="fa fa-download"></i> <?php echo __('Backup all Data Base', true);?></a><br>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php //debug($tables);?>
        <?php foreach ($tables as $table){ ?>
            <div class="col-lg-4">
                <a class="btn btn-primary btn-group-justified" href="<?php echo $this->Html->url(array('controller' => 'User', 'action' => 'mysqlDump/' . $table['TABLE_NAMES']['Tables_in_ibopfact_beta']))?>">
                    <i class="fa fa-download"></i> <?php echo __('Backup', true) . ' <strong>' . $table['TABLE_NAMES']['Tables_in_ibopfact_beta'] . '</strong> ' . __('Table');?>
                </a><br>
            </div>
        <?php } ?>
    </div>
</div>