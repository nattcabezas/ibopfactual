<style>
    .placeholder{
        height: 42px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Site Featured', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-star"></i></i> <?php echo __('Site Featured', true)?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#"><?php echo __('Featured Persons', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'featuredFights'));?>"><?php echo __('Featured Fights', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'featuredEvents'));?>"><?php echo __('Featured Events', true); ?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'featuredVenues'));?>"><?php echo __('Featured Venues', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-star"></i> <?php echo __('Add Featured Persons', true);?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('FeaturedIdentity', array('class' => 'form-horizontal')); ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Person');?></label>
                        <div class="col-sm-8">
                            <?php echo $this->Form->input('identities_label', array('label' => false, 'div' => false, 'class' => 'form-control input-identity', 'add-to' => '#FeaturedIdentityIdentitiesId'));?>
                            <?php echo $this->Form->input('identities_id', array('type' => 'hidden'));?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $this->Form->button(__('Add Featured'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?php echo __('List of Featured Persons', true);?></h3>
        <ul id="sortable" class="list-group">
            <?php foreach ($identities as $identity){ ?>
                <li class="list-group-item" id="<?php echo $identity['FeaturedIdentity']['id']?>">
                    <i class="fa fa-bars"></i> 
                    <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($identity['Identities']['id'])))?>" target="_blank">
                        <?php echo $identity['Identities']['name']?> <?php echo $identity['Identities']['last_name']?>
                    </a>
                    <a href="<?php echo $this->Html->url(array('action' => 'deltePerson/' . base64_encode($identity['FeaturedIdentity']['id']))); ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash-o"></i></a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $( "#sortable" ).sortable({
            placeholder: "list-group-item list-group-item-warning placeholder",
            update: function(event, ui){
                $('#sortable li').each( function(e) {
                    var updateOrder = "<?php echo $this->Html->url(array('action' => 'personsOrder')); ?>";
                    $.post(updateOrder, {id: $(this).attr('id'), newOrder: $(this).index() + 1});
                });
            }
        });
        $( "#sortable" ).disableSelection();
    });
</script>