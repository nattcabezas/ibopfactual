<style>
    .placeholder{
        height: 42px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Site Featured', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-star"></i></i> <?php echo __('Site Featured', true)?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?php echo $this->Html->url(array('action' => 'featuredPersons'));?>"><?php echo __('Featured Persons', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'featuredFights'));?>"><?php echo __('Featured Fights', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'featuredEvents'));?>"><?php echo __('Featured Events', true); ?></a></li>
            <li class="active"><a href="#"><?php echo __('Featured Venues', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-star"></i> <?php echo __('Add Featured Venue', true);?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('FeaturedVenue', array('class' => 'form-horizontal')); ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Venues');?></label>
                        <div class="col-sm-8">
                            <?php echo $this->Form->input('venues_label', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
                            <?php echo $this->Form->input('venues_id', array('type' => 'hidden'));?>
                        </div>
                        <div class="col-sm-2">
                            <?php echo $this->Form->button(__('Add Featured'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?php echo __('List of Featured Venues', true);?></h3>
        <ul id="sortable" class="list-group">
            <?php foreach ($venues as $venue){?>
                <li class="list-group-item" id="<?php echo $venue['FeaturedVenue']['id']?>">
                    <i class="fa fa-bars"></i>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'edit/' . base64_encode($venue['Venues']['id'])))?>" target="_blank">
                        <?php echo $venue['Venues']['name']?>
                    </a>
                    <a href="<?php echo $this->Html->url(array('action' => 'deleteVenue/' . base64_encode($venue['FeaturedVenue']['id']))); ?>" class="btn btn-danger btn-xs pull-right">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#FeaturedVenueVenuesLabel').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'getVenue'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('#FeaturedVenueVenuesLabel').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                $('#FeaturedVenueVenuesLabel').val(ui.item.label);
                $('#FeaturedVenueVenuesId').val(ui.item.id);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $( "#sortable" ).sortable({
            placeholder: "list-group-item list-group-item-warning placeholder",
            update: function(event, ui){
                $('#sortable li').each( function(e) {
                    var updateOrder = "<?php echo $this->Html->url(array('action' => 'venueOrder')); ?>";
                    $.post(updateOrder, {id: $(this).attr('id'), newOrder: $(this).index() + 1});
                });
            }
        });
        $( "#sortable" ).disableSelection();
        
    });
</script>