<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Assign venue to event' , true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-building"></i> <?php echo __('Venues');?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Assign to event', true)?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Venue', true); ?></th>
                        <th><?php echo __('Location', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($venues as $venue) { ?>
                        <tr>
                            <td>
                                <?php /*debug($venue);*/?>
                                <a href=<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'ibopadmin_save_location')) . '/' . base64_encode($idEvent) . '/' . $venue['Venue']['id'];?> ><?php echo $venue['Venue']['name']?></a>
                            </td>
                            <td>
                                <?php 
                                    if(isset( $venue['Cities']['name'] ) ){
                                        echo $venue['Cities']['name'] . ', '. $venue['States']['name'] .', '. $venue ['Countries']['name']; 
                                    }elseif(isset( $venue['States']['name'] ) ) {
                                        echo $venue['States']['name'] . ', ' . $venue['Countries']['name']; 
                                    }else{
                                        echo $venue ['Countries']['name'];
                                }?>
                            </td>                            
                        </tr>   
                    <?php } ?>    
                </tbody>
            </table>
        </div>
    </div>
</div>