<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Venues', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-building"></i> <?php echo __('Venues', true)?></li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-3">
        <div class="input-group">
            <input type="text" class="form-control search-identity" placeholder="<?php echo __('Search');?>">
            <span class="input-group-btn"><a class="btn btn-primary"><i class="fa fa-search"></i></a></span>
        </div>
    </div>
    <div class="col-lg-7">
        <?php echo $this->Form->create('Location', array('url' => array('controller' => 'Venues', 'action' => 'search'))); ?>
            <div class="col-lg-3">
                <div class="form-group input-group">
                    <span class="input-group-addon" id="flag-contry" style="height: 34px;">&nbsp;&nbsp;&nbsp;</span>
                    <?php echo $this->Form->input('countries_text', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Country', true))); ?>
                    <?php echo $this->Form->input('countries_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group input-group">
                    <div id="states-contend">
                        <select class="form-control">
                            <option><?php echo __('select state', true);?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                 <div class="form-group input-group">
                     <div id="cities-contend">
                        <select class="form-control">
                            <option><?php echo __('select city', true);?></option>
                        </select>
                     </div>
                </div>
            </div>
            <div class="col-lg-3">
                <?php echo $this->Form->input(__('Search', true), array('type' => 'submit', 'class' => 'btn btn-primary', 'label' => false))?>
            </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <div class="col-lg-2">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary pull-right">
            <?php echo __('add Venue', true);?>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($venues as $venue){ ?>
                        <tr>
                            <td><?php echo $venue['Venue']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Venue Location', true);?>">
                                            <i class="fa fa-globe"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Venue Images', true);?>">
                                            <i class="fa fa-file-image-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Venue Videos', true);?>">
                                            <i class="fa fa-file-video-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Venue Notes', true); ?>">
                                            <i class="fa fa-file-text-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'listEvents/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('list of events')?>">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Edit Venue', true);?>">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function(){        
        $('.search-identity').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'getVenue'));?>/" + request.term;
                console.log("Los datos son="+loadIdentities);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                })
                .fail(function() {
                    console.log( "error" );
                });
            },
            focus: function( event, ui ) {
                $('.search-identity').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                var editIdentity  = "<?php echo $this->Html->url(array('action' => 'edit'))?>/" + Base64.encode(ui.item.id);;
                window.location.replace(editIdentity);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + " - " + item.City + " " + item.State + ", " + item.Country + "</a>" )
            .appendTo( ul );
        };
        
        /** country autocomplete **/
         
        $("#LocationCountriesText").autocomplete({
	      minLength: 0,
	      source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(request.term);
                    $.getJSON(loadIdentities, function(data){
                        response(data);
                    });
                },
	      focus: function( event, ui ) {
	        $("#LocationCountriesText").val(ui.item.label);
	        return false;
	      },
	      select: function( event, ui ) {
	        $("#LocationCountriesText" ).val(ui.item.label);
	        $('#flag-contry').html("<img src='<?php echo $this->html->url('/img/flags')?>/" + ui.item.flag + "'>");
	        $('#LocationCountriesId').val(ui.item.id);
	        $('#cities-contend').html("");
                var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates'));?>";
	        $.post(loadStates, {countries_id: ui.item.id}, function(data){
	        	$('#states-contend').html(data);
	        	$('#LocationStatesId').change(function(){
	        		var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities'));?>";
	        		$.post(loadCities, {states_id: $(this).val()}, function(data){
	        			$('#cities-contend').html(data);
	        		});
	        	});
	        });
	        return false;
	      }
	    })
	    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append( "<a><img src='<?php echo $this->html->url('/img/flags')?>/" + item.flag + "'> " + item.label + "</a>" )
	        .appendTo( ul );
	    };
        
        
    });     
</script>