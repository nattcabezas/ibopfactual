<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Venues', true);?> - <?php echo $venueName; ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-building"></i> 
                    <?php echo __('Venue', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <?php if($user['Role']['manage'] == 1 ){ ?>
               
                <?php $locked = $venue['Venue']['locked'] == 1 ? '0' : '1'; ?>
                <a href="<?php echo $this->Html->url(array('action' => 'lock/' . base64_encode($idVenue) . '/' . $locked)); ?>" class="btn btn-primary tooltip-button">
                    <?php if( $venue['Venue']['locked'] == 1){ ?>
                        <i class="fa fa-lock"></i>
                    <?php }else{ ?>
                        <i class="fa fa-unlock-alt"></i>
                    <?php }?>
                </a>
            <?php } ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('locations'); ?>">
                <i class="fa fa-globe"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'listEvents/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('list of events')?>">
                <i class="fa fa-eye"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>
    
    <div class="col-md-6">
    	<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-building"></i> <?php echo __('Edit Venue', true);?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('Venue'); ?>
					
                    <?php echo $this->Form->input('id');?>
                    <div class="form-group">
                        <label><?php echo __('Source:', true);?></label>
                        <?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(0 => __('select source', true)) , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Name:', true);?></label>
                        <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Attached name:', true);?></label>
                        <?php echo $this->Form->input('complete_name', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
		    </div>
                    <div class="form-group">
                        <label><?php echo __('Description:', true);?></label>
                        <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Capacity:', true);?></label>
                        <?php echo $this->Form->input('capacity', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Owner:', true);?></label>
                        <?php echo $this->Form->input('owner', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Address 1:', true);?></label>
                        <?php echo $this->Form->input('address_1', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Address 2:', true);?></label>
                        <?php echo $this->Form->input('address_2', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Phone:', true);?></label>
                        <?php echo $this->Form->input('phone', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Box office phone:', true);?></label>
                        <?php echo $this->Form->input('box_office_phone', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Comments:', true);?></label>
                        <?php echo $this->Form->input('commente', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea' , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true)); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Map:', true);?></label>
                        <?php echo $this->Form->input('map', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <?php if( $venue['Venue']['locked'] == 0 ){ ?>
                            <?php echo $this->Form->button(__('save Venue'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        <?php } ?>
                    </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-building"></i> <?php echo __('Contact Information', true);?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('VenueContact', array('class' => 'form-horizontal', 'url' => array('controller' => 'Venues', 'action' => 'saveContact')));?>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <?php echo $this->Form->input('venues_id', array('type' => 'hidden', 'value' => $this->request->data('Venue.id'),   'readonly' => $venue['Venue']['locked'] == 0 ? false : true));?>
                            <?php $options = array('Facebook' => 'Facebook', 'Twitter' => 'Twitter', 'Google+' => 'Google+', 'E-mail' => 'E-mail', 'Website' => 'Website');?>
                            <?php echo $this->Form->input('type', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => array('' => 'Select Type') , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true))?>
                        </div>
                        <div class="col-sm-5">
                            <?php echo $this->Form->input('url', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('url', true) , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true ))?>
                        </div>
                        <div class="col-sm-2">
                            <?php if( $venue['Venue']['locked'] == 0 ){ ?>
                                <?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php echo $this->Form->end();?>
                
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Type', true);?></th>
                                <th><?php echo __('Url', true);?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($venueContact as $contact){ ?>
                                <tr>
                                    <td><?php echo $contact['VenueContact']['type']; ?></td>
                                    <td><?php echo $contact['VenueContact']['url']; ?></td>
                                    <td>
                                        <?php echo $this->Form->create('VenueContact', array('url' => array('controller' => 'Venues', 'action' => 'deleteContact')))?>
                                        <?php echo $this->Form->hidden('id', array('value' => $contact['VenueContact']['id']))?>
                                        <?php echo $this->Form->hidden('venues_id', array('value' => $contact['VenueContact']['venues_id']))?>
                                        <?php if( $venue['Venue']['locked'] == 0 ){ ?>
                                            <?php echo $this->Form->button('<i class="fa fa-trash-o"></i>', array('class' => 'btn btn-danger', 'type' => 'submit'))?>
                                        <?php } ?>
                                        <?php echo $this->Form->end(); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
    
</div>