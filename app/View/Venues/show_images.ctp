<section class="row">
    <div class="list-group gallery">
        <?php $count = 1;?>
        <?php foreach ($images as $image){ ?>
            <?php if( (isset($image['Images']['url'])) && ($image['Images']['url'] != null) ){ ?>
                <?php if($count == 1){ ?>
                    <div class="row">
                <?php }?>
                <div class="col-sm-4">
                    <a class="thumbnail fancybox" title="<?php echo $image['Images']['title']?>" rel="ligthbox" data-fancybox-group="person-gallery" href="<?php echo $this->Html->url('/files/img/' . $image['Images']['url']); ?>">
                        <img class="img-responsive" alt="<?php echo $image['Images']['title']?>" title="<?php echo $image['Images']['title']?>" src="<?php echo $this->Html->url('/files/img/' . $image['Images']['url']); ?>" />
                        <div class='text-right'>
                            <small class='text-muted'><?php echo $image['Images']['title']?></small>
                        </div>
                    </a>
                </div>
                <?php if($count == 3){ $count = 0;?>
                    </div><hr>
                <?php } $count++;?>
            <?php } ?>
        <?php } ?>
    </div>
</section>