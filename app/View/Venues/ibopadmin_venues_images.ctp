<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Venue', true);?> - <?php echo $venueName; ?> - <?php echo $venueName; ?></h1>
        <ol class="breadcrumb">
            <li>
            	<a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
            		<i class="fa fa-building"></i> <?php echo __('Venue', true)?>
            	</a>
        	</li>
            <li class="active">
                <?php echo __('Images', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('locations'); ?>">
                <i class="fa fa-globe"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'listEvents/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('list of events')?>">
                <i class="fa fa-eye"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
		  	<li><a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idVenue)));?>"><?php echo __('All Images', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'searchImages/' . base64_encode($idVenue)));?>"><?php echo __('Search Images', true);?></a></li>
            <li class="active"><a href="#"><?php echo __('Venue Images', true);?></a></li>
        </ul>
    </div>
</div><br>
<div class="row">
    <?php foreach($images as $image) { ?>
        <div class="col-sm-6 col-md-4" style="margin-bottom: 20px;">
            <div class="thumbnail">
                <img src="<?php echo $this->Html->url('/files/img/' . $image['Images']['url']);?>" style="height: 200px;">
                <div class="caption">
                        <div class="form-conted" id="form-<?php echo $image['Images']['id']?>" style="display: none;">
                        
                    </div>
                    <div id="button-<?php echo $image['Images']['id']?>">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php if($image['VenuesImage']['principal'] != 1){?>
                                <?php echo $this->Form->create('ImagePrincipal', array('url' => array('controller' => 'Venues', 'action' => 'imagePrincipal')));?>
                                <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $image['VenuesImage']['id'])); ?>
                                <?php echo $this->Form->input('idVenue', array('type' => 'hidden', 'value' => $idVenue)); ?>
                                <?php echo $this->Form->input('page', array('type' => 'hidden', 'value' => $page)); ?>
                                <?php echo $this->Form->input('make_principal', array('type' => 'hidden', 'value' => 1)); ?>
                                <?php if( $venue['Venue']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('Featured'), array('class' => 'btn btn-primary btn-group-justified', 'type' => 'submit')); ?>
                                <?php } ?>    
                                <?php echo $this->Form->end(); ?>
                            <?php } else { ?>
                                <?php echo $this->Form->create('ImagePrincipal', array('url' => array('controller' => 'Venues', 'action' => 'imagePrincipal')));?>
                                <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $image['VenuesImage']['id'])); ?>
                                <?php echo $this->Form->input('idVenue', array('type' => 'hidden', 'value' => $idVenue)); ?>
                                <?php echo $this->Form->input('page', array('type' => 'hidden', 'value' => $page)); ?>
                                <?php echo $this->Form->input('make_principal', array('type' => 'hidden', 'value' => 0)); ?>
                                <?php if( $venue['Venue']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('unFeatured'), array('class' => 'btn btn-primary btn-group-justified', 'type' => 'submit')); ?>
                                <?php } ?>    
                                <?php echo $this->Form->end(); ?>
                            <?php }?>
                        </div>
                        <div class="col-sm-4">
                               <a class="btn btn-info display-form  btn-group-justified " target-form="form-<?php echo $image['Images'] ['id']?>" id-image="<?php echo $image['Images']['id']?>">
                                <i class="fa fa-pencil "></i> <?php echo __('Edit Image', true);?>
                            </a>   
                            </div>
                        <div class="col-sm-4">
                            
                             
                            <?php echo $this->Form->create('VenuesImage');?>
                            <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $image['VenuesImage']['id'])); ?>
                            <?php if( $venue['Venue']['locked'] == 0 ){ ?>
                                <?php echo $this->Form->button(__('Delete Image'), array('class' => 'btn btn-danger btn-group-justified', 'type' => 'submit')); ?>
                            <?php } ?>
                            
                            <?php echo $this->Form->end(); ?>
                        </div>
                        
                    </div>
                        </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php
        if( count($images) == 0 ){
            echo '<div class="col-md-12"><h3>' . __('no results found', true) . '</h3></div>';
        }
    ?>
</div><br>
<div class="row">
    <div class="col-md-3">
        <?php if( $page != 1){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'venuesImages/' . base64_encode($idVenue) . '/' . ($page - 1)));?>" class="btn btn-primary"><?php echo __('Previous', true);?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if( count($images) > 0 ){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'venuesImages/' . base64_encode($idVenue) . '/' . ($page + 1)));?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true);?></a>
        <?php } ?>
    </div>
    
</div>




<script type="text/javascript">
    
    $(document).ready(function() {
        
        $('.display-form').click(function(event) {
            var idImage = $(this).attr('id-image');
            var urlForm = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'formImage'));?>/" + idImage;
            $.post(urlForm, function(data){
                $('#form-' + idImage).html(data);
                $('#form-' + idImage).toggle('slow');
                $('#button-' + idImage).fadeOut();
            });
        });
        
    });
    
</script>