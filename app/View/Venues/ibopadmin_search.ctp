<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Venues', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-building"></i> <?php echo __('Venues');?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Search by Locations', true)?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Venue', true); ?></th>
                        <th><?php echo __('Location', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($venues as $venue) { ?>
                        <tr>
                            <td>
                                <?php echo $venue['Venue']['name']?>
                            </td>
                            <td>
                                <?php 
                                    if(isset( $venue['Cities']['name'] ) ){
                                        echo $venue['Cities']['name'] . ', '. $venue['States']['name'] .', '. $venue ['Countries']['name']; 
                                    }elseif(isset( $venue['States']['name'] ) ) {
                                        echo $venue['States']['name'] . ', ' . $venue['Countries']['name']; 
                                    }else{
                                        echo $venue ['Countries']['name'];
                                }?>
                            </td>                            
                            <td>
                            <center>
                                <div class="btn-group">
                                    <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Venue Location', true);?>">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                    <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Venue Images', true);?>">
                                        <i class="fa fa-file-image-o"></i>
                                    </a>
                                    <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Venue Videos', true);?>">
                                        <i class="fa fa-file-video-o"></i>
                                    </a>
                                    <a href="<?php echo $this->Html->url(array('action' => 'listEvents/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('list of events')?>">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($venue['Venue']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Edit Venue', true);?>">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </div>
                            </center>
                            </td>
                        </tr>   
                    <?php } ?>    
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?php if ($page != 1) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'search/' . $location . '/' . ($page - 1))); ?>" class="btn btn-primary"><?php echo __('Previous', true); ?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if(count($venues) >= 20){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'search/' . $location . '/' . ($page + 1))); ?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true); ?></a>
        <?php } ?>
    </div>
    
</div>
