<div class="container detail-container">
    <div class="container-fluid">


        <div class="row mobile-social-share">
            <div class="col-md-9 col-md-3 col-sm-12 col-xs-12">
                <h2><?php echo $venue['Venue']['name'] ?></h2>
            </div>
            <?php
            if ((isset($venue['VenuesImage'][0]['Images']['url'])) && ($venue['VenuesImage'][0]['Images']['url'] != null)) {
                $imageShare = $this->Html->url('/files/img/' . $venue['VenuesImage'][0]['Images']['url']);
                $imageAlt = $venue['VenuesImage'][0]['Images']['title'];
            } else {
                $imageShare = $this->Html->url('/img/defaults/generic_event.jpg');
                $imageAlt = "venue default image";
            }
            ?>
            <?php echo $this->element('social-holder', array('shareTitle' => $venue['Venue']['name'], 'imageShare' => $imageShare)); ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <img src="<?php echo $imageShare; ?>" alt="<?php echo $imageAlt; ?>" class="img_entity" width="250">
            <?php echo $this->Qrcode->url('http://ibopfactual.com' . $this->here, array('size' => '250x250')); ?>
        </div>

        <div class="col-lg-9 ">
            <table class="table table-condensed">
                <tbody>
                    <?php if ($venue['Venue']['locations_id'] != "") { ?>
                        <tr>
                            <td>
                                <span><?php echo __('Location:', true); ?> </span>
                            </td>
                            <td>
                                <strong>
                                    <?php
                                    if (isset($location['Cities']['name']) && $location['Cities']['name'] != null) {
                                        echo $location['Cities']['name'] . ', ';
                                    }
                                    if (isset($location['States']['name']) && $location['States']['name'] != null) {
                                        echo $location['States']['name'] . ', ';
                                    }
                                    if (isset($location['Countries']['name']) && $location['Countries']['name'] != null) {
                                        echo $location['Countries']['name'] . ' ';
                                        echo '<img src="' . $this->Html->url('/img/flags/' . $location['Countries']['flag']) . '" alt="' . $location['Countries']['name'] . ' flag">';
                                    }
                                    ?>
                                </strong>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($venue['Venue']['owner'] != "") { ?>
                        <tr>
                            <td>
                                <span><?php echo __('Owner:', true); ?> </span>
                            </td>
                            <td>
                                <strong><?php echo $venue['Venue']['owner'] ?></strong>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($venue['Venue']['capacity'] != "") { ?>
                        <tr>
                            <td>
                                <span><?php echo __('Capacity:', true); ?> </span>
                            </td>
                            <td>
                                <strong><?php echo $venue['Venue']['capacity'] ?></strong>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($venue['Venue']['address_1'] != "") { ?>
                        <tr>
                            <td>
                                <span><?php echo __('Address:', true); ?> </span>
                            </td>
                            <td>
                                <strong><?php echo $venue['Venue']['address_1'] ?></strong>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($venue['Venue']['address_2'] != "") { ?>
                        <tr>
                            <td></td>
                            <td>
                                <strong><?php echo $venue['Venue']['address_2'] ?></strong>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($venue['Venue']['phone'] != "") { ?>
                        <tr>
                            <td>
                                <span><?php echo __('Phone:', true); ?> </span>
                            </td>
                            <td>
                                <strong><?php echo $venue['Venue']['phone'] ?></strong>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if ($venue['Venue']['box_office_phone'] != "") { ?>
                        <tr>
                            <td>
                                <span><?php echo __('Box Office Phone:', true); ?> </span>
                            </td>
                            <td>
                                <strong><?php echo $venue['Venue']['box_office_phone'] ?></strong>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            <hr>

            <div class="fighters-details">

                <ul id="myTab" class="nav nav-tabs responsive">
                    <li class="active">
                        <a href="#events-panel" class="h4" data-toggle="tab"><?php echo __('Events', true); ?></a>
                    </li>
                    <?php if ($countImages > 0) { ?>
                        <li>
                            <a href="#photos-panel" class="h4 load-images" data-toggle="tab"><?php echo __('Photos', true); ?></a>
                        </li>
                    <?php } ?>
                    <?php if ($venue['Venue']['map'] != "") { ?>
                        <li>
                            <a href="#map-panel" class="h4 show-map" data-toggle="tab"><?php echo __('Map', true) ?></a>
                        </li>
                    <?php } ?>
                </ul>

                <div id="myTabContent" class="tab-content">

                    <div class="tab-pane fade in active" id="events-panel">
                        <div class="col-lg-12" id="loading-events"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>

                    <?php if ($countImages > 0) { ?>
                        <div class="tab-pane fade" id="photos-panel">
                            <div class="col-lg-12" id="load-img"><br>
                                <center>
                                    <i class="fa fa-refresh fa-spin fa-3x"></i>
                                </center>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($venue['Venue']['map'] != "") { ?>
                        <div class="tab-pane fade in" id="map-panel">
                        </div>
                    <?php } ?>

                </div>

            </div>

        </div>

    </div>
</div>
<script type="text/javascript">

    (function ($) {
        fakewaffle.responsiveTabs(['xs', 'sm']);
    })(jQuery);



    $(document).ready(function () {
        var loadEvents = "<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'showEvents/' . $id)); ?>";
        $.post(loadEvents, function (data) {
            $('#loading-events').html(data);
        });

        var loadImages = true;
        $('.load-images').click(function () {
            if (loadImages) {
                var loadPageImages = "<?php echo $this->Html->url(array('action' => 'getImagesVenues/' . $id)); ?>";
                $.post(loadPageImages, function (data) {
                    $('#load-img').fadeOut(function () {
                        $('#photos-panel').append(data);
                        loadImages = false;
                    });
                });
            }
        });

        $('.show-map').click(function () {
            $('#map-panel').html('<?php echo $venue['Venue']['map'] ?>');
        });

    });
</script>