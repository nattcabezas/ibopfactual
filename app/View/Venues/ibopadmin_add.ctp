<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Venues', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-building"></i> 
                    <?php echo __('Venue', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-building"></i> <?php echo __('add Venue', true);?></h3>
            </div>
            <div class="panel-body">
				<?php echo $this->Form->create('Venue'); ?>
 
					<div class="form-group">
                                            <label><?php echo __('Source:', true);?></label>
                                            <?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(null => 'select source'), 'required' => true)); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Name:', true);?></label>
                                            <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>
                                        <div class="form-group">
                                            <label><?php echo __('Attached name:', true);?></label>
                                            <?php echo $this->Form->input('complete_name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>     
					<div class="form-group">
                                            <label><?php echo __('Description:', true);?></label>
                                            <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Capacity:', true);?></label>
                                            <?php echo $this->Form->input('capacity', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Owner:', true);?></label>
                                            <?php echo $this->Form->input('owner', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Address 1:', true);?></label>
                                            <?php echo $this->Form->input('address_1', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Address 2:', true);?></label>
                                            <?php echo $this->Form->input('address_2', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Phone:', true);?></label>
                                            <?php echo $this->Form->input('phone', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Box office phone:', true);?></label>
                                            <?php echo $this->Form->input('box_office_phone', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
                                            <label><?php echo __('Comments:', true);?></label>
                                            <?php echo $this->Form->input('commente', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>
                                        <div class="form-group">
                                            <label><?php echo __('Map:', true);?></label>
                                            <?php echo $this->Form->input('map', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>
                                           
					<div class="form-group">
                                            <?php echo $this->Form->button(__('Add Venue'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                                        </div>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>