<table class="responsive responsive-table table_basic" id="events-table">
    <thead>
        <tr>
            <th><?php echo __('Date', true);?></th>
            <th><?php echo __('Name', true);?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($events as $event) { ?>
            <tr>
                <td data-content="<?php echo __('Date', true);?>"><?php echo $event['Event']['date']?></td>
                <td data-content="<?php echo __('Name', true);?>">
                    <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Event']['id'], $event['Event']['name']));?>">
                        <?php echo $event['Event']['name']?>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php echo $this->Html->script('tablesorter/jquery.tablesorter'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#events-table").tablesorter({sortList: [[0,1]]});
    });
</script>