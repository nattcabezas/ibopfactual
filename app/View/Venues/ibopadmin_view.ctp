<div class="venues view">
<h2><?php echo __('Venue'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sources'); ?></dt>
		<dd>
			<?php echo $this->Html->link($venue['Sources']['name'], array('controller' => 'sources', 'action' => 'view', $venue['Sources']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Locations'); ?></dt>
		<dd>
			<?php echo $this->Html->link($venue['Locations']['id'], array('controller' => 'locations', 'action' => 'view', $venue['Locations']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Capacity'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['capacity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Owner'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['owner']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address 1'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['address_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address 2'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['address_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Box Office Phone'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['box_office_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Commente'); ?></dt>
		<dd>
			<?php echo h($venue['Venue']['commente']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Venue'), array('action' => 'edit', $venue['Venue']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Venue'), array('action' => 'delete', $venue['Venue']['id']), array(), __('Are you sure you want to delete # %s?', $venue['Venue']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Venues'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Venue'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sources'), array('controller' => 'sources', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sources'), array('controller' => 'sources', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Locations'), array('controller' => 'locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Locations'), array('controller' => 'locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Videos'), array('controller' => 'videos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Video'), array('controller' => 'videos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Images'); ?></h3>
	<?php if (!empty($venue['Image'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Image Types Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Source 1'); ?></th>
		<th><?php echo __('Source 2'); ?></th>
		<th><?php echo __('Donated By'); ?></th>
		<th><?php echo __('Collection'); ?></th>
		<th><?php echo __('Long Description'); ?></th>
		<th><?php echo __('Ibop Owned'); ?></th>
		<th><?php echo __('Url'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($venue['Image'] as $image): ?>
		<tr>
			<td><?php echo $image['id']; ?></td>
			<td><?php echo $image['image_types_id']; ?></td>
			<td><?php echo $image['title']; ?></td>
			<td><?php echo $image['description']; ?></td>
			<td><?php echo $image['source_1']; ?></td>
			<td><?php echo $image['source_2']; ?></td>
			<td><?php echo $image['donated_by']; ?></td>
			<td><?php echo $image['collection']; ?></td>
			<td><?php echo $image['long_description']; ?></td>
			<td><?php echo $image['ibop_owned']; ?></td>
			<td><?php echo $image['url']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'images', 'action' => 'view', $image['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'images', 'action' => 'edit', $image['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'images', 'action' => 'delete', $image['id']), array(), __('Are you sure you want to delete # %s?', $image['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Videos'); ?></h3>
	<?php if (!empty($venue['Video'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Short Description'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('M4v'); ?></th>
		<th><?php echo __('Ogv'); ?></th>
		<th><?php echo __('Webm'); ?></th>
		<th><?php echo __('Mp4'); ?></th>
		<th><?php echo __('Flv'); ?></th>
		<th><?php echo __('Ibop Owned'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($venue['Video'] as $video): ?>
		<tr>
			<td><?php echo $video['id']; ?></td>
			<td><?php echo $video['title']; ?></td>
			<td><?php echo $video['short_description']; ?></td>
			<td><?php echo $video['description']; ?></td>
			<td><?php echo $video['m4v']; ?></td>
			<td><?php echo $video['ogv']; ?></td>
			<td><?php echo $video['webm']; ?></td>
			<td><?php echo $video['mp4']; ?></td>
			<td><?php echo $video['flv']; ?></td>
			<td><?php echo $video['ibop_owned']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'videos', 'action' => 'view', $video['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'videos', 'action' => 'edit', $video['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'videos', 'action' => 'delete', $video['id']), array(), __('Are you sure you want to delete # %s?', $video['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Video'), array('controller' => 'videos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
