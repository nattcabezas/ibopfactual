<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Venues', true);?> - <?php echo $venueName; ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-building"></i> 
                    <?php echo __('Venue', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Location', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('locations'); ?>">
                <i class="fa fa-globe"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'listEvents/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('list of events')?>">
                <i class="fa fa-eye"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idVenue))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>

    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-globe"></i> <?php echo __('Venue Location', true);?></h3>
            </div>
            <div class="panel-body">
            	<?php echo $this->Form->create('Location'); ?>
					
					<?php echo $this->Form->input('id', array('value' => $location['Locations']['id']));?>
					<div class="form-group input-group">
						<span class="input-group-addon" id="flag-contry" style="height: 34px;">&nbsp;&nbsp;&nbsp;</span>
						<?php echo $this->Form->input('countries_text', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Country', true) , 'readonly' => $venue['Venue']['locked'] == 0 ? false : true )); ?>
						<?php echo $this->Form->input('countries_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $location['Locations']['countries_id'])); ?>
					</div>
					<div class="form-group">
						<label><?php echo __('States:', true);?></label>
						<div id="states-contend"></div>
					</div>
					<div class="form-group">
						<label><?php echo __('Cities:', true);?></label>
						<div id="cities-contend"></div>
					</div>
					<div class="form-group">
                                <?php if( $venue['Venue']['locked'] == 0 ){ ?>            
                                    <?php echo $this->Form->button(__('save Location'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                                <?php } ?>            
	                </div>

            	<?php echo $this->Form->end(); ?>
            </div>
    	</div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {

            $("#LocationCountriesText").autocomplete({
	      minLength: 0,
	      source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(request.term);
                    $.getJSON(loadIdentities, function(data){
                        response(data);
                    });
                },
	      focus: function( event, ui ) {
	        $("#LocationCountriesText").val(ui.item.label);
	        return false;
	      },
	      select: function( event, ui ) {
	        $("#LocationCountriesText" ).val(ui.item.label);
	        $('#flag-contry').html("<img src='<?php echo $this->html->url('/img/flags')?>/" + ui.item.flag + "'>");
	        $('#LocationCountriesId').val(ui.item.id);
	        $('#cities-contend').html("");
	        var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates'));?>";
	        $.post(loadStates, {countries_id: ui.item.id}, function(data){
	        	$('#states-contend').html(data);
	        	$('#LocationStatesId').change(function(){
	        		var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities'));?>";
	        		$.post(loadCities, {states_id: $(this).val()}, function(data){
	        			$('#cities-contend').html(data);
	        		});
	        	});

	        });
	        return false;
	      }
	    })
	    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append( "<a><img src='<?php echo $this->html->url('/img/flags')?>/" + item.flag + "'> " + item.label + "</a>" )
	        .appendTo( ul );
	    };

	    <?php if($location['Locations']['id'] != null){ ?>
		    if( ($('#LocationCountriesId').val() != "") || ($('#LocationCountriesId').val() != null) ){
                        var getContryUrl = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountryData'));?>" + "/" + $('#LocationCountriesId').val();
                        $.getJSON(getContryUrl, function(countryData){
                            var flag = '<img src="<?php echo $this->html->url('/img/flags')?>/' + countryData.flag + '">';
                            $('#LocationCountriesText').val(countryData.label);
                            $('#flag-contry').html(flag);
                            var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates/' . $location['Locations']['states_id']));?>";
                            var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/' . $location['Locations']['cities_id']));?>";
                            $.post(loadStates, {countries_id: $('#LocationCountriesId').val()}, function(selectStates){
                                    $('#states-contend').html(selectStates);
                                    $('#LocationStatesId').change(function(){
                                            var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities'));?>";
                                            $.post(loadCities, {states_id: $(this).val()}, function(data){
                                                    $('#cities-contend').html(data);
                                            });
                                    });

                            });
                            <?php if($location['Locations']['states_id']){?>
                                $.post(loadCities, {states_id: <?php echo $location['Locations']['states_id']?>}, function(selectCities){
                                        $('#cities-contend').html(selectCities);
                                });
                            <?php } ?>
                        });
		    }
	    <?php } ?>

	});
</script>