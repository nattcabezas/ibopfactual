<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Image Types', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-child"></i> 
                    <?php echo __('Image Types', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-child"></i> <?php echo __('add type', true);?></h3>
            </div>
            <div class="panel-body">
				<?php echo $this->Form->create('ImageType'); ?>

					<div class="form-group">
						<label><?php echo __('Type:', true);?></label>
						<?php echo $this->Form->input('type', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>
					
					<div class="form-group">
                		<?php echo $this->Form->button(__('Add Type'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                	</div>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>