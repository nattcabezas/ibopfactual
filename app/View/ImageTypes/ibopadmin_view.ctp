<div class="imageTypes view">
<h2><?php echo __('Image Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($imageType['ImageType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($imageType['ImageType']['type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Image Type'), array('action' => 'edit', $imageType['ImageType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Image Type'), array('action' => 'delete', $imageType['ImageType']['id']), array(), __('Are you sure you want to delete # %s?', $imageType['ImageType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Image Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image Type'), array('action' => 'add')); ?> </li>
	</ul>
</div>
