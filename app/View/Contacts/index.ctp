<?php
    if($typeMath == 0){
        $result = $firstNumber + $secondNumber;
    } else {
        $result = $firstNumber - $secondNumber;
    }
?>
<div class="container search-container">
    <div class="row"> 
        <?php echo $this->Session->flash();?>        
        <?php echo $this->Form->create('Contact', array('url' => array('controller' => 'Contacts', 'action' => 'add'))); ?>
            <div class="col-lg-6">        
                <div class="form-group">
                    <h1><?php echo __('Contact', true); ?></h1>  
                </div>          
                <div class="form-group">
                    <label for="InputName"><?php echo __('Name', true) ?></label>
                    <div class="input-group">
                        <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail"><?php  echo __('Your Email', true) ?></label>
                    <div class="input-group">
                      <?php echo $this->Form->input('email', array('label'=> false, 'div' => false, 'class' => 'form-control')); ?>
                      <span class="input-group-addon"><i class="fa fa-at"></i></span></div>
                    </div>
                <div class="form-group">
                    <label for="InputMessage"><?php echo __('Message', true) ?></label>
                    <div class="input-group">
                      <?php echo $this->Form->input('message', array('label'=>false,'div'=>false, 'class'=>'form-control text-area')); ?>
                      <span class="input-group-addon"><i class="fa fa-file"></i></span>
                    </div>
                </div>
                <div class="form-group">
                <label for="InputReal">
                    <?php echo __('What is the result of ', true ); ?>
                    <?php echo $firstNumber; ?>
                    <?php echo $typeMath == 0 ? '+' : '-';?>
                    <?php echo $secondNumber; ?>
                    ?
                </label>
                    <div class="input-group">
                      <?php echo $this->Form->input('captcha', array('label' => false, 'div' => false, 'class' => 'form-control'))?>
                        <?php echo $this->Form->input('captcha_result', array('type' => 'hidden', 'value' => base64_encode($result)));?>
                      <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                    </div>
                </div>      
              <?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?><hr>        
            </div>
        <?php echo $this->Form->end(); ?>
      <hr class="featurette-divider hidden-lg">
    <div class="col-lg-5 col-md-push-1"><br><br>
        <address>
            <h3>Contact information</h3>
              <p class="lead">                 
                    Phone: 1-XXXX-XXXXX<br>
                    Info Email: info@ibopfactual.com<br>
                    Archivist Email: mi@ibopfactual.com
              </p>
        </address>
    </div>
</div>
</div>
