
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> <?php echo __('Contacts', true) ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-user"></i> <?php echo __('Contacts', true)?>
                </a>    
            </li>
            <li class="active">
                <?php echo __('View', true); ?>   
            </li>
        </ol>       
    </div>
</div>    
<div class="row">
    <div class="col-lg-6 ">
        <dl>
            <div class="row">
                    <div class="col-md-4">        
                        <p class="leader "><dt><?php echo __('Name'); ?></dt> </p>
                    </div>
                    <div class="col-md-6">    
                        <dd>
                            <?php echo h($contact['Contact']['name']); ?>
                            &nbsp;
                        </dd>
                    </div>    
            </div>
            <div class="row">
                    <div class="col-md-4">        
                        <p class="leader "><dt><?php echo __('Email'); ?></dt> </p>
                    </div>
                    <div class="col-md-6">    
                        <dd>
                            <?php echo h($contact['Contact']['email']); ?>
                            &nbsp;
                        </dd>
                    </div>    
            </div>
            <div class="row">
                    <div class="col-md-4">        
                        <p class="leader "><dt><?php echo __('Message'); ?></dt> </p>
                    </div>
                    <div class="col-md-6">    
                         <dd>
                            <?php echo h($contact['Contact']['message']); ?>
                            &nbsp;
                        </dd>
                    </div>    
            </div>            
        </dl>
    </div>
</div>

