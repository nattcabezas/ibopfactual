<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Contacts', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-user"></i> <?php echo __('Contacts', true)?></li>
        </ol>
    </div>
</div>
<div class="row">    
    <div class="col-lg-6">        
        <?php echo $this->Form->create('searchContacts', array('url' => array('controller' => 'Contacts', 'action' => 'searchContacts'))); ?>
            <div class="col-lg-10">
                <?php echo $this->Form->input('keywork', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
            </div>
            <div class="col-lg-2">
                <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
        <?php echo $this->Form->end(); ?>
    </div>        
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Contact', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($contacts as $contact){ ?>
                        <?php if ($contact['Contact']['view'] == ''){
                            $variableClass = 'danger';
                        }else{
                            $variableClass = 'default';
                        } ?>
                        <tr class="<?php echo $variableClass; ?>">
                            <td><?php echo $contact['Contact']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'view/' . base64_encode($contact['Contact']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'delete/' . base64_encode($contact['Contact']['id'])));?>" class="btn btn-danger btn-delete">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>