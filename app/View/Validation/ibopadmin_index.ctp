<?php 
    echo $this->Form->create('User', array( 'class' => 'form-signin', 'url' => array( 'controller' => 'Validation', 'action' => 'login' ) ));
    echo '<img src="' . $this->Html->url('/img/ibop_logo_backend.svg') . '" class="img-logo-login">';
    echo $this->Session->flash(); 
    echo $this->Form->input('username', array('type' => 'text', 'div' => false, 'class' => 'form-control', 'label' => false, 'autofocus' => '', 'placeholder' => __('Username', true)));
    echo $this->Form->input('password', array('div' => false, 'class' => 'form-control', 'label' => false, 'placeholder' => __('Password', true)));
    echo $this->Form->input('url', array('type' => 'hidden', 'value' => $url));
    echo $this->Form->button('Login', array('class' => 'btn btn-lg btn-primary btn-block', 'type' => 'submit'));
    echo $this->Form->end(); 