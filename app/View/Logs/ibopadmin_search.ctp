<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Logs', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-bookmark"></i> <?php echo __('Logs', true)?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Search Result', true)?>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="col-lg-10">
            <?php echo $this->Form->input('keywork', array('id' => 'search-keyword', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'search-logs')); ?>
        </div>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('User', true);?></th>
                        <th><?php echo __('Date', true);?></th>
                        <th><?php echo __('Description', true);?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($logs as $log){ ?>
                        <tr>
                                <td><?php echo $log['Users']['name']; ?></td>
                                <td><?php echo date('l jS \of F Y h:i:s A', strtotime($log['Log']['date']));?></td>
                                <td>
                                    <?php echo $log['Log']['description']; ?>
                                </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-3">
        <?php if ($page != 1) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'search/' . base64_encode($keywork) . '/' . ($page - 1))); ?>" class="btn btn-primary"><?php echo __('Previous', true); ?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if (count($logs) > 0) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'search/' . base64_encode($keywork) . '/' . ($page + 1))); ?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true); ?></a>
        <?php } ?>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#search-logs').click(function(event){
            event.preventDefault();
            var keyword     = Base64.encode($('#search-keyword').val());
            var searchPage  = "<?php echo $this->Html->url(array('action' => 'search'));?>/" + keyword;
            window.location.replace(searchPage);
        });
    });
</script>