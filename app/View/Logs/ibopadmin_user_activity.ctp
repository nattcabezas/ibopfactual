<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Activity for all users', true);?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Username', true);?></th>
                        <th><?php echo __('Full Name', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user){ ?>
                        <tr>
                            <td><?php echo $user['User']['username']?></td>
                            <td><?php echo $user['User']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'ibopadmin_individualActivity/' . base64_encode($user['User']['id'])  . '/' . $user['User']['name'] ));?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="<?php echo __('See all activity', true);?>">
                                            <?php echo __('See all activity', true);?>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>