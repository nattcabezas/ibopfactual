<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('User activity for: ' . $name, true);?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h4><?php echo __('See activity for date range:', true);?></h4>
        <table>
            <thead>
                <tr>
                    <th><p>Start Date: <input type="text" id="start_datepicker"></p></th>
                    <th><p>End Date: <input type="text" id="end_datepicker"></p></th>
                    
                </tr>
            </thead>
        </table>
        <button id="date_range_button" class="btn btn-primary">See results for date range</button>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <h3 id="infoLabel">Data created today (EST time)</h3>
        <?php
            $totalFights = 0;
            $totalEvents = 0;
            $totalIdentities = 0;
            $totalVenues = 0;
            $totalTitles = 0;
            $totalImages = 0;
            $totalFightIdentities = 0;
            
            $totalIdentitiesImage = 0;
            $totalIdentitiesID = 0;
            $totalIdentitiesResidence = 0;
            $totalIdentitiesBirth = 0;
            $totalIdentitiesNickname = 0;
            $totalIdentitiesVideo = 0;
            $totalFightsImage = 0;
            
            $totalFightsDatum = 0;
            $totalFightsTitle = 0;
            $totalEventsJob = 0;
            $totalEventImages = 0;
            $totalEventSponsors = 0;
            $totalVenueImages = 0;
            $totalLocation = 0;
        ?>
        
        <?php foreach ($dailyLogs as $log){ ?>
               <?php
                   if (strcmp($log['Log']['table_name'], "Fight") == 0 )
                   {
                       $totalFights++;
                   }
                   if (strcmp($log['Log']['table_name'], "Event") == 0 )
                   {
                       $totalEvents++;
                   }
                   if (strcmp($log['Log']['table_name'], "Identity") == 0 )
                   {
                       $totalIdentities++;
                   }
                   if (strcmp($log['Log']['table_name'], "Venue") == 0 )
                   {
                       $totalVenues++;
                   }
                   if (strcmp($log['Log']['table_name'], "Title") == 0 )
                   {
                       $totalTitles++;
                   }
                   if (strcmp($log['Log']['table_name'], "Image") == 0 )
                   {
                       $totalImages++;
                   }
                   if (strcmp($log['Log']['table_name'], "FightIdentity") == 0 )
                   {
                       $totalFightIdentities++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentitiesImage") == 0 )
                   {
                       $totalIdentitiesImage++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityId") == 0 )
                   {
                       $totalIdentitiesID++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityResidence") == 0 )
                   {
                       $totalIdentitiesResidence++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityBirth") == 0 )
                   {
                       $totalIdentitiesBirth++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityNickname") == 0 )
                   {
                       $totalIdentitiesNickname++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentitiesVideo") == 0 )
                   {
                       $totalIdentitiesVideo++;
                   }
                   if (strcmp($log['Log']['table_name'], "FightsImage") == 0 )
                   {
                       $totalFightsImage++;
                   }

                   if (strcmp($log['Log']['table_name'], "FightDatum") == 0 )
                   {
                       $totalFightsDatum++;
                   }
                   if (strcmp($log['Log']['table_name'], "FightsTitle") == 0 )
                   {
                       $totalFightsTitle++;
                   }
                   if (strcmp($log['Log']['table_name'], "EventsJob") == 0 )
                   {
                       $totalEventsJob++;
                   }
                   if (strcmp($log['Log']['table_name'], "EventsImage") == 0 )
                   {
                       $totalEventImages++;
                   }
                   if (strcmp($log['Log']['table_name'], "EventsSponsor") == 0 )
                   {
                       $totalEventSponsors++;
                   }
                   if (strcmp($log['Log']['table_name'], "VenuesImage") == 0 )
                   {
                       $totalVenueImages++;
                   }
                   if (strcmp($log['Log']['table_name'], "Location") == 0 )
                   {
                       $totalLocation++;
                   }
               ?>
        <?php } ?>

        <table class="table table-striped table-hover table-striped tablesorter">
            <thead>
                <tr>
                    <th><h4 id="identitiesHeader">Identities: <?php echo $totalIdentities?></h4></th>
                    <th><h4 id="fightsHeader">Fights: <?php echo $totalFights?></h4></th>
                    <th><h4 id="eventsHeader">Events: <?php echo $totalEvents?></h4></th>
                    <th><h4 id="venuesHeader">Venues: <?php echo $totalVenues?></h4></th>
                    <th><h4 id="titleHeader">Title: <?php echo $totalTitles?></h4></th>
                    <th><h4 id="imageHeader">Image: <?php echo $totalImages?></h4></th>
                    <th><h4 id="FightIdentityHeader">Fight identities: <?php echo $totalFightIdentities?></h4></th>
                </tr>
                <tr>
                    <th><h4 id="IdentitiesImageHeader">Identities image: <?php echo $totalIdentitiesImage?></h4></th>
                    <th><h4 id="IdentityIdHeader">Identities ID: <?php echo $totalIdentitiesID?></h4></th>
                    <th><h4 id="IdentityResidenceHeader">Identities residence: <?php echo $totalIdentitiesResidence?></h4></th>
                    <th><h4 id="IdentityBirthHeader">Identities Birth: <?php echo $totalIdentitiesBirth?></h4></th>
                    <th><h4 id="IdentityNickNameHeader">Identities Nickname: <?php echo $totalIdentitiesNickname?></h4></th>
                    <th><h4 id="IdentitiesVideo">Identities Video: <?php echo $totalIdentitiesVideo?></h4></th>
                    <th><h4 id="FightsImage">Fights image: <?php echo $totalFightsImage?></h4></th>

                </tr>    
                <tr>
                    <th><h4 id="FightDatumHeader">Fight Datum: <?php echo $totalFightsDatum?></h4></th>
                    <th><h4 id="FightsTitleHeader">Fights Title: <?php echo $totalFightsTitle?></h4></th>
                    <th><h4 id="EventsJobHeader">Event Jobs: <?php echo $totalEventsJob?></h4></th>
                    <th><h4 id="EventsImageHeader">Events images: <?php echo $totalEventImages?></h4></th>
                    <th><h4 id="EventsSponsorHeader">Events sponsors: <?php echo $totalEventSponsors?></h4></th>
                    <th><h4 id="VenueImageHeader">Venues Images: <?php echo $totalVenueImages?></h4></th>
                    <th><h4 id="LocationHeader">Location:<?php echo $totalLocation?></h4></th>
                </tr>
            </thead>
        </table>
        
    </div>
    
    <div class="col-lg-12">
        <h3 id="infoLabelUpdate">Data modified today (EST time)</h3>
        
        <?php
            $totalFightsUpdate = 0;
            $totalEventsUpdate = 0;
            $totalIdentitiesUpdate = 0;
            $totalVenuesUpdate = 0;
            $totalTitlesUpdate = 0;
            $totalImagesUpdate = 0;
            $totalFightIdentitiesUpdate = 0;
            
            $totalIdentitiesImageUpdate = 0;
            $totalIdentitiesIDUpdate = 0;
            $totalIdentitiesResidenceUpdate = 0;
            $totalIdentitiesBirthUpdate = 0;
            $totalIdentitiesNicknameUpdate = 0;
            $totalIdentitiesVideoUpdate = 0;
            $totalFightsImageUpdate = 0;
            
            $totalFightsDatumUpdate = 0;
            $totalFightsTitleUpdate = 0;
            $totalEventsJobUpdate = 0;
            $totalEventImagesUpdate = 0;
            $totalEventSponsorsUpdate = 0;
            $totalVenueImagesUpdate = 0;
            $totalLocationUpdate = 0;
        ?>
        
        <?php foreach ($dailyLogsUpdate as $log){ ?>
               <?php
                   if (strcmp($log['Log']['table_name'], "Fight") == 0 )
                   {
                       $totalFightsUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "Event") == 0 )
                   {
                       $totalEventsUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "Identity") == 0 )
                   {
                       $totalIdentitiesUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "Venue") == 0 )
                   {
                       $totalVenuesUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "Title") == 0 )
                   {
                       $totalTitlesUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "Image") == 0 )
                   {
                       $totalImagesUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "FightIdentity") == 0 )
                   {
                       $totalFightIdentitiesUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentitiesImage") == 0 )
                   {
                       $totalIdentitiesImageUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityId") == 0 )
                   {
                       $totalIdentitiesIDUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityResidence") == 0 )
                   {
                       $totalIdentitiesResidenceUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityBirth") == 0 )
                   {
                       $totalIdentitiesBirthUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentityNickname") == 0 )
                   {
                       $totalIdentitiesNicknameUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "IdentitiesVideo") == 0 )
                   {
                       $totalIdentitiesVideoUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "FightsImage") == 0 )
                   {
                       $totalFightsImageUpdate++;
                   }

                   if (strcmp($log['Log']['table_name'], "FightDatum") == 0 )
                   {
                       $totalFightsDatumUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "FightsTitle") == 0 )
                   {
                       $totalFightsTitleUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "EventsJob") == 0 )
                   {
                       $totalEventsJobUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "EventsImage") == 0 )
                   {
                       $totalEventImagesUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "EventsSponsor") == 0 )
                   {
                       $totalEventSponsorsUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "VenuesImage") == 0 )
                   {
                       $totalVenueImagesUpdate++;
                   }
                   if (strcmp($log['Log']['table_name'], "Location") == 0 )
                   {
                       $totalLocationUpdate++;
                   }
               ?>
        <?php } ?>
        
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><h4 id="identitiesHeaderModify">Identities: <?php echo $totalIdentitiesUpdate?></h4></th>
                        <th><h4 id="fightsHeaderModify">Fights: <?php echo $totalFightsUpdate?></h4></th>
                        <th><h4 id="eventsHeaderModify">Events: <?php echo $totalEventsUpdate?></h4></th>
                        <th><h4 id="venuesHeaderModify">Venues: <?php echo $totalVenuesUpdate?></h4></th>
                        <th><h4 id="titleHeaderModify">Title: <?php echo $totalTitlesUpdate?></h4></th>
                        <th><h4 id="imageHeaderModify">Image: <?php echo $totalImagesUpdate?></h4></th>
                        <th><h4 id="FightIdentityHeaderModify">Fight identities: <?php echo $totalFightIdentitiesUpdate?></h4></th>
                    </tr>
                    <tr>
                        <th><h4 id="IdentitiesImageHeaderModify">Identities image: <?php echo $totalIdentitiesImageUpdate?></h4></th>
                        <th><h4 id="IdentityIdHeaderModify">Identities ID: <?php echo $totalIdentitiesIDUpdate?></h4></th>
                        <th><h4 id="IdentityResidenceHeaderModify">Identities residence: <?php echo $totalIdentitiesResidenceUpdate?></h4></th>
                        <th><h4 id="IdentityBirthHeaderModify">Identities Birth: <?php echo $totalIdentitiesBirthUpdate?></h4></th>
                        <th><h4 id="IdentityNickNameHeaderModify">Identities Nickname: <?php echo $totalIdentitiesNicknameUpdate?></h4></th>
                        <th><h4 id="IdentitiesVideoModify">Identities Video: <?php echo $totalIdentitiesVideoUpdate?></h4></th>
                        <th><h4 id="FightsImageModify">Fights image: <?php echo $totalFightsImageUpdate?></h4></th>

                    </tr>    
                    <tr>
                        <th><h4 id="FightDatumHeaderModify">Fight Datum: <?php echo $totalFightsDatumUpdate?></h4></th>
                        <th><h4 id="FightsTitleHeaderModify">Fights Title: <?php echo $totalFightsTitleUpdate?></h4></th>
                        <th><h4 id="EventsJobHeaderModify">Event Jobs: <?php echo $totalEventsJobUpdate?></h4></th>
                        <th><h4 id="EventsImageHeaderModify">Events images: <?php echo $totalEventImagesUpdate?></h4></th>
                        <th><h4 id="EventsSponsorHeaderModify">Events sponsors: <?php echo $totalEventSponsorsUpdate?></h4></th>
                        <th><h4 id="VenueImageHeaderModify">Venues Images: <?php echo $totalVenueImagesUpdate?></h4></th>
                        <th><h4 id="LocationHeaderModify">Location:<?php echo $totalLocationUpdate?></h4></th>
                    </tr>
                </thead>
            </table>
    </div>
</div>        

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($logs as $log){ ?>
                        <tr>
                                <td><?php echo $log['Users']['name']; ?></td>
                                <td><?php echo date('l jS \of F Y h:i:s A', strtotime($log['Log']['date']));?></td>
                                <td>
                                    <?php echo $log['Log']['description']; ?>
                                </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
    <?php 
        echo $this->Paginator->numbers(array(
        'before' => '',
        'after' => '',
        'separator' => '',
        'tag' => 'li',
        'currentClass' => 'active',
        'currentTag' => 'a'
        )); 
    ?>
    </ul>
</div>
  
<script>
    
    $(document).ready(function() {
        $( "#start_datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $( "#end_datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
        $("#date_range_button").click(function(event){
            event.preventDefault();
            var userID   = "<?php echo $userID; ?>";
            var startDate =  $( "#start_datepicker" ).val();
            var endDate =  $( "#end_datepicker" ).val();
            var loadPage  = "<?php echo $this->Html->url(array('controller' => 'Logs', 'action' => 'queryDateRange'));?>/" + userID + "/" + startDate + "/" + endDate+"/"+"CREATE";
            
            var totalFights = 0;
            var totalEvents = 0;
            var totalIdentities = 0;
            var totalVenues = 0;
            var totalTitles = 0;
            var totalImages = 0;
            var totalFightIdentities = 0;
            var totalIdentitiesImage = 0;
            var totalIdentitiesID = 0;
            var totalIdentitiesResidence = 0;
            var totalIdentitiesBirth = 0;
            var totalIdentitiesNickname = 0;
            var totalIdentitiesVideo = 0;
            var totalFightsImage = 0;
            var totalFightsDatum = 0;
            var totalFightsTitle = 0;
            var totalEventsJob = 0;
            var totalEventImages = 0;
            var totalEventSponsors = 0;
            var totalVenueImages = 0;
            var totalLocation = 0;
            
            $.post(loadPage, function(data){
                $.each(data, function(index, element) {
                        if ("Fight".localeCompare(element.Log.table_name) == 0)
                        {
                            totalFights++;
                        }
                        if ("Event".localeCompare(element.Log.table_name) == 0)
                        {
                            totalEvents++;
                        }
                        if ("Identity".localeCompare(element.Log.table_name) == 0)
                        {
                            totalIdentities++;
                        }
                        if ("Venue".localeCompare(element.Log.table_name) == 0)
                        {
                            totalVenues++;
                        }
                        
                        if ("Title".localeCompare(element.Log.table_name) == 0)
                        {
                            totalTitles++;
                        }
                        if ("Image".localeCompare(element.Log.table_name) == 0)
                        {
                            totalImages++;
                        }
                        if ("FightIdentity".localeCompare(element.Log.table_name) == 0)
                        {
                            totalFightIdentities++;
                        }
                        if ("IdentitiesImage".localeCompare(element.Log.table_name) == 0)
                        {
                            totalIdentitiesImage++;
                        }
                        if ("IdentityId".localeCompare(element.Log.table_name) == 0)
                        {
                            totalIdentitiesID++;
                        }
                        if ("IdentityResidence".localeCompare(element.Log.table_name) == 0)
                        {
                            totalIdentitiesResidence++;
                        }
                        if ("IdentityBirth".localeCompare(element.Log.table_name) == 0)
                        {
                            totalIdentitiesBirth++;
                        }
                        if ("IdentityNickname".localeCompare(element.Log.table_name) == 0)
                        {
                            totalIdentitiesNickname++;
                        }
                        if ("IdentitiesVideo".localeCompare(element.Log.table_name) == 0)
                        {
                            totalIdentitiesVideo++;
                        }
                        if ("FightsImage".localeCompare(element.Log.table_name) == 0)
                        {
                            totalFightsImage++;
                        }
                        if ("FightDatum".localeCompare(element.Log.table_name) == 0)
                        {
                            totalFightsDatum++;
                        }
                        if ("FightsTitle".localeCompare(element.Log.table_name) == 0)
                        {
                            totalFightsTitle++;
                        }
                        if ("EventsJob".localeCompare(element.Log.table_name) == 0)
                        {
                            totalEventsJob++;
                        }
                        if ("EventsImage".localeCompare(element.Log.table_name) == 0)
                        {
                            totalEventImages++;
                        }
                        if ("EventsSponsor".localeCompare(element.Log.table_name) == 0)
                        {
                            totalEventSponsors++;
                        }
                        if ("VenuesImage".localeCompare(element.Log.table_name) == 0)
                        {
                            totalVenueImages++;
                        }
                        if ("Location".localeCompare(element.Log.table_name) == 0)
                        {
                            totalLocation++;
                        }
                    });
                    $("#infoLabel").text("New events saved in range  "+startDate+" through  "+endDate+" (EST Time)");                                        
                    $("#identitiesHeader").text("Identities: "+totalIdentities);
                    $("#fightsHeader").text("Fights: "+totalFights);
                    $("#eventsHeader").text("Events: "+totalEvents);
                    $("#venuesHeader").text("Venues: "+totalVenues);
                    $("#titleHeader").text("Title: "+totalTitles);
                    $("#imageHeader").text("Image: "+totalImages);
                    $("#FightIdentityHeader").text("Fight identities: "+totalFightIdentities);
                    $("#IdentitiesImageHeader").text("Identities image: "+totalIdentitiesImage);
                    $("#IdentityIdHeader").text("Identities ID: "+totalIdentitiesID);
                    $("#IdentityResidenceHeader").text("Identities residence: "+totalIdentitiesResidence);
                    $("#IdentityBirthHeader").text("Identities Birth: "+totalIdentitiesBirth);
                    $("#IdentityNickNameHeader").text("Identities Nickname: "+totalIdentitiesNickname);
                    $("#IdentitiesVideo").text("Identities Video: "+totalIdentitiesVideo);
                    $("#FightsImage").text("Fights image: "+totalFightsImage);
                    $("#FightDatumHeader").text("Fight Datum: "+totalFightsDatum);
                    $("#FightsTitleHeader").text("Fights Title: "+totalFightsTitle);
                    $("#EventsJobHeader").text("Event Jobs: "+totalEventsJob);
                    $("#EventsImageHeader").text("Events images: "+totalEventImages);
                    $("#EventsSponsorHeader").text("Events sponsors: "+totalEventSponsors);
                    $("#VenueImageHeader").text("Venues Images: "+totalVenueImages);
                    $("#LocationHeader").text("Location: "+totalLocation);
            }, "json");
                        
            var totalFightsUpdate = 0;
            var totalEventsUpdate = 0;
            var totalIdentitiesUpdate = 0;
            var totalVenuesUpdate = 0;
            var totalTitlesUpdate =  0;
            var totalImagesUpdate =  0;
            var totalFightIdentitiesUpdate =  0;
            var totalIdentitiesImageUpdate =  0;
            var totalIdentitiesIDUpdate =  0;
            var totalIdentitiesResidenceUpdate =  0;
            var totalIdentitiesBirthUpdate =  0;
            var totalIdentitiesNicknameUpdate =  0;
            var totalIdentitiesVideoUpdate =  0;
            var totalFightsImageUpdate =  0;            
            var totalFightsDatumUpdate =  0;
            var totalFightsTitleUpdate =  0;
            var totalEventsJobUpdate =  0;
            var totalEventImagesUpdate =  0;
            var totalEventSponsorsUpdate =  0;
            var totalVenueImagesUpdate =  0;
            var totalLocationUpdate =  0;
                        
            var loadPageUpdate  = "<?php echo $this->Html->url(array('controller' => 'Logs', 'action' => 'queryDateRange'));?>/" + userID + "/" + startDate + "/" + endDate+"/"+"UPDATE";
            
            $.post(loadPageUpdate, function(data2){
                $.each(data2, function(index, element2) {
                        if ("Fight".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalFightsUpdate++;
                        }
                        if ("Event".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalEventsUpdate++;
                        }
                        if ("Identity".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalIdentitiesUpdate++;
                        }
                        if ("Venue".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalVenuesUpdate++;
                        }
                        if ("Title".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalTitlesUpdate++;
                        }
                        if ("Image".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalImagesUpdate++;
                        }
                        if ("FightIdentity".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalFightIdentitiesUpdate++;
                        }
                        if ("IdentitiesImage".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalIdentitiesImageUpdate++;
                        }
                        if ("IdentityId".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalIdentitiesIDUpdate++;
                        }
                        if ("IdentityResidence".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalIdentitiesResidenceUpdate++;
                        }
                        if ("IdentityBirth".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalIdentitiesBirthUpdate++;
                        }
                        if ("IdentityNickname".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalIdentitiesNicknameUpdate++;
                        }
                        if ("IdentitiesVideo".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalIdentitiesVideoUpdate++;
                        }
                        if ("FightsImage".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalFightsImageUpdate++;
                        }
                        if ("FightDatum".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalFightsDatumUpdate++;
                        }
                        if ("FightsTitle".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalFightsTitleUpdate++;
                        }
                        if ("EventsJob".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalEventsJobUpdate++;
                        }
                        if ("EventsImage".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalEventImagesUpdate++;
                        }
                        if ("EventsSponsor".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalEventSponsorsUpdate++;
                        }
                        if ("VenuesImage".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalVenueImagesUpdate++;
                        }
                        if ("Location".localeCompare(element2.Log.table_name) == 0)
                        {
                            totalLocationUpdate++;
                        }
                    });
                    
                    $("#infoLabelUpdate").text("Data modified in range  "+startDate+" through  "+endDate+" (EST Time)");                                        
                    $("#identitiesHeaderModify").text("Identities: "+totalIdentitiesUpdate);
                    $("#fightsHeaderModify").text("Fights: "+totalFightsUpdate);
                    $("#eventsHeaderModify").text("Events: "+totalEventsUpdate);
                    $("#venuesHeaderModify").text("Venues: "+totalVenuesUpdate);
                    $("#titleHeaderModify").text("Title: "+totalTitlesUpdate);
                    $("#imageHeaderModify").text("Image: "+totalImagesUpdate);
                    $("#FightIdentityHeaderModify").text("Fight identities: "+totalFightIdentitiesUpdate);
                    $("#IdentitiesImageHeaderModify").text("Identities image: "+totalIdentitiesImageUpdate);
                    $("#IdentityIdHeaderModify").text("Identities ID: "+totalIdentitiesIDUpdate);
                    $("#IdentityResidenceHeaderModify").text("Identities residence: "+totalIdentitiesResidenceUpdate);
                    $("#IdentityBirthHeaderModify").text("Identities Birth: "+totalIdentitiesBirthUpdate);
                    $("#IdentityNickNameHeaderModify").text("Identities Nickname: "+totalIdentitiesNicknameUpdate);
                    $("#IdentitiesVideoModify").text("Identities Video: "+totalIdentitiesVideoUpdate);
                    $("#FightsImageModify").text("Fights image: "+totalFightsImageUpdate);
                    $("#FightDatumHeaderModify").text("Fight Datum: "+totalFightsDatumUpdate);
                    $("#FightsTitleHeaderModify").text("Fights Title: "+totalFightsTitleUpdate);
                    $("#EventsJobHeaderModify").text("Event Jobs: "+totalEventsJobUpdate);
                    $("#EventsImageHeaderModify").text("Events images: "+totalEventImagesUpdate);
                    $("#EventsSponsorHeaderModify").text("Events sponsors: "+totalEventSponsorsUpdate);
                    $("#VenueImageHeaderModify").text("Venues Images: "+totalVenueImagesUpdate);
                    $("#LocationHeaderModify").text("Location: "+totalLocationUpdate);
            }, "json");
            
        });
    });
</script>