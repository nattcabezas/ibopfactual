<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Logs', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-bookmark"></i> <?php echo __('Logs', true)?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="col-lg-10">
            <?php echo $this->Form->input('keywork', array('id' => 'search-keyword', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'search-logs')); ?>
        </div>
        
        <br>
        <br>
        <br>
        <a href="<?php echo $this->Html->url(array('action' => 'ibopadmin_userActivity')); ?>" class="btn btn-primary">
            <?php echo __('Check activity for each user', true);?>
        </a>
    </div>
    
</div>
<hr>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('User', true);?></th>
                        <th><?php echo __('Date', true);?></th>
                        <th><?php echo __('Description', true);?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($logs as $log){ ?>
                        <tr>
                                <td><?php echo $log['Users']['name']; ?></td>
                                <td><?php echo date('l jS \of F Y h:i:s A', strtotime($log['Log']['date']));?></td>
                                <td>
                                    <?php echo $log['Log']['description']; ?>
                                </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#search-logs').click(function(event){
            event.preventDefault();
            var keyword     = Base64.encode($('#search-keyword').val());
            var searchPage  = "<?php echo $this->Html->url(array('action' => 'search'));?>/" + keyword;
            window.location.replace(searchPage);
        });
    });
</script>