<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?> - <?php echo $identityName ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Family', true); ?>
            </li>
        </ol>
    </div>

    <div class="btn-group pull-right" style="padding-bottom: 10px;">
        <a href="<?php echo $this->Html->url('/Person/'.$this->getUrlPerson->getUrl($idIdentity,$identityName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
            <i class="fa fa-desktop"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
            <i class="fa fa-file-image-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
            <i class="fa fa-file-video-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
            <i class="fa fa-file-text-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('family'); ?>">
            <i class="fa fa-group"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view fights'); ?>">
            <i class="fa fa-eye"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
            <i class="fa fa-pencil"></i>
        </a>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo __('Add Family', true); ?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('IdentityRelationship'); ?>

                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $idIdentity)); ?>
                <div class="form-group">
                    <label><?php echo __('Familiar:', true); ?></label>
                    <?php echo $this->Form->input('family_label', array('label' => false, 'div' => false, 'class' => 'form-control input-identity', 'add-to' => '#IdentityRelationshipFamily', 'required' => true)); ?>
                    <?php echo $this->Form->input('family', array('type' => 'hidden')); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Relationship:', true); ?></label>
                    
                    <div class="form-group">
                            <?php 
                                $optionsType = array(
                                    'aunt'          => 'aunt', 
                                    'cousin'        => 'cousin',
                                    'grandchild'    => 'grandchild',
                                    'grandparent'   => 'grandparent',
                                    'nephew'        => 'nephew',
                                    'niece'         => 'niece',
                                    'offspring'     => 'offspring',
                                    'parent'        => 'parent', 
                                    'sibling'       => 'sibling', 
                                    'spouse'        => 'spouse',
                                    'uncle'         => 'uncle'
                                );
                            ?>
                            <?php echo $this->Form->input('relationship', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $optionsType, 'required' => true)); ?>
                        </div>
                </div>

                <div class="form-group">
                    <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                        <?php echo $this->Form->button(__('Add Familiar'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    <?php } ?>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo __('List Family', true); ?></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Familiar', true); ?></th>
                                <th><?php echo __('Relationship', true); ?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($familiars as $familiar) { ?>
                                <tr>
                                    <td><?php echo $familiar['Family']['name'] ?> <?php echo $familiar['Family']['last_name'] ?></td>
                                    <td><?php echo $familiar['IdentityRelationship']['relationship'] ?></td>
                                    <td>
                                        <center>
                                            <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                                <a href="<?php echo $this->Html->url
                                                        (array('action' => 'deleteFamily/' . base64_encode($idIdentity) .
                                                    '/' . base64_encode($familiar['IdentityRelationship']['id']) . '/'. base64_encode($familiar['IdentityRelationship']['family'])));
                                                ?>" class="btn btn-primary"><i class="fa fa-trash-o"></i></a>
                                            <?php } ?>    
                                        </center>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>