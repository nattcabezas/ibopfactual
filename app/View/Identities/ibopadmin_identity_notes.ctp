<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?> - <?php echo $identityName ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Notes', true); ?>
            </li>
        </ol>

        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('family'); ?>">
                <i class="fa fa-group"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view fights'); ?>">
                <i class="fa fa-eye"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>

    </div>    
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>"><?php echo __('All Notes', true); ?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'searchNotes/' . base64_encode($idIdentity))); ?>"><?php echo __('Search Notes', true); ?></a></li>
            <li class="active"><a href="#"><?php echo __('Identity Notes', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Notas', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($notes as $note) { ?>
                        <tr>
                            <td>
                                <a href="#" id="note-link-<?php echo $note['Notes']['id']?>"><?php echo $note['Notes']['title']?></a>
                                <div style="display: none;">
                                    <div id="note-modal-<?php echo $note['Notes']['id']?>">
                                        <p>
                                            <?php echo $note['Notes']['note']?>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <?php echo $this->Form->create('IdentityNote'); ?>
                                <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $note['IdentityNote']['id'])); ?>
                                <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('Delete Note'), array('class' => 'btn btn-danger btn-group-justified', 'type' => 'submit')); ?>
                                <?php } ?>
                                <?php echo $this->Form->end(); ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <?php if ($page != 1) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'identityNotes/' . base64_encode($idIdentity) . '/' . ($page - 1))); ?>" class="btn btn-primary"><?php echo __('Previous', true); ?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if (count($notes) > 0) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'identityNotes/' . base64_encode($idIdentity) . '/' . ($page + 1))); ?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true); ?></a>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var modalID = "";
        <?php
            foreach ($notes as $note){
                $modalIDLink  = 'note-link-' . $note['Notes']['id'];
                $modalID  = 'note-modal-' . $note['Notes']['id'];

                echo "$( \"#". $modalIDLink . "\" ).click(function () {
                        $( \"#". $modalID . "\" ).dialog({
                            modal: true,
                            buttons: {
                              Ok: function() {
                                $( this ).dialog(\"close\" );
                              }
                            }
                        });


                    });\n";
                    
            }
        ?>
        
    });
</script>