<?php
echo $this->Html->css('colorbox');
echo $this->Html->script('jquery.colorbox-min');
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?> - <?php echo $identityName ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Videos', true); ?>
            </li>
        </ol>
    </div> 

    <div class="btn-group pull-right" style="padding-bottom: 10px;">
        <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
            <i class="fa fa-file-image-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
            <i class="fa fa-file-video-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
            <i class="fa fa-file-text-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('family'); ?>">
            <i class="fa fa-group"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view fights'); ?>">
            <i class="fa fa-eye"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
            <i class="fa fa-pencil"></i>
        </a>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>"><?php echo __('All Videos', true); ?></a></li>
            <li class="active"><a href="#"><?php echo __('Search Videos', true); ?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'identityVideos/' . base64_encode($idIdentity))); ?>"><?php echo __('Identity Videos', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group input-group">
            <input type="text" class="form-control" id="search-keyword" placeholder="<?php echo __('keyword', true); ?>">
            <span class="input-group-btn">
                <button class="btn btn-primary" id="search-images" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>

<div class="row">
    <?php
    if ($keywordData != null) {
        if (count($videos) == 0) {
            echo '<div class="col-md-12"><h3>' . __('no results found', true) . '</h3></div><br>';
        }
    }
    ?>
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Video', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($videos as $video) { ?>
                        <tr>
                            <td>
                                <a href="#video-player-<?php echo $video['Video']['id'] ?>" class="open-video"><?php echo $video['Video']['title'] ?></a>
                                <div style="display: none;">
                                    <div id="video-player-<?php echo $video['Video']['id'] ?>">
                                        <video width="420" height="315" controls>
                                            <source src="<?php echo $video['Video']['mp4'] ?>" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <?php echo $this->Form->create('IdentitiesVideo'); ?>
                                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $idIdentity)); ?>
                                <?php echo $this->Form->input('videos_id', array('type' => 'hidden', 'value' => $video['Video']['id'])); ?>
                                <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('Add Video'), array('class' => 'btn btn-primary btn-group-justified', 'type' => 'submit')); ?>
                                <?php } ?>
                                <?php echo $this->Form->end(); ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <?php if ($page != 1) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'searchVideos/' . base64_encode($idIdentity) . '/' . base64_encode($keywordData) . '/' . ($page - 1))); ?>" class="btn btn-primary"><?php echo __('Previous', true); ?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if (count($videos) > 0) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'searchVideos/' . base64_encode($idIdentity) . '/' . base64_encode($keywordData) . '/' . ($page + 1))); ?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true); ?></a>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#search-images').click(function(event) {
            var keyword = Base64.encode($('#search-keyword').val());
            var loadsearch = "<?php echo $this->Html->url(array('action' => 'searchVideos/' . base64_encode($idIdentity))) ?>/";
            window.location.replace(loadsearch + keyword);
        });
        $('.open-video').colorbox({
            inline: true
        });
    });
</script>