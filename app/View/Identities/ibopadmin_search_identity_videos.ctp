<?php
    echo "<h3>Showing " . count($videosArray) . " search  results for keyword: " . $keyword . "</h3>";
?>

<ul class="list-group">
        <?php
        foreach ($videosArray as $theVideos)
        {
            $find = "Robinson";                                
        ?>
                <li class="list-group-item" id="<?php echo $theVideos['IdentitiesVideo']['id']?>">                      
                    <i class="fa fa-bars"></i>
                    <a href="#video-player-<?php echo $theVideos['Videos']['id'] ?>" class="open-video"><?php echo $theVideos['Videos']['title'] ?></a>
                    <div style="display: none;">
                        <div id="video-player-<?php echo $theVideos['Videos']['id'] ?>">
                            <video width="420" height="315" controls>
                                <source src="<?php echo $theVideos['Videos']['mp4'] ?>" type="video/mp4">
                            </video>
                        </div>
                    </div>                                        
                    <?php echo $this->Form->create('IdentitiesVideo'); ?>
                    <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $theVideos['IdentitiesVideo']['id'])); ?>
                    <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                    <?php 
                        echo $this->Form->button(__('Delete Video'), array('class' => 'btn btn-danger', 'type' => 'submit')); 
                    ?>
                    <?php } ?>
                    <?php echo $this->Form->end(); ?>
                </li>
         <?php
        }
     ?>
</ul>

<?php
    echo $this->Html->css('colorbox');
    echo $this->Html->script('jquery.colorbox-min');
?>

<a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'searchVideos/' . base64_encode($identity['Identity']['id']))); ?>"><?php echo __('Return to Identity Videos', true); ?></a>

<script type="text/javascript">
    $(document).ready(function() {
        $('.open-video').colorbox({
            inline: true
        });
    });
    
</script>