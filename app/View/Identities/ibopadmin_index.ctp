<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-group"></i> <?php echo __('Persons', true) ?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="input-group">
            <input type="text" class="form-control search-identity" placeholder="<?php echo __('Search'); ?>">
            <span class="input-group-btn"><a class="btn btn-primary"><i class="fa fa-search"></i></a></span>
            
            <a class="btn btn-primary pull-right" id="botonBusquedaTotal">
                <?php echo __('Display all results', true); ?>
            </a>
            
        </div>

    </div>
    <div class="col-lg-6">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary pull-right">
            <?php echo __('add Person', true); ?>
        </a>
        <p>&emsp;</p>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true); ?></th>
                        <th><?php echo __('principal ID', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($identities as $identity) { ?>
                        <tr>
                            <td><?php echo $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'] ?></td>
                            <td>
                                <?php
                                if (isset($identity['IdentityId'][0]['type'])) {
                                    echo $identity['IdentityId'][0]['type'] . ' - ' . $identity['IdentityId'][0]['type_id'];
                                } else {
                                    echo __('The person has no principal id', true);
                                }
                                ?>
                            </td>
                            <td>
                    <center>
                        <div class="btn-group">
                            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($identity['Identity']['id']))) ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Add images to the person', true); ?>">
                                <i class="fa fa-file-image-o"></i>
                            </a>
                            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($identity['Identity']['id']))) ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Add video to the person', true); ?>">
                                <i class="fa fa-file-video-o"></i>
                            </a>
                            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($identity['Identity']['id']))) ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Add notes to the person', true); ?>">
                                <i class="fa fa-file-text-o"></i>
                            </a>
                            <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($identity['Identity']['id']))) ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Add family to the person', true); ?>">
                                <i class="fa fa-group"></i>
                            </a>
                            <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($identity['Identity']['id']))) ?>" class="btn btn-primary tooltip-button" title="<?php echo __('list of fights', true); ?>">
                                <i class="fa fa-eye"></i>
                            </a>                                                                                
                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($identity['Identity']['id'], $identity['Identity']['fullname'])) ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Professional record', true); ?>">
                                <i class="fa fa-info-circle"></i>
                            </a>                            
                            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($identity['Identity']['id']))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Edit', true); ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </div>
                    </center>
                    </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php
        echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->numbers(array(
            'before' => '',
            'after' => '',
            'separator' => '',
            'tag' => 'li',
            'currentClass' => 'active',
            'currentTag' => 'a',
        ));
        echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));
        ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        
        $("#botonBusquedaTotal").hide();
        var terminoBusqueda = "";
        
        $('.search-identity').autocomplete({
            minLength: 3,
            source: function(request, response) {
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data) {
                    console.log(data);
                    console.log(data.length);
                    if (data.length==20)
                    {
                        $("#botonBusquedaTotal").show();
                        console.log("Entro al if");
                        terminoBusqueda = Base64.encode(request.term);
                        console.log(loadIdentities);
                        $("#botonBusquedaTotal").attr("href", "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'seeAllResults'));?>/"+terminoBusqueda);
                        
                    }
                    else
                    {
                        $("#botonBusquedaTotal").hide();
                    }
                    
                    response(data);
                });
            },
            focus: function(event, ui) {
                $('.search-identity').val(ui.item.label);
                return false;
            },
            select: function(event, ui) {
                var editIdentity = "<?php echo $this->Html->url(array('action' => 'edit')) ?>/" + Base64.encode(ui.item.id);
                ;
                window.location.replace(editIdentity);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
        };
    });
</script>