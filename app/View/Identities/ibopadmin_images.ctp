<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?> - <?php echo $identityName ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Images', true); ?>
            </li>
        </ol>
    </div> 

    <div class="btn-group pull-right" style="padding-bottom: 10px;">
        <a href="<?php echo $this->Html->url('/Person/'.$this->getUrlPerson->getUrl($idIdentity,$identityName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
            <i class="fa fa-desktop"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
            <i class="fa fa-file-image-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
            <i class="fa fa-file-video-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
            <i class="fa fa-file-text-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('family'); ?>">
            <i class="fa fa-group"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view fights'); ?>">
            <i class="fa fa-eye"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
            <i class="fa fa-pencil"></i>
        </a>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#"><?php echo __('All Images', true); ?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'searchImages/' . base64_encode($idIdentity))); ?>"><?php echo __('Search Images', true); ?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'identityImages/' . base64_encode($idIdentity))); ?>"><?php echo __('Identity Images', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <?php foreach ($images as $image) { ?>
        <div class="col-sm-6 col-md-4" style="margin-bottom: 20px;">
            <div class="thumbnail">
                <img src="<?php echo $this->Html->url('/files/img/' . $image['Image']['url']); ?>" style="height: 200px;">
                <div class="caption">
                    <div class="form-conted" id="form-<?php echo $image['Image']['id']?>" style="display: none;">
                        
                    </div>
                    
                  
                    <?php echo $this->Form->create('IdentitiesImage'); ?>
                    <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $idIdentity)); ?>
                    <?php echo $this->Form->input('images_id', array('type' => 'hidden', 'value' => $image['Image']['id'])); ?>
                    <div id="button-<?php echo $image['Image']['id']?>" class="btn-group" style="left: 30%">
                            <a class="btn btn-info display-form" target-form="form-<?php echo $image['Image']['id']?>" id-image="<?php echo $image['Image']['id']?>">
                                <i class="fa fa-pencil"></i> <?php echo __('Edit Image', true);?>
                            </a>
                            <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                <?php echo $this->Form->button(__('Add Image'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                            <?php } ?>
                        </div>
                    <?php echo $this->Form->end(); ?>
                    
                    
                    
                    
                    
                </div>
            </div>
        </div>
    <?php } ?>
    <?php
    if (count($images) == 0) {
        echo '<div class="col-md-12"><h3>' . __('no results found', true) . '</h3></div>';
    }
    ?>
</div><br>

<div class="row">
    <div class="col-md-3">
        <?php if ($page != 1) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity) . '/' . ($page - 1))); ?>" class="btn btn-primary"><?php echo __('Previous', true); ?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if (count($images) > 0) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity) . '/' . ($page + 1))); ?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true); ?></a>
        <?php } ?>
    </div>

</div>

<script type="text/javascript">
    
    $(document).ready(function() {
        
        $('.display-form').click(function(event) {
            /*event.preventDefault();
            var elementDisplay = '#' + $(this).attr('target-form');
            $(this).parent().fadeOut(function(){
                $(elementDisplay).slideDown();
            });*/
            var idImage = $(this).attr('id-image');
            var urlForm = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'formImage'));?>/" + idImage;
            $.post(urlForm, function(data){
                $('#form-' + idImage).html(data);
                $('#form-' + idImage).toggle('slow');
                $('#button-' + idImage).fadeOut();
            });
        });
        
    });

</script>
