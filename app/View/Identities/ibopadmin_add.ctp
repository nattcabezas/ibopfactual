<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i> <?php echo __('Basic Person Information', true);?></h3>
            </div>
            <div class="panel-body">
			<?php echo $this->Form->create('Identity'); ?>
                                
                                <div class="form-group">
					<label><?php echo __('Source:', true);?></label>
					<?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $source, 'required' => true )); ?>
				</div>
                
				<div class="form-group">
					<label><?php echo __('Name:', true);?></label>
					<?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'required' => true, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">
					<label><?php echo __('Last Name:', true);?></label>
					<?php echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">
					<label><?php echo __('Birth Name:', true);?></label>
					<?php echo $this->Form->input('birth_name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">					
                                        <label><?php echo __('Height:', true);?></label>
                                        <?php echo $this->Form->input('height', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">
					<label><?php echo __('Reach:', true);?></label>
					<?php echo $this->Form->input('reach', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">
					<label><?php echo __('Gender:', true);?></label>
					<?php echo $this->Form->input('gender', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => array('U' => 'Unknown','M' => 'Male', 'F' => 'Female','D' => 'Default',), 'empty' => array(null => 'Select gender'), 'required' => true )); ?>
				</div>
                                <div class="form-group">
					<label><?php echo __('Stance:', true);?></label>
                                        <?php $stanceOptions = array('orthodox' => 'orthodox', 'southpaw' => 'southpaw');?>
					<?php echo $this->Form->input('stance', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $stanceOptions, 'empty' => array('' => 'Select Stance'))); ?>
				</div>
                                <div class="form-group">
					<label><?php echo __('Description (SEO):', true);?></label>
					<?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>                                				
				<div class="form-group">
                	<?php echo $this->Form->button(__('Add Person and continue'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                </div>

			<?php echo $this->Form->end(); ?>
		</div>
		</div>
	</div>
</div>
