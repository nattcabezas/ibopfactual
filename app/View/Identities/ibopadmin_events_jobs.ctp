<?php 
    echo $this->Html->script('tablesorter/jquery.tablesorter');
    echo $this->Html->script('tablesorter/tables');
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?> - <?php echo $identityName ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('List of Fights', true); ?>
            </li>
        </ol>

        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('family'); ?>">
                <i class="fa fa-group"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view fights'); ?>">
                <i class="fa fa-eye"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>


    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>"><?php echo __('List of Fights', true); ?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'fightJobs/' . base64_encode($idIdentity))); ?>"><?php echo __('Fight Jobs', true); ?></a></li>
            <li class="active"><a href="#"><?php echo __('Events Jobs', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Event', true); ?></th>
                        <th><?php echo __('Date', true); ?></th>
                        <th><?php echo __('Job', true); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($eventsJobs as $jobs) { ?>
                        <tr>
                            <td>
                                <a href="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'edit/' . base64_encode($jobs['Events']['id']))); ?>"><?php echo $jobs['Events']['name'] ?></a>
                            </td>
                            <td>
                                <?php echo date('F j, Y', strtotime($jobs['Events']['date'])); ?>
                            </td>
                            <td>
                                <?php echo $jobs['EventsJob']['type'] ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>