<div class="row">
    <?php
        //debug($identities);
    ?>
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Showing all results for search "' . $searchCriteria . "\"", true); ?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-group"></i> <?php echo __('Persons', true) ?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $contador = 0;?>
                    <?php foreach ($identities as $identity) { ?>
                        <tr>
                            <td>
                                <a id="<?php echo "row" . $contador; ?>" href=""> 
                                <script>
                                    var editIdentity = "<?php echo $this->Html->url(array('action' => 'edit')) ?>/" + Base64.encode("<?php echo $identity['id']?>");
                                    $("#row"+"<?php echo $contador; ?>").attr("href", editIdentity);
                                </script>
                                    <?php 
                                        echo $contador . "-) " . $identity['label'] ;
                                        $contador++;        
                                    ?>
                                </a>
                                
                            </td>
                        </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">    
    $(document).ready(function() {
        function asignarURL(rowID)
        {
            console.log("El row id es="+rowID);
        }
    
    });
</script>



    