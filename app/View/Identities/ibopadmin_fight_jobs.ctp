<?php 
    echo $this->Html->script('tablesorter/jquery.tablesorter');
    echo $this->Html->script('tablesorter/tables');
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?> - <?php echo $identityName ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('List of Fights', true); ?>
            </li>
        </ol>

        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('family'); ?>">
                <i class="fa fa-group"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view fights'); ?>">
                <i class="fa fa-eye"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>


    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>"><?php echo __('List of Fights', true); ?></a></li>
            <li class="active"><a href="#"><?php echo __('Fight Jobs', true); ?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'eventsJobs/' . base64_encode($idIdentity))); ?>"><?php echo __('Events Jobs', true); ?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <h2>Jobs</h2>
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Fight', true); ?></th>
                        <th><?php echo __('Job', true); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fighterJobs as $jobs) { ?>
                        <tr>
                            <td>
                                <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($jobs['Fights']['id']))); ?>"><?php echo $jobs['Fights']['title'] ?></a>
                            </td>
                            <td>
                                <?php echo $jobs['Jobs']['name'] ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <?php if ($referee) { ?>
            <div class="table-responsive">
                <h2>Referee</h2>
                <table class="table table-striped table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th><?php echo __('Fight', true); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($referee as $fight) { ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights']['id']))); ?>">
                                        <?php echo $fight['Fights']['title'] ?>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if ($ringAnnouncer) { ?>
            <div class="table-responsive">
                <h2>Ring Announcer</h2>
                <table class="table table-striped table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th><?php echo __('Fight', true); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($ringAnnouncer as $fight) { ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights']['id']))); ?>">
                                        <?php echo $fight['Fights']['title'] ?>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if ($judge) { ?>
            <div class="table-responsive">
                <h2>Judge</h2>
                <table class="table table-striped table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th><?php echo __('Fight', true); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($judge as $fight) { ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights']['id']))); ?>">
                                        <?php echo $fight['Fights']['title'] ?>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if ($inspector) { ?>
            <div class="table-responsive">
                <h2>Inspector</h2>
                <table class="table table-striped table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th><?php echo __('Fight', true); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($inspector as $fight) { ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights']['id']))); ?>">
                                        <?php echo $fight['Fights']['title'] ?>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if ($timekeeper) { ?>
            <div class="table-responsive">
                <h2>Timekeeper</h2>
                <table class="table table-striped table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th><?php echo __('Fight', true); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($timekeeper as $fight) { ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights']['id']))); ?>">
                                        <?php echo $fight['Fights']['title'] ?>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>

    </div>
</div>
