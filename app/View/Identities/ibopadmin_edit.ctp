<style type="text/css">
    .panel-body{
        display: none;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Persons', true); ?> - <?php echo $identityName ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-group"></i> 
                    <?php echo __('Persons', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true); ?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
             <a href="<?php echo $this->Html->url('/Person/'.$this->getUrlPerson->getUrl($idIdentity,$identityName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
                <i class="fa fa-desktop"></i>
            </a>
            <?php if($user['Role']['manage'] == 1 ){ ?>
                <?php $locked = $identity['Identity']['locked'] == 1 ? '0' : '1'; ?>
                <a href="<?php echo $this->Html->url(array('action' => 'lock/' . base64_encode($idIdentity) . '/' . $locked)); ?>" class="btn btn-primary tooltip-button">
                    <?php if( $identity['Identity']['locked'] == 1){ ?>
                        <i class="fa fa-lock"></i>
                    <?php }else{ ?>
                        <i class="fa fa-unlock-alt"></i>
                    <?php }?>
                        
                </a>
            <?php } ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'family/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('family'); ?>">
                <i class="fa fa-group"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'list/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('view fights'); ?>">
                <i class="fa fa-eye"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idIdentity))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>


    </div>
</div>

<!-- basic information / birth information -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#basic-panel"><i class="fa fa-group"></i> <?php echo __('Basic Information', true); ?></a></h3>
            </div>
            <div class="panel-body" id="basic-panel">
                <?php echo $this->Form->create('Identity'); ?>

                <?php echo $this->Form->input('id', array('value' => $identity['Identity']['id'])); ?>

                <div class="form-group">
                    <label><?php echo __('Source:', true); ?></label>
                    <?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $source, 'default' => $identity['Identity']['sources_id'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true ) ); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Name:', true); ?></label>
                    <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['Identity']['name'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true ) ); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Last Name:', true); ?></label>
                    <?php echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['Identity']['last_name'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true )); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Birth Name:', true); ?></label>
                    <?php echo $this->Form->input('birth_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['Identity']['birth_name'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true )); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Height:', true); ?></label>
                    <?php echo $this->Form->input('height', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['Identity']['height'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Reach:', true); ?></label>
                    <?php echo $this->Form->input('reach', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['Identity']['reach'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true )); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Gender:', true); ?></label>
                    <?php echo $this->Form->input('gender', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => array('D' => 'Default', 'M' => 'Male', 'F' => 'Female'), 'default' => $identity['Identity']['gender'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true )); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Stance:', true); ?></label>
                    <?php $stanceOptions = array('orthodox' => 'orthodox', 'southpaw' => 'southpaw'); ?>
                    <?php echo $this->Form->input('stance', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $stanceOptions, 'empty' => array('' => 'Select Stance'), 'default' => $identity['Identity']['stance'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true ) ); ?>
                </div>
                <div class="form-group">
                    <label><?php echo __('Description (SEO):', true);?></label>
                    <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['Identity']['description'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true ) ); ?>
                </div>
                
                <div class="form-group">
                      <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                        <?php echo $this->Form->button(__('save basic information'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                     <?php } ?>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#birth-panel"> <i class="fa fa-group"></i> <?php echo __('Birth Information', true); ?></a></h3>
            </div>
            <div class="panel-body" id="birth-panel">
                <?php echo $this->Form->create('IdentityBirth', array('role' => 'form', 'url' => array('controller' => 'Identities', 'action' => 'saveBirth'))); ?>

                <?php echo $this->Form->input('id', array('value' => $identity['IdentityBirth']['id'])); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'], )); ?>
                <div class="form-group">
                    <label><?php echo __('Date:', true); ?></label>
                    <?php echo $this->Form->input('date', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control form-date', 'value' => $identity['IdentityBirth']['date'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <?php echo $this->Form->input('countries_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['IdentityBirth']['countries_id'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                <div class="form-group input-group">
                    <span class="input-group-addon" id="birth-flag-contry" style="height: 34px;">&nbsp;&nbsp;&nbsp;</span>
                    <?php echo $this->Form->input('contries_label', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Country', true), 'readonly' => $identity['Identity']['locked'] == 0 ? false : true )); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('State:', true); ?></label>
                    <div id="birth-states"></div>
                </div>

                <div class="form-group">
                    <label><?php echo __('City:', true); ?></label>
                    <div id="birth-cities"></div>
                </div>

                <div class="form-group">
                    <label><?php echo __('Comment:', true); ?></label>
                    <?php echo $this->Form->input('comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'textarea', 'value' => $identity['IdentityBirth']['comments'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <div class="form-group">
                    <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                        <?php echo $this->Form->button(__('Save Birth'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        <a href="#" class="btn btn-danger deleteCountry" type-delte="birth">Delete Country</a>
                    <?php } ?>    
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<!-- residence information / death information -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i><a href="#" class="display-panel" target-display="#residence-panel"> <?php echo __('Residence Information', true); ?></a></h3>
            </div>
            <div class="panel-body" id="residence-panel">
                <?php echo $this->Form->create('IdentityResidence', array('role' => 'form', 'url' => array('controller' => 'Identities', 'action' => 'saveRecidence'))); ?>

                <?php echo $this->Form->input('id', array('value' => $identity['IdentityResidence']['id'])); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <?php echo $this->Form->input('countries_id', array('type' => 'hidden', 'value' => $identity['IdentityResidence']['countries_id'])); ?>
                <div class="form-group input-group">
                    <span class="input-group-addon" id="residence-flag-contry" style="height: 34px;">&nbsp;&nbsp;&nbsp;</span>
                    <?php echo $this->Form->input('contries_label', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Country', true), 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('State:', true); ?></label>
                    <div id="residence-states"></div>
                </div>

                <div class="form-group">
                    <label><?php echo __('City:', true); ?></label>
                    <div id="residence-cities"></div>
                </div>

                <div class="form-group">
                    <label><?php echo __('Comment:', true); ?></label>
                    <?php echo $this->Form->input('comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'textarea', 'value' => $identity['IdentityResidence']['comments'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <div class="form-group">
                     <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                        <?php echo $this->Form->button(__('Save Residence'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>
                        <a href="#" class="btn btn-danger deleteCountry" type-delte="residence">Delete Country</a>
                    <?php  } ?>    
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i><a href="#" class="display-panel" target-display="#death-panel"> <?php echo __('Death Information', true); ?></a></h3>
            </div>
            <div class="panel-body" id="death-panel">
                <?php echo $this->Form->create('IdentityDeath', array('role' => 'form', 'url' => array('controller' => 'Identities', 'action' => 'saveDeath'))); ?>

                <?php echo $this->Form->input('id', array('value' => $identity['IdentityDeath']['id'])); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <div class="form-group">
                    <label><?php echo __('Date:', true); ?></label>
                    <?php echo $this->Form->input('date', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control form-date', 'value' => $identity['IdentityDeath']['date'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <?php echo $this->Form->input('countries_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $identity['IdentityDeath']['countries_id'])); ?>
                <div class="form-group input-group">
                    <span class="input-group-addon" id="death-flag-contry" style="height: 34px;">&nbsp;&nbsp;&nbsp;</span>
                    <?php echo $this->Form->input('contries_label', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Country', true), 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('State:', true); ?></label>
                    <div id="death-states"></div>
                </div>

                <div class="form-group">
                    <label><?php echo __('City:', true); ?></label>
                    <div id="death-cities"></div>
                </div>

                <div class="form-group">
                    <label><?php echo __('Comment:', true); ?></label>
                    <?php echo $this->Form->input('comments', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'textarea', 'value' => $identity['IdentityDeath']['comments'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true)); ?>
                </div>

                <div class="form-group">
                     <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                        <?php echo $this->Form->button(__('Save Death'), array('class' => 'btn btn-info', 'type' => 'submit')); ?>
                        <a href="#" class="btn btn-danger deleteCountry" type-delte="death">Delete Country</a>
                    <?php } ?>    
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<!-- Nicknames / Alias -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i><a href="#" class="display-panel" target-display="#nicknames-panel"> <?php echo __('Nicknames', true); ?></a></h3>
            </div>
            <div class="panel-body" id="nicknames-panel">
                <div class="row">
                    <div class="col-lg-12">
                         <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                            <a href="#" class="btn btn-success pull-right" id="addNickname" data-toggle="modal" data-target="#saveNickname">
                                <?php echo __('add Nickname', true); ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-striped tablesorter">
                                <thead>
                                    <tr>
                                        <th><?php echo __('Nickname', true); ?></th>
                                        <th>&emsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($identity['IdentityNickname'] as $nickname) { ?>
                                        <tr>
                                            <td><?php echo $nickname['nickname']; ?></td>
                                            <td>
                                    <center>
                                        <div class="btn-group">
                                            <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                                <a href="#" class="btn btn-success edit-nickname" id-nickname="<?php echo $nickname['id'] ?>" data-toggle="modal" data-target="#saveNickname">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="<?php echo $this->Html->url(array('action' => 'deleteNickname/' . $nickname['id'] . '/' . $identity['Identity']['id'])); ?>" class="btn btn-success">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </center>
                                    </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i><a href="#" class="display-panel" target-display="#alias-panel"> <?php echo __('Alias', true); ?></a></h3>
            </div>
            <div class="panel-body" id="alias-panel">
                <div class="row">
                    <div class="col-lg-12">
                         <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                            <a href="#" class="btn btn-success pull-right" id="addAlias" data-toggle="modal" data-target="#saveAlias">
                                <?php echo __('add Alias', true); ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-striped tablesorter">
                                <thead>
                                    <tr>
                                        <th><?php echo __('Alias', true); ?></th>
                                        <th>&emsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($identity['IdentityAlias'] as $alias) { ?>
                                        <tr>
                                            <td><?php echo $alias['alias']; ?></td>
                                            <td>
                                    <center>
                                        <div class="btn-group">
                                            <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                                <a href="#" class="btn btn-success edit-alias" id-alias="<?php echo $alias['id'] ?>" data-toggle="modal" data-target="#saveAlias">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="<?php echo $this->Html->url(array('action' => 'deleteAlias/' . $alias['id'] . '/' . $identity['Identity']['id'])); ?>" class="btn btn-success">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </center>
                                    </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- contact information / id's information -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i><a href="#" class="display-panel" target-display="#contact-panel"> <?php echo __('Contact', true); ?></a></h3>
            </div>
            <div class="panel-body" id="contact-panel">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                            <a href="#" class="btn btn-warning pull-right" data-toggle="modal" data-target="#saveContact" id="addContact">
                                <?php echo __('add contact', true); ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-striped tablesorter">
                                <thead>
                                    <tr>
                                        <th><?php echo __('Type', true); ?></th>
                                        <th><?php echo __('Contact', true); ?></th>
                                        <th>&emsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($identity['IdentityContact'] as $contact) { ?>
                                        <tr>
                                            <td><?php echo $contact['type']; ?></td>
                                            <td><?php echo $contact['url']; ?></td>
                                            <td>
                                    <center>
                                        <div class="btn-group">
                                            <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                                <a href="#" class="btn btn-warning edit-contact" id-contact="<?php echo $contact['id'] ?>" data-toggle="modal" data-target="#saveContact">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="<?php echo $this->Html->url(array('action' => 'deleteContact/' . $contact['id'] . '/' . $identity['Identity']['id'])); ?>" class="btn btn-warning">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </center>
                                    </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i><a href="#" class="display-panel" target-display="#ids-panel"> <?php echo __('Person ID\'s', true); ?></a></h3>
            </div>
            <div class="panel-body" id="ids-panel">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                            <a href="#" class="btn btn-warning pull-right" id="addIds" data-toggle="modal" data-target="#saveIds">
                                <?php echo __('add ID', true); ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-striped tablesorter">
                                <thead>
                                    <tr>
                                        <th><?php echo __('Type', true); ?></th>
                                        <th><?php echo __('ID', true); ?></th>
                                        <th>&emsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($identity['IdentityId'] as $ids) { ?>
                                        <tr>
                                            <td><?php echo $ids['type'] ?></td>
                                            <td><?php echo $ids['type_id'] ?></td>
                                            <td>
                                    <center>
                                        <div class="btn-group">
                                            <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                                <a href="#" class="btn btn-warning edit-ids" id-ids="<?php echo $ids['id'] ?>" data-toggle="modal" data-target="#saveIds">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="<?php echo $this->Html->url(array('action' => 'deleteIds/' . $ids['id'] . '/' . $identity['Identity']['id'])); ?>" class="btn btn-warning">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </center>
                                    </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- IdentityBiography -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-group"></i><a href="#" class="display-panel" target-display="#biography-panel"> <?php echo __('add Biography', true); ?></a></h3>
            </div>
            <div class="panel-body" id="biography-panel">
                <?php echo $this->Form->create('IdentityBiography', array('url' => array('controller' => 'Identities', 'action' => 'saveBiography'))); ?>

                <?php echo $this->Form->input('id', array('value' => $identity['IdentityBiography']['id'])); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <div class="form-group">
                    <label><?php echo __('Biography:', true); ?></label>
                    <?php echo $this->Form->input('biography', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea', 'value' => $identity['IdentityBiography']['biography'], 'readonly' => $identity['Identity']['locked'] == 0 ? false : true )); ?>
                </div>

                <div class="form-group">
                    <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                        <?php echo $this->Form->button(__('Add Article'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    <?php } ?>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<!-- Sponsor  -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a href="#" class="display-panel" target-display="#sponsor-panel">
                        <i class="fa fa-group"></i> <?php echo __('Sponsors', true); ?>
                    </a>
                </h3>
            </div>
            <div class="panel-body" id="sponsor-panel">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                            <a href="#" class="btn btn-danger pull-right" data-toggle="modal" data-target="#saveSponsor" id="addSponsor">
                                <?php echo __('add sponsor', true); ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                   <div class="col-lg-12">
                        <table class="table table-striped table-hover table-striped tablesorter">
                            <thead>
                                <tr>
                                    <th><?php echo __('name', true); ?></th>

                                    <th>&emsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sponsors as $sponsor) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $sponsor['Sponsor']['name']; ?>
                                            <input type="hidden" id="sponsor-name-<?php echo $sponsor['Sponsor']['id'] ?>" value="<?php echo $sponsor['Sponsor']['name']; ?>">
                                        </td>
                                        <td>
                                            <center>
                                                <div class="btn-group">
                                                     <?php if( $identity['Identity']['locked'] == 0 ){ ?>
                                                        <a href="#" class="btn btn-danger edit-sponsor" id-sponsor="<?php echo $sponsor['Sponsor']['id'] ?>" data-toggle="modal" data-target="#saveSponsor">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a href="<?php echo $this->Html->url(array('action' => 'deleteSponsor/' . $sponsor['Sponsor']['id']  . '/' . $sponsor['Sponsor']['identities_id'])); ?>" class="btn btn-danger">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                     <?php } ?>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>       
            </div>
        </div>
    </div>
</div>


<!-- nickname modal form -->
<div class="modal fade" id="saveNickname" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('IdentityNickname', array('url' => array('controller' => 'Identities', 'action' => 'saveNicknames'))); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Nickname', true); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('id'); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <div class="form-group">
                    <label><?php echo __('Nickname:', true); ?></label>
                    <?php echo $this->Form->input('nickname', array('label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Description:', true); ?></label>
                    <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>

                <div class="checkbox">
                    <label>
                        <strong><?php echo __('Principal nickname', true); ?></strong>
                        <?php echo $this->Form->input('principal', array('type' => 'checkbox', 'val' => 1, 'label' => false, 'div' => false)); ?>
                    </label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close', true); ?></button>
                <?php echo $this->Form->button(__('save nickname'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<!-- alias modal form -->
<div class="modal fade" id="saveAlias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('IdentityAlias', array('url' => array('controller' => 'Identities', 'action' => 'saveAlias'))); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Alias', true); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('id'); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <div class="form-group">
                    <label><?php echo __('Alias:', true); ?></label>
                    <?php echo $this->Form->input('alias', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Description:', true); ?></label>
                    <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>

                <div class="checkbox">
                    <label>
                        <strong><?php echo __('Principal alias', true); ?></strong>
                        <?php echo $this->Form->input('principal', array('type' => 'checkbox', 'value' => 1, 'label' => false, 'div' => false)); ?>
                    </label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close', true); ?></button>
                <?php echo $this->Form->button(__('save alias'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<!-- contact modal form -->
<div class="modal fade" id="saveContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('IdentityContact', array('url' => array('controller' => 'Identities', 'action' => 'saveContact'))); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="contact_moda"><?php echo __('Contact Information', true); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('id'); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <div class="form-group">
                    <label><?php echo __('Type:', true); ?></label>
                    <?php echo $this->Form->input('type', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Url:', true); ?></label>
                    <?php echo $this->Form->input('url', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close', true); ?></button>
                <?php echo $this->Form->button(__('save contact'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<!-- Sponsor modal form -->
<div class="modal fade" id="saveSponsor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('Sponsor', array('url' => array('controller' => 'Identities', 'action' => 'saveSponsor'))); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="contact_moda"><?php echo __('Sponsors', true); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <div class="form-group">
                    <label><?php echo __('Name:', true); ?></label>
                    <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close', true); ?></button>
                <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<!-- ids modal form -->
<div class="modal fade" id="saveIds" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('IdentityId', array('url' => array('controller' => 'Identities', 'action' => 'saveId'))); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="ids_moda"><?php echo __('IDs Information', true); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('id'); ?>
                <?php echo $this->Form->input('identities_id', array('type' => 'hidden', 'value' => $identity['Identity']['id'])); ?>
                <div class="form-group">
                    <label><?php echo __('Type:', true); ?></label>
                    <?php echo $this->Form->input('type', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $idtypes, 'required' => true)); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('ID:', true); ?></label>
                    <?php echo $this->Form->input('type_id', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)); ?>
                </div>
                
                <div class="form-group" id="panelProfessions" hidden="true">
                    <label><?php echo __('Professions:', true); ?></label><br>
                    <input type="checkbox" id="checkboxReferee" value="Referee">Referee
                    <input type="checkbox" id="checkboxJudge" value="Judge">Judge
                    <input type="checkbox" id="checkboxPromoter" value="Promoter">Promoter
                    <input type="checkbox" id="checkboxMatchmaker" value="Matchmaker">Matchmaker
                    <input type="checkbox" id="checkboxDoctor" value="Doctor">Doctor
                    <input type="checkbox" id="checkboxCornerman" value="Cornerman">Cornerman<br>
                    <input type="checkbox" id="checkboxSparring" value="Sparring">Sparring Partner
                    <input type="checkbox" id="checkboxTrainer" value="Trainer">Trainer
                    <input type="checkbox" id="checkboxManager" value="Manager">Manager
                    <input type="checkbox" id="checkboxInspector" value="Inspector">Inspector
                    <input type="checkbox" id="checkboxAnnouncer" value="Announcer">Ring Announcer<br>
                    <input type="checkbox" id="checkboxBroadcast" value="Broadcast">Broadcast Team
                    <input type="checkbox" id="checkboxCoach" value="Coach">Coach
                    <input type="checkbox" id="checkboxTimekeeper" value="Timekeeper">Timekeeper
                    <input type="checkbox" id="checkboxAuthor" value="Author">Author
                    <input type="checkbox" id="checkboxHistorian" value="Historian">Historian
                    <input type="checkbox" id="checkboxBacker" value="Backer">Backer<br>
                    <input type="checkbox" id="checkboxCutman" value="Cutman">Cutman
                    <input type="checkbox" id="checkboxSponsor" value="Backer">Sponsor
                    <input type="checkbox" id="checkboxComissioner" value="Comissioner">Comissioner
                    
                    
                </div>

                <div class="form-group">
                    <label><?php echo __('Comments:', true); ?></label>
                    <?php echo $this->Form->input('comments', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>

                <div class="checkbox">
                    <label>
                        <strong><?php echo __('Principal ID', true); ?></strong>
                        <?php echo $this->Form->input('is_principal', array('type' => 'checkbox', 'value' => 1, 'label' => false, 'div' => false)); ?>
                    </label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close', true); ?></button>                
                
                <a type="button" class="btn btn-primary" data-dismiss="modal" hidden="true" type="submit" id="buttonSaveUnknown"><?php echo __('Save unknown ID', true); ?></a>
                                
                <?php echo $this->Form->button(__('save ID'), array('class' => 'btn btn-primary', 'type' => 'submit', 'id'=> "buttonSaveAll")); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
                
        $('#IdentityIdTypeId').prop('readonly', false);
        $('#buttonSaveUnknown').hide();
                
        $('#IdentityIdType').on('change', function() {
            $('#IdentityIdTypeId').prop('readonly', false);
            $('#panelProfessions').prop('hidden', true);
            $('#buttonSaveUnknown').hide();
            if ($( "#IdentityIdType option:selected" ).text() != "UNKNOWN")
            {
                $('#buttonSaveAll').show();
            }else if ($( "#IdentityIdType option:selected" ).text() == "UNKNOWN")
            {
                $('#buttonSaveUnknown').show();    
            }
            $('#IdentityIdIsPrincipal').show();
            
        });
        
        
        //Birth coutries auntocomplete
        $("#IdentityBirthContriesLabel").autocomplete({
            minLength: 0,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui) {
                $("#IdentityBirthContriesLabel").val(ui.item.label);
                return false;
            },
            select: function(event, ui) {
                $("#IdentityBirthContriesLabel").val(ui.item.label);
                $('#birth-flag-contry').html("<img src='<?php echo $this->html->url('/img/flags') ?>/" + ui.item.flag + "'>");
                $('#IdentityBirthCountriesId').val(ui.item.id);
                var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates/IdentityBirth')); ?>";
                $('#birth-cities').html("");
                $.post(loadStates, {countries_id: ui.item.id}, function(selectStates) {
                    $('#birth-states').html(selectStates);
                    $('#IdentityBirthStatesId').change(function() {
                        var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityBirth')); ?>";
                        $.post(loadCities, {states_id: $(this).val()}, function(selectCities) {
                            $('#birth-cities').html(selectCities);
                        });
                    });
                });
                return false;
            }
        })
                .data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
                    .append("<a><img src='<?php echo $this->html->url('/img/flags') ?>/" + item.flag + "'> " + item.label + "</a>")
                    .appendTo(ul);
        };

        //death coutries auntocomplete
        $('#IdentityDeathContriesLabel').autocomplete({
            minLength: 0,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui) {
                $("#IdentityDeathContriesLabel").val(ui.item.label);
                return false;
            },
            select: function(event, ui) {
                $("#IdentityDeathContriesLabel").val(ui.item.label);
                $('#death-flag-contry').html("<img src='<?php echo $this->html->url('/img/flags') ?>/" + ui.item.flag + "'>");
                $('#IdentityDeathCountriesId').val(ui.item.id);
                var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates/IdentityDeath')); ?>";
                $('#death-cities').html("");
                $.post(loadStates, {countries_id: ui.item.id}, function(selectStates) {
                    $('#death-states').html(selectStates);
                    $('#IdentityDeathStatesId').change(function() {
                        var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityDeath')); ?>";
                        $.post(loadCities, {states_id: $(this).val()}, function(selectCities) {
                            $('#death-cities').html(selectCities);
                        });
                    });
                });
                return false;
            }
        })
                .data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
                    .append("<a><img src='<?php echo $this->html->url('/img/flags') ?>/" + item.flag + "'> " + item.label + "</a>")
                    .appendTo(ul);
        };

        //residence coutries auntocomplete
        $('#IdentityResidenceContriesLabel').autocomplete({
            minLength: 0,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function(event, ui) {
                $("#IdentityResidenceContriesLabel").val(ui.item.label);
                return false;
            },
            select: function(event, ui) {
                $("#IdentityResidenceContriesLabel").val(ui.item.label);
                $('#residence-flag-contry').html("<img src='<?php echo $this->html->url('/img/flags') ?>/" + ui.item.flag + "'>");
                $('#IdentityResidenceCountriesId').val(ui.item.id);
                var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates/IdentityResidence')); ?>";
                $('#residence-cities').html("");
                $.post(loadStates, {countries_id: ui.item.id}, function(selectStates) {
                    $('#residence-states').html(selectStates);
                    $('#IdentityResidenceStatesId').change(function() {
                        var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityResidence')); ?>";
                        $.post(loadCities, {states_id: $(this).val()}, function(selectCities) {
                            $('#residence-cities').html(selectCities);
                        });
                    });
                });
                return false;
            }
        })
                .data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
                    .append("<a><img src='<?php echo $this->html->url('/img/flags') ?>/" + item.flag + "'> " + item.label + "</a>")
                    .appendTo(ul);
        };

        $('.display-panel').click(function(event) {
            event.preventDefault();
            var panel = $(this).attr('target-display');
            if ($(panel).is(':visible')) {
                $(panel).slideUp(1000);
            } else {
                $(panel).slideDown(1000);
            }
            return false;
        });

        <?php if ($identity['IdentityBirth']['countries_id']) { ?>
            if (($('#IdentityBirthCountriesId').val() != "") || ($('#IdentityBirthCountriesId').val() != null)) {
                var getContryUrl = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountryData'));?>" + "/" + $('#IdentityBirthCountriesId').val();
                $.getJSON(getContryUrl, function(countryData){
                    var flag = '<img src="<?php echo $this->html->url('/img/flags') ?>/' + countryData.flag + '">';
                    $('#IdentityBirthContriesLabel').val(countryData.label);
                    $('#birth-flag-contry').html(flag);
                    var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates/IdentityBirth/' . $identity['IdentityBirth']['states_id'])); ?>";
                    var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityBirth/' . $identity['IdentityBirth']['cities_id'])); ?>";
                    $.post(loadStates, {countries_id: $('#IdentityBirthCountriesId').val()}, function(selectStates) {
                        $('#birth-states').html(selectStates);
                        $('#IdentityBirthStatesId').change(function() {
                            var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityBirth')); ?>";
                            $.post(loadCities, {states_id: $(this).val()}, function(selectCities) {
                                $('#birth-cities').html(selectCities);
                            });
                        });
                    });
                    <?php if ($identity['IdentityBirth']['states_id']) { ?>
                        $.post(loadCities, {states_id: <?php echo $identity['IdentityBirth']['states_id'] ?>}, function(selectCities) {
                            $('#birth-cities').html(selectCities);
                        });
                    <?php } ?>
                });
            }
        <?php } ?>

        <?php if ($identity['IdentityResidence']['countries_id']) { ?>
            if (($('#IdentityResidenceCountriesId').val() != "") || ($('#IdentityResidenceCountriesId').val() != null)) {
                var getContryUrl = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountryData'));?>" + "/" + $('#IdentityResidenceCountriesId').val();
                $.getJSON(getContryUrl, function(countryData){
                    var flag = '<img src="<?php echo $this->html->url('/img/flags') ?>/' + countryData.flag + '">';
                    $('#IdentityResidenceContriesLabel').val(countryData.label);
                    $('#residence-flag-contry').html(flag);
                    var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates/IdentityResidence/' . $identity['IdentityResidence']['states_id'])); ?>";
                    var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityResidence/' . $identity['IdentityResidence']['cities_id'])); ?>";
                    $.post(loadStates, {countries_id: $('#IdentityResidenceCountriesId').val()}, function(selectStates) {
                        $('#residence-states').html(selectStates);
                        $('#IdentityResidenceStatesId').change(function() {
                            var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityResidence')); ?>";
                            $.post(loadCities, {states_id: $(this).val()}, function(selectCities) {
                                $('#residence-cities').html(selectCities);
                            });
                        });
                    });
                    <?php if ($identity['IdentityResidence']['states_id']) { ?>
                        $.post(loadCities, {states_id: <?php echo $identity['IdentityResidence']['states_id'] ?>}, function(selectCities) {
                            $('#residence-cities').html(selectCities);
                        });
                    <?php } ?>
                    
                });
            }
        <?php } ?>

        <?php if ($identity['IdentityDeath']['countries_id']) { ?>
            if (($('#IdentityDeathCountriesId').val() != "") || ($('#IdentityDeathCountriesId').val() != null)) {             
                var getContryUrl = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountryData'));?>" + "/" + $('#IdentityDeathCountriesId').val();
                $.getJSON(getContryUrl, function(countryData){
                    var flag = '<img src="<?php echo $this->html->url('/img/flags') ?>/' + countryData.flag + '">';
                    $('#IdentityDeathContriesLabel').val(countryData.label);
                    $('#death-flag-contry').html(flag);
                    var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates/IdentityDeath/' . $identity['IdentityDeath']['states_id'])); ?>";
                    var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityDeath/' . $identity['IdentityDeath']['cities_id'])); ?>";
                    $.post(loadStates, {countries_id: $('#IdentityDeathCountriesId').val()}, function(selectStates) {
                        $('#death-states').html(selectStates);
                        $('#IdentityDeathStatesId').change(function() {
                            var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities/IdentityResidence')); ?>";
                            $.post(loadCities, {states_id: $(this).val()}, function(selectCities) {
                                $('#death-cities').html(selectCities);
                            });
                        });
                    });
                    <?php if ($identity['IdentityDeath']['states_id']) { ?>
                        $.post(loadCities, {states_id: <?php echo $identity['IdentityDeath']['states_id'] ?>}, function(selectCities) {
                            $('#death-cities').html(selectCities);
                        });
                    <?php } ?>
                });
            }
        <?php } ?>

        $('#addNickname').click(function(event) {
            $('#IdentityNicknameId').val("");
            $('#IdentityNicknameNickname').val("");
            $('#IdentityNicknameDescription').val("");
            $('#IdentityNicknamePrincipal').removeAttr('checked');
        });
        
        $('#IdentityIdType').click(function(event) {
            if ($( "#IdentityIdType option:selected" ).text() == "UNKNOWN")
            {
                $('#buttonSaveAll').hide();
                $('#buttonSaveUnknown').show();
                $('#IdentityIdIsPrincipal').hide();
                
                console.log("ENTRO AL UNKNOWN");
                var lastUnknownID = "<?php echo $this->Html->url(array('action' => 'lastUnknownId')) ?>/";
                var objectLastID;
                
                $.getJSON(lastUnknownID, function getLastUnknownID(lastID){
                        //The lastID object have the information of
                        //the last identity ID saved on the DB
                        console.log("LastID tiene="+lastID);
                        if (!lastID)
                        {
                            $('#IdentityIdTypeId').val("1");
                        }
                        else
                        {
                            $('#IdentityIdTypeId').val(parseInt(lastID)+1);
                        }
                        $('#IdentityIdTypeId').prop('readonly', true);
                        $('#panelProfessions').prop('hidden', false);
                                              
                 });
                 
                
                //TRAER EL ID DE LA BASE DE DATOS
                //SI NO EXISTE, PONER 1
                //SI EXISTE, TRAER EL DATO Y SUMAR 1 
                
            }
        });
        
        /*
        * Checks if the checkboxes of the professions
        * of the unknown ID's are checked or not
        */
        function checkIfChecked(checkBoxName)
        {
            if ($(checkBoxName).is(":checked")==true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        $('#buttonSaveUnknown').click(function(event) {
            event.preventDefault();
            console.log("ENTRO AL SAVE!!!");
            var typeId = $('#IdentityIdTypeId').val();  //el id para el nuevo identity id que se va crear
            var identityID = "<?php echo $identity['Identity']['id'] ?>" //
            var professions = ["#checkboxReferee", "#checkboxJudge", "#checkboxPromoter", "#checkboxMatchmaker", "#checkboxDoctor", "#checkboxCornerman", "#checkboxSparring", "#checkboxTrainer", "#checkboxManager", "#checkboxInspector", "#checkboxAnnouncer", "#checkboxBroadcast", "#checkboxCoach", "#checkboxTimekeeper", "#checkboxAuthor", "#checkboxHistorian", "#checkboxBacker", "#checkboxCutman", "#checkboxSponsor", "#checkboxComissioner"];
            var comments = "";
            
            /*var addEnv = "<?php echo $this->Html->url(array('action' => 'ibopadmin_saveUnknownId', 'full_base' => true)) ?>/"+identityID+"/"+typeId+"/"+comments;
            console.log("El addEnv es="+addEnv);
            $.ajax({
                url:addEnv,
                type: 'POST',
                error: function (xhr, textStatus, errorThrown) {
                     //alert('error ' + (errorThrown ? errorThrown : xhr.status));
                     alert('error ' + xhr.getAllResponseHeaders());
                }
            });*/
            
            for (index = 0; index <= professions.length; index++) {
                
                
                if (checkIfChecked(professions[index])==true)
                {
                    //console.log(checkIfChecked(professions[index]));
                    comments = professions[index].substring(9);
                
                    var addEnv = "<?php echo $this->Html->url(array('action' => 'ibopadmin_saveUnknownId')) ?>/"+identityID+"/"+typeId+"/"+comments;
                    typeId = parseInt(typeId)+1;
                    
                    
                    //Post call to save information on DB
                    //$.post(addEnv);
                    
                    $.ajax({
                       url:addEnv,
                       type: 'POST',
                       beforeSend: function (xhr) {
                            xhr.setRequestHeader("Access-Control-Allow-Origin","*");
                       },
                       success: function () { 
                           location.reload();
                       },
                       error: function (jqXHR, exception,textStatus, errorThrown) {
                           
                       }
                    });
                }
                comments = "";
            }
            return false;
        });
        


        $('.edit-nickname').click(function(event) {
            event.preventDefault();
            var loadnicknames = "<?php echo $this->Html->url(array('action' => 'getNickname')) ?>/" + $(this).attr('id-nickname');
            $.getJSON(loadnicknames, function(nicknamedata) {
                $('#IdentityNicknameId').val(nicknamedata[0].id);
                $('#IdentityNicknameNickname').val(nicknamedata[0].nickname);
                $('#IdentityNicknameDescription').val(nicknamedata[0].description);
                if (nicknamedata[0].principal == 1) {
                    $('#IdentityNicknamePrincipal').prop('checked', true);
                    $('#IdentityNicknamePrincipal').attr('checked', 'checked');
                } else {
                    $('#IdentityNicknamePrincipal').removeAttr('checked');
                }
            });

        });

        $('#addAlias').click(function(event) {
            event.preventDefault();
            $('#IdentityAliasId').val("");
            $('#IdentityAliasAlias').val("");
            $('#IdentityAliasDescription').val("");
            $('#IdentityAliasPrincipal').removeAttr('checked');
        });

        $('.edit-alias').click(function(event) {
            event.preventDefault();
            var loadAlias = "<?php echo $this->Html->url(array('action' => 'getAlias')) ?>/" + $(this).attr('id-alias');
            $.getJSON(loadAlias, function(aliasdata) {
                $('#IdentityAliasId').val(aliasdata[0].id);
                $('#IdentityAliasAlias').val(aliasdata[0].alias);
                $('#IdentityAliasDescription').val(aliasdata[0].description);
                if (aliasdata[0].principal == 1) {
                    $('#IdentityAliasPrincipal').prop('checked', true);
                    $('#IdentityAliasPrincipal').attr('checked', 'checked');
                } else {
                    $('#IdentityAliasPrincipal').removeAttr('checked');
                }
            });
        });

        $('#addContact').click(function(event) {
            event.preventDefault();
            $('#IdentityContactId').val("");
            $('#IdentityContactType').val("");
            $('#IdentityContactUrl').val("");
        });

        $('.edit-contact').click(function(event) {
            event.preventDefault();
            var loadContact = "<?php echo $this->Html->url(array('action' => 'getContact')) ?>/" + $(this).attr('id-contact');
            $.getJSON(loadContact, function(contactdata) {
                $('#IdentityContactId').val(contactdata[0].id);
                $('#IdentityContactType').val(contactdata[0].type);
                $('#IdentityContactUrl').val(contactdata[0].url);
            });
        });
        
        $('.edit-sponsor').click(function(event){
            event.preventDefault();
            var idSponsor = $(this).attr('id-sponsor');
            var nameSponsor = $('#sponsor-name-' + idSponsor).val();
            $('#SponsorId').val(idSponsor);
            $('#SponsorName').val(nameSponsor);
            
        });
        
        $('#addSponsor').click(function(event){
            event.preventDefault();
            $('#SponsorId').val("");
            $('#SponsorName').val("");
        });
        
        $('#addIds').click(function(event) {
            $('#IdentityIdId').val("");
            $('#IdentityIdType').val("");
            $('#IdentityIdTypeId').val("");
            $('#IdentityIdComments').val("");
        });

        $('.edit-ids').click(function(event) {
            event.preventDefault();
            var loadIds = "<?php echo $this->Html->url(array('action' => 'getIds')) ?>/" + $(this).attr('id-ids');
            $.getJSON(loadIds, function(idsData) {
                console.log(idsData);
                $('#IdentityIdId').val(idsData[0].id);
                $('#IdentityIdType').val(idsData[0].type);
                $('#IdentityIdTypeId').val(idsData[0].type_id);
                $('#IdentityIdComments').val(idsData[0].comments);
                if (idsData[0].is_principal == 1) {
                    $('#IdentityIdIsPrincipal').prop('checked', true);
                    $('#IdentityIdIsPrincipal').attr('checked', 'checked');
                } else {
                    $('#IdentityIdIsPrincipal').removeAttr('checked');
                }
            });
        });

        $('.deleteCountry').click(function(event) {
            event.preventDefault();
            var typeDelete = $(this).attr('type-delte');
            var emptyInputsKey = "";

            if (typeDelete === 'birth') {
                emptyInputsKey = 'Birth';
            } else if (typeDelete === 'residence') {
                emptyInputsKey = 'Residence';
            } else if (typeDelete === 'death') {
                emptyInputsKey = 'Death';
            }

            $('#Identity' + emptyInputsKey + 'ContriesLabel').val("");
            $('#Identity' + emptyInputsKey + 'CountriesId').val("");

            $('#' + typeDelete + '-states').html("");
            $('#' + typeDelete + '-cities').html("");

            $('#' + typeDelete + '-flag-contry').html("&nbsp;&nbsp;&nbsp;");

            return false;
        });

    });
</script>