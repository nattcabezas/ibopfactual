<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Weights', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-child"></i> 
                    <?php echo __('Weights', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-child"></i> <?php echo __('add weight', true);?></h3>
            </div>
            <div class="panel-body">
			<?php echo $this->Form->create('Weight'); ?>

				<div class="form-group">
					<label><?php echo __('Name:', true);?></label>
					<?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">
					<label><?php echo __('Organisation:', true);?></label>
					<?php echo $this->Form->input('organisation', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">
					<label><?php echo __('Weight Limit lbs:', true);?></label>
					<?php echo $this->Form->input('limit', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
				</div>

				<div class="form-group">
                	<?php echo $this->Form->button(__('Add weight'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                </div>

			<?php echo $this->Form->end(); ?>
		</div>
		</div>
	</div>
</div>
