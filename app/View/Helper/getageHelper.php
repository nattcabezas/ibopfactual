<?php

/**
 * CakePHP getageHelper
 * @author RudySibaja <neo.nou@gmail.com>
 */
class getageHelper extends AppHelper {

    public function age($birthDate = null, $endDate = null){
        list ($year, $month, $day) = explode('-', $endDate);
        list ($birthYear, $birthMonth, $birthDay) = explode('-', $birthDate);
        
        if($birthMonth > $month){
            $age = $year - $birthYear -1;
        } else {
            if( ($month = $birthMonth) && ($birthDay > $day) ){
                $age = $year - $birthYear -1;
            } else {
                $age = $year - $birthYear;
            }
        }
        
        return $age;
    }
    
    public function getCm($height){
        $tofoot = str_replace("'", ".", $height);
        $foot   = str_replace('"', "", $tofoot);
        return round($foot * 30.48, 2);
    }
    
    public function getReachCm($reach){
        $inch = str_replace('"', "", $reach);
        return round($inch * 2.54, 2);
    }

}
