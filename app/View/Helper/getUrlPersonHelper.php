<?php
/**
 * @author RudySibaja <neo.nou@gmail.com>
 */
class getUrlPersonHelper extends AppHelper {
    
    public function getUrl($id = null, $name = null){
        $name = trim($name);
        $name = strtolower($name);
        $name = $this->accentDelete($name);
        $name = str_replace(" ", "_", $name);
        $name = str_replace("ñ", "nn", $name);
        $name = str_replace("-", "__", $name);
        $name = str_replace(",", "", $name);
        return $id . '-' .$name;
    }
    
    public function getEventUrl($id = null, $name = null){
        $name = trim($name);
        $name = strtolower($name);
        $name = $this->accentDelete($name);
        $name = str_replace(" - ", "_", $name);
        $name = str_replace(" ", "_", $name);
        $name = str_replace("ñ", "nn", $name);
        $name = str_replace("-", "_", $name);
        $name = str_replace(",", "", $name);
        return $id . '-' .$name;
    }
    
    public function getArticletUrl($category = null, $id = null, $name = null){
        $name = trim($name);
        $name = strtolower($name);
        $name = $this->accentDelete($name);
        $name = str_replace(".", "", $name);
        $name = str_replace(" ", "_", $name);
        $name = str_replace("ñ", "nn", $name);
        $name = str_replace("-", "_", $name);
        $name = str_replace(",", "", $name);
        if($category == null){
            $category = "Category";
        }
        return $category . '/' . $id . '-' .$name;
    }
    
    public function accentDelete($name = null){
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$name);
        return $texto;
    }
    public function getPagesUrl($page = null){
        $page = trim($page);
        $page = strtolower($page);
        $page = $this->accentDelete($page);
        $page = str_replace(".", "", $page);
        $page = str_replace(" ", "_", $page);
        $page = str_replace("ñ", "nn", $page);
        $page = str_replace("-","_", $page);
        $page = str_replace(",", "", $page);
        return $page;
    }
}

