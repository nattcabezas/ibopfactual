<?php
/**
 * @author RudySibaja <neo.nou@gmail.com>
 */
class utf8EncodeArrayHelper extends AppHelper {

	/** 
    * Encodes an ISO-8859-1 mixed variable to UTF-8 (PHP 4, PHP 5 compat)
    * @param    mixed    $input An array, associative or simple
    * @param    boolean  $encode_keys optional
    * @return    mixed     ( utf-8 encoded $input)
    */
	public function utf8_encode_mix($input, $encode_keys=false){
        if(is_array($input)){
            $result = array();
            foreach($input as $k => $v){                
                $key = ($encode_keys)? utf8_encode($k) : $k;
                $result[$key] = $this->utf8_encode_mix( $v, $encode_keys);
            }
        } else {
            $result = utf8_encode($input);
        }

        return $result;
    }

}