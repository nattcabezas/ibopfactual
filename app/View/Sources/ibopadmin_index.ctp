<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Sources', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-briefcase"></i> <?php echo __('Sources', true)?></li>
        </ol>
    </div>
    
    <div class="col-lg-2 col-md-offset-10">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Source', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sources as $source){ ?>
                        <tr>
                            <td><?php echo $source['Source']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($source['Source']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
    <?php 
        echo $this->Paginator->numbers(array(
        'before' => '',
        'after' => '',
        'separator' => '',
        'tag' => 'li',
        'currentClass' => 'active',
        'currentTag' => 'a'
        )); 
    ?>
    </ul>
</div>