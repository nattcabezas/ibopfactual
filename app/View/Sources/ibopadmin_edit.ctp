<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Source', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-briefcase"></i> 
                    <?php echo __('Source', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-briefcase"></i> <?php echo __('add Source', true);?></h3>
            </div>
            <div class="panel-body">
				<?php echo $this->Form->create('Source'); ?>
					
					<?php echo $this->Form->input('id');?>
					<div class="form-group">
						<label><?php echo __('Name:', true);?></label>
						<?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
					</div>
					
					<div class="form-group">
						<label><?php echo __('Description:', true);?></label>
						<?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
                		<?php echo $this->Form->button(__('save Source'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                	</div>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
