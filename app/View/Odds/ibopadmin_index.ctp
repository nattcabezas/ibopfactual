<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('ODDS', true);?> - <?php echo $titleName?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-usd"></i> <?php echo __('OODS', true)?></li>
        </ol>
    </div>
    
    <div class="btn-group pull-right" style="padding-bottom: 10px;">
        <a href="<?php echo $this->Html->url('/Fight/'.$this->getUrlPerson->getUrl($idFights,$titleName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
            <i class="fa fa-desktop"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('controller' => 'Fights',  'action' => 'images/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
            <i class="fa fa-file-image-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'videos/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
            <i class="fa fa-file-video-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'notes/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
            <i class="fa fa-file-text-o"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'controller' => 'Odds', 'action' => 'index/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('odds'); ?>">
            <i class="fa fa-usd"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'titles/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Titles'); ?>">
            <i class="fa fa-bookmark"></i>
        </a>
        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
            <i class="fa fa-pencil"></i>
        </a>
    </div>
    
    <div class="col-lg-2 col-md-offset-10">
        <?php if( $fight['Fight']['locked'] == 0 ){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'add/'. base64_encode($idFights))); ?>" class="btn btn-primary">
                <?php echo __('add Odd', true);?>
            </a>
        <?php } ?>
        <p>&emsp;</p>
    </div>
    
    
    
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Source', true);?></th>
                        <th><?php echo __('Fighter 1', true);?></th>
                        <th><?php echo __('Fighter 2', true);?></th>
                        <th><?php echo __('Date', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($odds as $odd){ ?>
                        <tr>
                            <td><?php echo $odd['SourceOdds']['name']?></td>
                            <td><?php echo $odd['Odd']['fighter1_odd']?></td>
                            <td><?php echo $odd['Odd']['fighter2_odd']?></td>
                            <td><?php echo $odd['Odd']['date']?></td>
                            <td>
                                <div class="btn-group">
                                    <a href="<?php echo $this->Html->url(array('action' => 'edit/'.  base64_encode($idFights) . '/' . base64_encode($odd['Odd']['id'])))?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo $this->Html->url(array('action' => 'delete/'.  base64_encode($idFights) . '/' . base64_encode($odd['Odd']['id'])))?>" class="btn btn-primary"><i class="fa fa-trash-o"></i></a>  
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>