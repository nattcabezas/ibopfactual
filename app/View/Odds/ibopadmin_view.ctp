<div class="odds view">
<h2><?php echo __('Odd'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($odd['Odd']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fights'); ?></dt>
		<dd>
			<?php echo $this->Html->link($odd['Fights']['title'], array('controller' => 'fights', 'action' => 'view', $odd['Fights']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Source Odds'); ?></dt>
		<dd>
			<?php echo $this->Html->link($odd['SourceOdds']['name'], array('controller' => 'source_odds', 'action' => 'view', $odd['SourceOdds']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fighter1 Odd'); ?></dt>
		<dd>
			<?php echo h($odd['Odd']['fighter1_odd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fighter2 Odd'); ?></dt>
		<dd>
			<?php echo h($odd['Odd']['fighter2_odd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($odd['Odd']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comment'); ?></dt>
		<dd>
			<?php echo h($odd['Odd']['comment']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Odd'), array('action' => 'edit', $odd['Odd']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Odd'), array('action' => 'delete', $odd['Odd']['id']), array(), __('Are you sure you want to delete # %s?', $odd['Odd']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Odds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Odd'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fights'), array('controller' => 'fights', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fights'), array('controller' => 'fights', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Source Odds'), array('controller' => 'source_odds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Source Odds'), array('controller' => 'source_odds', 'action' => 'add')); ?> </li>
	</ul>
</div>
