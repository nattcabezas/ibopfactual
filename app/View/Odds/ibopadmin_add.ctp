<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('ODDS', true);?> - <?php echo $titleName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index/' . base64_encode($idFights)));?>">
                    <i class="fa fa-usd"></i> 
                    <?php echo __('ODDS', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('controller' => 'Fights',  'action' => 'images/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'videos/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'notes/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('notes'); ?>">
                <i class="fa fa-file-text-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'controller' => 'Odds', 'action' => 'index/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('odds'); ?>">
                <i class="fa fa-usd"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'titles/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Titles'); ?>">
                <i class="fa fa-bookmark"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($idFights))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-usd"></i> <?php echo __('add odd', true);?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('Odd'); ?>
                
                    <?php echo $this->Form->input('fights_id', array('type' => 'hidden', 'value' => $idFights)); ?>

                    <div class="form-group">
                        <label><?php echo __('source odds', true);?></label>
                        <?php echo $this->Form->input('source_odds_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'empty' => array(0 => 'select source'))); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('fighter 1 odd', true);?></label>
                        <?php echo $this->Form->input('fighter1_odd', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('fighter 2 odd', true);?></label>
                        <?php echo $this->Form->input('fighter2_odd', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Date', true);?></label>
                        <?php echo $this->Form->input('date', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control form-date')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Comments', true);?></label>
                        <?php echo $this->Form->input('comment', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $this->Form->button(__('Add Odd'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    </div>
                
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

