<div class="container detail-container">
    <div class="row">
      
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?php
                echo $this->element('fighter-compare-data', array(
                    'fighterData' => $firstFighterData,
                    'fighterPhoto' => $firstFighterPhoto,
                    'fighterRecord' => $firstFighterRecord
                    
                ));
            ?>
            
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            
            <?php if( $secondFighter == null ){ ?>
            
                <h4><?php echo __('Search for persons to compare', true);?></h4>
            
                <form action="#" role="form" id="person-search-compare" method="post" accept-charset="utf-8">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="icon-addon addon-lg">
                                <input type="text" placeholder="Name" class="form-control" name="name">
                                <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="icon-addon addon-lg">
                                <input type="text" placeholder="Last Name" class="form-control" name="last-name">
                                <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <button class="btn btn-primary btn-lg btn-group-justified" id="persons-button-compare" type="submit"><?php echo __('Find!', true);?></button>
                    </div>
                </form>
                
                <div id="content-result"></div>
            
            <?php
                }  else { 
                
                    echo $this->element('fighter-compare-data', array(
                        'fighterData' => $secondFighterData,
                        'fighterPhoto' => $secondFighterPhoto,
                        'fighterRecord' => $secondFighterRecord
                    ));
                } 
            ?>
            
        </div>
        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('#person-search-compare').submit(function(event){
            event.preventDefault();
            var pageLoad = "<?php echo $this->Html->url(array('controller' => 'Comparisons', 'action' => 'searchFighter/' . $firstFighter))?>";
            $.post(pageLoad, $('#person-search-compare').serialize(), function(data){
                $('#content-result').html(data);
            });
        });
        
        
    });
</script>