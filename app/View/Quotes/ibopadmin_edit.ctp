<?php
echo $this->Html->css('fileinput');
echo $this->Html->script('fileinput');
?>
<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Quotes', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-quote-left"></i> 
                    <?php echo __('Quotes', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
    </div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-quote-left"></i> <?php echo __('edit quote', true);?></h3>
            </div>
            <div class="panel-body">

            	<?php echo $this->Form->create('Quote', array('type' => 'file')); ?>
                
                    <?php echo $this->Form->input('id');?>
                
                    <div class="form-group">
                        <label><?php echo __('Author:', true);?></label>
                        <div class="form-group">
                            <?php 
                                $options = array(
                                    '1'  => 'Person', 
                                    '2'  => 'Author'

                                );
                            ?>
                            <?php echo $this->Form->input('via', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options, 'empty' => array('0' => 'select type of author'))); ?>
                        </div>
                    </div>
                    <div class="form-group" id="display-person" style="display: <?php echo $quote['Quote']['identities_id'] != null ? 'block;' : 'none;'; ?>">
                        <label><?php echo __('Person', true);?></label>
                        <?php echo $this->Form->input('labelIdentity', array('value' => $quote['Identity']['name']. ' ' . $quote['Identity']['last_name'], 'label' => false, 'div' => false, 'class' => 'form-control input-identity', 'add-to' => '#QuoteIdentitiesId')); ?>
                        <?php echo $this->Form->input('identities_id', array('type' => 'hidden'))?>
                    </div>
                    <div class="form-group" id="display-author" style="display: <?php echo $quote['Quote']['quotes_by'] != null ? 'block;' : 'none;'; ?>">
                        <label><?php echo __('Author', true);?></label>
                        <?php echo $this->Form->input('quotes_by', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control'))?>
                       
                    </div>
                    <div class="form-group">
                            <label><?php echo __('Image:', true); ?></label>
                            <?php echo $this->Form->input('img_file', array('type' => 'file', 'label' => false, 'div' => false, 'class' => 'form-control file-input')); ?>
                            <?php echo $this->Form->input('img', array('type' => 'hidden')); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Quote:', true);?></label>
                        <?php echo $this->Form->input('quote', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Note:', true);?></label>
                        <?php echo $this->Form->input('note', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>								
                    <div class="form-group">
                        <?php echo $this->Form->button(__('Edit Quote'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    </div>

                <?php echo $this->Form->end(); ?>

            </div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function(){
        $('#QuoteVia').change(function(){
            if($(this).val() == 1){
                $('#display-author').fadeOut(function(){
                    $('#display-person').fadeIn();
                });
                $('#QuoteQuotesBy').val("");
            } else if($(this).val() == 2){
                $('#display-person').fadeOut(function(){
                    $('#display-author').fadeIn();
                });
                $('#QuoteIdentitiesId').val("");
                $('#QuoteLabelIdentity').val("");
            } else {
                $('#display-person').fadeOut();
                $('#display-author').fadeOut();
                $('#QuoteIdentitiesId').val("");
                $('#QuoteLabelIdentity').val("");
                $('#QuoteQuotesBy').val("");
            }
        });
    });
    
    // image edit function
    <?php if ($this->request->data('Quote.img') != null) { ?>
        $('.file-input').fileinput({
            showUpload: false,
            initialPreview: ["<img src='<?php echo $this->Html->url('/files/quotes/' . $this->request->data('Quote.img')); ?>' class='file-preview-image'>"],
            initialCaption: "<?php echo $this->request->data('Quote.img'); ?>"
        });
<?php } else { ?>
        $('.file-input').fileinput({showUpload: false});
<?php } ?>

    $(document).ready(function() {
        $('.note-codable').code($('.form-textarea').val());

        $('#QuoteImgFile').change(function() {
            $('#QuoteImg').val($('.file-caption-name').html());
        });
        $('.fileinput-remove-button').click(function(event) {
            $('#QuoteImg').val("");
        });
        $('.fileinput-remove').click(function() {
            $('#QuoteImg').val("");
        });

    });
</script>
