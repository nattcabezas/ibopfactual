<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Quotes', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-quote-left"></i> <?php echo __('Quotes', true)?></li>
        </ol>
    </div>
</div>

<div class="row">
    
    <div class="col-lg-6">
        <?php echo $this->Form->create('searchQuotes', array('url' => array('controller' => 'Quotes', 'action' => 'searchQuotes'))); ?>
            <div class="col-lg-10">
                <?php echo $this->Form->input('keywork', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
            </div>
            <div class="col-lg-2">
                <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
            </div>
        <?php echo $this->Form->end(); ?>
    </div>
    
    <div class="col-lg-2 col-md-offset-4">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Quotes', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Quote', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($quotes as $quote){ ?>
                        <tr>
                            <td><?php echo $quote['Quote']['quote']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($quote['Quote']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'delete/' . base64_encode($quote['Quote']['id'])));?>" class="btn btn-danger btn-delete">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>