<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Events', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-calendar"></i> <?php echo __('Events', true)?></li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-3">
        <div class="input-group">
            <input type="text" class="form-control search-identity" placeholder="<?php echo __('Search');?>">
            <span class="input-group-btn"><a class="btn btn-primary"><i class="fa fa-search"></i></a></span>
        </div>    
    </div>
    
    <div class="col-lg-6">
        <div class="row">
            <?php echo $this->Form->create('SearchEvent', array('url' => array('controller' => 'Events', 'action' => 'searchEvent'))); ?>
                <div class="col-lg-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn btn-default" type="button">From:</a>
                        </span>
                        <?php echo $this->Form->input('fromDate', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <a class="btn btn-default" type="button">To:</a>
                        </span>
                        <?php echo $this->Form->input('toDate', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    </div>
                </div>
            <?php echo $this->Form->end(); ?>
      </div>
    </div>
    
    <div class="col-lg-2 col-md-offset-1">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Event', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Name', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($events as $event){ ?>
                        <tr>
                            <td><?php echo $event['Event']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($event['Event']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Edit Event', true);?>">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($event['Event']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Location', true); ?>">
                                            <i class="fa fa-globe"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($event['Event']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Images', true); ?>">
                                            <i class="fa fa-file-image-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($event['Event']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Videos', true); ?>">
                                            <i class="fa fa-file-video-o"></i>
                                        </a>
                                        
                                        <a href="<?php echo $this->Html->url(array('action' => 'notes/' . base64_encode($event['Event']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Notes', true); ?>">
                                            <i class="fa fa-file-text-o"></i>
                                        </a>
                                        
                                        <a href="<?php echo $this->Html->url(array('action' => 'listFights/' . base64_encode($event['Event']['id'])));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Fights');?>">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.search-identity').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getEvents'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('.search-identity').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                var editIdentity  = "<?php echo $this->Html->url(array('action' => 'edit'))?>/" + Base64.encode(ui.item.id);;
                window.location.replace(editIdentity);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $( "#SearchEventFromDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "-700:+0",
            onClose: function( selectedDate ) {
                $( "#SearchEventToDate" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $( "#SearchEventToDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "-700:+0",
            onClose: function( selectedDate ) {
                $( "#SearchEventFromDate" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
    });
</script>