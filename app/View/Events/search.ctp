<div class="container search-container">
    
    <div class="row">
        <div class="col-lg-12">
            
            <form action="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'search'));?>" role="form" class="search-person-form" method="post">  
                <div class="col-md-8">

                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="search by date" class="form-control date-search" id="date" name="date-event">
                            <label for="date" class="glyphicon glyphicon-search" rel="tooltip" title="date"></label>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
            
        </div>
    </div>
    
    <?php echo $this->element('banners', array('thisBanner' => $detailBanner)); ?>
    <?php if(count($events) > 0){ ?>
        <table id="events_search" class="responsive responsive-table table_basic">
            <thead>
                <tr>
                    <th></th>
                    <th><?php echo __('Name', true); ?></th>
                    <th><?php echo __('Location', true); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($events as $event){ ?>
                    <tr>
                        <td>
                            <?php if(count($event['EventsImage']) > 0){ ?>
                                <img src="<?php echo $this->Html->url('/files/img/' . $event['EventsImage'][0]['Images']['url']);?>" alt="<?php echo $event['EventsImage'][0]['Images']['description'];?>" title="<?php echo $event['EventsImage'][0]['Images']['title']?>" class="img-responsive img-thumbnail event_img">
                            <?php } else { ?>
                                <img src="<?php echo $this->Html->url('/img/defaults/generic_event.jpg');?>" alt="<?php echo __('event default image');?>" title="<?php echo __('default image');?>" class="img-responsive img-thumbnail event_img">
                            <?php } ?>
                        </td>
                        <td>
                            <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getEventUrl($event['Event']['id'], $event['Event']['name']));?>">
                                <?php echo $event['Event']['name']?>
                            </a>
                        </td>
                        <td>
                            <?php if((isset($event['Countries']['flag'])) && ($event['Countries']['flag'] != "")){ ?>
                                <img src="<?php echo $this->Html->url('/img/flags/' . $event['Countries']['flag']);?>">
                            <?php } ?>
                            <?php
                                if( (isset($event['Cities']['name']))  && ($event['Cities']['name'])){
                                    echo $event['Cities']['name'] . ', ';
                                }
                                if( (isset($event['States']['name']))  && ($event['States']['name'])){
                                    echo $event['States']['name'] . ', ';
                                }
                                if( (isset($event['Countries']['name']))  && ($event['Countries']['name'])){
                                    echo $event['Countries']['name'];
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <h3 class="page-header" style="text-align: center"><?php echo __('No result was found for ') . $searchDate;?></h3>
    <?php } ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){
      
        $('.responsive-table').ngResponsiveTables({
            smallPaddingCharNo: 13,
            mediumPaddingCharNo: 18,
            largePaddingCharNo: 30
        });
    });
</script>