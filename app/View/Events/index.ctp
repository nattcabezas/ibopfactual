<div class="container detail-container">
    <div class="container">
        <div class="row mobile-social-share">
            <div class="col-md-9">
                <h2><?php echo $event['Event']['name'] ?></h2>
            </div>
            <?php
            if ((isset($image['Images']['url'])) && ($image['Images']['url'] != null)) {
                $imageShare = $this->Html->url('/files/img/' . $image['Images']['url']);
                $imageAlt = $image['Images']['title'];
            } else {
                $imageShare = $this->Html->url('/img/defaults/generic_event.jpg');
                $imageAlt = "event default image";
            }
            ?>
            <?php echo $this->element('social-holder', array('shareTitle' => $event['Event']['name'], 'imageShare' => $imageShare)); ?>

        </div>
    </div>

    <div class="col-lg-3">
        <img src="<?php echo $imageShare; ?>" alt="<?php echo $imageAlt; ?>" class="img_entity" width="250">
        <?php echo $this->Qrcode->url('http://ibopfactual.com' . $this->here, array('size' => '250x250')); ?>
    </div>

    <div class="col-lg-9 col-xs-12">
        <table class="table  ">
            <tbody>
                <?php if ($event['Event']['venues_id'] != null) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Venue:', true); ?> </span>
                        </td>
                        <td>
                            <strong>
                                <?php
                                if($venue['Venue']['id']== 13509){
                                     echo __('unknown');
                                }else{  ?>
                                    <a href="<?php echo $this->Html->url('/Venue/' . $this->getUrlPerson->getUrl($venue['Venue']['id'], $venue['Venue']['name'])); ?>">
                                        <?php echo $venue['Venue']['name'] ?>
                                    </a>
                               
                                <?php   }?>
                                
                            </strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ((isset($location['Countries']['id'])) && ($location['Countries']['id'] != null)) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Location:', true); ?> </span>
                        </td>
                        <td>
                            <strong>
                                <?php
                                if ((isset($location['Cities']['id'])) && ($location['Cities']['id'] != null)) {
                                    echo utf8_encode($location['Cities']['name']) . ', ';
                                }
                                if ((isset($location['States']['id'])) && ($location['States']['id'] != null)) {
                                    echo utf8_encode($location['States']['name']) . ', ';
                                }
                                if ((isset($location['Countries']['id'])) && ($location['Countries']['id'] != null)) {
                                    echo utf8_encode($location['Countries']['name']) . ' ';
                                    echo '<img src="' . $this->Html->url('/img/flags/' . $location['Countries']['flag']) . '" alt="' . $location['Countries']['name'] . ' flag">';
                                }
                                ?>
                            </strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ((isset($event['Event']['description'])) && ($event['Event']['description'] != null)) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Description:', true); ?></span>
                        </td>
                        <td>
                            <strong>
                                <?php echo $event['Event']['description'] ?>
                            </strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (count($sponsors) > 0) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Sponsors:', true); ?></span>
                        </td>
                        <td>
                            <strong>
                                <?php
                                $numSponsors = 0;
                                foreach ($sponsors as $sponsor) {
                                    if ($numSponsors > 0) {
                                        echo ', ';
                                    }
                                    echo $sponsor['EventsSponsor']['name'];
                                    $numSponsors++;
                                }
                                ?>
                            </strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ($event['Event']['total_attendance'] != null) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Total Attendance:', true); ?></span>
                        </td>
                        <td>
                            <strong><?php echo $event['Event']['total_attendance'] ?></strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ($event['Event']['paid_attendance'] != null) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Paid Attendance:', true); ?></span>
                        </td>
                        <td>
                            <strong><?php echo $event['Event']['paid_attendance'] ?></strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ($event['Event']['overseeing_body'] != null) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Overseeing Body:', true); ?></span>
                        </td>
                        <td>
                            <strong><?php echo $event['Event']['overseeing_body'] ?></strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ($event['Event']['start_time'] != null) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Start Time:', true); ?></span>
                        </td>
                        <td>
                            <strong><?php echo $event['Event']['start_time'] ?></strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ($event['Event']['actual_start_time'] != null) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Actual Start Time:', true); ?></span>
                        </td>
                        <td>
                            <strong><?php echo $event['Event']['actual_start_time'] ?></strong>
                        </td>
                    </tr>
                <?php } ?>
                <?php if ($event['Event']['main_event_time'] != null) { ?>
                    <tr>
                        <td>
                            <span><?php echo __('Main Event Time:', true); ?></span>
                        </td>
                        <td>
                            <strong><?php echo $event['Event']['main_event_time'] ?></strong>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <hr>

        <div class="fighters-details">
            
            <ul id="myTab" class="nav nav-tabs responsive">
                <li class="active">
                    
                    
                    <a href="#fight-panel" class="h4" data-toggle="tab"><?php echo __('Fights', true); ?></a>
                </li>
                <?php if( $countImages >  0){ ?>
                            <li>
                                <a href="#photos-panel" class="h4 load-images" data-toggle="tab"><?php echo __('Images', true);?></a>
                            </li>
                <?php } ?>
                    
                <?php if ((isset($venue['Venue']['map'])) && ($venue['Venue']['map'] != "")) { ?>
                    <li>
                        <a href="#map-panel" class="h4 show-map" data-toggle="tab"><?php echo __('Map', true) ?></a>
                    </li>
                <?php } ?>
            </ul>
          
            <div id="myTabContent" class="tab-content">
              
                <div class="tab-pane fade in active" id="fight-panel">
                    <div class="col-lg-12" id="loading-record"><br>
                        <center>
                            <i class="fa fa-refresh fa-spin fa-3x"></i>
                        </center>
                        
                    </div>                    
                </div>
                
                

                <?php if ($countImages > 0) { ?>
                    <div class="tab-pane fade" id="photos-panel">
                        <div class="col-lg-12" id="load-img"><br>
                            <center>
                                
                                
                                
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
                
                
                
                <?php } ?>
                <?php if ((isset($venue['Venue']['map'])) && ($venue['Venue']['map'] != "")) { ?>
                    <div class="tab-pane fade in" id="map-panel">
                    </div>
                <?php } ?>
                
            </div>
            
            <br>

        </div>                
    </div>



    <div class="clear"></div>
    <?php echo $this->element('banners', array('thisBanner' => $detailBanner)); ?>  
</div>
<div class='footer_separator'></div>
<script type="text/javascript">

    (function ($) {
        fakewaffle.responsiveTabs(['xs', 'sm']);
    })(jQuery);

    $(document).ready(function () {
         var loadImages = true;
        $('.load-images').click(function(){
            if(loadImages){
                var loadPageImages = "<?php echo $this->Html->url(array('action' => 'getImagesEvent/' . $id));?>";
                $.post(loadPageImages, function(data){
                    $('#load-img').fadeOut(function(){
                        $('#photos-panel').append(data);
                        loadImages = false;
                    });
                });
            }
        });
        var loadFights = "<?php echo $this->Html->url(array('action' => 'listFights/' . $id)); ?>";
        $.post(loadFights, function (data) {
            $('#fight-panel').html(data);
        });

        var showImages = true;
        $('#show-images').click(function () {
            if (showImages) {
                var loadImages = "<?php echo $this->Html->url(array('action' => 'showImages/' . $id)); ?>";
                $.post(loadImages, function (data) {
                    $('#loading-images').html(data);
                });
            }
        });
<?php if ((isset($venue['Venue']['map'])) && ($venue['Venue']['map'] != "")) { ?>
            $('.show-map').click(function () {
                $('#map-panel').html('<?php echo $venue['Venue']['map'] ?>');
            });
<?php } ?>

    });
</script>
