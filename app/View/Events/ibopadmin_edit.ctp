<style type="text/css">
	#event-data{
		display: none;
	}
</style>
<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Events', true);?> - <?php echo $eventName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-calendar"></i> 
                    <?php echo __('Events', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Edit', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            
            <a href="<?php echo $this->Html->url('/Event/'.$this->getUrlPerson->getUrl($idEvent,$eventName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
                <i class="fa fa-desktop"></i>
            </a>
            <?php if($user['Role']['manage'] == 1 ){ ?>
                <?php $locked = $event['Event']['locked'] == 1 ? '0' : '1'; ?>
                <a href="<?php echo $this->Html->url(array('action' => 'lock/' . base64_encode($idEvent) . '/' . $locked)); ?>" class="btn btn-primary tooltip-button">
                    <?php if( $event['Event']['locked'] == 1){ ?>
                        <i class="fa fa-lock"></i>
                    <?php }else{ ?>
                        <i class="fa fa-unlock-alt"></i>
                    <?php }?>
                </a>
            <?php } ?>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Locations'); ?>">
                <i class="fa fa-globe"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'listFights/' . base64_encode($idEvent)));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Fights');?>">
                <i class="fa fa-eye"></i>
            </a>
        </div>
        
    </div>    
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#event-data"><i class="fa fa-calendar"></i> <?php echo __('Edit Event', true);?></a></h3>
            </div>
            
            <div class="panel-body" id="event-data">
                <?php echo $this->Form->create('Event'); ?>
					
                    <?php echo $this->Form->input('id', array('value' => $event['Event']['id']));?>
                
                    <div class="form-group">
                        <label><?php echo __('Source:', true);?></label>
                        <?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $source, 'default' => $event['Event']['sources_id'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true )); ?>
                    </div>
                
                    <div class="form-group">
                            <label><?php echo __('Name:', true);?></label>
                            <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $event['Event']['name'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Description:', true);?></label>
                            <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $event['Event']['description'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Date:', true);?></label>
                            <?php echo $this->Form->input('date', array('label' => false, 'div' => false, 'class' => 'form-control form-date', 'value' => $event['Event']['date'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Total attendance:', true);?></label>
                            <?php echo $this->Form->input('total_attendance', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $event['Event']['total_attendance'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Paid attendance:', true);?></label>
                            <?php echo $this->Form->input('paid_attendance', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $event['Event']['paid_attendance'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Broad cast:', true);?></label>
                            <?php echo $this->Form->input('broad_cast', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $event['Event']['broad_cast'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Overseeing body:', true);?></label>
                            <?php echo $this->Form->input('overseeing_body', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $event['Event']['overseeing_body'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Notes:', true);?></label>
                            <?php echo $this->Form->input('notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea', 'value' => $event['Event']['notes'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Boxrec Notes:', true);?></label>
                            <?php echo $this->Form->input('boxrec_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea', 'value' => $event['Event']['boxrec_notes'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Other Notes:', true);?></label>
                            <?php echo $this->Form->input('other_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea', 'value' => $event['Event']['other_notes'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Broad Cast Comments:', true);?></label>
                            <?php echo $this->Form->input('broad_cast_comments', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea', 'value' => $event['Event']['broad_cast_comments'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Start Time:', true);?></label>
                            <?php echo $this->Form->input('start_time', array('label' => false, 'div' => false, 'class' => 'form-control form-time', 'value' => $event['Event']['start_time'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Actual Start Time:', true);?></label>
                            <?php echo $this->Form->input('actual_start_time', array('label' => false, 'div' => false, 'class' => 'form-control form-time', 'value' => $event['Event']['actual_start_time'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                            <label><?php echo __('Main Event Time:', true);?></label>
                            <?php echo $this->Form->input('main_event_time', array('label' => false, 'div' => false, 'class' => 'form-control form-time', 'value' => $event['Event']['main_event_time'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                    </div>

                    <div class="form-group">
                        <?php if( $event['Event']['locked'] == 0 ){ ?>
                            <?php echo $this->Form->button(__('Save Event'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        <?php } ?>
                    </div>

                <?php echo $this->Form->end(); ?>
            </div>
            
	</div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#jobs-data"><i class="fa fa-calendar"></i> <?php echo __('Event Jobs', true);?></a></h3>
            </div>
            <div class="panel-body" id="jobs-data">
               
                
                <?php echo $this->Form->create('EventsJob', array('class' => 'form-horizontal', 'url' => array('controller' => 'Events', 'action' => 'saveJob'))); ?>
                    <?php echo $this->Form->input('events_id', array('type' => 'hidden'));?>
                    <div class="form-group">
                        <label class="col-sm-1 control-label"><?php echo __('Name');?></label>
                        <div class="col-sm-4">
                            <?php echo $this->Form->input('events_id', array('type' => 'hidden', 'value' => $event['Event']['id']));?>
                            <?php echo $this->Form->input('identities_label', array('label' => false, 'div' => false, 'class' => 'form-control input-identity', 'add-to' => '#EventsJobIdentitiesId',  'readonly' => $event['Event']['locked'] == 0 ? false : true));?>
                            <?php echo $this->Form->input('identities_id', array('type' => 'hidden'));?>
                        </div>
                        <div class="col-sm-4">
                            <?php 
                                $opntions = array('Broadcasters' => 'Broadcaster', 'Doctor' => 'Doctor', 'Promoter' => 'Promoter', 'Matchmaker' => 'Matchmaker', 'Inspector' => 'Inspector', 'Commissioner' => 'Commissioner', 'Referee' => 'Referee', 'Judge' => 'Judge', 'Timekeeper' => 'Timekeeper');
                                echo $this->Form->input('type', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $opntions, 'empty' => array('' => 'select type'),  'readonly' => $event['Event']['locked'] == 0 ? false : true));
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <?php if( $event['Event']['locked'] == 0 ){ ?>    
                                <?php echo $this->Form->button(__('save job'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Name', true);?></th>
                                <th><?php echo __('Job', true);?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php foreach ($jobs as $job){ ?>
                                    <tr>
                                        <td><?php echo $job['Identities']['name']; ?> <?php echo $job['Identities']['last_name']; ?></td>
                                        <td><?php echo $job['EventsJob']['type']; ?></td>
                                        <td>
                                            <?php echo $this->Form->create('EventsJob', array('url' => array('controller' => 'Events', 'action' => 'deleteJob')))?>
                                            <?php echo $this->Form->hidden('id', array('value' => $job['EventsJob']['id']))?>
                                            <?php echo $this->Form->hidden('events_id', array('value' => $event['Event']['id']))?>
                                            <?php if( $event['Event']['locked'] == 0 ){ ?>
                                                <?php echo $this->Form->button('<i class="fa fa-trash-o"></i>', array('class' => 'btn btn-danger', 'type' => 'submit'))?>
                                            <?php } ?>
                                            <?php echo $this->Form->end(); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="#" class="display-panel" target-display="#broadcasters-data"><i class="fa fa-calendar"></i> <?php echo __('Event Promotion', true); ?></a></h3>
            </div>
            <div class="panel-body" id="broadcasters-data">
                <?php echo $this->Form->create('EventsSponsor', array('class' => 'form-horizontal', 'url' => array('controller' => 'Events', 'action' => 'saveSponsor'))); ?>
                    <div class="form-group">
                        
                        <label class="col-sm-3 control-label"><?php echo __('Promotional company'); ?></label>
                        <div class="col-sm-6">
                            <?php echo $this->Form->input('events_id', array('type' => 'hidden', 'value' => $event['Event']['id'])); ?>
                            <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control' , 'readonly' => $event['Event']['locked'] == 0 ? false : true, 'required' => true)); ?>
                        </div>
                        <div class="col-sm-3">
                            <?php if( $event['Event']['locked'] == 0 ){ ?>
                                <?php echo $this->Form->button(__('save company'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Name', true); ?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($sponsors as $sponsor) { ?>
                                <tr>
                                    <td><?php echo $sponsor['EventsSponsor']['name']?></td>
                                    <td>
                                        <?php echo $this->Form->create('EventsSponsor', array('url' => array('controller' => 'Events', 'action' => 'deleteSponsor')))?>
                                        <?php echo $this->Form->hidden('id', array('value' => $sponsor['EventsSponsor']['id']))?>
                                        <?php echo $this->Form->hidden('events_id', array('value' => $event['Event']['id']))?>
                                        <?php if( $event['Event']['locked'] == 0 ){ ?>
                                            <?php echo $this->Form->button('<i class="fa fa-trash-o"></i>', array('class' => 'btn btn-danger', 'type' => 'submit'))?>
                                        <?php } ?>
                                        <?php echo $this->Form->end(); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('jquery-ui-timepicker-addon');?>
<script type="text/javascript">
	$(function() {

            $(".form-time").timepicker({
               timeFormat: "hh:mm tt"
            }); 

            $('.display-panel').click(function(event){
               event.preventDefault();
               var panel = $(this).attr('target-display');
               if( $(panel).is(':visible') ){  
                       $(panel).slideUp(1000);
               } else {
                       $(panel).slideDown(1000);
               }
               return false;
            });

	});
</script>
