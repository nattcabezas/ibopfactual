<div class="panel panel-primary filterable"> 
    <div class="">
        <table class="table_basic responsive" id="boxing-record-table">
            <thead>
                <tr>
                    <th><?php echo __('Weight Class', true);?></th>
                    <th colspan="3"><?php echo __('Fighters', true);?></th>
                    <th><?php echo __('Via', true);?></th>
                    <th><?php echo __('RS/BR', true);?></th>
                    <th><?php echo __('Time', true);?></th>
                    <th><?php echo __('Detail', true);?></th>
                    <th><?php echo __('Rules', true);?></th>
                    <th>&emsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($fights as $fight) { ?>
                    <tr>
                        <td data-content="<?php echo __('Weight Class:', true);?>" class="weight_class"> 
                            <?php 
                                if((isset($fight['Weights']['name'])) && ($fight['Weights']['name'] != "")){
                                    echo $fight['Weights']['name'];
                                } else {
                                    echo __('N/A');
                                }
                            ?>
                        </td>
                       
                       
                        <td data-content="<?php echo __('Fighters:', true);?>">
                            <?php if( (isset($fight['FightIdentity'][0])) && (($fight['FightIdentity'][0]['Identities']['name'] != 'unknown') || ($fight['FightIdentity'][0]['Identities']['last_name'] != ""))) { ?>
                                <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][0]['Identities']['id'], $fight['FightIdentity'][0]['Identities']['name'] . ' ' . $fight['FightIdentity'][0]['Identities']['last_name']))?>">
                                    <?php echo $fight['FightIdentity'][0]['Identities']['name'] . ' ' . $fight['FightIdentity'][0]['Identities']['last_name']; ?>
                                </a>
                            <?php } else { ?>
                                <?php echo __('unknown', true);?>
                            <?php } ?>
                        </td>
                        <td>
                            <span> VS </span>
                        </td>
                        <td >
                            <?php if( (isset($fight['FightIdentity'][1])) && (($fight['FightIdentity'][1]['Identities']['name'] != 'unknown') || ($fight['FightIdentity'][1]['Identities']['last_name'] != ""))) { ?>
                                <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($fight['FightIdentity'][1]['Identities']['id'], $fight['FightIdentity'][1]['Identities']['name'] . ' ' . $fight['FightIdentity'][1]['Identities']['last_name']))?>">
                                    <?php echo $fight['FightIdentity'][1]['Identities']['name'] . ' ' . $fight['FightIdentity'][1]['Identities']['last_name']?>
                                </a>
                            <?php } else { ?>
                                <?php echo __('unknown', true);?>
                            <?php } ?>
                        </td>
                        <td data-content="<?php echo __('Via:', true);?>"><?php echo $fight['Fight']['via']?></td>
                        <td data-content="<?php echo __('BR/RS:', true);?>">
                            <?php 
                              
                                echo $fight['Fight']['boxed_roundas'];
                                if ( ($fight['FightIdentity'][0]['Identities']['gender']==='F') || (strcmp($fight['Type']['name'], 'Boxing') != 0) )
                                {
                                    echo '/';
                                }
                                echo $fight['Fight']['schedule_rounds'];
                                
                                /*
                                Knowing the gender of just 1 fighter is enough,
                                * since the fights are done between people of the
                                * same gender  
                                */
                                if ( ($fight['FightIdentity'][0]['Identities']['gender']==='F') || (strcmp($fight['Type']['name'], 'Boxing') != 0) )
                                {
                                    echo 'x';
                                    echo $fight['Fight']['time_per_round'];
                                }
                                
                            ?>
                        </td>
                        <td data-content="<?php echo __('Time:', true);?>"><?php echo $fight['Fight']['time']?></td>
                        <td data-content="<?php echo __('Detail:', true);?>">
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($fight['Fight']['id'], $fight['Fight']['title'])); ?>">
                                <i class="fa fa-boxers fa-2x"></i>
                            </a>
                        </td>
                        <td data-content="<?php echo __('Rules:', true);?>">
                            <?php echo $fight['Type']['name']?>
                        </td>
                        <td data-content="<?php echo __('Title Detail:', true);?>">
                            <?php if(count($fight['FightsTitle']) > 0){ ?>
                                <a href="#show-titles" class="show-title-detail" id-fight="<?php echo $fight['Fight']['id']?>" data-toggle="modal" data-target="#myModalTitle">
                                    <i class="fa fa-champ-belt fa-2x"></i>
                                </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    
    <div class="col-lg-12">

        <hr> 
        
        <table class="table table-striped">
           
            <?php foreach ($eventJobs as $promo) { ?>
             <tr>
                <td><strong><?php echo __('Name') ?></strong></td>
                <td><strong><?php echo __('Job') ?></strong></td> 
            </tr>
                <tr>
                    <td>
                        <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($promo['Identities']['id'], $promo['Identities']['name'] . ' ' . $promo['Identities']['last_name'])); ?>">
                            <?php echo $promo['Identities']['name'] . ' ' . $promo['Identities']['last_name'] ?>
                        </a>
                    </td>
                    <td><?php echo $promo['EventsJob']['type'] ?></td>
                </tr>
            <?php } ?>

        </table>
    </div>
    
</div>
<!-- Modal -->
<div class="modal fade" id="myModalTitle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo __('Close', true);?></span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Title Details', true);?></h4>
            </div>
            <div class="modal-body" id="title-content-modal"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.show-title-detail').click(function(){
            var idFight     = $(this).attr('id-fight');
            var loadTitles  = "<?php echo $this->Html->url(array('controller' => 'Persons', 'action' => 'showTitles'));?>/" + idFight;
            $.post(loadTitles, function(data){
                $('#title-content-modal').html(data);
            });
        });
        
    });
</script>