<div class="events view">
<h2><?php echo __('Event'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($event['Event']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($event['Event']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($event['Event']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($event['Event']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Venues'); ?></dt>
		<dd>
			<?php echo $this->Html->link($event['Venues']['name'], array('controller' => 'venues', 'action' => 'view', $event['Venues']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Locations'); ?></dt>
		<dd>
			<?php echo $this->Html->link($event['Locations']['id'], array('controller' => 'locations', 'action' => 'view', $event['Locations']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Attendance'); ?></dt>
		<dd>
			<?php echo h($event['Event']['total_attendance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Paid Attendance'); ?></dt>
		<dd>
			<?php echo h($event['Event']['paid_attendance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Broad Cast'); ?></dt>
		<dd>
			<?php echo h($event['Event']['broad_cast']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Overseeing Body'); ?></dt>
		<dd>
			<?php echo h($event['Event']['overseeing_body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Matchmaker'); ?></dt>
		<dd>
			<?php echo h($event['Event']['matchmaker']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inspector'); ?></dt>
		<dd>
			<?php echo h($event['Event']['inspector']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($event['Event']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Boxrec Notes'); ?></dt>
		<dd>
			<?php echo h($event['Event']['boxrec_notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Other Notes'); ?></dt>
		<dd>
			<?php echo h($event['Event']['other_notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Broad Cast Comments'); ?></dt>
		<dd>
			<?php echo h($event['Event']['broad_cast_comments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Time'); ?></dt>
		<dd>
			<?php echo h($event['Event']['start_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Actual Start Time'); ?></dt>
		<dd>
			<?php echo h($event['Event']['actual_start_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Main Event Time'); ?></dt>
		<dd>
			<?php echo h($event['Event']['main_event_time']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Event'), array('action' => 'edit', $event['Event']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Event'), array('action' => 'delete', $event['Event']['id']), array(), __('Are you sure you want to delete # %s?', $event['Event']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Events'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Event'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Venues'), array('controller' => 'venues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Venues'), array('controller' => 'venues', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Locations'), array('controller' => 'locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Locations'), array('controller' => 'locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fights'), array('controller' => 'fights', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fight'), array('controller' => 'fights', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promotional Companies'), array('controller' => 'promotional_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promotional Company'), array('controller' => 'promotional_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Videos'), array('controller' => 'videos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Video'), array('controller' => 'videos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Fights'); ?></h3>
	<?php if (!empty($event['Fight'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sources Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Weights Id'); ?></th>
		<th><?php echo __('Schedule Rounds'); ?></th>
		<th><?php echo __('Time Per Round'); ?></th>
		<th><?php echo __('Boxed Roundas'); ?></th>
		<th><?php echo __('Final Figths Id'); ?></th>
		<th><?php echo __('Rules Id'); ?></th>
		<th><?php echo __('Time'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Boxrec Notes'); ?></th>
		<th><?php echo __('Other Notes'); ?></th>
		<th><?php echo __('Winner'); ?></th>
		<th><?php echo __('Types Id'); ?></th>
		<th><?php echo __('Is Competitor'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($event['Fight'] as $fight): ?>
		<tr>
			<td><?php echo $fight['id']; ?></td>
			<td><?php echo $fight['sources_id']; ?></td>
			<td><?php echo $fight['title']; ?></td>
			<td><?php echo $fight['description']; ?></td>
			<td><?php echo $fight['weights_id']; ?></td>
			<td><?php echo $fight['schedule_rounds']; ?></td>
			<td><?php echo $fight['time_per_round']; ?></td>
			<td><?php echo $fight['boxed_roundas']; ?></td>
			<td><?php echo $fight['final_figths_id']; ?></td>
			<td><?php echo $fight['rules_id']; ?></td>
			<td><?php echo $fight['time']; ?></td>
			<td><?php echo $fight['notes']; ?></td>
			<td><?php echo $fight['boxrec_notes']; ?></td>
			<td><?php echo $fight['other_notes']; ?></td>
			<td><?php echo $fight['winner']; ?></td>
			<td><?php echo $fight['types_id']; ?></td>
			<td><?php echo $fight['is_competitor']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'fights', 'action' => 'view', $fight['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'fights', 'action' => 'edit', $fight['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'fights', 'action' => 'delete', $fight['id']), array(), __('Are you sure you want to delete # %s?', $fight['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Fight'), array('controller' => 'fights', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Images'); ?></h3>
	<?php if (!empty($event['Image'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Image Types Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Source 1'); ?></th>
		<th><?php echo __('Source 2'); ?></th>
		<th><?php echo __('Donated By'); ?></th>
		<th><?php echo __('Collection'); ?></th>
		<th><?php echo __('Long Description'); ?></th>
		<th><?php echo __('Ibop Owned'); ?></th>
		<th><?php echo __('Url'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($event['Image'] as $image): ?>
		<tr>
			<td><?php echo $image['id']; ?></td>
			<td><?php echo $image['image_types_id']; ?></td>
			<td><?php echo $image['title']; ?></td>
			<td><?php echo $image['description']; ?></td>
			<td><?php echo $image['source_1']; ?></td>
			<td><?php echo $image['source_2']; ?></td>
			<td><?php echo $image['donated_by']; ?></td>
			<td><?php echo $image['collection']; ?></td>
			<td><?php echo $image['long_description']; ?></td>
			<td><?php echo $image['ibop_owned']; ?></td>
			<td><?php echo $image['url']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'images', 'action' => 'view', $image['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'images', 'action' => 'edit', $image['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'images', 'action' => 'delete', $image['id']), array(), __('Are you sure you want to delete # %s?', $image['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Promotional Companies'); ?></h3>
	<?php if (!empty($event['PromotionalCompany'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Comments'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($event['PromotionalCompany'] as $promotionalCompany): ?>
		<tr>
			<td><?php echo $promotionalCompany['id']; ?></td>
			<td><?php echo $promotionalCompany['name']; ?></td>
			<td><?php echo $promotionalCompany['comments']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'promotional_companies', 'action' => 'view', $promotionalCompany['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'promotional_companies', 'action' => 'edit', $promotionalCompany['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'promotional_companies', 'action' => 'delete', $promotionalCompany['id']), array(), __('Are you sure you want to delete # %s?', $promotionalCompany['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Promotional Company'), array('controller' => 'promotional_companies', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Videos'); ?></h3>
	<?php if (!empty($event['Video'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Short Description'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('M4v'); ?></th>
		<th><?php echo __('Ogv'); ?></th>
		<th><?php echo __('Webm'); ?></th>
		<th><?php echo __('Mp4'); ?></th>
		<th><?php echo __('Flv'); ?></th>
		<th><?php echo __('Ibop Owned'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($event['Video'] as $video): ?>
		<tr>
			<td><?php echo $video['id']; ?></td>
			<td><?php echo $video['title']; ?></td>
			<td><?php echo $video['short_description']; ?></td>
			<td><?php echo $video['description']; ?></td>
			<td><?php echo $video['m4v']; ?></td>
			<td><?php echo $video['ogv']; ?></td>
			<td><?php echo $video['webm']; ?></td>
			<td><?php echo $video['mp4']; ?></td>
			<td><?php echo $video['flv']; ?></td>
			<td><?php echo $video['ibop_owned']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'videos', 'action' => 'view', $video['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'videos', 'action' => 'edit', $video['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'videos', 'action' => 'delete', $video['id']), array(), __('Are you sure you want to delete # %s?', $video['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Video'), array('controller' => 'videos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
