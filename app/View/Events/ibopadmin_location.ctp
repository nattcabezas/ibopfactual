<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Events', true);?> - <?php echo $eventName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-calendar"></i> 
                    <?php echo __('Events', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Location', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url('/Event/'.$this->getUrlPerson->getUrl($idEvent,$eventName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
                <i class="fa fa-desktop"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Locations'); ?>">
                <i class="fa fa-globe"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'listFights/' . base64_encode($idEvent)));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Fights');?>">
                <i class="fa fa-eye"></i>
            </a>
        </div>
        
    </div>    
</div>

<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#"><?php echo __('Venue', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'geoLocation/' . base64_encode($venue['Event']['id'])));?>"><?php echo __('Geo Location', true);?></a></li>
        </ul>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo __('Edit Event Venue', true);?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('Event'); ?>

                    <?php echo $this->Form->input('id', array('value' => $venue['Event']['id'], 'type' => 'hidden'));?>
                    <div class="form-group">
                        <label><?php echo __('Venue:', true);?></label>
                        <?php echo $this->Form->input('venues_label', array('div' => false, 'label' => false, 'class' => 'form-control', 'value' => $venue['Venues']['name'] , 'readonly' => $event['Event']['locked'] == 0 ? false : true)); ?>
                        <?php echo $this->Form->input('venues_id', array('type' => 'hidden', 'value' => $venue['Venues']['id'])); ?>
                    </div>
                                    
                    <div class="form-group">
                        <?php if( $event['Event']['locked'] == 0 ){ ?>
                            <?php echo $this->Form->button(__('save event venue', true), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                        <?php } ?>
                    </div>
                    
                    <button class="btn btn-primary" id="buttonDelete">Delete venue from event</button>
                    
                <?php echo $this->Form->end(); ?>
            </div>
            
        </div>
    </div>
</div>

<!----------------------- -->
<div class="col-lg-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo __('Search venues by geographical location', true);?></h3>
        </div>
        <div class="panel-body">
            <?php echo $this->Form->create('Location', array('url' => array('controller' => 'Venues', 'action' => 'assign_to_event'))); ?>
                <div class="col-lg-3">
                    <div class="form-group input-group">
                        <span class="input-group-addon" id="flag-contry" style="height: 34px;">&nbsp;&nbsp;&nbsp;</span>
                         <?php echo $this->Form->input('countries_text', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Country', true))); ?>
                         <?php echo $this->Form->input('countries_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
                    </div>
                </div>
                <?php echo $this->Form->input('idEvent', array('type' => 'hidden', 'value' => $idEvent)); ?>

                <div class="col-lg-3">
                    <div class="form-group input-group">
                        <div id="states-contend">
                            <select class="form-control">
                                <option><?php echo __('select state', true);?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group input-group">
                        <div id="cities-contend">
                            <select class="form-control">
                                <option><?php echo __('select city', true);?></option>
                            </select>
                        </div>
                    </div>
                </div>
                    <?php echo $this->Form->button(__('Search', true), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    <?php echo $this->Form->end(); ?>
        </div>
   </div>
</div>

<!----------------------- -->           

<script type="text/javascript">
    $(document).ready(function(){
        $('#EventVenuesLabel').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'getVenue'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('#EventVenuesLabel').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                $('#EventVenuesLabel').val(ui.item.label);
                $('#EventVenuesId').val(ui.item.id);
                return false;
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + " - " + item.City + " " + item.State + ", " + item.Country + "</a>" )
            .appendTo( ul );
        };
        
                /** country autocomplete **/
         
        $("#LocationCountriesText").autocomplete({
	      minLength: 0,
	      source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Country', 'action' => 'getCountry'));?>/" + Base64.encode(request.term);
                    $.getJSON(loadIdentities, function(data){
                        response(data);
                    });
                },
	      focus: function( event, ui ) {
	        $("#LocationCountriesText").val(ui.item.label);
	        return false;
	      },
	      select: function( event, ui ) {
	        $("#LocationCountriesText" ).val(ui.item.label);
	        $('#flag-contry').html("<img src='<?php echo $this->html->url('/img/flags')?>/" + ui.item.flag + "'>");
	        $('#LocationCountriesId').val(ui.item.id);
	        $('#cities-contend').html("");
                var loadStates = "<?php echo $this->Html->url(array('action' => 'getStates'));?>";
	        $.post(loadStates, {countries_id: ui.item.id}, function(data){
	        	$('#states-contend').html(data);
	        	$('#LocationStatesId').change(function(){
	        		var loadCities = "<?php echo $this->Html->url(array('action' => 'getCities'));?>";
	        		$.post(loadCities, {states_id: $(this).val()}, function(data){
	        			$('#cities-contend').html(data);
	        		});
	        	});
	        });
	        return false;
	      }
	    })
	    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append( "<a><img src='<?php echo $this->html->url('/img/flags')?>/" + item.flag + "'> " + item.label + "</a>" )
	        .appendTo( ul );
	    };
            
        $('#buttonDelete').click(function(event){
            event.preventDefault();
            var loadPage = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'deleteVenueFromEvent/' . $idEvent));?>/";
            $.post(loadPage, function(data){
                console.log(data);
                if(data == '1'){
                    $('#EventVenuesLabel').val('UNKNOWN');
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('Venue removed from event', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                }
            });

        });
        
    });
</script>