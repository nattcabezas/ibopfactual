<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Events', true);?> - <?php echo $eventName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-calendar"></i> 
                    <?php echo __('Events', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('List of Fights', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url('/Event/'.$this->getUrlPerson->getUrl($idEvent,$eventName)); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('frontend'); ?>">
                <i class="fa fa-desktop"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Locations'); ?>">
                <i class="fa fa-globe"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'listFights/' . base64_encode($idEvent)));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Fights');?>">
                <i class="fa fa-eye"></i>
            </a>
        </div>
        
    </div>    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Fight', true); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <ul id="sortable" class="list-group">
                                <?php foreach ($fights as $fight) { ?>
                                    <li class="list-group-item" id="<?php echo $fight['Fight']['id']?>">
                                        <i class="fa fa-bars"></i>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fight']['id']))) ?>">
                                            <?php echo $fight['Fight']['title']?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </td>
                    </tr>    
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $( "#sortable" ).sortable({
            placeholder: "list-group-item list-group-item-warning placeholder",
            update: function(event, ui){
                $('#sortable li').each( function(e) {
                   var updateOrder = "<?php echo $this->Html->url(array('action' => 'fightOrder')); ?>";
                   $.post(updateOrder, {id: $(this).attr('id'), newOrder: $(this).index() + 1});
                });
            }
        });
        $( "#sortable" ).disableSelection();
        
    });
</script>