<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Events', true);?> - <?php echo $eventName?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-calendar"></i> 
                    <?php echo __('Events', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Images', true);?>
            </li>
        </ol>
        
        <div class="btn-group pull-right" style="padding-bottom: 10px;">
            <a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('images'); ?>">
                <i class="fa fa-file-image-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'videos/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('videos'); ?>">
                <i class="fa fa-file-video-o"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'location/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('Locations'); ?>">
                <i class="fa fa-globe"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($idEvent))); ?>" class="btn btn-primary tooltip-button" title="<?php echo __('edit'); ?>">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="<?php echo $this->Html->url(array('action' => 'listFights/' . base64_encode($idEvent)));?>" class="btn btn-primary tooltip-button" title="<?php echo __('Event Fights');?>">
                <i class="fa fa-eye"></i>
            </a>
        </div>
        
    </div>    
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?php echo $this->Html->url(array('action' => 'images/' . base64_encode($idEvent)));?>"><?php echo __('All Images', true);?></a></li>
            <li><a href="<?php echo $this->Html->url(array('action' => 'searchImages/' . base64_encode($idEvent)));?>"><?php echo __('Search Images', true);?></a></li>
            <li class="active"><a href="#"><?php echo __('Event Images', true); ?></a></li>
        </ul>
    </div>
</div><br>
<div class="row">
    <?php foreach($images as $image) { ?>
        <div class="col-sm-6 col-md-4" style="margin-bottom: 20px;">
            <div class="thumbnail">
                <img src="<?php echo $this->Html->url('/files/img/' . $image['Images']['url']);?>" style="height: 200px;">
                <div class="caption">
                     <div class="form-conted" id="form-<?php echo $image['Images']['id']?>" style="display: none;">
                        
                    </div>
                    <div id="button-<?php echo $image['Images']['id']?>">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php if($image['EventsImage']['principal'] != 1){?>
                                <?php echo $this->Form->create('ImagePrincipal', array('url' => array('controller' => 'Events', 'action' => 'imagePrincipal')));?>
                                <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $image['EventsImage']['id'])); ?>
                                <?php echo $this->Form->input('idEvent', array('type' => 'hidden', 'value' => $idEvent)); ?>
                                <?php echo $this->Form->input('page', array('type' => 'hidden', 'value' => $page)); ?>
                                <?php echo $this->Form->input('make_principal', array('type' => 'hidden', 'value' => 1)); ?>
                                <?php if( $event['Event']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('Featured'), array('class' => 'btn btn-primary btn-group-justified', 'type' => 'submit')); ?>
                                <?php } ?>    
                                <?php echo $this->Form->end(); ?>
                            <?php } else { ?>
                                <?php echo $this->Form->create('ImagePrincipal', array('url' => array('controller' => 'Events', 'action' => 'imagePrincipal')));?>
                                <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $image['EventsImage']['id'])); ?>
                                <?php echo $this->Form->input('idEvent', array('type' => 'hidden', 'value' => $idEvent)); ?>
                                <?php echo $this->Form->input('page', array('type' => 'hidden', 'value' => $page)); ?>
                                <?php echo $this->Form->input('make_principal', array('type' => 'hidden', 'value' => 0)); ?>
                                <?php if( $event['Event']['locked'] == 0 ){ ?>
                                    <?php echo $this->Form->button(__('unFeatured'), array('class' => 'btn btn-primary btn-group-justified', 'type' => 'submit')); ?>
                                    
                                    <?php
                                        $eventString = $idEvent . "-"; //the - is added because is used on the algorithm of the ImagesController assign_all method
                                    ?>  
                                    <br/>
                                    <a href="<?php echo $this->Html->url(array('controller'=>'Images', 'action' => 'ibopadmin_assign_all/'.$eventString . "/" . $image['Images']['id']));?>" class="btn btn-primary" id="linkAllButton">Link event elements to featured image</a>
                                <?php } ?>    
                                <?php echo $this->Form->end(); ?>
                            <?php }?>
                        </div>
                        <div class="col-sm-4">
                               <a class="btn btn-info display-form btn-group-justified" target-form="form-<?php echo $image['Images'] ['id']?>" id-image="<?php echo $image['Images']['id']?>">
                                <i class="fa fa-pencil "></i> <?php echo __('Edit Image', true);?>
                            </a>   
                            </div>
                                           
                        <div class="col-sm-4">
                            <?php echo $this->Form->create('EventsImage');?>
                            <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $image['EventsImage']['id'])); ?>
                            <?php if( $event['Event']['locked'] == 0 ){ ?>
                                <?php echo $this->Form->button(__('Delete Image '), array('class' => 'btn btn-danger btn-group-justified', 'type' => 'submit')); ?>
                            <?php } ?>
                            <?php echo $this->Form->end(); ?>
                        </div>
                        
                    </div>
                       </div>
                </div>
                 
            </div>
        </div>
    <?php } ?>
    <?php
        if( count($images) == 0 ){
            echo '<div class="col-md-12"><h3>' . __('no results found', true) . '</h3></div>';
        }
    ?>
</div><br>
<div class="row">
    <div class="col-md-3">
        <?php if( $page != 1){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'eventsImages/' . base64_encode($idEvent) . '/' . ($page - 1)));?>" class="btn btn-primary"><?php echo __('Previous', true);?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if( count($images) > 0 ){ ?>
            <a href="<?php echo $this->Html->url(array('action' => 'eventsImages/' . base64_encode($idEvent) . '/' . ($page + 1)));?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true);?></a>
        <?php } ?>
    </div>
</div>





<script type="text/javascript">
    
    $(document).ready(function() {
        
        $('.display-form').click(function(event) {
            /*event.preventDefault();
            var elementDisplay = '#' + $(this).attr('target-form');
            $(this).parent().fadeOut(function(){
                $(elementDisplay).slideDown();
            });*/
            var idImage = $(this).attr('id-image');
            var urlForm = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'formImage'));?>/" + idImage;
            $.post(urlForm, function(data){
                $('#form-' + idImage).html(data);
                $('#form-' + idImage).toggle('slow');
                $('#button-' + idImage).fadeOut();
            });
        });
        
    });
    
</script>
