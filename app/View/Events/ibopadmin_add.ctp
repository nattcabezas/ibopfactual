<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Events', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-calendar"></i> 
                    <?php echo __('Events', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo __('add Event', true);?></h3>
            </div>
            <div class="panel-body">
				<?php echo $this->Form->create('Event'); ?>
                                        
                                        <div class="form-group">
                                            <label><?php echo __('Source:', true);?></label>
                                            <?php echo $this->Form->input('sources_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $source)); ?>
                                        </div>
                
					<div class="form-group">
						<label><?php echo __('Name:', true);?></label>
						<?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Description:', true);?></label>
						<?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>
					
					<div class="form-group">
						<label><?php echo __('Date:', true);?></label>
						<?php echo $this->Form->input('date', array('label' => false, 'div' => false, 'class' => 'form-control form-date')); ?>
					</div>
					
					<div class="form-group">
						<label><?php echo __('Total attendance:', true);?></label>
						<?php echo $this->Form->input('total_attendance', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Paid attendance:', true);?></label>
						<?php echo $this->Form->input('paid_attendance', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Broad cast:', true);?></label>
						<?php echo $this->Form->input('broad_cast', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Overseeing body:', true);?></label>
						<?php echo $this->Form->input('overseeing_body', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Notes:', true);?></label>
						<?php echo $this->Form->input('notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Boxrec Notes:', true);?></label>
						<?php echo $this->Form->input('boxrec_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Other Notes:', true);?></label>
						<?php echo $this->Form->input('other_notes', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Broad Cast Comments:', true);?></label>
						<?php echo $this->Form->input('broad_cast_comments', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Start Time:', true);?></label>
						<?php echo $this->Form->input('start_time', array('label' => false, 'div' => false, 'class' => 'form-control form-time')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Actual Start Time:', true);?></label>
						<?php echo $this->Form->input('actual_start_time', array('label' => false, 'div' => false, 'class' => 'form-control form-time')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Main Event Time:', true);?></label>
						<?php echo $this->Form->input('main_event_time', array('label' => false, 'div' => false, 'class' => 'form-control form-time')); ?>
					</div>

					<div class="form-group">
                		<?php echo $this->Form->button(__('Add Event'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                	</div>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Html->script('jquery-ui-timepicker-addon');?>
<script type="text/javascript">
	$(function() {

	     $(".form-time").timepicker({
	     	timeFormat: "hh:mm tt"
	     }); 

	});
</script>
