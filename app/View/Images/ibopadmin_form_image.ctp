<?php echo $this->Form->create('Image', array('url' => '#', 'id' => 'image-form-' . $image['Image']['id'])); ?>
                            
    <?php echo $this->Form->input('id', array('value' => $image['Image']['id'])); ?>
    <div class="form-group">
        <label><?php echo __('Type:', true);?></label>
        <?php echo $this->Form->input('image_types_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $types, 'default' => $image['Image']['image_types_id'])); ?>
    </div>

    <div class="form-group">
        <label><?php echo __('Title:', true);?></label>
        <?php echo $this->Form->input('title', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['title'])); ?>
    </div>

    <div class="form-group">
        <label><?php echo __('Description:', true);?></label>
        <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['description'])); ?>
    </div>

    <div class="form-group">
        <label><?php echo __('Source 1:', true);?></label>
        <?php echo $this->Form->input('source_1', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['source_1'])); ?>
    </div>

    <div class="form-group">
        <label><?php echo __('Source 2:', true);?></label>
        <?php echo $this->Form->input('source_2', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['source_2'])); ?>
    </div>

    <div class="form-group">
        <label><?php echo __('Donated by:', true);?></label>
        <?php echo $this->Form->input('donated_by', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['donated_by'])); ?>
    </div>

    <div class="form-group">
        <label><?php echo __('Collection:', true);?></label>
        <?php echo $this->Form->input('collection', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['collection'])); ?>
    </div>


    <div class="form-group">
        <label><?php echo __('Long description:', true);?></label>
        <?php echo $this->Form->input('long_description', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['long_description'])); ?>
    </div>

    <div class="checkbox">
        <label>
            <?php echo $this->Form->input('ibop_owned', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => 1, 'checked' => $image['Image']['ibop_owned'])); ?>
            <?php echo __('ibop owned', true);?>
        </label>
    </div>

    <div class="form-group">
        <?php echo $this->Form->input('pageurl', array('type' => 'hidden', 'value' => $this->request->here))?>
        <?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary save-image-form', 'type' => 'submit', 'id-image' => $image['Image']['id'], 'id' => 'save-button-' . $image['Image']['id'])); ?>
        <a href="#" class="btn btn-default display-button" id="cancel-<?php echo $image['Image']['id']; ?>"><?php echo __('Cancel', true);?></a>
    </div>

<?php echo $this->Form->end(); ?>
<script type="text/javascript">

    $(document).ready(function() {
        
        $('#cancel-<?php echo $image['Image']['id']; ?>').click(function(event){
           event.preventDefault();
           $('#form-<?php echo $image['Image']['id']; ?>').toggle( "slow" );
           $('#button-<?php echo $image['Image']['id']; ?>').fadeIn();
        });
        
        $('#save-button-<?php echo $image['Image']['id']; ?>').click(function(event){
            event.preventDefault();
            var urlSubmitForm = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'saveForm'))?>";
            $.post(urlSubmitForm, $('#image-form-<?php echo $image['Image']['id']?>').serialize(), function(data){
                if(data === '1'){
                    $('#form-<?php echo $image['Image']['id']; ?>').toggle( "slow" );
                    $('#button-<?php echo $image['Image']['id']; ?>').fadeIn();
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image has been saved.', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                    $('#event-' + idEventImage).fadeOut();
                }
            });
        });
        
    });

</script>