<?php 
	echo $this->Html->css('fileinput');
	echo $this->Html->script('fileinput');
?>
<div class="container">
	<h1 class="page-header"><?php echo __('Add Images', true);?></h1>
	<?php echo $this->Form->create('Image', array('type' => 'file'));?>
		<div class="form-group">
			<?php echo $this->Form->input('url', array('type' => 'file', 'div' => false, 'label' => false, 'multiple' => true, 'name' => 'ImageUrl[]', 'accept' => 'image/*'))?>
		</div>
	<?php echo $this->Form->end();?>
</div>
<script type="text/javascript">
	$("#ImageUrl").fileinput({
		previewFileType: "image",
		browseClass: "btn btn-success",
		browseLabel: " <?php echo __('Pick Images');?>",
		browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
		removeClass: "btn btn-danger",
		removeLabel: " <?php echo __('Delete');?>",
		removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
		uploadClass: "btn btn-info",
		uploadLabel: " <?php echo __('Upload');?>",
		uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
	});
</script>