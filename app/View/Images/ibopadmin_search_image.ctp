<style type="text/css">
    .form-conted{
        display: none;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Images', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-file-image-o"></i> <?php echo __('Images');?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Search Result', true)?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-6">
        <div class="col-lg-10">
            <?php echo $this->Form->input('keywork', array('id' => 'search-keyword', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true) , 'value' => $keywork)); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'search-images')); ?>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-offset-4">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add images', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>

<div class="row">
    <?php foreach ($images as $image) { ?>
        <div class="col-sm-6 col-md-4" style="margin-bottom: 20px;">
            <div class="thumbnail">
                <img src="<?php echo $this->Html->url('/files/img/' . $image['Image']['url']);?>" style="height: 200px;">
                <div class="caption">
                    <div class="form-conted" id="form-<?php echo $image['Image']['id']?>">
                        <?php echo $this->Form->create('Image', array('url' => '#', 'id' => 'image-form-' . $image['Image']['id'])); ?>
                            
                            <?php echo $this->Form->input('id', array('value' => $image['Image']['id'])); ?>
                            <div class="form-group">
                                <label><?php echo __('Type:', true);?></label>
                                <?php echo $this->Form->input('image_types_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $types, 'empty' => array(0 => __('--select type--', true)), 'default' => $image['Image']['image_types_id'])); ?>
                            </div>

                            <div class="form-group">
                                <label><?php echo __('Title:', true);?></label>
                                <?php echo $this->Form->input('title', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['title'])); ?>
                            </div>

                            <div class="form-group">
                                <label><?php echo __('Description:', true);?></label>
                                <?php echo $this->Form->input('description', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['description'])); ?>
                            </div>

                            <div class="form-group">
                                <label><?php echo __('Source 1:', true);?></label>
                                <?php echo $this->Form->input('source_1', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['source_1'])); ?>
                            </div>

                            <div class="form-group">
                                <label><?php echo __('Source 2:', true);?></label>
                                <?php echo $this->Form->input('source_2', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['source_2'])); ?>
                            </div>

                            <div class="form-group">
                                <label><?php echo __('Donated by:', true);?></label>
                                <?php echo $this->Form->input('donated_by', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['donated_by'])); ?>
                            </div>

                            <div class="form-group">
                                <label><?php echo __('Collection:', true);?></label>
                                <?php echo $this->Form->input('collection', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['collection'])); ?>
                            </div>


                            <div class="form-group">
                                <label><?php echo __('Long description:', true);?></label>
                                <?php echo $this->Form->input('long_description', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $image['Image']['long_description'])); ?>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <?php echo $this->Form->input('ibop_owned', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => 1, 'checked' => $image['Image']['ibop_owned'])); ?>
                                    <?php echo __('ibop owned', true);?>
                                </label>
                            </div>

                            <div class="form-group">
                                <?php 
                                    echo $this->Form->input('keyworkData', array('type' => 'hidden', 'value' => base64_encode($keywork)));
                                    echo $this->Form->input('pageData', array('type' => 'hidden', 'value' => $page));
                                ?>
                                <?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary save-image-form', 'type' => 'submit', 'id-image' => $image['Image']['id'])); ?>
                                <a href="" class="btn btn-default display-button" target-button="button-<?php echo $image['Image']['id']?>"><?php echo __('Cancel', true);?></a>
                            </div>

                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div id="button-<?php echo $image['Image']['id']?>" class="btn-group btn-group-justified">
                        <a class="btn btn-info display-form" target-form="form-<?php echo $image['Image']['id']?>">
                            <i class="fa fa-pencil"></i> 
                            <?php echo __('Edit Image', true);?>
                        </a>
                        <a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'assign/' . base64_encode($image['Image']['id']) ));?>">
                            <i class="fa fa-share"></i> <?php echo __('Assign Images', true); ?>
                        </a>
                        <a href="<?php echo $this->Html->url(array('action' => 'delete/' . $image['Image']['id']));?>" class="btn btn-danger btn-delete">
                            <i class="fa fa-trash-o"></i>  
                            <?php echo __('Delete Image')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div><br>

<div class="row">
    <div class="col-md-3">
        <?php if ($page != 1) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'searchImage/' . base64_encode($keywork) . '/' . ($page - 1))); ?>" class="btn btn-primary"><?php echo __('Previous', true); ?></a>
        <?php } ?>
    </div>
    <div class="col-md-3 col-md-offset-6">
        <?php if (count($images) > 0) { ?>
            <a href="<?php echo $this->Html->url(array('action' => 'searchImage/' . base64_encode($keywork) . '/' . ($page + 1))); ?>" class="btn btn-primary" style="float: right;"><?php echo __('Next', true); ?></a>
        <?php } ?>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('.display-form').click(function(event) {
            event.preventDefault();
            var elementDisplay = '#' + $(this).attr('target-form');
            $(this).parent().fadeOut(function(){
                $(elementDisplay).slideDown();
            });
        });

        $('.display-button').click(function(event) {
            event.preventDefault();
            var elementDisplay = '#' + $(this).attr('target-button');
            $(this).parent().parent().parent().slideUp(function(){
                $(elementDisplay).fadeIn();
            });
        });
        
        $('#search-images').click(function(event) {
            var keyword = Base64.encode($('#search-keyword').val());
            var loadsearch = "<?php echo $this->Html->url(array('action' => 'searchImage')) ?>/";
            window.location.replace(loadsearch + keyword);
        });
        
        $('.save-image-form').click(function(event){
            event.preventDefault();
            var id_image = $(this).attr('id-image');
            var loadPage = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'saveImage'));?>";
            $.post(loadPage, $('#image-form-' + id_image).serialize(), function(data){
                if(data === '1'){
                    $('#form-' + id_image).slideUp(function(){
                        $('#button-' + id_image).fadeIn();
                    });
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image has been saved', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                } else {
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-danger alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image could not be saved. Please, try again.', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                }
                $('html,body').animate( { scrollTop:$("html").offset().top } , 1000);
            });
            //alert(id_image);
        });

    });
</script>