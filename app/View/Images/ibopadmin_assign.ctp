<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?php echo __('Images', true);?>
            <?php 
                if($image!=null)
                {
                    if($image['Image']['title'] != null){
                        echo ' - ' . $image['Image']['title'];
                    }    
                }
                
            ?>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-file-image-o"></i> <?php echo __('Images');?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Assign Image', true)?>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="thumbnail">
            <img src="<?php if ($image!=null){echo $this->Html->url('/files/img/' . $image['Image']['url']);}?>"  style="height: 200px;">
        </div>
    </div>
    <div class="col-lg-6">
        <ul class="nav nav-tabs" role="tablist" id="tab-list">
            <li class="active">
                <a href="#persons" role="tab" data-toggle="tab"><?php echo __('Persons', true);?></a>
            </li>
            <li>
                <a href="#fights" role="tab" data-toggle="tab"><?php echo __('Fights', true);?></a>
            </li>
            <li>
                <a href="#events" role="tab" data-toggle="tab"><?php echo __('Events', true);?></a>
            </li>
            <li>
                <a href="#venues" role="tab" data-toggle="tab"><?php echo __('Venues', true);?></a>
            </li>
        </ul>
        
        <div class="tab-content">
            <div class="tab-pane active" id="persons">
                <br>
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Add Person');?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control search-identity" placeholder="<?php echo __('Search'); ?>">
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Person', true); ?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody class="person-data">
                            <?php foreach ($identitiesImage as $identity){ ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit/' . base64_encode($identity['Identities']['id'])));?>">
                                            <?php echo $identity['Identities']['name']?> <?php echo $identity['Identities']['last_name']?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo $this->Html->url(array('action' => 'deleteIdentity/'.$identity['IdentitiesImage']['id'] . '/' . $idImage));?>" class="btn btn-danger btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="fights">
                <br>
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Add Fight');?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control search-fight" placeholder="<?php echo __('Search'); ?>">
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Fight', true); ?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody class="fight-data">
                            <?php foreach ($fightsImage as $fight){ ?>
                                <tr id="fight-<?php echo $fight['FightsImage']['id']?>">
                                    <td>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit/' . base64_encode($fight['Fights']['id'])));?>" id-fight-image="<?php echo $fight['FightsImage']['id']?>">
                                            <?php echo $fight['Fights']['title']?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-danger delete-fight" id-FightsImage="<?php echo $fight['FightsImage']['id']?>">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="events">
                <?php //debug($eventsImage);?>
                <br>
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Add Event');?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control search-events" placeholder="<?php echo __('Search'); ?>">
                                <?php 
                                    if ($image!=null)
                                    {
                                        if (strcmp($image['Image']['image_types_id'],'5')==0)
                                        {
                                ?>
                                 <?php                    
                                    $eventString = "";
                                    foreach ($eventsImage as $events)
                                    {
                                        $eventString .= $events['Events']['id'] . '-';
                                    }
                                ?>
                                        <a href="<?php echo $this->Html->url(array('action' => 'assign_all/'.$eventString . "/" . $image['Image']['id']));?>" class="btn btn-primary" id="linkAllButton">Link event elements to poster</a>
                                <?php
                                        }
                                    }
                                ?>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Event', true); ?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody class="event-data events-table">
                            <?php foreach ($eventsImage as $event){ ?>
                                <tr id="event-<?php echo $event['EventsImage']['id']?>">
                                    <td>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'edit/' . base64_encode($event['Events']['id'])));?>">
                                            <?php echo $event['Events']['name']?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="" class="btn btn-danger delete-event" id-event="<?php echo $event['EventsImage']['id']?>">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="venues">
                <?php //debug($venuesImage);?>
                <br>
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo __('Add Venue');?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control search-venues" placeholder="<?php echo __('Search'); ?>">
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th><?php echo __('Venue', true); ?></th>
                                <th>&emsp;</th>
                            </tr>
                        </thead>
                        <tbody class="venue-data">
                            <?php foreach ($venuesImage as $venue){ ?>
                                <tr id="venue-<?php echo $venue['VenuesImage']['id']?>">
                                    <td>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'edit/' . base64_encode($venue['Venues']['id'])));?>">
                                            <?php echo $venue['Venues']['name']?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-danger delete-venue" id-venue="<?php echo $venue['VenuesImage']['id']?>">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tab-list a').click(function (e) { 
            e.preventDefault();
            $(this).tab('show');
        });
        
        $('.search-identity').autocomplete({
            minLength: 3,
            source: function(request, response) {
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data) {
                    response(data);
                });
            },
            focus: function(event, ui) {
                $('.search-identity').val(ui.item.label);
                return false;
            },
            select: function(event, ui) {
                $('.search-identity').val(ui.item.label);   
                var getImageIdentity = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'saveIdentity/' . $idImage))?>/" + ui.item.id;
                $.getJSON(getImageIdentity, function(data){
                    var identityData = "<tr>";
                    identityData += "<td>";
                    identityData += "<a href='<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'edit'));?>/" + Base64.encode(data.idIdentity) + "'>";
                    identityData += data.identity.Identity.name + " " + data.identity.Identity.last_name;
                    identityData += "</a>";
                    identityData += "</td>";
                    identityData += "<td>";
                    identityData += "<a href='<?php echo $this->Html->url(array('action' => 'deleteIdentity'));?>/" + data.id + "/<?php echo $idImage?>' class='btn btn-danger btn-delete'>";
                    identityData += "<i class='fa fa-trash'></i>";
                    identityData += "</a>";
                    identityData += "</td>";
                    identityData += "</tr>";
                    $('.person-data').append(identityData);
                    
                });
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
        };
        
        $('.search-fight').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'getFights'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('.search-fight').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                var saveFight = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'saveFight/' . $idImage))?>/" + ui.item.id;
                $.getJSON(saveFight, function(data){
                    var FightData = "<tr id='fight-" + data.id + "'>";
                    FightData += "<td>";
                    FightData += "<a href='<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit'));?>/" + Base64.encode(data.Fight.Fight.id) + "'>";
                    FightData += data.Fight.Fight.title;
                    FightData += "</a>";
                    FightData += "</td>";
                    FightData += "<td>";
                    FightData += "<a href='#' class='btn btn-danger delete-fight' id-fightsimage='" + data.id + "'>";
                    FightData += "<i class='fa fa-trash'></i>";
                    FightData += "</a>";
                    FightData += "</td>";
                    FightData += "</tr>";
                    $('.fight-data').append(FightData);
                    
                    $('.search-fight').val("");
                    
                    $('.delete-fight').click(function(event){
                        event.preventDefault();
                        var idFightsImage   = $(this).attr('id-FightsImage');
                        var loadPage        = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'deleteFight'));?>/" + idFightsImage;
                        $.post(loadPage, function(data){
                            if(data == '1'){
                                var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                                htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                                htmlMessage += '<?php echo __('The Image relationship has been deleted', true);?>';
                                htmlMessage += '</div></div></div>';
                                $('#page-wrapper').prepend(htmlMessage);
                                $('#fight-' + idFightsImage).fadeOut();
                            }
                        });
                    });
                    
                });
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $('.delete-fight').click(function(event){
            event.preventDefault();
            var idFightsImage   = $(this).attr('id-FightsImage');
            var loadPage        = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'deleteFight'));?>/" + idFightsImage;
            $.post(loadPage, function(data){
                if(data == '1'){
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image relationship has been deleted', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                    $('#fight-' + idFightsImage).fadeOut();
                }
            });
        });
        
        $('.search-events').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Events', 'action' => 'getEvents'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('.search-events').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                var saveEvent = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'saveEvent/' . $idImage));?>/" + ui.item.id;
                $.getJSON(saveEvent, function(data){
                    
                    var EventData = "<tr id='event-" + data.id + "'>";
                    EventData += "<td>";
                    EventData += "<a href='<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit'));?>/" + Base64.encode(data.Event.Event.id) + "'>";
                    EventData += data.Event.Event.name;
                    EventData += "</a>";
                    EventData += "</td>";
                    EventData += "<td>";
                    EventData += "<a href='#' class='btn btn-danger delete-event' id-event='" + data.id + "'>";
                    EventData += "<i class='fa fa-trash'></i>";
                    EventData += "</a>";
                    EventData += "</td>";
                    EventData += "</tr>";
                    
                    $('.search-events').val("");
                    
                    $('.events-table').append(EventData);
                    
                    $('.delete-event').click(function(event){
                        event.preventDefault();
                        var idEventImage    = $(this).attr('id-event');
                        var loadPage        = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'deleteEvent'));?>/" + idEventImage;
                        $.post(loadPage, function(data){
                            if(data == '1'){
                                var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                                htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                                htmlMessage += '<?php echo __('The Image relationship has been deleted', true);?>';
                                htmlMessage += '</div></div></div>';
                                $('#page-wrapper').prepend(htmlMessage);
                                $('#event-' + idEventImage).fadeOut();
                            }
                        });
                    });
                    
                });
                
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $('.delete-event').click(function(event){
            event.preventDefault();
            var idEventImage    = $(this).attr('id-event');
            var loadPage        = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'deleteEvent'));?>/" + idEventImage;
            $.post(loadPage, function(data){
                if(data == '1'){
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image relationship has been deleted', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                    $('#event-' + idEventImage).fadeOut();
                }
            });
        });
        
        $('.search-venues').autocomplete({
            minLength: 3,
            source: function(request, response){
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Venues', 'action' => 'getVenue'));?>/" + request.term;
                $.getJSON(loadIdentities, function(data){
                    response(data);
                });
            },
            focus: function( event, ui ) {
                $('.search-identity').val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                
                var saveVenue = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'saveVenues/' . $idImage));?>/" + ui.item.id;
                $.getJSON(saveVenue, function(data){
                    var VenueData = "<tr id='event-" + data.id + "'>";
                    VenueData += "<td>";
                    VenueData += "<a href='<?php echo $this->Html->url(array('controller' => 'Fights', 'action' => 'edit'));?>/" + Base64.encode(data.Venue.Venue.id) + "'>";
                    VenueData += data.Venue.Venue.name;
                    VenueData += "</a>";
                    VenueData += "</td>";
                    VenueData += "<td>";
                    VenueData += "<a href='#' class='btn btn-danger delete-event' id-event='" + data.id + "'>";
                    VenueData += "<i class='fa fa-trash'></i>";
                    VenueData += "</a>";
                    VenueData += "</td>";
                    VenueData += "</tr>";
                    $('.search-venues').val("");
                    $('.venue-data').append(VenueData);
                    
                    $('.delete-venue').click(function(){
                        event.preventDefault();
                        var idVenueImage    = $(this).attr('id-venue');
                        var loadPage        = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'deleteVenue'));?>/" + idVenueImage;
                        $.post(loadPage, function(data){
                            if(data == '1'){
                                var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                                htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                                htmlMessage += '<?php echo __('The Image relationship has been deleted', true);?>';
                                htmlMessage += '</div></div></div>';
                                $('#page-wrapper').prepend(htmlMessage);
                                $('#venue-' + idVenueImage).fadeOut();
                            }
                        });
                    });
                    
                });
                
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li>" )
            .append( "<a>" + item.label + " - " + item.City + " " + item.State + ", " + item.Country + "</a>" )
            .appendTo( ul );
        };
        
        $('.delete-venue').click(function(){
            event.preventDefault();
            var idVenueImage    = $(this).attr('id-venue');
            var loadPage        = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'deleteVenue'));?>/" + idVenueImage;
            $.post(loadPage, function(data){
                if(data == '1'){
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image relationship has been deleted', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                    $('#venue-' + idVenueImage).fadeOut();
                }
            });
        });
        
    });
</script>