<style type="text/css">
    .form-conted{
        display: none;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Images', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-file-image-o"></i> <?php echo __('Images', true)?></li>
        </ol>
    </div>
</div>
<div class="row">
    
    <div class="col-lg-6">
        <div class="col-lg-10">
            <?php echo $this->Form->input('keywork', array('id' => 'search-keyword', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
        </div>
        <div class="col-lg-2">
            <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'search-images')); ?>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-offset-4">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add images', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>

<div class="row">
    <?php foreach ($images as $image) { ?>
        <div class="col-sm-6 col-md-4" style="margin-bottom: 20px;">
            <div class="thumbnail">
                <a class="thumbnail" title="<?php echo $image['Image']['title']?>" href="<?php echo $this->Html->url('/files/img/' . $image['Image']['url']); ?>" target="_black">
                    <img src="<?php echo $this->Html->url('/files/img/' . $image['Image']['url']);?>" style="height: 200px;">
                </a>    
                <div class="caption">
                    <div class="form-conted" id="form-<?php echo $image['Image']['id']?>">
                        
                    </div>
                    <div id="button-<?php echo $image['Image']['id']?>" class="btn-group btn-group-justified">
                        <a class="btn btn-info display-form" target-form="form-<?php echo $image['Image']['id']?>" id-image="<?php echo $image['Image']['id']?>">
                            <i class="fa fa-pencil"></i> <?php echo __('Edit Image', true);?>
                        </a>
                        <a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'assign/' . base64_encode($image['Image']['id']) ));?>">
                            <i class="fa fa-share"></i> <?php echo __('Assign Images', true); ?>
                        </a>
                        <a href="<?php echo $this->Html->url(array('action' => 'delete/' . $image['Image']['id']));?>" class="btn btn-danger btn-delete">
                            <i class="fa fa-trash-o"></i> <?php echo __('Delete Image')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        
        $('.save-image-form').click(function(event){
            event.preventDefault();
            var id_image = $(this).attr('id-image');
            var loadPage = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'saveImage'));?>";
            $.post(loadPage, $('#image-form-' + id_image).serialize(), function(data){
                if(data === '1'){
                    $('#form-' + id_image).slideUp(function(){
                        $('#button-' + id_image).fadeIn();
                    });
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-success alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image has been saved', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                } else {
                    var htmlMessage = '<div class="row"><div class="col-lg-12"><div class="alert alert-danger alert-dismissable">'; 
                    htmlMessage += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    htmlMessage += '<?php echo __('The Image could not be saved. Please, try again.', true);?>';
                    htmlMessage += '</div></div></div>';
                    $('#page-wrapper').prepend(htmlMessage);
                }
                $('html,body').animate( { scrollTop:$("html").offset().top } , 1000);
            });
            //alert(id_image);
        });
        
        $('.display-form').click(function(event) {
            /*event.preventDefault();
            var elementDisplay = '#' + $(this).attr('target-form');
            $(this).parent().fadeOut(function(){
                $(elementDisplay).slideDown();
            });*/
            var idImage = $(this).attr('id-image');
            var urlForm = "<?php echo $this->Html->url(array('controller' => 'Images', 'action' => 'formImage'));?>/" + idImage;
            $.post(urlForm, function(data){
                $('#form-' + idImage).html(data);
                $('#form-' + idImage).toggle('slow');
                $('#button-' + idImage).fadeOut();
            });
        });

        $('.display-button').click(function(event) {
            event.preventDefault();
            var elementDisplay = '#' + $(this).attr('target-button');
            $(this).parent().parent().parent().slideUp(function(){
                $(elementDisplay).fadeIn();
            });
        });
        
        $('#search-images').click(function(event) {
            var keyword = Base64.encode($('#search-keyword').val());
            var loadsearch = "<?php echo $this->Html->url(array('action' => 'searchImage')) ?>/";
            window.location.replace(loadsearch + keyword);
        });
       $(".prevented").click(function(event){
           event.preventDefault();
           
       });
         /*$(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none",
            closeBtn  : false,
            helpers : {
                title : {
                    type : 'inside'
                }
            },
            afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
        });*/
        
    });
</script>