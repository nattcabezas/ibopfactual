<?php foreach ($identities as $identity){ ?>
    <tr>
        <td>
            <?php if( (isset($identity['IdentitiesImage']['Image'])) && ($identity['IdentitiesImage']['Image']['url'] != null)) {?>
                <img src="<?php echo $this->Html->url('/files/img/' . $identity['IdentitiesImage']['Image']['url']);?>" class="img img-responsive img-thumbnail img-search" alt="<?php echo $identity['Identity']['fullname']?>">
            <?php } else {?>
                <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg')?>" class="img img-responsive img-thumbnail img-search" alt="<?php echo $identity['Identity']['fullname']?>">
            <?php }?>
        </td>
        <td>
            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($identity['Identity']['id'], $identity['Identity']['fullname']))?>">
                <?php echo $identity['Identity']['fullname']?>
            </a>
        </td>
        <td>
            <?php 
                if(isset($identity['IdentityAlias'][0]['alias'])){
                    echo $identity['IdentityAlias'][0]['alias'];
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($identity['IdentityAlias'][0]['nickname'])){
                    echo $identity['IdentityNickname'][0]['nickname'];
                }
            ?>
        </td>
        <td><?php echo $identity['Identity']['birth_name']?></td>
        <td>
            <?php 
                if(isset($identity['Career']['win'])){
                    echo '<span class="win-label">'. __('W', true) . '('.$identity['Career']['win'].')</span> ';
                }
                if(isset($identity['Career']['lost'])){
                    echo '<span class="lost-label">' . __('L') . '('.$identity['Career']['lost'].')</span> ';
                }
                if(isset($identity['Career']['draw'])){
                    echo '<span class="draw-label">'. __('D') .'('.$identity['Career']['draw'].')</span> ';
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($identity['Career']['firts'])){
                    echo 'Firts Fight: ' . date('F j, Y', strtotime($identity['Career']['firts'])) . '<br>';
                }
                if(isset($identity['Career']['last'])){
                    echo 'Last Fight: ' . date('F j, Y', strtotime($identity['Career']['last']));
                }
            ?>
        </td>
        <td>
            <?php
                if(isset($identity['IdentityResidence']['Country'])){
                    if($identity['IdentityResidence']['Country'] != null){
                        echo '<img src="' . $this->Html->url('/img/flags/' . $identity['IdentityResidence']['Flag']) . '"> ';
                        echo $identity['IdentityResidence']['Country'];
                        if( (isset($identity['IdentityResidence']['State'])) && ($identity['IdentityResidence']['State'] != null) ){
                            echo ', ' . $identity['IdentityResidence']['State'];
                            if( (isset($identity['IdentityResidence']['City'])) && ($identity['IdentityResidence']['City'] != null) ){
                                echo ', ' . $identity['IdentityResidence']['City'];
                            }
                        }
                    }
                }
            ?>
        </td>
    </tr>
<?php }