<div class="container search-container">
    <div class="row">
        <div class="col-lg-12">
            <center>
                <form action="<?php echo $this->Html->url(array('controller' => 'Persons', 'action' => 'search'));?>" class="col-lg-6 search-person-form" role="form" id="person-search" method="post" accept-charset="utf-8">
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="icon-addon addon-lg">
                                <input type="text" placeholder="Name" value="<?php echo $keyword?>" class="form-control" id="keyword-person" name="keyword">
                                <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <button class="btn btn-primary btn-lg btn-group-justified" id="persons-button" type="submit"><?php echo __('Find!', true);?></button>
                    </div>
                </form>
            </center>
        </div>
    </div>
    
    <?php echo $this->element('banners', array('thisBanner' => $searchPersons)); ?>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th></th>
                            <th><?php echo __('Name', true);?></th>
                            <th><?php echo __('Alias', true);?></th>
                            <th><?php echo __('Nickname', true);?></th>
                            <th><?php echo __('Birth Name', true);?></th>
                            <th><?php echo __('W-L-D', true);?></th>
                            <th><?php echo __('career', true);?></th>
                            <th><?php echo __('residence', true);?></th>
                        </tr>
                    </thead>
                    <tbody class="persons-data">
                        <?php foreach ($identities as $identity){ ?>
                            <?php if( $identity['Identity']['full_name'] != "unknown " ){ ?>
                                <tr>
                                    <td>
                                        <?php if( (isset($identity['IdentitiesImage']['Image'])) && ($identity['IdentitiesImage']['Image']['url'] != null)) {?>
                                            <img src="<?php echo $this->Html->url('/files/img/' . $identity['IdentitiesImage']['Image']['url']);?>" class="img img-responsive img-thumbnail img-search" alt="<?php echo $identity['Identity']['full_name']?>">
                                        <?php } else {?>
                                            <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg')?>" class="img img-responsive img-thumbnail img-search" alt="<?php echo $identity['Identity']['full_name']?>">
                                        <?php }?>
                                    </td>
                                    <td>
                                        <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($identity['Identity']['id'], $identity['Identity']['full_name']))?>">
                                            <?php echo $identity['Identity']['full_name']?>
                                        </a>
                                    </td>
                                    <td>
                                        <?php 
                                            if(isset($identity['IdentityAlias'][0]['alias'])){
                                                echo $identity['IdentityAlias'][0]['alias'];
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            if(isset($identity['IdentityNickname'][0]['nickname'])){
                                                echo $identity['IdentityNickname'][0]['nickname'];
                                            }
                                        ?>
                                    </td>
                                    <td><?php echo $identity['Identity']['birth_name']?></td>
                                    <td>
                                        <?php 
                                            if(isset($identity['Career']['win'])){
                                                echo '<span class="win-label">'. __('W', true) . '('.$identity['Career']['win'].')</span> ';
                                            }
                                            if(isset($identity['Career']['lost'])){
                                                echo '<span class="lost-label">' . __('L') . '('.$identity['Career']['lost'].')</span> ';
                                            }
                                            if(isset($identity['Career']['draw'])){
                                                echo '<span class="draw-label">'. __('D') .'('.$identity['Career']['draw'].')</span> ';
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            if(isset($identity['Career']['firts'])){
                                                echo 'First Fight: ' . date('F j, Y', strtotime($identity['Career']['firts'])) . '<br>';
                                            }
                                            if(isset($identity['Career']['last'])){
                                                echo 'Last Fight: ' . date('F j, Y', strtotime($identity['Career']['last']));
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if(isset($identity['IdentityResidence']['Country'])){
                                                if($identity['IdentityResidence']['Country'] != null){
                                                    echo '<img src="' . $this->Html->url('/img/flags/' . $identity['IdentityResidence']['Flag']) . '"> ';
                                                    echo $identity['IdentityResidence']['Country'];
                                                    if( (isset($identity['IdentityResidence']['State'])) && ($identity['IdentityResidence']['State'] != null) ){
                                                        echo ', ' . $identity['IdentityResidence']['State'];
                                                        if( (isset($identity['IdentityResidence']['City'])) && ($identity['IdentityResidence']['City'] != null) ){
                                                            echo ', ' . $identity['IdentityResidence']['City'];
                                                        }
                                                    }
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="load-persons">
                <center><i class="fa fa-gloves fa-spin fa-3x"></i></center>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var numPage = 2;
        $(window).scroll(function(){
            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100){
                $('.load-persons').fadeIn();
                var loadPage = "<?php echo $this->Html->url(array('action' => 'searchPage/' . $keywordSearch));?>/" + numPage;
                numPage++;
                $.post(loadPage, function(data){
                    $('.persons-data').append(data);
                    $('.load-persons').fadeOut();
                });
            }
        });
    });
</script>