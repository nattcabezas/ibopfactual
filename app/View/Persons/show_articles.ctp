<table class="table table-hover table-condensed">
     <tbody>
        <?php foreach ($articles as $article){ ?>
            <tr>
                <td>
                    <a id="<?php echo "search-articles" . $article['Article']['id']  ?>" >
                        <h2><?php echo $article['Article']['title']; ?></h2>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php
    echo $this->Html->script('article_scripts.js');
?>


<script type="text/javascript">
    $(document).ready(function(){
        <?php foreach ($articles as $article){ ?>
            var articleID = "<?php echo $article['Article']['id'];?>";           
            var loadsearch = "<?php echo $this->Html->url('/Article/')?>";
            var category = "<?php echo $article['Category']['name']?>";
            var idConNombre = "<?php echo $article['Article']['id']?>" + "-" + "<?php echo $article['Article']['title']?>";
            $("#search-articles"+articleID).attr("href", loadsearch+category+"/"+idConNombre);
        <?php } ?>

    });
</script>