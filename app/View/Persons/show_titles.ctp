<?php $count = 0; ?>
<?php foreach ($titles as $title){ ?>
<h3 class="text-center"><?php echo $title['Titles']['name']?> <br>(<?php echo $title['Titles']['description']?>)</h3>
    <div class="">
        <table class="responsive responsive-table table_basic" id="titles-table">
            <thead>
                <tr>
                    <th><?php echo __('Date', true);?></th>
                    <th><?php echo __('Fight', true);?></th>
                    <th><?php echo __('Via', true);?></th>
                    <th><?php echo __('Previously Owned', true);?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($titlesDetails[$count] as $fights){ ?>
                    <tr>
                        
                        <td class="fight_date" data-content="<?php echo __('Date', true);?>"><?php echo $fights['Event']['date']?></td>
                        <td data-content="<?php echo __('Fight', true);?>"><a href=""><?php echo $fights['Fights']['title']?></a></td>
                        <td data-content="<?php echo __('Via', true);?>"><?php echo $fights['Fights']['via']?></td>
                        <td data-content="<?php echo __('Previously', true);?>">
                            <a href="">
                                <?php echo $fights['Identity']['name']?> <?php echo $fights['Identity']['last_name']?>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php $count++; ?>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#titles-table").tablesorter({sortList: [[0,1]]});        
    });
</script>