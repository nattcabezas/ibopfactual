<div class="container detail-container">
    <div class="container-fluid">
        <div class="col-lg-12">
            
            <div class="container">
                <div class="row mobile-social-share">
                    <div class="col-md-9">
                        <h1><?php echo $identity['Identity']['name']?> <?php echo $identity['Identity']['last_name']?></h1>
                    </div>
                    <?php 
                        if( (isset($identity['IdentitiesImage']['Images']['url'])) && ($identity['IdentitiesImage']['Images']['url'] != null) ){
                            $imageShare = $this->Html->url('/files/img/' . $identity['IdentitiesImage']['Images']['url']);
                        } else {
                            $imageShare = $this->Html->url('/img/defaults/generic_fighter.jpg');
                        }
                    ?>
                    <?php echo $this->element('social-holder', array('shareTitle' => $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'], 'imageShare' => $imageShare));?>
                    
                </div>
            </div>
            
            <div class="col-lg-3">
                <?php if( (isset($identity['IdentitiesImage']['Images']['url'])) && ($identity['IdentitiesImage']['Images']['url'] != null) ){?>
                    <img src="<?php echo $this->Html->url('/files/img/' . $identity['IdentitiesImage']['Images']['url']);?>" class="img_entity" width="250" >  
                <?php } else { ?>
                    <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg')?>" class="img_entity" width="250" >
                <?php } ?>
                <?php echo $this->Qrcode->url('http://ibopfactual.com'.$this->here, array('size' => '250x250')); ?>
            </div>

            <div class="col-lg-9 ">
                
                <?php if( ($record['boxing']['is'] > 0) || ($record['mma']['is'] > 0) ) {?>
                    <div class="row">
                        <div class="col-lg-12 col-md-5 col-sm-8 col-xs-11 bhoechie-tab-container">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                                <div class="list-group">
                                    <?php if($record['boxing']['is'] > 0){ ?>
                                        <a href="#" class="list-group-item active text-center" style="min-height: <?php echo $record['mma']['is'] > 0 ? '122px' : '244px'; ?>; padding-top: <?php echo $record['mma']['is'] > 0 ? '25px' : '80px'; ?>";>
                                            <i class="fa fa-gloves fa-3x"></i><br>Boxing
                                        </a>
                                    <?php } ?>
                                    <?php if($record['mma']['is'] > 0){ ?>
                                    <a href="#" class="list-group-item text-center <?php echo $record['boxing']['is'] == 0 ? 'active' : ''; ?>" style="min-height: <?php echo $record['boxing']['is'] > 0 ? '122px' : '244px'; ?>; padding-top: <?php echo $record['boxing']['is'] > 0 ? '25px' : '80px'; ?>;">
                                        <i class="fa fa-mma-ring fa-3x"></i><br>MMA
                                    </a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">

                                <?php if($record['boxing']['is'] > 0){ ?>
                                <div class="bhoechie-tab-content active">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <thead><h4><?php echo __('Boxing Record', true);?>: </h4></thead>
                                            <tbody>
                                                <tr>
                                                    <td><span class="won"><?php echo __('won', true);?>: </span></td>
                                                    <td>
                                                        <strong>
                                                            <?php 
                                                                echo $record['boxing']['won'];
                                                                if( ($record['boxing']['win_ko'] > 0) || ($record['boxing']['won_nws'] > 0)){
                                                                    echo ' (';
                                                                    if($record['boxing']['win_ko'] > 0){
                                                                        echo __('KO', true) . ' '. $record['boxing']['win_ko'];
                                                                        if($record['boxing']['won_nws'] > 0){
                                                                            echo ', ';
                                                                        }
                                                                    }
                                                                    if($record['boxing']['won_nws'] > 0){
                                                                        echo __('NWS') . ' ' . $record['boxing']['won_nws'];
                                                                    }
                                                                    echo ')';
                                                                }
                                                            ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="lost"><?php echo __('lost', true);?>: </span>
                                                    <td>
                                                        <strong>
                                                            <?php 
                                                                echo $record['boxing']['lost'];
                                                                if( ($record['boxing']['lost_ko'] > 0) || ($record['boxing']['lost_nws'] > 0)){
                                                                    echo ' (';
                                                                    if($record['boxing']['lost_ko'] > 0){
                                                                        echo __('KO', true) . ' '. $record['boxing']['lost_ko'];
                                                                        if($record['boxing']['lost_nws'] > 0){
                                                                            echo ', ';
                                                                        }
                                                                    }
                                                                    if($record['boxing']['lost_nws'] > 0){
                                                                        echo __('NWS') . ' ' . $record['boxing']['lost_nws'];
                                                                    }
                                                                    echo ')';
                                                                }
                                                            ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="draw"><?php echo __('draw', true);?>:</span></td>
                                                    <td>
                                                        <strong>
                                                            <?php 
                                                                echo $record['boxing']['draw'];
                                                                if( $record['boxing']['draw_nws'] > 0 ){
                                                                    echo '(' . __('D - NWS') . ' ' . $record['boxing']['draw_nws'] . ')';
                                                                }
                                                            ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="won"><?php echo __('Box rounds fought', true);?>: </span></td>
                                                    <td><strong><?php echo $record['boxing']['rounds']?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td><span class="won"><?php echo __('Box KO pct', true);?>:</span></td>
                                                    <td>
                                                        <strong>
                                                            <?php echo round(($record['boxing']['win_perc'] / $record['boxing']['total']) * 100, 2) . '%'; ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if($record['mma']['is'] > 0){ ?>
                                <div class="bhoechie-tab-content <?php echo $record['boxing']['is'] == 0 ? 'active' : ''; ?>">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <thead><h4><?php echo __('MMA Record', true);?>:</h4></thead>
                                            <tbody>
                                                <tr>
                                                    <td><span class="won"><?php echo __('won', true);?>: </span></td>
                                                    <td>
                                                        <strong>
                                                            <?php 
                                                                echo $record['mma']['won'];
                                                                if($record['mma']['win_ko'] > 0){
                                                                    echo '('. __('KO', true) . ' ' . $record['mma']['win_ko'] . ')';
                                                                }
                                                            ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="lost"><?php echo __('lost', true);?>: </span>
                                                    <td>
                                                        <strong>
                                                            <?php 
                                                                echo $record['mma']['lost'];
                                                                if($record['mma']['lost_ko'] > 0){
                                                                    echo '('. __('KO', true) . ' ' . $record['mma']['lost_ko'] . ')';
                                                                }
                                                            ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="draw"><?php echo __('draw', true);?>: </span></td>
                                                    <td>
                                                        <strong><?php echo $record['mma']['draw']; ?></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="won"><?php echo __('MMA rounds fought', true);?>:</span></td>
                                                    <td>
                                                        <strong><?php echo $record['mma']['rounds']; ?></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="won"><?php echo __('MMA KO pct', true);?>:</span></td>
                                                    <td>
                                                        <strong>
                                                            <?php 
                                                                echo round(($record['mma']['won'] / $record['mma']['is']) * 100, 2) . '%';
                                                            ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                    <hr>
                <?php } ?>
                
                <div class="fighters-details">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#general-panel" class="h4" data-toggle="tab"><?php echo __('General details', true);?></a>
                        </li>
                        <?php if( $countFights >  0){ ?>
                            <li>
                                <a href="#professional-record" id="load-record" class="h4" data-toggle="tab"><?php echo __('Professional record', true);?></a>
                            </li>
                        <?php } ?>
                        <?php if($countJobs) {?>
                            <li>
                                <a href="#professional-jobs" id="load-jobs" class="h4" data-toggle="tab"><?php echo __('Jobs', true);?></a>
                            </li>
                        <?php } ?>
                        <?php if( $countImages >  0){ ?>
                            <li>
                                <a href="#photos-panel" class="h4" id="load-images" data-toggle="tab"><?php echo __('Photos', true);?></a>
                            </li>
                        <?php } ?>
                        <?php if( $countVideos >  0){ ?>
                            <li>
                                <a href="#videos-panel" class="h4" id="load-videos" data-toggle="tab"><?php echo __('Videos', true);?></a>
                            </li>
                        <?php } ?>
                        <?php if( (isset($identity['IdentityBiography']['biography'])) && ($identity['IdentityBiography']['biography'] != null) ){ ?>
                            <li>
                                <a href="#biography-panel" class="h4" id="load-biography" data-toggle="tab"><?php echo __('Biography', true);?></a>
                            </li>
                        <?php } ?>
                        <?php if($countNotes > 0){ ?>
                            <li>
                                <a href="#notes-panel" class="h4" id="load-notes" data-toggle="tab"><?php echo __('Notes', true);?></a>
                            </li>
                        <?php } ?>
                        <?php if($countFamily > 0){ ?>
                            <li>
                                <a href="#family-panel" class="h4" id="load-family" data-toggle="tab"><?php echo __('Relationships', true);?></a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        
                        <div class="tab-pane fade in active" id="general-panel"><br>
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed">
                                    <tbody>
                                        <?php if($identity['Identity']['birth_name'] != null){ ?>
                                            <tr>
                                                <td>Birth name:</td>
                                                <td>
                                                    <strong><?php echo $identity['Identity']['birth_name']?></strong>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php if( (isset($identity['IdentityAlias'][0]['alias'])) && ($identity['IdentityAlias'][0]['alias'] != null) ){ ?>    
                                            <tr>
                                                <td>Alias:</td>
                                                <td>
                                                    <strong><?php echo $identity['IdentityAlias'][0]['alias']; ?></strong>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php if( (isset($identity['IdentityBirth']['countries_id'])) && ($identity['IdentityBirth']['Country'] != null) ){ ?>
                                            <tr>
                                                <td>Birth location:</td>
                                                <td>
                                                    <strong>
                                                        <?php 
                                                            if($identity['IdentityBirth']['City'] != null){
                                                                echo $identity['IdentityBirth']['City'] . ', ';
                                                            }
                                                            if($identity['IdentityBirth']['State'] != null){
                                                                echo $identity['IdentityBirth']['State'] . ', ';
                                                            }
                                                            if($identity['IdentityBirth']['Country'] != null){
                                                                echo $identity['IdentityBirth']['Country'];
                                                            }
                                                        ?>
                                                    </strong>
                                                </td>

                                            </tr>
                                        <?php } ?>
                                        <?php if( (isset($identity['IdentityBirth']['date'])) && ($identity['IdentityBirth']['date'] != null) ) { ?>
                                            <tr>
                                                <td>Date of birth:</td>
                                                <td>
                                                    <strong>
                                                        <?php echo date('l jS \of F Y', strtotime($identity['IdentityBirth']['date']));?>
                                                    </strong>
                                                    <?php 
                                                        if( (!isset($identity['IdentityDeath']['date'])) || ($identity['IdentityDeath']['date'] == null) ) { 
                                                            echo ' (age ' . $this->getage->age($identity['IdentityBirth']['date'], date('Y-m-d')) . ')';
                                                        }
                                                    ?>
                                                </td>

                                            </tr>
                                        <?php } ?>
                                        <?php if( (isset($identity['IdentityDeath']['date'])) && ($identity['IdentityDeath']['date'] != null) ) { ?>
                                            <tr>
                                                <td>Date of death:</td>
                                                <td>
                                                    <strong>
                                                        <?php echo date('l jS \of F Y', strtotime($identity['IdentityDeath']['date']));?>
                                                    </strong>
                                                    <?php 
                                                        if( (isset($identity['IdentityBirth']['date'])) && ($identity['IdentityBirth']['date'] != null) ) { 
                                                            echo ' (age ' . $this->getage->age($identity['IdentityBirth']['date'], $identity['IdentityDeath']['date']) . ')';
                                                        }
                                                    ?>
                                                </td>

                                            </tr>
                                        <?php } ?>
                                        <?php if( (isset($identity['IdentityResidence']['countries_id'])) && ($identity['IdentityResidence']['countries_id'] != null) ) { ?>
                                            <tr>
                                                <td>Residence:</td>
                                                <td>
                                                    <strong>
                                                        <?php 
                                                            if((isset($identity['IdentityResidence']['Cities']['name'])) && ($identity['IdentityResidence']['Cities']['name'] != null)){
                                                                echo $identity['IdentityResidence']['Cities']['name'] . ', ';
                                                            }
                                                            if((isset($identity['IdentityResidence']['States']['name'])) && ($identity['IdentityResidence']['States']['name'] != null)){
                                                                echo $identity['IdentityResidence']['States']['name'] . ', ';
                                                            }
                                                            if((isset($identity['IdentityResidence']['Countries']['name'])) && ($identity['IdentityResidence']['Countries']['name'] != null)){
                                                                echo $identity['IdentityResidence']['Countries']['name'];
                                                            }
                                                        ?>
                                                    </strong>
                                                </td>

                                            </tr>
                                        <?php } ?>
                                        <?php if( $identity['Identity']['height'] != null){ ?>
                                            <tr>
                                                <td>Height:</td>
                                                <td>
                                                    <strong>
                                                        <?php 
                                                            echo $identity['Identity']['height'] . ' / ' . $this->getage->getCm($identity['Identity']['height'] ).' cm';   
                                                        ?>
                                                    </strong>
                                                </td>

                                            </tr>
                                        <?php } ?>
                                        <?php if( $identity['Identity']['reach'] != null){ ?>
                                            <tr>
                                                <td>Reach:</td>
                                                <td>
                                                    <strong>
                                                        <?php 
                                                            echo $identity['Identity']['reach'] . ' / ' . $this->getage->getReachCm($identity['Identity']['reach'] ).' cm';   
                                                        ?>
                                                    </strong>
                                                </td>

                                            </tr>
                                        <?php } ?>
                                        <?php if( $identity['Identity']['stance'] != null){ ?>
                                            <tr>
                                                <td>Stance:</td>
                                                <td><strong><?php echo $identity['Identity']['stance']; ?></strong></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <?php if( $countFights >  0){ ?>
                            <div class="tab-pane fade" id="professional-record">
                                <div class="col-lg-12" id="loading-record"><br>
                                    <center>
                                        <i class="fa fa-refresh fa-spin fa-3x"></i>
                                    </center>
                                </div>
                            </div>
                        <?php } ?>
                        
                        <?php if($countJobs) {?>
                            <div class="tab-pane fade" id="professional-jobs">
                                <div class="col-lg-12" id="loading-jobs"><br>
                                    <center>
                                        <i class="fa fa-refresh fa-spin fa-3x"></i>
                                    </center>
                                </div>
                            </div>
                        <?php } ?>
                        
                        
                        <?php if( $countImages >  0){ ?>
                            <div class="tab-pane fade" id="photos-panel">
                                <div class="col-lg-12" id="load-img"><br>
                                    <center>
                                        <i class="fa fa-refresh fa-spin fa-3x"></i>
                                    </center>
                                </div>
                            </div>
                        <?php } ?>
                        
                        <?php if( $countVideos >  0){ ?>
                            <div class="tab-pane fade" id="videos-panel">
                                <div class="col-lg-12" id="load-video"><br>
                                    <center>
                                        <i class="fa fa-refresh fa-spin fa-3x"></i>
                                    </center>
                                </div>
                            </div>
                        <?php } ?>
                        
                        <?php if( (isset($identity['IdentityBiography']['biography'])) && ($identity['IdentityBiography']['biography'] != null) ){ ?>
                        <div class="tab-pane fade" id="biography-panel">
                            <div class="col-lg-12"><br>
                                <?php echo $identity['IdentityBiography']['biography']?>
                            </div>
                        </div>
                        <?php } ?>
                        
                        <?php if($countNotes > 0){ ?>
                            <div class="tab-pane fade" id="notes-panel">
                                <div class="col-lg-12" id="loading-notes"><br>
                                    <center>
                                        <i class="fa fa-refresh fa-spin fa-3x"></i>
                                    </center>
                                </div>
                            </div>
                        <?php } ?>
                        
                        <?php if($countFamily > 0){ ?>
                            <div class="tab-pane fade" id="family-panel">
                                <div class="col-lg-12" id="loading-family"><br>
                                    <center>
                                        <i class="fa fa-refresh fa-spin fa-3x"></i>
                                    </center>
                                </div>
                            </div>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>    
    <div class="clear"></div>
    <?php echo $this->element('banners', array('thisBanner' => $detailPersons)); ?>    
</div>
<div class='footer_separator'></div>
<script type="text/javascript">
    
    $(document).ready(function() {
        
        var loadImages = true;
        $('#load-images').click(function(){
            if(loadImages){
                var loadPageImages = "<?php echo $this->Html->url(array('action' => 'getImagesPerson/' . $id));?>";
                $.post(loadPageImages, function(data){
                    $('#load-img').fadeOut(function(){
                        $('#photos-panel').append(data);
                        loadImages = false;
                    });
                });
            }
        });
        
        var loadVideos = true;
        $('#load-videos').click(function(){
            if(loadVideos){
                var loadPageVideos = "<?php echo $this->Html->url(array('action' => 'getVideosPerson/' . $id));?>";
                $.post(loadPageVideos, function(data){
                    $('#load-video').fadeOut(function(){
                        $('#videos-panel').append(data);
                        loadVideos = false;
                    });
                });
            }
        });
        
        var loadRecord = true;
        $('#load-record').click(function(){
            if(loadRecord){
                var LoadRecordPage = "<?php echo $this->Html->url(array('action' => 'showRecord/' . $id));?>";
                $.post(LoadRecordPage, function(data){
                    $('#loading-record').fadeOut(function(){
                        $('#professional-record').append(data);
                        loadRecord = false;
                    });
                });
            }
        });
        
        var loadJobs = true;
        $('#load-jobs').click(function(){
            if(loadJobs){
                var loadJobsPage = "<?php echo $this->Html->url(array('action' => 'showJobs/' . $id)); ?>";
                $.post(loadJobsPage, function(data){
                    $('#loading-jobs').html(data);
                    loadJobs = false;
                });
            }
        });
        
        var loadNotes = true;
        $('#load-notes').click(function(){
            if(loadNotes){
                var loadNotesPage = "<?php echo $this->Html->url(array('action' => 'showNotes/' . $id));?>";
                $.post(loadNotesPage, function(data){
                    $('#loading-notes').html(data);
                    loadNotes = false;
                });
            }
        });
        
        var loadfamily = true;
        $('#load-family').click(function(){
            if(loadfamily){
                var loadFamilyPage = "<?php echo $this->Html->url(array('action' => 'showRelationships/' . $id));?>";
                $.post(loadFamilyPage, function(data){
                    $('#loading-family').html(data);
                    loadfamily = false;
                });
            }
        });
        
    });
</script>