<div class="container search-container">
    <div class="row">
        
        <div class="col-lg-12">
            <form action="<?php echo $this->Html->url(array('controller' => 'Persons', 'action' => 'search'));?>" class="search-person-form" role="form" id="person-search" method="post" accept-charset="utf-8">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Name" value="<?php echo $name?>" class="form-control" id="keyword-person" name="name">
                            <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <input type="text" placeholder="Last Name" value="<?php echo $lastName?>" class="form-control" id="keyword-person" name="last-name">
                            <label for="name" class="glyphicon glyphicon-search" rel="tooltip" title="name"></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <button class="btn btn-primary btn-lg btn-group-justified" id="persons-button" type="submit"><?php echo __('Find!', true);?></button>
                </div>
            </form>
        </div>
        
    </div>
    
    <?php echo $this->element('banners', array('thisBanner' => $searchPersons)); ?>

    <table class="responsive table_basic">

        <thead>
            <tr>                        
                <th></th>
                <th><?php echo __('Name', true); ?></th>
                <th><?php echo __('Alias', true); ?></th>
                <th><?php echo __('Nickname', true); ?></th>
                <th><?php echo __('Birth Name', true); ?></th>
                <th><?php echo __('W-L-D', true); ?></th>
                <th><?php echo __('Career', true); ?></th>
                <th><?php echo __('Residence', true); ?></th>
            <tr>
        </thead>
        <tbody class="persons-data table_strips">
            <?php foreach ($identities as $identity) { ?>
                <?php if ($identity['Identity']['fullname'] != "unknown ") { ?>
                    <tr>
                        <td>
                            <?php if ((isset($identity['IdentitiesImage']['Image'])) && ($identity['IdentitiesImage']['Image']['url'] != null)) { ?>
                                <img src="<?php echo $this->Html->url('/files/img/' . $identity['IdentitiesImage']['Image']['url']); ?>" class="img img-responsive img-thumbnail img-search" alt="<?php echo $identity['Identity']['fullname'] ?>">
                            <?php } else { ?>
                                <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" class="img img-responsive img-thumbnail img-search" alt="<?php echo $identity['Identity']['fullname'] ?>">
                            <?php } ?>
                        </td>
                        <td data-content="<?php echo __('Name', true);?>">
                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($identity['Identity']['id'], $identity['Identity']['fullname'])) ?>">
                                <?php echo $identity['Identity']['fullname'] ?>
                            </a>
                        </td>
                        <td data-content="<?php echo __('Alias', true);?>">
                            <?php
                            if (isset($identity['IdentityAlias'][0]['alias'])) {
                                echo $identity['IdentityAlias'][0]['alias'];
                            }
                            ?>
                        </td>
                        <td data-content="<?php echo __('Nickname', true);?>">
                            <?php
                            if (isset($identity['IdentityNickname'][0]['nickname'])) {
                                echo $identity['IdentityNickname'][0]['nickname'];
                            }
                            ?>
                        </td>
                        <td data-content="<?php echo __('Birth Name', true);?>"><?php echo $identity['Identity']['birth_name'] ?></td>
                        <td data-content="<?php echo __('W-L-D', true);?>">
                            <?php
                            if (isset($identity['Career']['win'])) {
                                echo '<span class="win-label">' . __('W', true) . '(' . $identity['Career']['win'] . ')</span> ';
                            }
                            if (isset($identity['Career']['lost'])) {
                                echo '<span class="lost-label">' . __('L') . '(' . $identity['Career']['lost'] . ')</span> ';
                            }
                            if (isset($identity['Career']['draw'])) {
                                echo '<span class="draw-label">' . __('D') . '(' . $identity['Career']['draw'] . ')</span> ';
                            }
                            ?>
                        </td>
                        <td data-content="<?php echo __('Career', true);?>">
                            <?php
                            if (isset($identity['Career']['firts'])) {
                                echo '<span class="career_detail">First Fight: </span>' . date('F j, Y', strtotime($identity['Career']['firts'])) . '<br>';
                            }
                            if (isset($identity['Career']['last'])) {
                                echo '<span class="career_detail">Last Fight: </span>' . date('F j, Y', strtotime($identity['Career']['last']));
                            }
                            ?>
                        </td>
                        <td data-content="<?php echo __('Residence', true);?>">
                            <?php
                            if (isset($identity['IdentityResidence']['Country'])) {
                                if ($identity['IdentityResidence']['Country'] != null) {
                                    echo '<img src="' . $this->Html->url('/img/flags/' . $identity['IdentityResidence']['Flag']) . '"> ';
                                    echo $identity['IdentityResidence']['Country'];
                                    if ((isset($identity['IdentityResidence']['State'])) && ($identity['IdentityResidence']['State'] != null)) {
                                        echo ', ' . $identity['IdentityResidence']['State'];
                                        if ((isset($identity['IdentityResidence']['City'])) && ($identity['IdentityResidence']['City'] != null)) {
                                            echo ', ' . $identity['IdentityResidence']['City'];
                                        }
                                    }
                                }
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>

    </table>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="load-persons">
                <center><i class="fa fa-gloves fa-spin fa-3x"></i></center>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var numPage = 2;
        $(window).scroll(function(){
            if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100){
                $('.load-persons').fadeIn();
                var loadPage = "<?php echo $this->Html->url(array('action' => 'searchPage/' . $keywordSearch));?>/" + numPage + "/<?php echo $secondQuery?>";
                numPage++;
                $.post(loadPage, function(data){
                    $('.persons-data').append(data);
                    $('.load-persons').fadeOut();
                });
            }
        });
    });
</script>
