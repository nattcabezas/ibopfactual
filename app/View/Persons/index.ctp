<div class="container detail-container">

    <!-- titulo y sharing -->
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-7">
            <h1><?php echo $identity['Identity']['name'] ?> <?php echo $identity['Identity']['last_name']?></h1>
            <?php
            if ((isset($identity['IdentitiesImage']['Images']['url'])) && ($identity['IdentitiesImage']['Images']['url'] != null)) {
                $imageShare = $this->Html->url('/files/img/' . $identity['IdentitiesImage']['Images']['url']);
            } else {
                $imageShare = $this->Html->url('/img/defaults/generic_fighter.jpg');
            }
            ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
            <?php echo $this->element('social-holder', array('shareTitle' => $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'], 'imageShare' => $imageShare)); ?>
        </div>
    </div>

    <!-- imagen y qr code -->     
    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php if ((isset($identity['IdentitiesImage']['Images']['url'])) && ($identity['IdentitiesImage']['Images']['url'] != null)) { ?>
                <img src="<?php echo $this->Html->url('/files/img/' . $identity['IdentitiesImage']['Images']['url']); ?>" class="img-responsive" width="200em" >  
            <?php } else { ?>
                <img src="<?php echo $this->Html->url('/img/defaults/generic_fighter.jpg') ?>" class=" img-responsive" width="200em" >
            <?php } ?>
        </div>
        <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
            <a href="<?php echo $this->Html->url('/Compare/' . $this->getUrlPerson->getUrl($identity['Identity']['id'], $identity['Identity']['name'].' '.$identity['Identity']['last_name']));?>" class="btn btn-primary btn-block"><i class="fa fa-exchange"></i>   <?php echo __('Compare');?></a>
        </div>
        <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
            <?php echo $this->Qrcode->url('http://ibopfactual.com' . $this->here, array('size' => '200x200')); ?>
        </div>
    </div>

    <!-- detalles -->        
    <div class="col-lg-9 col-md-9 col-sm-9">
        <?php if (($record['boxing']['is'] > 0) || ($record['mma']['is'] > 0)) { ?>
            <div class="row div_spacer">

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <div class="list-group">
                        <?php if ($record['boxing']['is'] > 0) { ?>
                            <a href="#" class="list-group-item active text-center" style="min-height: <?php echo $record['mma']['is'] > 0 ? '122px' : '244px'; ?>; padding-top: <?php echo $record['mma']['is'] > 0 ? '25px' : '80px'; ?>";>
                                <i class="fa fa-gloves fa-3x"></i><br>Boxing
                            </a>
                        <?php } ?>
                        <?php if ($record['mma']['is'] > 0) { ?>
                            <a href="#" class="list-group-item text-center <?php echo $record['boxing']['is'] == 0 ? 'active' : ''; ?>" style="min-height: <?php echo $record['boxing']['is'] > 0 ? '122px' : '244px'; ?>; padding-top: <?php echo $record['boxing']['is'] > 0 ? '25px' : '80px'; ?>;">
                                <i class="fa fa-mma-ring fa-3x"></i><br>MMA
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">

                    <?php if ($record['boxing']['is'] > 0) { ?>
                        <div class="bhoechie-tab-content active">
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead><h4><?php echo __('Boxing Record', true); ?>: </h4></thead>
                                    <tbody>
                                        <tr>
                                            <td><span class="won"><?php echo __('Won', true); ?>: </span></td>
                                            <td>
                                                <strong>
                                                    <?php
                                                    echo $record['boxing']['won'];
                                                    if (($record['boxing']['win_ko'] > 0) || ($record['boxing']['won_nws'] > 0)) {
                                                        echo ' (';
                                                        if ($record['boxing']['win_ko'] > 0) {
                                                            echo __('KO', true) . ' ' . $record['boxing']['win_ko'];
                                                            if ($record['boxing']['won_nws'] > 0) {
                                                                echo ', ';
                                                            }
                                                        }
                                                        if ($record['boxing']['won_nws'] > 0) {
                                                            echo __('NWS') . ' ' . $record['boxing']['won_nws'];
                                                        }
                                                        echo ')';
                                                    }
                                                    ?>
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="lost"><?php echo __('Lost', true); ?>: </span>
                                            <td>
                                                <strong>
                                                    <?php
                                                    echo $record['boxing']['lost'];
                                                    if (($record['boxing']['lost_ko'] > 0) || ($record['boxing']['lost_nws'] > 0)) {
                                                        echo ' (';
                                                        if ($record['boxing']['lost_ko'] > 0) {
                                                            echo __('KO', true) . ' ' . $record['boxing']['lost_ko'];
                                                            if ($record['boxing']['lost_nws'] > 0) {
                                                                echo ', ';
                                                            }
                                                        }
                                                        if ($record['boxing']['lost_nws'] > 0) {
                                                            echo __('NWS') . ' ' . $record['boxing']['lost_nws'];
                                                        }
                                                        echo ')';
                                                    }
                                                    ?>
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="draw"><?php echo __('Draw', true); ?>:</span></td>
                                            <td>
                                                <strong>
                                                    <?php
                                                    echo $record['boxing']['draw'];
                                                    if ($record['boxing']['draw_nws'] > 0) {
                                                        echo '(' . __('D - NWS') . ' ' . $record['boxing']['draw_nws'] . ')';
                                                    }
                                                    ?>
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="won"><?php echo __('Box rounds fought', true); ?>: </span></td>
                                            <td><strong><?php echo $record['boxing']['rounds'] ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td><span class="won"><?php echo __('Box KO pct', true); ?>:</span></td>
                                            <td>
                                                <strong>
                                                    <?php echo round(($record['boxing']['win_perc'] / $record['boxing']['total']) * 100, 2) . '%'; ?>
                                                </strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($record['mma']['is'] > 0) { ?>
                        <div class="bhoechie-tab-content <?php echo $record['boxing']['is'] == 0 ? 'active' : ''; ?>">
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead><h4><?php echo __('MMA Record', true); ?>:</h4></thead>
                                    <tbody>
                                        <tr>
                                            <td><span class="won"><?php echo __('won', true); ?>: </span></td>
                                            <td>
                                                <strong>
                                                    <?php
                                                    echo $record['mma']['won'];
                                                    if ($record['mma']['win_ko'] > 0) {
                                                        echo '(' . __('KO', true) . ' ' . $record['mma']['win_ko'] . ')';
                                                    }
                                                    ?>
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="lost"><?php echo __('lost', true); ?>: </span>
                                            <td>
                                                <strong>
                                                    <?php
                                                    echo $record['mma']['lost'];
                                                    if ($record['mma']['lost_ko'] > 0) {
                                                        echo '(' . __('KO', true) . ' ' . $record['mma']['lost_ko'] . ')';
                                                    }
                                                    ?>
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="draw"><?php echo __('draw', true); ?>: </span></td>
                                            <td>
                                                <strong><?php echo $record['mma']['draw']; ?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="won"><?php echo __('MMA rounds fought', true); ?>:</span></td>
                                            <td>
                                                <strong><?php echo $record['mma']['rounds']; ?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="won"><?php echo __('MMA KO pct', true); ?>:</span></td>
                                            <td>
                                                <strong>
                                                    <?php
                                                    echo round(($record['mma']['win_ko'] / $record['mma']['is']) * 100, 2) . '%';
                                                    ?>
                                                </strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>

                </div>

            </div>
            <hr>
        <?php } ?>

        <div class="fighters-details">
            <ul id="myTab" class="nav nav-tabs responsive">
                <li class="active">
                    <a href="#general-panel" class="h4" data-toggle="pill"><?php echo __('General details', true); ?></a>
                </li>
                <?php if ($countFights > 0) { ?>
                    <li>
                        <a href="#professional-record" id="load-record" class="h4 load-professional-record" data-toggle="tab"><?php echo __('Professional record', true); ?></a>
                    </li>
                <?php } ?>

                <?php if ($countJobs) { ?>
                    <li>
                        <a href="#professional-jobs" class="h4 load-jobs" data-toggle="tab"><?php echo __('Jobs', true); ?></a>
                    </li>
                <?php } ?>
                <?php if ($countImages > 0) { ?>
                    <li>
                        <a href="#photos-panel" class="h4 load-images" data-toggle="tab"><?php echo __('Photos', true); ?></a>
                    </li>
                <?php } ?>
                <?php if ($countVideos > 0) { ?>
                    <li>
                        <a href="#videos-panel" class="h4 load-videos" data-toggle="tab"><?php echo __('Videos', true); ?></a>
                    </li>
                <?php } ?>
                <?php if ((isset($identity['IdentityBiography']['biography'])) && ($identity['IdentityBiography']['biography'] != null)) { ?>
                    <li>
                        <a href="#biography-panel" class="h4" id="load-biography" data-toggle="tab"><?php echo __('Biography', true); ?></a>
                    </li>
                <?php } ?>                            
                <?php if ($countNotes > 0) { ?>
                    <li>
                        <a href="#notes-panel" class="h4 load-notes" data-toggle="tab"><?php echo __('Notes', true); ?></a>
                    </li>
                <?php } ?>
                <?php if ($countFamily > 0) { ?>
                    <li>
                        <a href="#family-panel" class="h4 load-family" data-toggle="tab"><?php echo __('Relationships', true); ?></a>
                    </li>
                <?php } ?>
                <?php if ($countContacts > 0) { ?>    
                    <li>
                        <a href="#contact-panel" class="h4 load-contact" data-toggle="tab"><?php echo __('Contact', true); ?></a>
                    </li>
                <?php } ?>    
                <?php if ($countArticles > 0) { ?>    
                    <li>
                        <a href="#article-panel" class="h4 load-articles" data-toggle="tab"><?php echo __('Articles', true); ?></a>
                    </li>
                <?php } ?>    
            </ul>
            <div id="myTabContent" class="tab-content">

                <div class="tab-pane fade in active" id="general-panel"><br>
                    <div class="">
                        <table class="responsive responsive-table table_basic_generaldetails" >
                            <tbody>
                                <?php if ($identity['Identity']['birth_name'] != null) { ?>
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Birth name:', true); ?></td>
                                        <td data-content="<?php echo __('Birth name:', true); ?>">
                                            <strong><?php echo $identity['Identity']['birth_name'] ?></strong>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if ((isset($identity['IdentityAlias'][0]['alias'])) && ($identity['IdentityAlias'][0]['alias'] != null)) { ?>    
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Alias:', true); ?></td>
                                        <td data-content="<?php echo __('Alias:', true); ?>">  
                                            <strong><?php echo $identity['IdentityAlias'][0]['alias']; ?></strong>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <?php if ((isset($identity['IdentityNickname'][0]['nickname'])) && ($identity['IdentityNickname'][0]['nickname'] != null)) { ?>    
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Nicknames:', true); ?></td>
                                        <td data-content="<?php echo __('Nicknames:', true); ?>">  
                                            <strong>
                                                <?php
                                                $numNickname = 0;
                                                foreach ($identity['IdentityNickname'] as $nicknames) {
                                                    if ($numNickname > 0) {
                                                        echo ', ';
                                                    }
                                                    echo ($nicknames ['nickname']);
                                                    $numNickname ++;
                                                }
                                                ?>

                                            </strong>
                                        </td>
                                    </tr>
                                <?php } ?>    


                                <?php if ((isset($identity['IdentityBirth']['countries_id'])) && ($identity['IdentityBirth']['Country'] != null)) { ?>
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Birth location:', true); ?></td>
                                        <td data-content="<?php echo __('Birth location:', true); ?>">  
                                            <strong>
                                                <?php
                                                if ($identity['IdentityBirth']['City'] != null) {
                                                    echo $identity['IdentityBirth']['City'] . ', ';
                                                }
                                                if ($identity['IdentityBirth']['State'] != null) {
                                                    echo $identity['IdentityBirth']['State'] . ', ';
                                                }
                                                if ($identity['IdentityBirth']['Country'] != null) {
                                                    echo $identity['IdentityBirth']['Country'];
                                                }
                                                ?>
                                            </strong>
                                        </td>

                                    </tr>
                                <?php } ?>
                                <?php if ((isset($identity['IdentityBirth']['date'])) && ($identity['IdentityBirth']['date'] != null)) { ?>
                                    <tr>
                                        <?php
                                        list($anno, $mes, $dia) = explode('-', $identity['IdentityBirth']['date']);
                                        ?>
                                        <?php if ($anno != 0) { ?>
                                            <td class="hidden-1025"><?php echo __('Date of birth:', true); ?></td>
                                            <td data-content="<?php echo __('Date of birth:', true); ?>">    
                                                <strong>
                                                    <?php
                                                    
                                                    if ($mes == 00 && $anno >= 0) {
                                                        echo ("Year " . $anno );
                                                    } elseif ($dia == 0 && $anno >= 0) {
                                                        echo date('F Y', strtotime($anno . '-' . $mes . '-01'));
                                                    } elseif ($dia >= 0 && $mes >= 0) {
                                                        echo date('l jS \of F Y', strtotime($identity['IdentityBirth']['date']));
                                                    }
                                                    ?>
                                                </strong>
                                                    <?php
                                                        if (!isset($identity['IdentityDeath']['date']))
                                                        {
                                                            echo ' (age ' . $this->getage->age($identity['IdentityBirth']['date'], date('Y-m-d')) . ')';
                                                        }
                                                        else
                                                        {
                                                            echo ' (' . $this->getage->age($identity['IdentityBirth']['date'], date('Y-m-d')) . ' years ago)';
                                                        }
                                                            
                                                    
                                                }
                                                ?>
                                        </td>

                                    </tr>
                                        <?php } ?>



                                <?php if ((isset($identity['IdentityDeath']['date'])) && ($identity['IdentityDeath']['date'] != null)) { ?>
                                    <?php
                                    //****************************///

                                    $elementosFechaMuerte = explode("-", $identity['IdentityDeath']['date']);

                                    $mesVacio = 0;
                                    $diaVacio = 0;
                                    $annoVacio = 0;
                                    $noMuestra = 0;
                                    if (strcmp($elementosFechaMuerte[0], "0000") == 0) {
                                        $annoVacio = 1;
                                    }
                                    ?>

                                    <tr>
                                    <?php
                                    if ($annoVacio == 0) {
                                        ?>
                                            <td class="hidden-1025"><?php echo __('Date of death:', true); ?></td>
                                            <td data-content="<?php echo __('Date of death:', true); ?>">
                                                <strong>
                                            <?php
                                            if (strcmp($elementosFechaMuerte[1], "00") == 0) {
                                                $mesVacio = 1;
                                            }
                                            if (strcmp($elementosFechaMuerte[2], "00") == 0) {
                                                $diaVacio = 1;
                                            }
                                            if (strcmp($elementosFechaMuerte[0], "0000") == 0) {
                                                $annoVacio = 1;
                                            }
                                            if ($annoVacio == 0) {


                                                if ($mesVacio == 1) {
                                                    echo ($elementosFechaMuerte[0] . " month and day unknown");
                                                }

                                                if ($diaVacio == 1 && $mesVacio == 0) {
                                                    echo ($elementosFechaMuerte[0] . "-" . $elementosFechaMuerte[1] . " day unknown");
                                                }

                                                if ($diaVacio == 0 && $mesVacio == 0) {
                                                    echo date('l jS \of F Y', strtotime($identity['IdentityDeath']['date']));
                                                }
                                            }
                                            ?>
                                                </strong>
                                                    <?php
                                                    if ((isset($identity['IdentityBirth']['date'])) && ($identity['IdentityBirth']['date'] != null)) {
                                                        echo ' (age ' . $this->getage->age($identity['IdentityBirth']['date'], $identity['IdentityDeath']['date']) . ')';
                                                    }
                                                    ?>
                                            </td>

                                        </tr>
                                            <?php }
                                        }
                                        ?>
<?php if ((isset($identity['IdentityResidence']['countries_id'])) && ($identity['IdentityResidence']['countries_id'] != null)) { ?>
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Residence:', true); ?></td>
                                        <td data-content="<?php echo __('Residence:', true); ?>">
                                            <strong>
    <?php
    if ((isset($identity['IdentityResidence']['Cities']['name'])) && ($identity['IdentityResidence']['Cities']['name'] != null)) {
        echo $identity['IdentityResidence']['Cities']['name'] . ', ';
    }
    if ((isset($identity['IdentityResidence']['States']['name'])) && ($identity['IdentityResidence']['States']['name'] != null)) {
        echo $identity['IdentityResidence']['States']['name'] . ', ';
    }
    if ((isset($identity['IdentityResidence']['Countries']['name'])) && ($identity['IdentityResidence']['Countries']['name'] != null)) {
        echo $identity['IdentityResidence']['Countries']['name'];
    }
    ?>
                                            </strong>
                                        </td>

                                    </tr>
<?php } ?>
<?php if ($identity['Identity']['height'] != null) { ?>
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Height:', true); ?></td>
                                        <td data-content="<?php echo __('Height:', true); ?>">
                                            <strong>
    <?php
    echo $identity['Identity']['height'] . ' / ' . $this->getage->getCm($identity['Identity']['height']) . ' cm';
    ?>
                                            </strong>
                                        </td>

                                    </tr>
<?php } ?>
<?php if ($identity['Identity']['reach'] != null) { ?>
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Reach:', true); ?></td>
                                        <td data-content="<?php echo __('Reach:', true); ?>">
                                            <strong>
    <?php
    echo $identity['Identity']['reach'] . ' / ' . $this->getage->getReachCm($identity['Identity']['reach']) . ' cm';
    ?>
                                            </strong>
                                        </td>

                                    </tr>
<?php } ?>

<?php if ($identity['Identity']['gender'] != null && $identity['Identity']['gender'] != 'D') { ?>
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Gender:', true); ?></td>
                                        <td data-content="<?php echo __('Gender:', true); ?>">
                                            <strong>
    <?php
    if ($identity['Identity']['gender'] == 'F') {
        echo __('Female');
    }
    if ($identity['Identity']['gender'] == 'M') {
        echo __('Male');
    }
    ?>
                                            </strong>
                                        </td>

                                    </tr>
<?php } ?>


                                <?php if ($identity['Identity']['stance'] != null) { ?>
                                    <tr>
                                        <td class="hidden-1025">Stance:</td>
                                        <td data-content="<?php echo __('Stance:', true); ?>"><strong><?php echo $identity['Identity']['stance']; ?></strong></td>
                                    </tr>
<?php } ?>


                                <?php if ( (isset($sponsor)) && (count($sponsor) > 0)) { ?>
                                    <tr>
                                        <td class="hidden-1025"><?php echo __('Sponsor:', true); ?></td>
                                        <td data-content="<?php echo __('Sponsor:', true); ?>">
                                            <strong>
                                                    <?php
                                                    $numSponsor = 0;
                                                    foreach ($sponsor as $spon) {
                                                        if ($numSponsor > 0) {
                                                            echo ', ';
                                                        }
                                                        echo ($spon['Sponsor']['name']);
                                                        $numSponsor ++;
                                                    }
                                                    ?>

                                            </strong>
                                        </td>
                                    </tr>
<?php } ?>    
                            </tbody>
                        </table>
                    </div>
                </div>

<?php if ($countFights > 0) { ?>
                    <div class="tab-pane fade" id="professional-record">
                        <div class="col-lg-12" id="loading-record"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
<?php } ?>
               

<?php if ($countJobs) { ?>
                    <div class="tab-pane fade" id="professional-jobs">
                        <div class="col-lg-12" id="loading-jobs"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
<?php } ?>


                <?php if ($countImages > 0) { ?>
                    <div class="tab-pane fade" id="photos-panel">
                        <div class="col-lg-12" id="load-img"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
<?php } ?>

<?php if ($countVideos > 0) { ?>
                    <div class="tab-pane fade" id="videos-panel">
                        <div class="col-lg-12" id="load-video"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
<?php } ?>              
                
<?php if ((isset($identity['IdentityBiography']['biography'])) && ($identity['IdentityBiography']['biography'] != null)) { ?>
                    <div class="tab-pane fade" id="biography-panel">
                        <div class="col-lg-12"><br>
                    <?php echo $identity['IdentityBiography']['biography'] ?>
                        </div>
                    </div>
                        <?php } ?>

<?php if ($countNotes > 0) { ?>
                    <div class="tab-pane fade" id="notes-panel">
                        <div class="col-lg-12" id="loading-notes"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
<?php } ?>
                
<?php if ($countArticles > 0) { ?>
        <div class="tab-pane fade" id="article-panel">
            <div class="col-lg-12" id="loading-article"><br>
                <center>
                    <i class="fa fa-refresh fa-spin fa-3x"></i>
                </center>
            </div>
        </div>
<?php } ?>
                
                
<?php if ($countFamily > 0) { ?>
                    <div class="tab-pane fade" id="family-panel">
                        <div class="col-lg-12" id="loading-family"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
<?php } ?>
<?php if ($countContacts > 0) { ?>
                    <div class="tab-pane fade" id="contact-panel">
                        <div class="col-lg-12" id="loading-contact"><br>
                            <center>
                                <i class="fa fa-refresh fa-spin fa-3x"></i>
                            </center>
                        </div>
                    </div>
<?php } ?>
            </div>
        </div>
    </div>



    <div class="clear"></div>
<?php echo $this->element('banners', array('thisBanner' => $detailPersons)); ?>    
</div>
<div class='footer_separator'></div>
<script type="text/javascript">

    (function ($) {
        fakewaffle.responsiveTabs(['xs', 'sm']);
    })(jQuery);

    $(document).ready(function () {

        var loadImages = true;
        $('.load-images').click(function () {
            if (loadImages) {
                var loadPageImages = "<?php echo $this->Html->url(array('action' => 'getImagesPerson/' . $id)); ?>";
                $.post(loadPageImages, function (data) {
                    $('#load-img').fadeOut(function () {
                        $('#photos-panel').append(data);
                        loadImages = false;
                    });
                });
            }
        });

        var loadVideos = true;
        $('.load-videos').click(function () {
            if (loadVideos) {
                var loadPageVideos = "<?php echo $this->Html->url(array('action' => 'getVideosPerson/' . $id)); ?>";
                $.post(loadPageVideos, function (data) {
                    $('#load-video').fadeOut(function () {
                        $('#videos-panel').append(data);
                        loadVideos = false;
                    });
                });
            }
        });

        var loadRecord = true;
        $('.load-professional-record').click(function () {
            if (loadRecord) {
                var LoadRecordPage = "<?php echo $this->Html->url(array('action' => 'showRecord/' . $id)); ?>";
                console.log(LoadRecordPage);
                $.post(LoadRecordPage, function (data) {
                    $('#loading-record').fadeOut(function () {
                        $('#professional-record').append(data);
                        loadRecord = false;
                    });
                });
            }
        });

        var loadJobs = true;
        $('.load-jobs').click(function () {
            if (loadJobs) {
                var loadJobsPage = "<?php echo $this->Html->url(array('action' => 'showJobs/' . $id)); ?>";
                $.post(loadJobsPage, function (data) {
                    $('#loading-jobs').html(data);
                    loadJobs = false;
                });
            }
        });

        var loadNotes = true;
        $('.load-notes').click(function () {
            if (loadNotes) {
                var loadNotesPage = "<?php echo $this->Html->url(array('action' => 'showNotes/' . $id)); ?>";
                $.post(loadNotesPage, function (data) {
                    $('#loading-notes').html(data);
                    loadNotes = false;
                });
            }
        });

        var loadfamily = true;
        $('.load-family').click(function () {
            if (loadfamily) {
                var loadFamilyPage = "<?php echo $this->Html->url(array('action' => 'showRelationships/' . $id)); ?>";
                $.post(loadFamilyPage, function (data) {
                    $('#loading-family').html(data);
                    loadfamily = false;
                });
            }
        });
        
        var loadArticles = true;
        $('.load-articles').click(function () {
            if (loadArticles) {
                var loadPageArticles = "<?php echo $this->Html->url(array('action' => 'showArticles/' . $id)); ?>";
                $.post(loadPageArticles, function (data) {
                    $('#loading-article').html(data);
                    loadArticles = false;
                });
            }
        });
        
        var loadContact = true;
        $('.load-contact').click(function () {
            if (loadContact) {
                var loadContactPage = "<?php echo $this->Html->url(array('action' => 'showContacts/' . $id)); ?>";
                $.post(loadContactPage, function (data) {
                    $('#loading-contact').html(data);
                    loadContact = false;
                });
            }
        });
        
    });
</script>