<div class="table-responsive">
    <?php if (($fighterJobs) || ($referee) || ($ringAnnouncer) || ($judge) || ($inspector) || ($timekeeper)) { ?>
        <table class="table table-striped table-condensed" id="boxing-record-table">
            <thead>
                <tr>
                    <th><?php echo __('Fight', true); ?></th>
                    <th><?php echo __('Job', true); ?></th>

                    <?php if (($fighterJobs)) { ?>
                        <th><?php echo __('Corner', true); ?></th>
                    <?php } ?>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($fighterJobs as $job) {
                    ?>
                    <tr>

                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($job['Fights']['id'], $job['Fights']['title'])); ?>">
                                <?php echo $job['Fights']['title'] ?>

                            </a>

                        </td>
                        <td><?php echo $job['Jobs']['name'] ?></td>
                        <td><?php
                            $names = explode("vs", $job['Fights']['title']);
                            if ($job['FighterJob']['corner'] == 1) {
                                echo $names[0];
                            } else {
                                echo $names[1];
                            }
                            ?></td>
                    </tr>
                        <?php } ?>
                        <?php foreach ($referee as $job) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($job['Fights']['id'], $job['Fights']['title'])); ?>">
        <?php echo $job['Fights']['title'] ?>
                            </a>
                        </td>
                        <td><?php echo __('Referee', true); ?></td>

                    </tr>
    <?php } ?>
    <?php foreach ($ringAnnouncer as $job) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($job['Fights']['id'], $job['Fights']['title'])); ?>">
        <?php echo $job['Fights']['title'] ?>
                            </a>
                        </td>
                        <td><?php echo __('Ring Announcer', true); ?></td>
                    </tr>
    <?php } ?>
    <?php foreach ($judge as $job) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($job['Fights']['id'], $job['Fights']['title'])); ?>">
        <?php echo $job['Fights']['title'] ?>
                            </a>
                        </td>
                        <td><?php echo __('Judge', true); ?></td>
                    </tr>
    <?php } ?>
    <?php foreach ($inspector as $job) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($job['Fights']['id'], $job['Fights']['title'])); ?>">
        <?php echo $job['Fights']['title'] ?>
                            </a>
                        </td>
                        <td><?php echo __('Inspector', true); ?></td>
                    </tr>
    <?php } ?>
    <?php foreach ($timekeeper as $job) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($job['Fights']['id'], $job['Fights']['title'])); ?>">
        <?php echo $job['Fights']['title'] ?>
                            </a>
                        </td>
                        <td><?php echo __('Timekeeper', true); ?></td>
                    </tr>
    <?php } ?>
            </tbody>
        </table>
            <?php } ?>
<?php if ($eventJobs) { ?>
        <table class="table table-striped table-condensed" id="boxing-record-table">
            <thead>
                <tr>
                    <th><?php echo __('Event', true); ?></th>
                    <th><?php echo __('Job', true); ?></th>
                </tr>
            </thead>
            <tbody>
    <?php foreach ($eventJobs as $job) { ?>
                    <tr>
                        <td>
                    <?php
                    if ((strcmp($job['EventsJob']['type'], 'Broadcasters') != 0) || (strcmp($job['EventsJob']['type'], 'Doctor') != 0) || (strcmp($job['EventsJob']['type'], 'Matchmaker') != 0) || (strcmp($job['EventsJob']['type'], 'Promoter') != 0) || (strcmp($job['EventsJob']['type'], 'Inspector') != 0)) {
                        ?>
                                <a href="<?php echo $this->Html->url('/Event/' . $this->getUrlPerson->getUrl($job['Events']['id'], $job['Events']['name'])); ?>">
                                <?php echo $job['Events']['name'] ?>
                                </a>
                                    <?php
                                }
                                ?>

                        </td>
                        <td><?php echo $job['EventsJob']['type'] ?></td>
                    </tr>
    <?php } ?>
            </tbody>
        </table>
            <?php } ?>
</div>