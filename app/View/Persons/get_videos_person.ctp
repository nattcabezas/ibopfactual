<?php 
    echo $this->Html->css('video-js');
    echo $this->Html->css('video-playlist');
    echo $this->Html->script('video');
    echo $this->Html->script('videojs-playlists');
?>
<p>Filter videos by: </p>
<select id="selectFilter">
    <option value="0" id="selectValue">Select value</option>
    <option value="20" id="fullFight">Full Fight</option>
    <option value="21" id="partialFightsAndHighlights">Partial Fights and Highlights</option>
    <option value="22" id="sparring">Sparring</option>
    <option value="23" id="weighIns">Weigh-Ins</option>
    <option value="24" id="interview">Interview</option>
    <option value="25" id="training">Training</option>
    <option value="26" id="televisionAppearance">Television Appearance</option>
    <option value="27" id="pressConference">Press Conference</option>
    <option value="28" id="movies">Movies</option>
    <option value="29" id="miscellaneous">Miscellaneous</option>
</select>

<section class="row">
    <div class="col-sm-12">
        
        <div class="video-holder centered" style="width: 100%">
            <video id="video" class="video-js vjs-default-skin vjs-big-play-centered"
                   controls preload="auto"
                   data-setup=''
                   poster="">
            </video>
            <div class="playlist-components" style="width: 49%;">
                <div class="playlist" style="width: 100%;">
                    <ul style="width: 100%"></ul> 
               </div>
                <div class="button-holder">
                    <img id="prev" alt="Previous video" src="<?php echo $this->Html->url('/img/Previous.png')?>">
                    <img id="next" alt="Next video" src="<?php echo $this->Html->url('/img/Next.png')?>">
                </div>
            </div>
        </div>
        <hr>
    </div>
</section>

<p id="prueba"></p>


<?php /*echo $this->Form->create('Person',array('action' => 'getVideosPerson'));*/ ?>
    <?php /*echo $this->Form->input('note_text', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $note['Notes']['note']));*/ ?>
    <?php /*echo $this->Form->button(__('Read more'), array('class' => 'btn btn-primary', 'type' => 'submit'));*/ ?>
<?php /*echo $this->Form->end();*/ ?>

<?php debug ($videos);?>

<?php 
/*
Put if for each video type
*/

    /*$newArray = array();
    foreach ($videos as $video)
    {
        if ($video['Videos']['image_types_id']==0)
        {
            array_push($newArray,$video);
        }
    }
    debug($newArray);*/

    $prueba = "";
?>



<script type="text/javascript">
   
    $( "#selectFilter" ).change(function() {
        /*alert($( "#selectFilter option:selected" ).val());
        var selectValue = $( "#selectFilter option:selected" ).val();*/
        
        var elVideo = "PUM PUM";
        
        var createNewArray = "<?php echo $this->Html->url(array('action' => 'filterVideos/' . $id ));?>" + "/" + $( "#selectFilter option:selected" ).val();
        //console.log(createNewArray);
        //var
        
        var filter = $( "#selectFilter option:selected" ).val();
        
        
    $.post(createNewArray, function(data){
        console.log(createNewArray);
        //$('#prueba').append(data);
        var videoArray = JSON.parse(data);
        console.log(videoArray);
        
        console.log("<?php $_POST['TEST'] = 20;?>");
        console.log("<?php echo $_POST['TEST'];?>"); /////SEGUIR AQUI!!!!
        $('#prueba').append("----------");
        
        
    /*****************/    
        });
        
    }); 
    /*****************/
    var number = 0;
    (function($){
        
        var videos = [
            <?php if( $videos > 0) { ?>
                <?php foreach ($videos as $video) { ?>
                      <?php 
                          /*if ($video[])
                          {
                              ///CONTINUE WORKNNG ON THIS IF, IF THE CONDITION IS EQUAL TO THE COMBOBOX, DO NOT EVALUATE THE VIDEO
                          
                          }*/
                      ?>  
                        {
                            src: [
                                "<?php echo $video['Videos']['mp4']?>"
                                
                            ],
                            poster: "<?php echo $this->Html->url('/img/defaults/videoGeneric-03.png');?>",
                            title: "<?php 
                                        if (strcmp("",$video['Videos']['language']) != 0 )
                                        {
                                            echo $video['Videos']['title'] . " (" . $video['Videos']['language'] . ")";    
                                        }
                                        else
                                        {
                                            echo $video['Videos']['title'];
                                        }
                                        
                                    ?>"
                        },
                        
                <?php }?>
            <?php }?>
            
            <?php if( (isset($videosVIP)) && ($videosVIP > 0) ) { ?>
                <?php foreach ($videosVIP as $video) { ?>
                        {
                            src: [
                                "<?php echo $video['Videos']['mp4']?>"
                            ],
                            poster: "<?php echo $this->Html->url('/img/defaults/videoGeneric-03.png');?>",
                            title: "<?php echo $video['Videos']['title']?> (V.I.P video)"
                        },
                <?php }?>
            <?php }?>
            <?php if( (isset($videosPre)) && ($videosPre > 0) ) { ?>
                <?php foreach ($videosPre as $video) { ?>
                        {
                            src: [
                                "<?php echo $video['Videos']['mp4']?>"
                            ],
                            poster: "<?php echo $this->Html->url('/img/defaults/videoGeneric-03.png');?>",
                            title: "<?php echo $video['Videos']['title']?> (Premier video)"
                        },
                <?php }?>
            <?php }?>
        ];


    var demoModule = {
    init : function(){
      this.els = {};
      this.cacheElements();
      this.initVideo();
      this.createListOfVideos();
      this.bindEvents();
      this.overwriteConsole();
    },
    overwriteConsole : function(){
      console._log = console.log;
      console.log = this.log;
    },
    log : function(string){
      demoModule.els.log.append('<p>' + string + '</p>');
      console._log(string);
    },
    cacheElements : function(){
      this.els.$playlist = $('div.playlist > ul');
      this.els.$next = $('#next');
      this.els.$prev = $('#prev');
      this.els.log = $('div.panels > pre');
    },
    initVideo : function(){
      this.player = videojs('video');
      this.player.playList(videos);
    },
    createListOfVideos : function(){
      var html = '';
      for (var i = 0, len = this.player.pl.videos.length; i < len; i++){
        html += '<li data-videoplaylist="'+ i +'">'+
                  '<span class="number">' + (i + 1) + '</span>'+
                  '<span class="poster"><img src="'+ videos[i].poster +'"></span>' +
                  '<span class="title">'+ videos[i].title +'</span>' +
                '</li>';
      }
      this.els.$playlist.empty().html(html);
      this.updateActiveVideo();
    },
    updateActiveVideo : function(){
      var activeIndex = this.player.pl.current;

      this.els.$playlist.find('li').removeClass('active');
      this.els.$playlist.find('li[data-videoplaylist="' + activeIndex +'"]').addClass('active');
    },
    bindEvents : function(){
      var self = this;
      this.els.$playlist.find('li').on('click', $.proxy(this.selectVideo,this));
      this.els.$next.on('click', $.proxy(this.nextOrPrev,this));
      this.els.$prev.on('click', $.proxy(this.nextOrPrev,this));
      this.player.on('next', function(e){
        console.log('Next video');
        self.updateActiveVideo.apply(self);
      });
      this.player.on('prev', function(e){
        console.log('Previous video');
        self.updateActiveVideo.apply(self);
      });
      this.player.on('lastVideoEnded', function(e){
        console.log('Last video has finished');
      });
    },
    nextOrPrev : function(e){
      var clicked = $(e.target);
      this.player[clicked.attr('id')]();
    },
    selectVideo : function(e){
      var clicked = e.target.nodeName === 'LI' ? $(e.target) : $(e.target).closest('li');

      if (!clicked.hasClass('active')){
        console.log('Selecting video');
        var videoIndex = clicked.data('videoplaylist');
        this.player.playList(videoIndex);
        this.updateActiveVideo();
      }
    }
  };

  demoModule.init();
  
  $('#video').attr('style', 'width: 49%; height: 357px;');
  
})(jQuery);

/*************/
/*Si se comentan descomentar los de cierre de post y combobox*/

//}); //cierre del post

//}); //cierre del combobox
/*************/

</script>