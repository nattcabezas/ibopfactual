<table class="table table-hover table-condensed">
    <tbody>
        <?php foreach ($relationships as $family){ ?>
            <tr>
                <td>
                    <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($family['Family']['id'], $family['Family']['name'] . ' ' . $family['Family']['last_name']))?>">
                        <?php echo $family['Family']['name'] . ' ' . $family['Family']['last_name']?>
                    </a>
                </td>
                <td>
                    <strong>
                        <?php echo $family['IdentityRelationship']['relationship']?>
                    </strong>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>