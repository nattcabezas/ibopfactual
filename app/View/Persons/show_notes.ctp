<p>Showing newest notes first (notes with no defined date will be showed at the end)</p>
<!-------------------uncomment lines above to continue working on the notes pagination------------------------->
    <?php foreach($data as $note){ ?>
        <?php /*debug($note);*/ ?>
        <h3><?php echo $note['Notes']['title']; ?></h3>
            <?php
                if (strcmp($note['Notes']['creation_date'], "0000-00-00 00:00:00") !== 0 )
                {
            ?>        
                <p>Created on <?php echo $note['Notes']['creation_date'];?></p>    
            <?php    
                }
            ?>
            
            <?php
                //$theNoteText = substr($note['Notes']['note'],0,750);
                if (strlen($note['Notes']['note'])>600)
                {
                    $theNoteText = strtok($note['Notes']['note'], '/') . "....>";
                }
                else
                {
                    $theNoteText = $note['Notes']['note'];
                }
                
            ?>
                
            <div class="notes-content"><?php echo $theNoteText;?></div>
            
            <?php 
                if (strlen($note['Notes']['note'])>600)
                {
            ?>
            <?php echo $this->Form->create('Note',array('action' => 'showIndividualNote')); ?>
                <?php echo $this->Form->input('note_text', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $note['Notes']['note'])); ?>
                <?php echo $this->Form->input('identity_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $note['Identities']['id'])); ?>
                <?php echo $this->Form->input('identity_name', array('type' => 'hidden', 'label' => false, 'div' => false, 'class' => 'form-control', 'value' => $note['Identities']['name'] . " " . $note['Identities']['last_name'] )); ?>
                <?php echo $this->Form->button(__('Read more'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                
            <?php echo $this->Form->end(); ?>

            <?php        
                }
            ?>
            <hr>
    <?php } ?>
    
    <?php 
        
        $totalPages = substr($this->Paginator->counter(), strpos($this->Paginator->counter(), "of ") + 3);
        if ($totalPages>1)
        {
    ?>        
            <button type="button" class="btn btn-primary" id="load-notes-prev">Previous</button>
            <button type="button" class="btn btn-primary" id="load-notes-next">Next</button>
            <div class="col-lg-12" id="loading-notes"></div>
            <?php echo $this->Paginator->counter();?>
     <?php
        }
     ?>
    
    <?php
        $htmlNext = $this->Paginator->next(null, null, null, array('class' => 'disabled'));
        $domNext = new DOMDocument();
        $domNext->loadHTML($htmlNext);
        $tagsNext = $domNext->getElementsByTagName('a');
        $theLinkNext = "";
        foreach ($tagsNext as $tag) 
        {
            $theLinkNext = $tag->getAttribute('href');
        }
        
        $htmlPrev = $this->Paginator->prev(null, array(), null, array('class' => 'prev disabled'));
        $domPrev = new DOMDocument();
        $domPrev->loadHTML($htmlPrev);
        $tagsPrev = $domPrev->getElementsByTagName('a');
        $theLinkPrev = "";
        foreach ($tagsPrev as $tag)
        {
            $theLinkPrev = $tag->getAttribute('href');
        }
    ?>
<script type="text/javascript">
    
    $(document).ready(function () {
        
        var numberPagesString = "<?php echo $this->Paginator->counter(); ?>";
        var lastPage = numberPagesString.substr(numberPagesString.indexOf("of ") + 3);
        var currentPage = numberPagesString.substr(0, numberPagesString.indexOf(' of')); 

        var loadNotes = true;
        $('#load-notes-next').click(function () {
            if (currentPage<lastPage)
            {
                if (loadNotes) {
                    var loadNotesPage = "<?php echo $theLinkNext ?>";
                    $.post(loadNotesPage, function (data) {
                        $('#loading-notes').html(data);
                        loadNotes = false;
                    });
                }    
            }
        });
        
        $('#load-notes-prev').click(function () {
            if (currentPage>1)
            {
                if (loadNotes) {
                    var loadNotesPage = "<?php echo $theLinkPrev ?>";
                    $.post(loadNotesPage, function (data) {
                        $('#loading-notes').html(data);
                        loadNotes = false;
                    });
                }
            }        
        });
    });
</script>