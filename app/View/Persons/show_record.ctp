<?php echo $this->Html->script('tablesorter/jquery.tablesorter'); ?>

<?php
    /*debug($boxingRecord);
    debug($mmaRecord);
    debug("Count boxing " . $countBoxing . "<br>");
    debug("Count MMA " . $countMMA . "<br>");*/
    
?>

<?php if($countBoxing > 0) { ?>
    <div class="panel panel-primary ">
        <div class="record-heading">
            <h3 class="record-title"><?php echo __('Boxing Record', true);?></h3>   
         </div>
        <div >
            
 
            <table class="responsive responsive-table table_basic" id="boxing-record-table"  >
                <thead>
                    <tr>
                        <th style="width: 85px;"><?php echo __('Date', true);?></th>
                        <th><?php echo __('Opponent', true);?></th>
                        <th><?php echo __('Venue', true);?></th>
                        <th><?php echo __('Location', true);?></th>
                        <th><?php echo __('Result', true);?></th>
                        <th><?php echo __('Via', true);?></th>
                        <th><?php echo __('Rounds', true);?></th>
                        <th><?php echo __('Time', true);?></th>
                        <th><?php echo __('Details', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($boxingRecord as $record) { ?>
                    <tr>
                        <td class="fight_date">
                            <?php 
                                $elementosFechaPelea = explode("-",$record['Fight']['date']);
                                echo ($elementosFechaPelea[0] . '-' . $elementosFechaPelea[1] . '-' . $elementosFechaPelea[2]);
                            ?>
                        </td>
                        <td>
                            <?php if(($record['Fight']['opponent']['name'] != 'unknown') || ($record['Fight']['opponent']['last_name'] != "")) { ?>
                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($record['Fight']['opponent']['id'], $record['Fight']['opponent']['name'] . ' ' . $record['Fight']['opponent']['last_name']))?>">
                                <?php echo $record['Fight']['opponent']['name'] . ' ' . $record['Fight']['opponent']['last_name']?>
                            </a>
                            <?php } else { ?>
                                <?php echo __('unknown', true);?>
                            <?php } ?>
                        </td>
                        <td>
                            <?php
                                if(isset($record['Fight']['venue']['name'])){
                                    echo $record['Fight']['venue']['name'];
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if( (isset($record['Fight']['venue']['flag'])) && ($record['Fight']['venue']['flag'] != null) ){
                                    echo '<img src="'.$this->Html->url('/img/flags/' . $record['Fight']['venue']['flag']).'"> ';
                                } else if( (isset($record['Fight']['locations']['Countries']['flag'])) && ($record['Fight']['locations']['Countries']['flag'] != null) ){
                                    echo '<img src="'.$this->Html->url('/img/flags/' . $record['Fight']['locations']['Countries']['flag']).'"> ';
                                }
                                if( (isset($record['Fight']['venue']['Citiy'])) && ($record['Fight']['venue']['Citiy'] != null) ){
                                    echo $record['Fight']['venue']['Citiy'] . ', ';
                                } else if( (isset($record['Fight']['locations']['Cities']['name'])) && ($record['Fight']['locations']['Cities']['name'] != null) ){
                                    echo $record['Fight']['locations']['Cities']['name'] . ', ';
                                }
                                if( (isset($record['Fight']['venue']['State'])) && ($record['Fight']['venue']['State'] != null) ){
                                    echo $record['Fight']['venue']['State'] . ', ';
                                } else if( (isset($record['Fight']['locations']['States']['name'])) && ($record['Fight']['locations']['States']['name'] != null) ){
                                    echo $record['Fight']['locations']['States']['name'] . ', ';
                                }
                                if( (isset($record['Fight']['venue']['Country'])) && ($record['Fight']['venue']['Country'] != null) ){
                                    echo $record['Fight']['venue']['Country'];
                                } else if( (isset($record['Fight']['locations']['Countries']['name'])) && ($record['Fight']['locations']['Countries']['name'] != null) ){
                                    echo $record['Fight']['locations']['Countries']['name'];
                                }
                            ?>
                        </td>
                        <?php 
                            if($record['Fight']['winner'] != 4){
                                if($record['Fight']['winner'] == 3){
                                    echo '<td class="result-fight result-draw">D</td>';
                                } else if($record['Fight']['winner'] == $record['Fight']['corner']){
                                    echo '<td class="result-fight result-win">W</td>';
                                } else {
                                    echo '<td class="result-fight result-lost">L</td>';
                                }
                            } else {
                                echo '<td></td>';
                            }
                        ?>
                        <td><?php echo $record['Fight']['via']; ?></td>
                        <td>
                            <?php 
                                echo $record['Fight']['boxed_roundas'] . '/' .$record['Fight']['schedule_rounds'];
                                if($record['Fight']['time_per_round'] != null){
                                    if ($identity['Identity']['gender'] === 'F')
                                    {
                                        echo 'x' . $record['Fight']['time_per_round'];    
                                    }
                                    
                                } else {
                                    if ($identity['Identity']['gender'] === 'F')
                                    {
                                        echo 'x3';
                                    }
                                }
                            ?>
                        </td>
                        <td><?php echo $record['Fight']['time']; ?></td>
                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($record['Fight']['id'], $record['Fight']['title'])); ?>">
                                <i class="fa fa-boxers fa-2x"></i>
                            </a>
                        </td>
                        <td>
                            <?php if(count($record['Fight']['Title']) > 0){ ?>
                                <a href="#show-titles" class="show-title-detail" id-fight="<?php echo $record['Fight']['id']?>" data-toggle="modal" data-target="#myModalTitle">
                                    <i class="fa fa-champ-belt fa-2x"></i>
                                </a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>

<?php if($countMMA > 0) { ?>
    <div class="panel panel-primary ">
        <div class="record-heading">
            <h3 class="record-title"><?php echo __('MMA Record', true);?></h3>   
         </div>
        <div class="">
            <table class="responsive responsive-table table_basic" id="mma-record-table">
                <thead>
                    <tr class="filters">
                        <th style="width: 85px;"><?php echo __('Date', true);?></th>
                        <th><?php echo __('Opponent', true);?></th>
                        <th><?php echo __('Venue', true);?></th>
                        <th><?php echo __('Location', true);?></th>
                        <th><?php echo __('Result', true);?></th>
                        <th><?php echo __('Via', true);?></th>
                        <th><?php echo __('Rounds', true);?></th>
                        <th><?php echo __('Time', true);?></th>
                        <th><?php echo __('Details', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($mmaRecord as $record) { ?>
                    
                    <tr>
                        <td><?php echo date('Y-m-d', strtotime($record['Fight']['date']));?></td>
                        <td>
                            <?php if($record['Fight']['opponent']['name'] != 'unknown' && $record['Fight']['opponent']['last_name'] != "") { ?>
                            <a href="<?php echo $this->Html->url('/Person/' . $this->getUrlPerson->getUrl($record['Fight']['opponent']['id'], $record['Fight']['opponent']['name'] . ' ' . $record['Fight']['opponent']['last_name']))?>">
                                <?php echo $record['Fight']['opponent']['name'] . ' ' . $record['Fight']['opponent']['last_name']?>
                            </a>
                            <?php } else { ?>
                                <?php echo __('unknown', true);?>
                            <?php } ?>
                        </td>
                        <td>
                            <?php
                                if(isset($record['Fight']['venue']['name'])){
                                    echo $record['Fight']['venue']['name'];
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (isset($record['Fight']['venue']))
                            {
                                if( (isset($record['Fight']['venue']['flag'])) && ($record['Fight']['venue']['flag'] != null) ){
                                    echo '<img src="'.$this->Html->url('/img/flags/' . $record['Fight']['venue']['flag']).'"> ';
                                }
                                if( (isset($record['Fight']['venue']['Citiy'])) && ($record['Fight']['venue']['Citiy'] != null) ){
                                    echo $record['Fight']['venue']['Citiy'] . ', ';
                                }
                                if( (isset($record['Fight']['venue']['State'])) && ($record['Fight']['venue']['State'] != null) ){
                                    echo $record['Fight']['venue']['State'] . ', ';
                                }
                                if( (isset($record['Fight']['venue']['Country'])) && ($record['Fight']['venue']['Country'] != null) ){
                                    echo $record['Fight']['venue']['Country'];
                                }
                                
                            }
                            else //if no venue is found, then query the location directly
                            {
                                if( (isset($record['Fight']['locations']['Countries'])) ){
                                    echo '<img src="'.$this->Html->url('/img/flags/' . $record['Fight']['locations']['Countries']['flag']).'"> ';
                                }
                                if( (isset($record['Fight']['locations']['Cities'])) ){
                                    echo $record['Fight']['locations']['Cities']['name'] . ', ';
                                }
                                if( (isset($record['Fight']['locations']['States'])) ){
                                    echo $record['Fight']['locations']['States']['name'] . ', ';
                                }
                                if( (isset($record['Fight']['locations']['Countries'])) ){
                                    echo $record['Fight']['locations']['Countries']['name'] . ', ';
                                }
                                
                            }
                                
                            ?>
                        </td>
                        <?php 
                            if($record['Fight']['winner'] != 4){
                                if($record['Fight']['winner'] == 3){
                                    echo '<td class="result-fight result-draw">D</td>';
                                } else if($record['Fight']['winner'] == $record['Fight']['corner']){
                                    echo '<td class="result-fight result-win">W</td>';
                                } else {
                                    echo '<td class="result-fight result-lost">L</td>';
                                }
                            } else {
                                echo '<td></td>';
                            }
                        ?>
                        <td>
                            <?php 
                                if( (isset($record['Fight']['submission_comments'])) && ($record['Fight']['submission_comments'] != "") ){
                                    echo $record['Fight']['submission_comments'];
                                } else {
                                    echo $record['Fight']['via']; 
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                echo $record['Fight']['boxed_roundas'] . '/' .$record['Fight']['schedule_rounds'];
                                if($record['Fight']['time_per_round'] != null){
                                    echo 'x' . $record['Fight']['time_per_round'];
                                } else {
                                    echo 'x5';
                                }
                            ?>
                        </td>
                        <td><?php echo $record['Fight']['time']; ?></td>
                        <td>
                            <a href="<?php echo $this->Html->url('/Fight/' . $this->getUrlPerson->getUrl($record['Fight']['id'], $record['Fight']['title'])); ?>">
                                <i class="fa fa-mma-fighters fa-2x"></i>
                            </a>
                        </td>
                        <td>
                            <?php if(count($record['Fight']['Title']) > 0){ ?>
                                <a href="#show-titles" class="show-title-detail" id-fight="<?php echo $record['Fight']['id']?>" data-toggle="modal" data-target="#myModalTitle">
                                    <i class="fa fa-champ-belt fa-2x"></i>
                                </a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="myModalTitle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo __('Close', true);?></span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Title Detail', true);?></h4>
                
                
               
            </div>
            <div class="modal-body" id="title-content-modal"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
                
        $('.show-title-detail').click(function(){
            var idFight     = $(this).attr('id-fight');
            var loadTitles  = "<?php echo $this->Html->url(array('action' => 'showTitles'));?>/" + idFight;
            $.post(loadTitles, function(data){
                $('#title-content-modal').html(data);
            });
        });
        
        $('.responsive-table').ngResponsiveTables({
            smallPaddingCharNo: 13,
            mediumPaddingCharNo: 18,
            largePaddingCharNo: 30
        });
    });
</script>