<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Users', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-user"></i> 
                    <?php echo __('Users', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('User Roles', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo __('user roles', true);?></h3>
            </div>
            <div class="panel-body">
            	<?php echo $this->Form->create('Role', array('role' => 'form', 'class' => 'col-md-offset-1')); ?>

            		<?php 
            			echo $this->Form->input('id', array('value' => $roles['Role']['id']));
            			echo $this->Form->input('users_id', array('type' => 'hidden', 'value' => $roles['Role']['users_id']));
        			?>
        			<div class="form-group">
                		<label><?php echo __('User Roles');?></label>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('users', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['users']));?>
                    			<?php echo __('User Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('persons', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['persons']));?>
                    			<?php echo __('Persons Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('events', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['events']));?>
                    			<?php echo __('Events Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('figth', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['figth']));?>
                    			<?php echo __('Fights Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('images', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['images']));?>
                    			<?php echo __('Images Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('videos', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['videos']));?>
                    			<?php echo __('Videos Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('manage', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['manage']));?>
                    			<?php echo __('Manage Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('venues', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['venues']));?>
                    			<?php echo __('Venues Access', true);?>
                  			</label>
                		</div>
                		<div class="checkbox">
                  			<label>
                    			<?php echo $this->Form->input('notes', array('type' => 'checkbox', 'value' => 1, 'div' => false, 'label' => false, 'checked' => $roles['Role']['notes']));?>
                    			<?php echo __('Notes Access', true);?>
                  			</label>
                		</div>

                		<div class="form-group">
                        	<?php echo $this->Form->button(__('Save Roles'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    	</div>

              		</div>

            	<?php echo $this->Form->end(); ?>

            </div>
    	</div>
    </div>
</div>