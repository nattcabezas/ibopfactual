<div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Users', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-user"></i> 
                    <?php echo __('Users', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
    	<div class="panel panel-default">
    		<div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo __('add user', true);?></h3>
            </div>
            <div class="panel-body">
            	<?php echo $this->Form->create('User', array('role' => 'form')); ?>

					<div class="form-group">
                        <label><?php echo __('Username:', true);?></label>
                        <?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'div' => false, 'required' => true))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Full Name:', true);?></label>
                        <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'div' => false, 'required' => true))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('E-mail:', true);?></label>
                        <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'div' => false, 'required' => true))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Password:', true);?></label>
                        <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'div' => false, 'required' => true))?>
                    </div>

                    <div class="form-group">
                        <label><?php echo __('Confirm Password:', true);?></label>
                        <?php echo $this->Form->input('confirm-password', array('type' => 'password', 'class' => 'form-control', 'label' => false, 'div' => false, 'required' => true))?>
                    </div>
					
					<div class="form-group">
                        <?php echo $this->Form->button(__('Add User'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                    </div>

            	<?php echo $this->Form->end(); ?>
            </div>
    	</div>
    </div>
</div>