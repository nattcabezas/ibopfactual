<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Users', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-user"></i> <?php echo __('Users', true)?></li>
        </ol>
    </div>
    
    <div class="col-lg-12">
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary pull-right">
            <?php echo __('add user', true);?>
        </a>
        <p>&emsp;</p>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Username', true);?></th>
                        <th><?php echo __('Full Name', true);?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user){ ?>
                        <tr>
                            <td><?php echo $user['User']['username']?></td>
                            <td><?php echo $user['User']['name']?></td>
                            <td>
                                <center>
                                    <div class="btn-group">
                                        <a href="<?php echo $this->Html->url(array('action' => 'roles/' . base64_encode($user['User']['id'])));?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="<?php echo __('User Roles', true);?>">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </a>
                                        <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($user['User']['id'])));?>" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php 
            echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->numbers(array(
                'before' => '',
                'after' => '',
                'separator' => '',
                'tag' => 'li',
                'currentClass' => 'active',
                'currentTag' => 'a',
            ));
            echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
            echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));

        ?>
    </ul>
</div>