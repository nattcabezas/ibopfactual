<div class="col-lg-12">
    <h1 class="page-header"><?php echo __('Articles - Search results for: ' . $searchCriteria, true); ?></h1>
       <ol class="breadcrumb">
           <li class="active"><i class="fa fa-file"></i> <?php echo __('Articles', true) ?></li>
       </ol>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true); ?></th>
                        <th><?php echo __('Date', true); ?></th>
                        <th><?php echo __('Category', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($articles as $article) { ?>
                    <?php if ($article['Article']['publish'] == '0'){
                            $variableClass = 'danger';
                        }else{
                            $variableClass = 'default';
                        } ?>
                        <tr  class="<?php echo $variableClass; ?>" >
                            <td><?php echo $article['Article']['title'] ?></td>
                            <td><?php echo $article['Article']['date'] ?></td>
                            <td><?php echo $article['Category']['name'] ?></td>
                            <td>
                    <center>
                        <div class="btn-group">
                            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($article['Article']['id']))); ?>" class="btn btn-primary">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="<?php echo $this->Html->url(array('action' => 'delete/' .  base64_encode($article['Article']['id'])));?>" class="btn btn-danger btn-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </center>
                    </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>