<div class="container">
    
    <div class="banner_top">
        <img src="<?php echo $this->Html->url('/img/970x90b-2.gif')?>" class="img img-responsive img_center" alt=""/>
    </div>
    
    <div class="col-lg-9 articles_list">
        <h2><?php echo __('List of articles', true); ?></h2>
        <div class="col-lg-12 main_article  ">
        
        <?php foreach ($articles as $article) { ?>

            <div class="article_listing"> 
                <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Category']['name'], $article['Article']['id'], $article['Article']['title'])); ?>">
                    <?php if ($article['Article']['img'] != null) { ?>
                        <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380); ?>" class="img-thumbnail img-responsive article_listing_img">
                    <?php } else { ?>
                        <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg') ?>" class="img-thumbnail img-responsive article_listing_img">
                    <?php } ?>
                    <h4><?php echo $article['Article']['title'] ?></h4>
                </a>
                <div class="article_credit">
                    <?php if ($article['Category']['name']) { ?>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'Category/' . $article['Category']['name']));?>">
                            <i class="fa fa-tags"></i>
                            <span>
                                <?php echo $article['Category']['name'] ?>
                            </span>
                        </a>
                    <?php } ?>
                    <i class="fa fa-calendar-o"></i><span> <?php echo $article['Article']['date'] ?></span>
                </div>
                <p><?php echo substr(strip_tags($article['Article']['contend']), 0, 150); ?>...</p>
            </div>

        <?php } ?>

            
            <div class="col-lg-4">
                <?php if ($page != 1) { ?>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'Category/' . $categoryName . '/' . ($page - 1))); ?>" class="btn btn-primary btn-group-justified">
                        <?php echo __('Previous'); ?>
                    </a>
                <?php } ?>
            </div>
            <div class="col-lg-4 col-lg-offset-4">
                <?php if (count($articles) >= 6) { ?>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'Category/' . $categoryName . '/' . ($page + 1))); ?>" class="btn btn-primary btn-group-justified">
                        <?php echo __('Next'); ?>
                    </a>
                <?php } ?>
            </div>
            <hr>
        </div>
 
   </div>
    

            <div class="col-lg-3">
                <div class="col-lg-12 col-sm-6 cat_menu">
                    <h3><?php echo __('Categories Menu');?></h3>
                    <ul>
                        <?php foreach ($categories as $category){ ?>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'Category/' . $category['Category']['name']));?>">
                                <li class="">
                                    <?php echo $category['Category']['name']?>
                                </li>
                            </a>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-lg-12 col-sm-6 side_banner">
                    <img src="<?php echo $this->Html->url('/img/300x600c-2.gif')?>" class="img_center img-responsive" alt=""/>
                </div>
                <div class='col-lg-12 col-sm-6 side_banner'>
                    <img src="<?php echo $this->Html->url('/img/300x250.gif')?>" class="img_center img-responsive" alt=""/>
                </div>
            </div>
</div>