<div class="container">
    
    <div class="banner_top">
        <img src="<?php echo $this->Html->url('/img/970x90b-2.gif')?>" class="img img-responsive img_center" alt=""/>
    </div>
    
    <div class="col-lg-9 articles_list">
        
        <h2><?php echo __('List of articles', true); ?></h2>
        
        <div class="col-lg-12 main_article  ">
        
        <?php foreach ($articles as $article) { ?>

            <div class="article_listing"> 
                <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Category']['name'], $article['Article']['id'], $article['Article']['title'])); ?>">
                    <?php if ($article['Article']['img'] != null) { ?>
                        <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380); ?>" class="img-thumbnail img-responsive article_listing_img">
                    <?php } else { ?>
                        <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg') ?>" class="img-thumbnail img-responsive article_listing_img">
                    <?php } ?>
                    <h4><?php echo $article['Article']['title'] ?></h4>
                </a>
                <div class="article_credit">
                    <?php if ($article['Category']['name']) { ?>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'Category/' . $article['Category']['name']));?>">
                            <i class="fa fa-tags"></i>
                            <span>
                                <?php echo $article['Category']['name'] ?>
                            </span>
                        </a>
                    <?php } ?>
                    <i class="fa fa-calendar-o"></i>
                    <span> 
                        <?php echo date('Y-m-d', strtotime($article['Article']['date'])) ?>
                    </span>
                </div>
                <p><?php echo substr(strip_tags($article['Article']['contend']), 0, 150); ?>...</p>
            </div>

        <?php } ?>

            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <?php if ($page != 1) { ?>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index/' . ($page - 1))); ?>" class="btn btn-primary btn-group-justified">
                        <?php echo __('Previous'); ?>
                    </a>
                <?php } ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <?php if (count($articles) >= 6) { ?>
                    <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index/' . ($page + 1))); ?>" class="btn btn-primary btn-group-justified">
                        <?php echo __('Next'); ?>
                    </a>
                <?php } ?>
            </div>
            
        </div>
        <div class="footer_separator"></div>
   </div>
    
   <div class="col-lg-3">
        <div>
            <?php echo $this->Form->input('keywork', array('id' => 'search-keyword', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
        </div>
        <div>
            <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'search-articles')); ?>
        </div>
    </div>
    
    
    <div class="col-lg-3">
        <div class="col-lg-12 col-sm-6 cat_menu">
            <h3><?php echo __('Categories Menu');?></h3>
            <ul>
                <?php foreach ($categories as $category){ ?>
                <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'Category/' . $category['Category']['name']));?>">
                        <li class="">
                            <?php echo $category['Category']['name']?>
                        </li>
                    </a>
                <?php } ?>
            </ul>
        </div>
        <div class="col-lg-12 col-sm-6 side_banner">
            <img src="<?php echo $this->Html->url('/img/300x600c-2.gif')?>" class="img_center img-responsive" alt=""/>
        </div>
        <div class='col-lg-12 col-sm-6 side_banner'>
            <img src="<?php echo $this->Html->url('/img/300x250.gif')?>" class="img_center img-responsive" alt=""/>
        </div>
    </div>
</div>


<?php
    echo $this->Html->script('article_scripts.js');
?>

<script type="text/javascript">
$(document).ready(function() {
    
    $('#search-articles').click(function(event) {
            searchArticle();
    });
    
    $('#search-keyword').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
           searchArticle();
         }
    });
    
    
    function searchArticle()
    {
        var keyword = Base64.encode($('#search-keyword').val());
        var loadsearch = "<?php echo $this->Html->url(array('action' => 'searchArticle')) ?>/";
        window.location.replace(loadsearch + keyword);        
    }
        
});
</script>