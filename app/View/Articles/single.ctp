<?php echo $this->Html->css('social-icons');?>
<div class="container">
    <div class="row">
       
           
            <div class="row mobile-social-share">
                
            
                <div class="col-lg-9">
                    <h1><?php echo $article['Article']['title']; ?></h1>
                    <div class="row" >
                        <div class="container" >
                            <div id="bc1" class="btn-group btn-breadcrumb" itemscope itemtype="http://data-vocabulary.org/BreadcrumbList">
                                <div class="btn btn-default" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a href="<?php echo $this->Html->url('/'); ?>"  itemprop="url"><i class="fa fa-home"></i></a>
                                </div>
                                <?php if ($article['Category']['name']) { ?>
                                    <div class="btn btn-default" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                        <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'Category/' . $article['Category']['name'])); ?>" itemprop="url">
                                            <div itemprop="title"><?php echo $article['Category']['name'] ?></div></a>
                                    </div>
                                <?php } ?>
                                <div class="btn btn-default" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a href="#<?php echo $article['Category']['name'] ?>" itemprop="url"><div id="truncated" itemprop="title">Born on December 15th, 1862... Jack Dempsey, 'The Nonpareil'</div></a>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-12">
                        <?php if ($article['Article']['img'] != null) { ?>
                            <img src="<?php echo $this->Html->url('/files/articles/' . $article['Article']['img']) ?>" class="img_article img img-responsive " width="700em">
                        <?php } else { ?>
                            <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg') ?>" class="img_article img img-responsive " width="700em">
                        <?php } ?>

                    </div>
                    
                
               
                
                <div id="article-details" class="container">
                    
                    <h5 class="">
                        <?php if($article['Category']['name']){ ?>
                            <i class="fa fa-tags"></i>
                            <?php echo __('Category: ');?><?php echo $article['Category']['name'] ?>
                        <?php } ?>
                        <i class="fa fa-calendar-o "></i>
                        <?php echo __('Date: ')?><?php echo date('Y-m-d', strtotime($article['Article']['date']));?>
                    </h5> <br>
                    
                    <?php echo $article['Article']['contend']?>
                </div>
                
                <div class="social-button text-center caeme-bien-rudy">
                    <a href="#" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>                        
                    <a href="#" class="btn btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="btn btn-social-icon btn-tumblr"><i class="fa fa-tumblr"></i></a>
                    <a href="#" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                </div>
                
                <hr>
                
                <h2><?php echo __('Related Articles');?></h2>
                <div class="articles-cell">
                    <?php foreach ($relatedArticles as $article) { ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <h4 class="text-center">
                            <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Category']['name'], $article['Article']['id'], $article['Article']['title']));?>">
                                <?php echo $article['Article']['title']?>
                            </a>
                        </h4>
                        <div class="feed-conted">
                            <p>
                                <?php if($article['Article']['img'] != null) { ?>
                                    <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380);?>" class="img-responsive img-thumbnail">
                                <?php } else { ?>
                                    <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg')?>"  class="img-responsive img-thumbnail">
                                <?php } ?>
                            </p>
                            <p><?php echo substr(strip_tags($article['Article']['contend']), 0, 150);?>...</p>
                        </div>
                    </div>
                    <?php } ?>                    
                </div>
                
                <div class="clear"></div>
                
                <div class="banner_bottom">
                    <img src="<?php echo $this->Html->url('/img/970x90.gif')?>" class="img img-responsive " alt=""/>
                </div>
                
            </div>

            <div class="col-lg-3">
                
                <div id="side-banner" class="side_banner">
                    <img src="<?php echo $this->Html->url('/img/300x600c.gif')?>" class="img img-responsive"width="250px"alt=""/>
                </div>
                
                <div id='more_articles'>
                    <h3><?php echo __('More Articles', true);?></h3>
                    
                    <?php foreach ($lastArticles as $article){ ?>
                        <div class="article_listing_side">
                            <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Category']['name'], $article['Article']['id'], $article['Article']['title']));?>">
                                <?php if($article['Article']['img'] != null) { ?>
                                    <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380);?>" class="img img-thumbnail img-responsive article_listing_img">
                                <?php } else { ?>
                                    <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg')?>" class="img img-thumbnail img-responsive article_listing_img">
                                <?php } ?>
                                <h4><?php echo $article['Article']['title']?></h4>
                            </a>
                            <p><?php echo substr(strip_tags($article['Article']['contend']), 0, 150);?>...</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
                    
        
    </div>
</div>


<script type="text/javascript">

$(document).ready(function(){
    $(window).resize(function() {

       ellipses1 = $("#bc1 :nth-child(2)");
        if ($("#bc1 a:hidden").length >0) {
            ellipses1.show();
        } else {
            ellipses1.hide();
        }
        
        ellipses2 = $("#bc2 :nth-child(2)");
        if ($("#bc2 a:hidden").length >0) {
            ellipses2.show();
        } else {
            ellipses2.hide();
        }
        
     });
    
    
    var windowSize = $(window).width();

    var title = $("#truncated").text();
    var shortTitle = jQuery.trim(title).substring(0, 10)
            .split(" ").slice(0, 3).join(" ") + "...";
    var medTitle = jQuery.trim(title).substring(0, 25)
            .split(" ").slice(0, 4).join(" ") + "...";

    if (windowSize <= 480){
        $("#truncated").replaceWith(shortTitle);

    } else if (windowSize <= 768){
        $("#truncated").replaceWith(medTitle);
            } 
    
});
</script>
