<?php
echo $this->Html->css('fileinput');
echo $this->Html->script('fileinput');
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Articles', true); ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index')); ?>">
                    <i class="fa fa-file"></i> 
                    <?php echo __('Articles', true); ?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true); ?>
            </li>
        </ol>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-file"></i> <?php echo __('add article', true); ?></h3>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('Article', array('type' => 'file')); ?>

                <div class="form-group">
                    <label><?php echo __('Title:', true); ?></label>
                    <?php echo $this->Form->input('title', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>
                
                
                <div class="form-group">
                    <label><?php echo __('Related identity:', true); ?></label>
                    <input type="text" id="identities_id_text" class="form-control input-sm search-identity">
                    <?php echo $this->Form->input('identities_id', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text', 'id' => 'identities_id', 'type' => 'hidden')); ?>
                </div>
                
                

                <div class="form-group">
                    <label><?php echo __('Date:', true); ?></label>
                    <?php echo $this->Form->input('date', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'datepicker-articles form-control')); ?>
                </div>                
                <div class="form-group">
                    <label><?php echo __('Principal Image:', true); ?></label>
                    <?php echo $this->Form->input('img_file', array('type' => 'file', 'label' => false, 'div' => false, 'class' => 'form-control file-input')); ?>
                    <?php echo $this->Form->input('img', array('type' => 'hidden')); ?>
                    <span><?php echo __('preferably dimensional image with width 760px and height 400px', true);?><span>
                </div>

                <div class="form-group">
                    <label><?php echo __('Content:', true); ?></label>
                    <?php echo $this->Form->input('contend', array('label' => false, 'div' => false, 'class' => 'form-control form-textarea')); ?>
                </div>

                <div class="form-group">
                    <label><?php echo __('Tags:', true); ?></label>
                    <?php echo $this->Form->input('tags', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
                </div>
                <div class="form-group">
                        <label><?php echo __('Status:', true);?></label>
                        <div class="form-group">
                            <?php 
                                $options = array(
                                    '0'  => 'Hide', 
                                    '1'  => 'Published'                                    
                                );
                            ?>
                            <?php echo $this->Form->input('publish', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => $options )); ?>
                        </div>
                </div>
                <div class="form-group">
                    <label><?php echo __('Category:', true);?></label>
                    <?php echo $this->Form->input('categories_id', array('label'=> false, 'div' => false, 'class' => 'form-control', 'options' => $category)); ?>
                </div> 
                <div class="form-group">
                    <?php echo $this->Form->button(__('Add Article'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('jquery-ui-timepicker-addon');?>
<script type="text/javascript">
    $('.file-input').fileinput({showUpload: false});
    $(document).ready(function() {
        
        $('.search-identity').autocomplete({
            minLength: 3,
            source: function(request, response) {
                var loadIdentities = "<?php echo $this->Html->url(array('controller' => 'Identities', 'action' => 'getIdentity')); ?>/" + Base64.encode(request.term);
                $.getJSON(loadIdentities, function(data) {
                    response(data);
                });
            },
            change: function(request, response) {
                $("#identities_id_text").text(response.item.label);
                $("#identities_id").val(response.item.id);
                console.log(response.item.id);
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            //console.log(item.id);
            return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
        };
        
        
        $('#ArticleImgFile').change(function() {
            $('#ArticleImg').val($('.file-caption-name').html());
        });
        $('.fileinput-remove-button').click(function(event) {
            $('#ArticleImg').val("");
        });
        $('.fileinput-remove').click(function() {
            $('#ArticleImg').val("");
        });        
        $('.datepicker-articles').datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat:'hh:mm:ss'
        });
    });
       
</script>