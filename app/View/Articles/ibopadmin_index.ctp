<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Articles', true); ?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-file"></i> <?php echo __('Articles', true) ?></li>
        </ol>
    </div>    
    
    
    <div class="col-lg-3">
        <div>
           <?php echo $this->Form->input('keywork', array('id' => 'search-keyword', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Search', true))); ?>
        </div>
        <div>
           <?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'search-articles')); ?>
        </div>
    </div>
    
    <div class="col-lg-2 col-md-offset-10"> 
        <a href="<?php echo $this->Html->url(array('action' => 'add')); ?>" class="btn btn-primary">
            <?php echo __('add Article', true); ?>
        </a>
       
        <a href="<?php echo $this->Html->url(array('controller' => 'Categories', 'action' => 'index')); ?>" class="btn btn-primary">
            <?php echo __('Categories', true); ?>
        </a>
        
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-striped tablesorter">
                <thead>
                    <tr>
                        <th><?php echo __('Title', true); ?></th>
                        <th><?php echo __('Date', true); ?></th>
                        <th><?php echo __('Category', true); ?></th>
                        <th>&emsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($articles as $article) { ?>
                    <?php if ($article['Article']['publish'] == '0'){
                            $variableClass = 'danger';
                        }else{
                            $variableClass = 'default';
                        } ?>
                        <tr  class="<?php echo $variableClass; ?>" >
                            <td><?php echo $article['Article']['title'] ?></td>
                            <td><?php echo $article['Article']['date'] ?></td>
                            <td><?php echo $article['Category']['name'] ?></td>
                            <td>
                    <center>
                        <div class="btn-group">
                            <a href="<?php echo $this->Html->url(array('action' => 'edit/' . base64_encode($article['Article']['id']))); ?>" class="btn btn-primary">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="<?php echo $this->Html->url(array('action' => 'delete/' .  base64_encode($article['Article']['id'])));?>" class="btn btn-danger btn-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </center>
                    </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="bs-example">
    <ul class="pagination">
        <?php
        echo $this->Paginator->first('<<', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->prev('<', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->numbers(array(
            'before' => '',
            'after' => '',
            'separator' => '',
            'tag' => 'li',
            'currentClass' => 'active',
            'currentTag' => 'a',
        ));
        echo $this->Paginator->next('>', array('tag' => 'li', 'class' => 'list-pagination'));
        echo $this->Paginator->last('>>', array('tag' => 'li', 'class' => 'list-pagination'));
        ?>
    </ul>
</div>

<script type="text/javascript">
$(document).ready(function() {
    
    $('#search-articles').click(function(event) {
            searchArticle();
    });
    
    $('#search-keyword').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
           searchArticle();
         }
    });
    
    
    function searchArticle()
    {
        var keyword = Base64.encode($('#search-keyword').val());
        var loadsearch = "<?php echo $this->Html->url(array('action' => 'ibopadmin_searchArticle')) ?>/";
        window.location.replace(loadsearch + keyword);
    }
        
});
</script>
