<div class="container">
    
    <div class="banner_top">
        <img src="<?php echo $this->Html->url('/img/970x90.gif')?>" class="img img-responsive img_center" alt=""/>
    </div>
    
    <div class="col-lg-9">
        <h2><?php echo __('List of articles', true);?></h2>
        
        <?php $firstArticel = true;?>
        <?php foreach ($articles as $article){ ?>
        
            <?php if($firstArticel){ ?>
        
                <div class="col-lg-10 main_article">
                    <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Article']['id'], $article['Article']['title']));?>">
                        <?php if($article['Article']['img'] != null) { ?>
                            <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380);?>" class="img-thumbnail img-responsive article_listing_img_main">
                        <?php } else { ?>
                            <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg')?>" class="img-thumbnail img-responsive article_listing_img_main">
                        <?php } ?>
                        <h4><?php echo $article['Article']['title']?></h4> 
                    </a>
                    <div class="article_credit">
                        <?php if($article['Category']['name']){ ?>
                            <i class="fa fa-tags"></i><span><?php echo $article['Category']['name'] ?></span>
                        <?php } ?>
                        <i class="fa fa-calendar-o"></i><span> <?php echo $article['Article']['date']?></span>
                    </div>
                    <p><?php echo substr(strip_tags($article['Article']['contend']), 0, 250);?>...</p>
                </div>
                <?php $firstArticel = false;?>
                <div class='col-lg-8 articles_list'>
            <?php } else { ?>
                    
                <div class="article_listing"> 
                    <a href="<?php echo $this->Html->url('/Article/' . $this->getUrlPerson->getArticletUrl($article['Article']['id'], $article['Article']['title']));?>">
                        <?php if($article['Article']['img'] != null) { ?>
                            <img src="<?php echo $this->Image->resize('/files/articles/' . $article['Article']['img'], 1140, 380);?>" class="img-thumbnail img-responsive article_listing_img">
                        <?php } else { ?>
                            <img src="<?php echo $this->Html->url('/img/defaults/generic_article.jpg')?>" class="img-thumbnail img-responsive article_listing_img">
                        <?php } ?>
                        <h4><?php echo $article['Article']['title']?></h4>
                    </a>
                    <div class="article_credit">
                        <?php if($article['Category']['name']){ ?>
                            <i class="fa fa-tags"></i><span><?php echo $article['Category']['name'] ?></span>
                        <?php } ?>
                        <i class="fa fa-calendar-o"></i><span> <?php echo $article['Article']['date']?></span>
                    </div>
                    <p><?php echo substr(strip_tags($article['Article']['contend']), 0, 150);?>...</p>
                </div>
                    
            <?php } ?>
        
        <?php } ?>
                    <div class="col-lg-4">
                        <?php if($page != 1 ){ ?>
                            <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index/' . ($page - 1)));?>" class="btn btn-primary btn-group-justified">
                                <?php echo __('Previous');?>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="col-lg-4 col-lg-offset-4">
                        <?php if(count($articles) >= 6 ){ ?>
                            <a href="<?php echo $this->Html->url(array('controller' => 'Articles', 'action' => 'index/' . ($page + 1)));?>" class="btn btn-primary btn-group-justified">
                                <?php echo __('Next');?>
                            </a>
                        <?php } ?>
                    </div>
                    <hr>
                </div>
        
            
            <div class="col-lg-4 articles_list">
                <h4 class="section_header">
                    From the blog of TheSweetScience.com
                </h4>
                <div class="article_listing">
                    <a href="">
                        <h4 class="side_article_title">Remembering 'Sugar' Ray Robinson's Madison Square Garden Farewell</h4>
                    </a>
                    <div class="article_credit">
                    <i>By </i><span > Miguel Factual</span>
                    <span> 2014-12-15</span>
                    </div>
                </div>
                <div class="article_listing">
                    <a href=""><h4 class="side_article_title">Debuting on December 10th, 1917 - 'The Wild Bull of the Pampas'</h4></a>
                    <div class="article_credit">
                    <i>By </i><span > Miguel Factual</span>
                    <span> 2014-12-15</span>
                    </div>
                </div>
                <div class="article_listing">
                    <a href="">

                        <h4 class="side_article_title">Remembering 'Sugar' Ray Robinson's Madison Square Garden Farewell</h4>
                    </a>
                    <div class="article_credit">
                    <i>By </i><span > Miguel Factual</span>
                    <span> 2014-12-15</span>
                    </div>
                </div>
                <div class="article_listing">
                    <a href=""><h4 class="side_article_title">Debuting on December 10th, 1917 - 'The Wild Bull of the Pampas'</h4></a>
                    <div class="article_credit">
                    <i>By </i><span> Miguel Factual</span>
                    <span> 2014-12-15</span>
                    </div>
                </div>
            </div>
        
    </div>


    <div class="col-lg-3">
                <div class="col-lg-12 col-sm-6 side_banner">
                    <img src="<?php echo $this->Html->url('/img/300x600c.gif')?>" class="img_center img-responsive" alt=""/>
                </div>
                <div class='col-lg-12 col-sm-6 side_banner'>
                    <img src="<?php echo $this->Html->url('/img/300xx250.gif')?>" class="img_center img-responsive" alt=""/>
                </div>
            </div>
</div>
</div>