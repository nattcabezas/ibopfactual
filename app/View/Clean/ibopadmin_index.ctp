<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Clean', true);?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-refresh"></i> <?php echo __('Clean Data', true)?></li>
        </ol>
    </div>
</div><br>
<div class="row">
    <div class="col-lg-3">
        <ul class="nav nav-pills nav-stacked" role="tablist">
            <li class="active">
                <a href="<?php echo $this->Html->url(array('action' => 'clean/1'));?>">
                    <i class="fa fa-group"></i>
                    <?php echo __('Clean Persons', true);?>
                </a>
            </li>
            <li class="active">
                <a href="<?php echo $this->Html->url(array('action' => 'clean/2'));?>">
                    <i class="fa fa-shield"></i>
                    <?php echo __('Clean Fights', true);?>
                </a>
            </li>
            <li class="active">
                <a href="<?php echo $this->Html->url(array('action' => 'clean/3'));?>">
                    <i class="fa fa-calendar"></i>
                    <?php echo __('Clean Venues', true);?>
                </a>
            </li>
            <li class="active">
                <a href="<?php echo $this->Html->url(array('action' => 'clean/4'));?>">
                    <i class="fa fa-building"></i>
                    <?php echo __('Clean Events', true);?>
                </a>
            </li>
        </ul>
    </div>
</div>

