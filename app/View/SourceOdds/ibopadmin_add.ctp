<?php 
	echo $this->Html->css('fileinput');
	echo $this->Html->script('fileinput');
?>
    <div class="row">
	<div class="col-lg-12">
        <h1 class="page-header"><?php echo __('Source ODDS', true);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $this->Html->url(array('action' => 'index'));?>">
                    <i class="fa fa-usd"></i> 
                    <?php echo __('Source ODDS', true);?>
                </a>
            </li>
            <li class="active">
                <?php echo __('Add', true);?>
            </li>
        </ol>
    </div>
	

    <div class="col-lg-12">
    	<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-usd"></i> <?php echo __('add Source', true);?></h3>
            </div>
            <div class="panel-body">
				<?php echo $this->Form->create('SourceOdd', array('type' => 'file')); ?>

					<div class="form-group">
						<label><?php echo __('Name:', true);?></label>
						<?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Url:', true);?></label>
						<?php echo $this->Form->input('url', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
						<label><?php echo __('Image:', true);?></label>
						<?php echo $this->Form->input('img_file', array('type' => 'file', 'label' => false, 'div' => false, 'class' => 'form-control file-input')); ?>
						<?php echo $this->Form->input('img', array('type' => 'hidden'));?>
					</div>

					<div class="form-group">
						<label><?php echo __('notes:', true);?></label>
						<?php echo $this->Form->input('notes', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>

					<div class="form-group">
	            		<?php echo $this->Form->button(__('Add Source'), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
	            	</div>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.file-input').fileinput({showUpload: false});
	$(document).ready(function() {
		$('#SourceOddImgFile').change(function(){
			$('#SourceOddImg').val($('.file-caption-name').html());
		});
		$('.fileinput-remove-button').click(function(event) {
			$('#SourceOddImg').val("");
		});
		$('.fileinput-remove').click(function() {
			$('#SourceOddImg').val("");
		});
	});
</script>