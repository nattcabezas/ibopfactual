<div class="sourceOdds view">
<h2><?php echo __('Source Odd'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sourceOdd['SourceOdd']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($sourceOdd['SourceOdd']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url'); ?></dt>
		<dd>
			<?php echo h($sourceOdd['SourceOdd']['url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img'); ?></dt>
		<dd>
			<?php echo h($sourceOdd['SourceOdd']['img']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($sourceOdd['SourceOdd']['notes']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Source Odd'), array('action' => 'edit', $sourceOdd['SourceOdd']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Source Odd'), array('action' => 'delete', $sourceOdd['SourceOdd']['id']), array(), __('Are you sure you want to delete # %s?', $sourceOdd['SourceOdd']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Source Odds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Source Odd'), array('action' => 'add')); ?> </li>
	</ul>
</div>
