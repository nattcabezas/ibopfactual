<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'users_id'
		)
	);
        
}
