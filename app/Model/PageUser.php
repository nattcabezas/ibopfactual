<?php
App::uses('AppModel', 'Model');
/**
 * PageUser Model
 *
 */
class PageUser extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        
     
}
