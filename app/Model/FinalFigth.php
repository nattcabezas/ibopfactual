<?php
App::uses('AppModel', 'Model');
/**
 * FinalFigth Model
 *
 */
class FinalFigth extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
