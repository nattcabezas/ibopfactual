<?php
App::uses('AppModel', 'Model');
/**
 * EventsFight Model
 *
 * @property Events $Events
 * @property Fights $Fights
 */
class EventsFight extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'events_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fights_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Events' => array(
			'className' => 'Events',
			'foreignKey' => 'events_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Fights' => array(
			'className' => 'Fights',
			'foreignKey' => 'fights_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
