<?php
App::uses('AppModel', 'Model');
/**
 * Title Model
 *
 */
class Title extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
