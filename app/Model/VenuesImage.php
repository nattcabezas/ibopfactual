<?php
App::uses('AppModel', 'Model');
/**
 * VenuesImage Model
 *
 * @property Venues $Venues
 * @property Images $Images
 */
class VenuesImage extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'venues_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'images_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Venues' => array(
			'className' => 'Venues',
			'foreignKey' => 'venues_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Images' => array(
			'className' => 'Images',
			'foreignKey' => 'images_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
