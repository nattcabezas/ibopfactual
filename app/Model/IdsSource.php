<?php
App::uses('AppModel', 'Model');
/**
 * IdsSource Model
 *
 */
class IdsSource extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
