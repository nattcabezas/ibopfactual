<?php
App::uses('AppModel', 'Model');
/**
 * Fight Model
 *
 * @property Sources $Sources
 * @property Weights $Weights
 * @property FinalFigths $FinalFigths
 * @property Rules $Rules
 * @property Event $Event
 * @property Image $Image
 * @property Note $Note
 * @property Title $Title
 * @property Video $Video
 */
class Fight extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';



/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Sources' => array(
			'className' => 'Sources',
			'foreignKey' => 'sources_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Weights' => array(
			'className' => 'Weights',
			'foreignKey' => 'weights_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinalFigths' => array(
			'className' => 'FinalFigths',
			'foreignKey' => 'final_figths_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Rules' => array(
			'className' => 'Rules',
			'foreignKey' => 'rules_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'Event' => array(
                        'className' => 'Event',
                        'foreignKey' => 'events_id'
                )
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Image' => array(
			'className' => 'Image',
			'joinTable' => 'fights_images',
			'foreignKey' => 'fights_id',
			'associationForeignKey' => 'images_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Note' => array(
			'className' => 'Note',
			'joinTable' => 'fights_notes',
			'foreignKey' => 'fights_id',
			'associationForeignKey' => 'notes_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Title' => array(
			'className' => 'Title',
			'joinTable' => 'fights_titles',
			'foreignKey' => 'fights_id',
			'associationForeignKey' => 'titles_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'fights_videos',
			'foreignKey' => 'fights_id',
			'associationForeignKey' => 'videos_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);
        
        public $hasMany = array(
            'FightIdentity' => array(
                'className'     => 'FightIdentity',
                'foreignKey'    => 'fights_id',
                'order'         => array(
                    'FightIdentity.corner' => 'ASC'
                )
            )
        );

}
