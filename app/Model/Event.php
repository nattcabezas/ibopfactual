<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property Venues $Venues
 * @property Locations $Locations
 * @property Fight $Fight
 * @property Image $Image
 * @property PromotionalCompany $PromotionalCompany
 * @property Video $Video
 */
class Event extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Venues' => array(
			'className' => 'Venues',
			'foreignKey' => 'venues_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Locations' => array(
			'className' => 'Locations',
			'foreignKey' => 'locations_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Fight' => array(
			'className' => 'Fight',
			'joinTable' => 'events_fights',
			'foreignKey' => 'events_id',
			'associationForeignKey' => 'fights_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Image' => array(
			'className' => 'Image',
			'joinTable' => 'events_images',
			'foreignKey' => 'events_id',
			'associationForeignKey' => 'images_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => array(
                            'EventsImage.principal' => 'DESC'
                        ),
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'PromotionalCompany' => array(
			'className' => 'PromotionalCompany',
			'joinTable' => 'events_promotional_companies',
			'foreignKey' => 'events_id',
			'associationForeignKey' => 'promotional_companies_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'events_videos',
			'foreignKey' => 'events_id',
			'associationForeignKey' => 'videos_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);
	
	
	public $hasMany = array(
		'EventsJob' => array(
                    'className' 	=> 'EventsJob',
                    'foreignKey'	=> 'events_id'
		),
                'EventsImage' => array(
                    'className' => 'EventsImage',
                    'foreignKey' => 'events_id',
                    'order' => array(
                        'EventsImage.principal' => 'DESC'
                    )
                )
	);

}
