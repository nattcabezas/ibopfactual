<?php
App::uses('AppModel', 'Model');
/**
 * IdentityNickname Model
 *
 * @property Identities $Identities
 */
class IdentityNickname extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'identities_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Identities' => array(
			'className' => 'Identities',
			'foreignKey' => 'identities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
