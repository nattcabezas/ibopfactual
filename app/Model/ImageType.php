<?php
App::uses('AppModel', 'Model');
/**
 * ImageType Model
 *
 */
class ImageType extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'type';
}
