<?php
App::uses('AppModel', 'Model');
/**
 * EventsNote Model
 *
 * @property Events $Events
 * @property Notes $Notes
 */
class EventsNote extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'events_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'notes_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Events' => array(
			'className' => 'Events',
			'foreignKey' => 'events_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Notes' => array(
			'className' => 'Notes',
			'foreignKey' => 'notes_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
