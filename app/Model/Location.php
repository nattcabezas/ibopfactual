<?php
App::uses('AppModel', 'Model');
/**
 * Location Model
 *
 * @property Countries $Countries
 * @property States $States
 * @property Cities $Cities
 */
class Location extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Countries' => array(
			'className' => 'Countries',
			'foreignKey' => 'countries_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'States' => array(
			'className' => 'States',
			'foreignKey' => 'states_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cities' => array(
			'className' => 'Cities',
			'foreignKey' => 'cities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
        public $hasOne = array(
            'Venue' => array(
                'className' => 'Venue',
                'foreignKey' => 'locations_id'
            )
        );
}
