<?php
App::uses('AppModel', 'Model');
/**
 * Source Model
 *
 */
class Source extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
