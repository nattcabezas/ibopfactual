<?php


App::uses('AppModel', 'Model');
/*
 * Category model
 */
class Category extends  AppModel{
    /*
     * Display field
     * @var string
     */
    public $displayfield = 'name'; 
    
}