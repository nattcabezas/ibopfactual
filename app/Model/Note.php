<?php
App::uses('AppModel', 'Model');
/**
 * Note Model
 *
 * @property Fight $Fight
 */
class Note extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Fight' => array(
			'className' => 'Fight',
			'joinTable' => 'fights_notes',
			'foreignKey' => 'notes_id',
			'associationForeignKey' => 'fights_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

	public $belongsTo = array(
		'Identity' => array(
			'className' 	=> 'Identity',
			'joinTable'		=> 'Identity',
			'foreignKey' 	=> 'autor'
		)
	);

}
