<?php
App::uses('AppModel', 'Model');
/**
 * Image Model
 *
 * @property ImageTypes $ImageTypes
 * @property Event $Event
 * @property Fight $Fight
 * @property Identity $Identity
 * @property Venue $Venue
 */
class Image extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'image_types_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ImageTypes' => array(
			'className' => 'ImageTypes',
			'foreignKey' => 'image_types_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'EventsImages' => array(
			'className' 	=> 'EventsImages',
			'foreignKey' 	=> 'images_id',
		),
		'FightsImages' => array(
			'className' 	=> 'FightsImages',
			'foreignKey' 	=> 'images_id',
		),
		'IdentitiesImages' => array(
			'className' 	=> 'IdentitiesImages',
			'foreignKey' 	=> 'images_id',
		),
		'VenuesImage' => array(
			'className' 	=> 'VenuesImage',
			'foreignKey' 	=> 'images_id',
		)
	);
        
        public function beforeDelete($cascade = true){
            if($cascade){
                $this->query('DELETE FROM `identities_images` WHERE `images_id` = ' . $this->id);
                $this->query('DELETE FROM `events_images` WHERE `images_id` = ' . $this->id);
                $this->query('DELETE FROM `fights_images` WHERE `images_id` = ' . $this->id);
                $this->query('DELETE FROM `venues_images` WHERE `images_id` = ' . $this->id);
                return true;
            } else {
                return false;
            }
        }
}
