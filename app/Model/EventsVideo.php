<?php
App::uses('AppModel', 'Model');
/**
 * EventsVideo Model
 *
 * @property Events $Events
 * @property Videos $Videos
 */
class EventsVideo extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'events_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'videos_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Events' => array(
			'className' => 'Events',
			'foreignKey' => 'events_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Videos' => array(
			'className' => 'Videos',
			'foreignKey' => 'videos_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
