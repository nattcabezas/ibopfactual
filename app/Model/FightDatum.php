<?php
App::uses('AppModel', 'Model');
/**
 * FightDatum Model
 *
 * @property Fights $Fights
 */
class FightDatum extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fights_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
            'Fights' => array(
                    'className' => 'Fights',
                    'foreignKey' => 'fights_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
            ),
            'Referee' => array(
                'className' => 'Identity',
                'joinTable' => 'Identity',
                'foreignKey'=> 'referee',
                'fields'    => array(
                    'id',
                    'name',
                    'last_name'
                )
            ),
            'ring_announcer' => array(
                'className' => 'Identity',
                'joinTable' => 'Identity',
                'foreignKey'=> 'ring_announcer',
                'fields'    => array(
                    'id',
                    'name',
                    'last_name'
                )
            ),
            'judge_1' => array(
                'className' => 'Identity',
                'joinTable' => 'Identity',
                'foreignKey'=> 'judge_1',
                'fields'    => array(
                    'id',
                    'name',
                    'last_name'
                )
            ),
            'judge_2' => array(
                'className' => 'Identity',
                'joinTable' => 'Identity',
                'foreignKey'=> 'judge_2',
                'fields'    => array(
                    'id',
                    'name',
                    'last_name'
                )
            ),
            'judge_3' => array(
                'className' => 'Identity',
                'joinTable' => 'Identity',
                'foreignKey'=> 'judge_3',
                'fields'    => array(
                    'id',
                    'name',
                    'last_name'
                )
            ),
            'inspector' => array(
                'className' => 'Identity',
                'joinTable' => 'Identity',
                'foreignKey'=> 'inspector',
                'fields'    => array(
                    'id',
                    'name',
                    'last_name'
                )
            ),
            'timekeeper' => array(
                'className' => 'Identity',
                'joinTable' => 'Identity',
                'foreignKey'=> 'timekeeper',
                'fields'    => array(
                    'id',
                    'name',
                    'last_name'
                )
            )
	);
}
