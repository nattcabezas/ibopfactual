<?php
App::uses('AppModel', 'Model');
/**
 * Rule Model
 *
 */
class Sponsor extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'sponsor';
        
        public $belongsTo = array(
		'Identity' => array(
                    'className'     => 'Identity',
                    'joinTable'     => 'Identity',
                    'foreignKey'    => 'identities_id'
		)
	);

}


