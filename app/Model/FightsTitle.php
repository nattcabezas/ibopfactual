<?php
App::uses('AppModel', 'Model');
/**
 * FightsTitle Model
 *
 * @property Fights $Fights
 * @property Titles $Titles
 */
class FightsTitle extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fights_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'titles_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Fights' => array(
			'className' => 'Fights',
			'foreignKey' => 'fights_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Titles' => array(
			'className' => 'Titles',
			'foreignKey' => 'titles_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                'Identity' => array(
                            'className' => 'Identity',
                            'foreignKey' => 'previously_owned',
                )
	);
}
