<?php
App::uses('AppModel', 'Model');
/**
 * SourceOdd Model
 *
 */
class SourceOdd extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
