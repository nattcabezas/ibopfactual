<?php
App::uses('AppModel', 'Model');
/**
 * Weight Model
 *
 */
class Weight extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
