<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
     
    public function afterSave($created, $options = array()) {
        $data = $this->data;
        //if the value its != de Statisctic excutes regular code
        
        if( ! isset($data['Statistic'] ) ){
            if($created){
                $this->saveLog("CREATE");
            }    
            else //this means that it will do an update
            {
                $this->saveLog("UPDATE");
            }
        }
    }
    
    /*
    Continue working on this method
    */
    public function saveLog($modificationType)
    {
        if((isset($_SESSION['User']['User']['id'])) && ($_SESSION['User']['User']['id'] != null))
        {
            $ip = "";
            date_default_timezone_set('America/New_York');
            $time = date("Y-m-d H:i:s");
            $description = "The user: <strong>" . $_SESSION['User']['User']['name'] . "</strong> save ";
                    
            $tableName = "";
                        
            foreach ($this->data as $table => $saveData) {
                $description .= "on <strong>" . addslashes($table). "</strong> the following data:<br>";
                    foreach ($saveData as $campo => $valor) {
                        if (!is_array($valor)) {
                            $description .= "<strong>" . addslashes($campo) . ":</strong> " . addslashes($valor).'<br>';
                        }
                    }
                $tableName = addslashes($table);
            }

            $description .= "<br>";

            if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $description .= "<strong>connected on:</strong>" . $ip . "<br>";
            $description .= $time;
                    
            $sql = 'INSERT INTO logs (users_id, date, description, table_name, modification_type) VALUES ("'.$_SESSION['User']['User']['id'].'", "'.$time.'", "'.$description.'", "'.$tableName.'", "'.$modificationType.'")';
            $this->query($sql);
        }
        return "SUCESS";
    }
    
}
