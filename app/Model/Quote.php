<?php
App::uses('AppModel', 'Model');
/**
 * Quote Model
 *
 * @property Quote $Quote
 */

class Quote extends AppModel{
 /**
 * Display field
 *
 * @var string
 */
	public $displayField = 'quote';


	//The Associations below have been created with all possible keys, those that are not needed can be removed
public $belongsTo = array(
		'Identity' => array(
                    'className'     => 'Identity',
                    'joinTable'     => 'Identity',
                    'foreignKey'    => 'identities_id'
		)
	);

}