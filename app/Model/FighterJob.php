<?php
App::uses('AppModel', 'Model');
/**
 * FighterJob Model
 *
 * @property Fights $Fights
 * @property Identities $Identities
 * @property Jobs $Jobs
 */
class FighterJob extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fights_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'identities_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'jobs_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Fights' => array(
			'className' => 'Fights',
			'foreignKey' => 'fights_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Identities' => array(
			'className' => 'Identities',
			'foreignKey' => 'identities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Jobs' => array(
			'className' => 'Jobs',
			'foreignKey' => 'jobs_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
