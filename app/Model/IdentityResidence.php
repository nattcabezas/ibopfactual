<?php
App::uses('AppModel', 'Model');
/**
 * IdentityResidence Model
 *
 * @property Identities $Identities
 * @property Countries $Countries
 * @property States $States
 * @property Cities $Cities
 */
class IdentityResidence extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'identities_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Identities' => array(
			'className' => 'Identities',
			'foreignKey' => 'identities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Countries' => array(
			'className' => 'Countries',
			'foreignKey' => 'countries_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'States' => array(
			'className' => 'States',
			'foreignKey' => 'states_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cities' => array(
			'className' => 'Cities',
			'foreignKey' => 'cities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
