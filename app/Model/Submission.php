<?php
App::uses('AppModel', 'Model');
/**
 * Title Model
 *
 */
class Submission extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
