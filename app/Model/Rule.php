<?php
App::uses('AppModel', 'Model');
/**
 * Rule Model
 *
 */
class Rule extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

}
