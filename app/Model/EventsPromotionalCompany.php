<?php
App::uses('AppModel', 'Model');
/**
 * EventsPromotionalCompany Model
 *
 * @property PromotionalCompanies $PromotionalCompanies
 * @property Events $Events
 */
class EventsPromotionalCompany extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'promotional_companies_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'events_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PromotionalCompanies' => array(
			'className' => 'PromotionalCompanies',
			'foreignKey' => 'promotional_companies_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Events' => array(
			'className' => 'Events',
			'foreignKey' => 'events_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
