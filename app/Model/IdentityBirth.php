<?php
App::uses('AppModel', 'Model');
/**
 * IdentityBirth Model
 *
 * @property Identities $Identities
 * @property Countries $Countries
 * @property States $States
 * @property Cities $Cities
 */
class IdentityBirth extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'identities_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
        
        public $belongsTo = array(
		'Identities' => array(
			'className' => 'Identities',
			'foreignKey' => 'identities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
}
