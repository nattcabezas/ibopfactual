<?php
App::uses('AppModel', 'Model');
/**
 * Identity Model
 *
 * @property Image $Image
 * @property Video $Video
 */
class Identity extends AppModel {


	public $virtualFields = array(
            'fullname' => 'CONCAT(Identity.name, " ", Identity.last_name)'
        );
        
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'IdentitiesImage' => array(
			'className' 	=> 'IdentitiesImage',
			'foreignKey' 	=> 'identities_id',
                        'order' => array(
                            'IdentitiesImage.principal' => 'DESC'
                        )
		),
		'IdentitiesVideo' => array(
			'className' 	=> 'IdentitiesVideo',
			'foreignKey'	=> 'identities_id'
		),
		'IdentityAlias' => array(
			'className' 	=> 'IdentityAlias',
			'foreignKey' 	=> 'identities_id'
		),
		'IdentityContact' => array(
			'className' 	=> 'IdentityContact',
			'foreignKey' 	=> 'identities_id'
		),
		'IdentityId' => array(
			'className' 	=> 'IdentityId',
			'foreignKey' 	=> 'identities_id',
			'order'			=> 'IdentityId.is_principal DESC'
		),
		'IdentityNickname' => array(
			'className' 	=> 'IdentityNickname',
			'foreignKey' 	=> 'identities_id'
		),
		'IdentityNote' => array(
			'className' 	=> 'IdentityNote',
			'foreignKey' 	=> 'identities_id'
		),
	);

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'IdentityBiography' => array(
			'className' 	=> 'IdentityBiography',
			'foreignKey'	=> 'identities_id'
		),
		'IdentityBirth' => array(
			'className' 	=> 'IdentityBirth',
			'foreignKey'	=> 'identities_id',
		),
		'IdentityDeath' => array(
			'className' 	=> 'IdentityDeath',
			'foreignKey'	=> 'identities_id'
		),
		'IdentityResidence' => array(
			'className' 	=> 'IdentityResidence',
			'foreignKey'	=> 'identities_id'
		),
	);

}
