<?php
App::uses('AppModel', 'Model');
/**
 * Video Model
 *
 * @property Event $Event
 * @property Fight $Fight
 * @property Identity $Identity
 * @property Venue $Venue
 */
class Video extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Event' => array(
			'className' => 'Event',
			'joinTable' => 'events_videos',
			'foreignKey' => 'videos_id',
			'associationForeignKey' => 'events_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Fight' => array(
			'className' => 'Fight',
			'joinTable' => 'fights_videos',
			'foreignKey' => 'videos_id',
			'associationForeignKey' => 'fights_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Identity' => array(
			'className' => 'Identity',
			'joinTable' => 'identities_videos',
			'foreignKey' => 'videos_id',
			'associationForeignKey' => 'identities_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Venue' => array(
			'className' => 'Venue',
			'joinTable' => 'venues_videos',
			'foreignKey' => 'videos_id',
			'associationForeignKey' => 'venues_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
