<?php

App::uses('AppModel', 'Model');
/**
 * page Model
 *
 * @property Title $Tittle
 */

class Page extends AppModel{
  /**
 * Display field
 *
 * @var string
 */
    
   public $displayField = 'title';
   
    
}

