<?php
App::uses('AppModel', 'Model');
/**
 * IdentityDeath Model
 *
 * @property Identities $Identities
 * @property Countries $Countries
 * @property States $States
 * @property Cities $Cities
 */
class IdentityDeath extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Identities' => array(
			'className' => 'Identities',
			'foreignKey' => 'identities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Countries' => array(
			'className' => 'Countries',
			'foreignKey' => 'countries_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'States' => array(
			'className' => 'States',
			'foreignKey' => 'states_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cities' => array(
			'className' => 'Cities',
			'foreignKey' => 'cities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
