<?php
App::uses('AppModel', 'Model');
/**
 * Venue Model
 *
 * @property Sources $Sources
 * @property Locations $Locations
 * @property Image $Image
 * @property Video $Video
 */
class Venue extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'sources_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Sources' => array(
                    'className' => 'Sources',
                    'foreignKey' => 'sources_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
		),
		'Locations' => array(
                    'className' => 'Locations',
                    'foreignKey' => 'locations_id',
		)
	);
        
/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Image' => array(
			'className' => 'Image',
			'joinTable' => 'venues_images',
			'foreignKey' => 'venues_id',
			'associationForeignKey' => 'images_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Video' => array(
			'className' => 'Video',
			'joinTable' => 'venues_videos',
			'foreignKey' => 'venues_id',
			'associationForeignKey' => 'videos_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
