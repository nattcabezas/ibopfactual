<?php
App::uses('AppModel', 'Model');
/**
 * Banner Model
 *
 * @property Banner $Banner
 */
class Banner extends AppModel{
    /**
 * Display field
 *
 * @var string
 */
    public $displayField = 'banner';
}
