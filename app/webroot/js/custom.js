// When the DOM is ready, run this function
$(document).ready(function() {
    //Set the carousel options
    $('.menu-display').click(function(event) {
        event.preventDefault();
        var target = $(this).attr('display-target');
        
        $('.mainmenu-item').each(function() {
            $(this).removeClass('active-menu');
        });
        
        $('.mainmenu-item-mobile').each(function() {
            $(this).removeClass('active-menu');
        });

        $('.search-div').each(function() {
            if ($(this).is(':visible')) {
                $(this).slideUp();
            }
        });

        if ($(target).is(':visible')) {
            $(target).slideUp();
            $(this).parent().removeClass('active-menu');
        } else {
            $(target).slideDown();
            $(this).parent().addClass('active-menu');
        }
        return false;
    });

});

/*for lightbox*/
$(document).ready(function() {
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none",
        closeBtn: false,
        helpers: {
            title: {
                type: 'inside'
            }
        },
        afterLoad: function() {
            this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
        }
    });
});


/* end lightbox */

/*start figths column filters*/

$(document).ready(function() {
    $('.filterable .btn-filter').click(function() {
        var $panel = $(this).parents('.filterable'),
            $filters = $panel.find('.filters input'),
            $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e) {
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
            inputContent = $input.val().toLowerCase(),
            $panel = $input.parents('.filterable'),
            column = $panel.find('.filters th').index($input.parents('th')),
            $table = $panel.find('.table'),
            $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function() {
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' + $table.find('.filters th').length + '">No result found</td></tr>'));
        }
    });
});

/* end column filters*/

/*start fighters detail*/
$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
/*end fighters details*/





// For side menu on devices

$(document).ready(function() {
   
 
 if ($("#doni-side-menu").is(':visible')) {
                $("#doni-side-menu").hide();
            }
       
 
    $("#burger").click(function(){
        if ($("#doni-side-menu").is(':visible')) {
            $("#doni-side-menu").slideUp();
        } else {
            $("#doni-side-menu").slideDown();     
        }
    });
    
    
});
