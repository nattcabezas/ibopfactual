$(document).ready(function(){

    //for carousel
   $('#myCarousel').carousel({
        interval: 7000
    });

    var clickEvent = false;
    $('#myCarousel').on('click', '.carousel-nav .nav a', function() {
        clickEvent = true;
        $('.nav li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function(e) {
        if (!clickEvent) {
            var count = $('.carousel-nav .nav').children().length - 1;
            var current = $('.carousel-nav .nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if (count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
    
    $('.feed-conted img').addClass('img-responsive img-thumbnail');
    $('.feed-conted a').attr('target', '_balck');
    
    //Set the carousel options
    $('#quote-carousel').carousel({
        pause: true,
        interval: 8000
    });
    
});

$(window).load(function() {
    var boxheight = $('#myCarousel .carousel-inner').innerHeight();
    var itemlength = $('#myCarousel .item').length;
    var triggerheight = Math.round(boxheight/itemlength+1);
    $('#myCarousel .list-group-item').outerHeight(triggerheight);
});