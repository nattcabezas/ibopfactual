<?php
App::uses('AppController', 'Controller');
/**
 * Videos Controller
 *
 * @property Video $Video
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class VideosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
        public $helper     = array('newJsonEncode');
        public $uses       = array('Video', 'ImageType', 'IdentitiesVideo', 'FightsVideo', 'EventsVideo', 'VenuesVideo', 'Identity','Fight','Event', 'Venue');


        /**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Video->recursive = 0;                               
                $this->Paginator->settings = array('order' => array('Video.id DESC'),'order' => 'Video.id DESC');
                $this->set('videos', $this->Paginator->paginate());
	}

        public function ibopadmin_comptonVideos()
        {            
            $videosCompton = $this->Video->find(
                'all', array(
                    'conditions' => array(
                        'or' => array(
                            'Video.source_1 LIKE' => '%' . 'Stephen Compton' . '%'
                        )
                    ),
                    'recursive' => -1
                )
            );
            $totalVideoCount = count($videosCompton);
            
            $this->Paginator->settings = array(
                'conditions' => array('Video.source_1 LIKE' => '%' . 'Stephen Compton' . '%'),
                'order' => array('Video.id DESC'),
                'order' => 'Video.id DESC',
                'limit' => 20
            );
            $this->set('videosCompton', $this->Paginator->paginate());
            $this->set('totalVideoCount', $totalVideoCount); 
        }
        
        public function ibopadmin_ibopownedVideos()
        {
            $videosIBOPOwned = $this->Video->find(
                'all', array(
                    'conditions' => array(
                        'or' => array(
                            'Video.ibop_owned' => 1
                        )
                    ),
                    'recursive' => -1
                )
            );
            $totalVideoCount = count($videosIBOPOwned);
            $this->Paginator->settings = array(
                'conditions' => array('Video.ibop_owned' => 1),
                'order' => array('Video.id DESC'),
                'order' => 'Video.id DESC',
                'limit' => 20
            );
            $this->set('videosIBOPOwned', $this->Paginator->paginate());            
            $this->set('totalVideoCount', $totalVideoCount);
        }
        
/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
                    
                    if( $this->request->data('Video.img_file.name') != ""){
                            $hostimages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR;                                                        
                            $upload = move_uploaded_file($this->request->data('Video.img_file.tmp_name'), $hostimages . $this->request->data('Video.img_file.name'));
                            if(!$upload){
                                $this->Session->setFlash(__('The Video image could not be saved. error upload the image', true), 'alert-danger');
                                return $this->redirect(array('action' => 'add'));
                            }
                        }
			$this->Video->create();
			if ($this->Video->save($this->request->data)) {
				$this->Session->setFlash(__('The video has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The video could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
                $this->set('types', $this->ImageType->find('list'));
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->Video->exists($id)) {
			$this->Session->setFlash(__('Invalid video', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
                        if( $this->request->data('Video.img_file.name') != ""){
                            $hostimages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR;                                                        
                            $upload = move_uploaded_file($this->request->data('Video.img_file.tmp_name'), $hostimages . $this->request->data('Video.img_file.name'));
                            if(!$upload){
                                $this->Session->setFlash(__('The Video image could not be saved. error upload the image', true), 'alert-danger');
                                return $this->redirect(array('action' => 'add'));
                            }
                        }
			if ($this->Video->save($this->request->data)) {
				$this->Session->setFlash(__('The video has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The video could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('Video.' . $this->Video->primaryKey => $id), 'recursive' => 1);
			//$this->request->data = $this->Video->find('first', $options);                       
                        $videoData = $this->Video->find('first',$options);
                        $this->request->data = $videoData;
                        $this->set('video',$videoData);                                                                 
		}

                $videoLanguage = $this->Video->find(
                        'all', array(
                            'conditions' => array(
                                'Video.id' => $id
                            ),
                            'fields' => array(
                                'Video.language'
                            )
                        )
                );
                $this->set('language',$videoLanguage);
                $this->set('types', $this->ImageType->find('list'));
	}

/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($id = null) {
		$id = base64_decode($id);
		$this->Video->id = $id;
		if (!$this->Video->exists()) {
			$this->Session->setFlash(__('Invalid video', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->Video->delete()) {
			$this->Session->setFlash(__('The video has been deleted.'), 'alert-success');
		} else {
			$this->Session->setFlash(__('The video could not be deleted. Please, try again.'), 'alert-danger');
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * ibopadmin_searchVideos method
 *
 * @return void
 */
        public function ibopadmin_searchVideos(){
            if($this->request->is('post')){
                
                $videos = $this->Video->find(
                    'all', array(
                        'conditions' => array(
                            'or' => array(
                                'Video.title LIKE'         => '%' . $this->request->data('searchVideos.keywork') . '%',
                                'Video.description LIKE'   => '%' . $this->request->data('searchVideos.keywork') . '%'
                            )
                        ),
                        'recursive' => -1
                    )
                );
                $this->set('videos', $videos);
            }
        }
        
         /**
	 * ibopadmin_saveIdentity method
	 *
	 * @param string $idVideo
         * @param string $idIdentity
	 * @return void
	 */
        public function ibopadmin_saveIdentity($idVideo = null, $idIdentity = null){
            $this->layout = 'ajax';
            if (($idVideo != null) || ($idIdentity != null)){
                $identitiesVideo['IdentitiesVideo']['videos_id'] = $idVideo;
                $identitiesVideo['IdentitiesVideo']['identities_id'] = $idIdentity;
                if($this->IdentitiesVideo->save($identitiesVideo)){
                    $identityName = $this->Identity->find('first', array(
                        'conditions' => array(
                            'Identity.id' => $idIdentity
                        ),
                        'fields' => array(
                            'Identity.name',
                            'Identity.last_name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->IdentitiesVideo->getLastInsertId(), 'identity' => $identityName, 'idIdentity' => $idIdentity));
                } else {
                    $this->set('data', 'error');
                }
            }
        }
        
        
        /**
	 * ibopadmin_assign method
	 *
	 * @param string $id
	 * @return void
	 */
        public function ibopadmin_assign($id = null){
            if($id != null){
                $id = base64_decode($id);
                $video = $this->Video->find('first', array(
                    'conditions' => array(
                        'Video.id' => $id
                    ),
                    'recursive' => -1
                ));
                
                $identitiesVideo = $this->IdentitiesVideo->find('all', array(
                    'conditions' => array(
                        'IdentitiesVideo.videos_id' => $id
                    )
                ));
                
                $fightsVideo = $this->FightsVideo->find('all', array(
                    'conditions' => array(
                        'FightsVideo.videos_id' => $id
                    )
                ));
                
                $eventsVideo = $this->EventsVideo->find('all', array(
                    'conditions' => array(
                        'EventsVideo.videos_id' => $id
                    )
                ));
                
                $venuesVideo = $this->VenuesVideo->find('all', array(
                    'conditions' => array(
                        'VenuesVideo.videos_id' => $id
                    )
                ));
                
                $this->set('idVideo', $id);
                $this->request->data = $video;
                $this->set('video', $video);              
                $this->set('identitiesVideo', $identitiesVideo);
                $this->set('fightsVideo', $fightsVideo);
                $this->set('eventsVideo', $eventsVideo);
                $this->set('venuesVideo', $venuesVideo);
            }
        }
        
        
        /**
	 * ibopadmin_saveIdentity method
	 *
	 * @param string $id
         * @param string $idImage
	 * @return void
	*/
        public function ibopadmin_deleteIdentity($id = null, $idVideo = null){
            if($id != null){
                if($this->IdentitiesVideo->delete($id)){
                    $this->Session->setFlash(__('The Image relationship has been deleted', true), 'alert-success');
                    return $this->redirect(array('action' => 'assign/' .  base64_encode($idVideo)));    
                } else {
                    $this->Session->setFlash(__('The Image relationship could not be deleted. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'assign/' .  base64_encode($idVideo))); 
                }
            }
        }
        
        /**
	 * ibopadmin_saveFight method
	 *
	 * @param string $idImage
         * @param string $idFight
	 * @return void
	 */
        public function ibopadmin_saveFight($idVideo = null, $idFight = null){
            $this->layout = 'ajax';
            if (($idVideo != null) || ($idFight != null)){
                
                $fightsVideo['FightsVideo']['fights_id'] = $idFight;
                $fightsVideo['FightsVideo']['videos_id'] = $idVideo;
                
                if($this->FightsVideo->save($fightsVideo)){
                    
                    $fight = $this->Fight->find('first', array(
                        'conditions' => array(
                            'Fight.id' => $idFight 
                        ),
                        'fields' => array(
                            'Fight.id',
                            'Fight.title'
                        ),
                        'recursive' => -1
                    ));
                    
                    $this->set('data', array('id' => $this->FightsVideo->getLastInsertId(), 'Fight' => $fight));
                } else {
                    $this->set('data', 'error');
                }
            }
        }
        
        /**
	 * ibopadmin_deleteFight method
	 *
	 * @param string $idFightVideo
	 */
        public function ibopadmin_deleteFight($idFightVideo = null){
            if($idFightVideo != null){
                if($this->FightsVideo->delete($idFightVideo)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        /**
	 * ibopadmin_saveEvent method
	 *
	 * @param string $idVideo
         * @param string $idEvent
	 * @return void
	*/
        public function ibopadmin_saveEvent($idVideo = null, $idEvent = null){
            $this->layout = 'ajax';
            if(($idVideo != null) || ($idEvent != null)){
                $eventsVideo['EventsVideo']['events_id'] = $idEvent;
                $eventsVideo['EventsVideo']['videos_id'] = $idVideo;
                if($this->EventsVideo->save($eventsVideo)){
                    $event = $this->Event->find('first', array(
                        'conditions' => array(
                            'Event.id' => $idEvent
                        ),
                        'fields' => array(
                            'Event.id',
                            'Event.name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->EventsVideo->getLastInsertId(), 'Event' => $event));
                }
            }
        }
        
        /**
	 * ibopadmin_deleteEvent method
	 *
	 * @param string $idEventVideo
	 */
        public function ibopadmin_deleteEvent($idEventVideo = null){
            if($idEventVideo != null){
                if($this->EventsVideo->delete($idEventVideo)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        /**
	* ibopadmin_deleteVenue method
	*
	* @param string $idVenueVideo
	*/
        public function ibopadmin_deleteVenue($idVenueVideo = null){
            if($idVenueVideo != null){
                if($this->VenuesVideo->delete($idVenueVideo)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
         /**
	 * ibopadmin_saveVenues method
	 *
	 * @param string $idVideo
         * @param string $idVenue
	 * @return void
	 */
        public function ibopadmin_saveVenues($idVideo = null, $idVenue = null){
            $this->layout = 'ajax';
            if(($idVideo != null) || ($idVenue != null)){
                $venuesVideo['VenuesVideo']['venues_id'] = $idVenue;
                $venuesVideo['VenuesVideo']['videos_id'] = $idVideo;
                if($this->VenuesVideo->save($venuesVideo)){
                    $venues = $this->Venue->find('first', array(
                        'conditions' => array(
                            'Venue.id' => $idVenue
                        ),
                        'fields' => array(
                            'Venue.id',
                            'Venue.name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->VenuesVideo->getLastInsertId(), 'Venue' => $venues));
                }
            }
        }
       

        
        
        
}
