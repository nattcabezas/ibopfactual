<?php
App::uses('AppController', 'Controller');
/**
 * Notes Controller
 *
 * @property Note $Note
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class NotesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components 	= array('Paginator', 'Session');
	public $uses 		= array('Note', 'Identity', 'IdentityNote', 'FightsNote', 'Fight', 'EventsNote', 'VenuesNote', 'Event', 'Venue');
	public $helper		= array('newJsonEncode');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Note->recursive = -1;
                $this->Paginator->settings = array('order' => array('Note.id DESC'),'order' => 'Note.id DESC');
		$this->set('notes', $this->Paginator->paginate());
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->Note->create();
			if ($this->Note->save($this->request->data)) {
				$this->Session->setFlash(__('The note has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The note could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
		
	}

    public function showIndividualNote()
    {
        $this->set('note', $this->request->data);
    }
        
/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
                $id = base64_decode($id);
		if (!$this->Note->exists($id)) {
			$this->Session->setFlash(__('Invalid note', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Note->save($this->request->data)) {
				$this->Session->setFlash(__('The note has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The note could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options 	= array('conditions' => array('Note.' . $this->Note->primaryKey => $id));
			$noteData	= $this->Note->find('first', $options);
			$this->request->data = $noteData;
			$this->set('note', $noteData);
		}
	}
        
 /**
 * ibopadmin_assign method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_assign($id = null) {
            if($id != null){
                $id = base64_decode($id);
                $note = $this->Note->find('first', array(
                    'conditions' => array(
                        'Note.id' => $id
                    ),
                    'recursive' => -1
                ));
                
                $identitiesNote = $this->IdentityNote->find('all', array(
                    'conditions' => array(
                        'IdentityNote.notes_id' => $id
                    )
                ));
                
                
                $fightsNote = $this->FightsNote->find('all', array(
                    'conditions' => array(
                        'FightsNote.notes_id' => $id
                    )
                ));
                
                $eventsNote = $this->EventsNote->find('all', array(
                    'conditions' => array(
                        'EventsNote.notes_id' => $id
                    )
                ));
                
                $venuesNote  = $this->VenuesNote->find('all', array(
                    'conditions' => array(
                        'VenuesNote.notes_id' => $id
                    )
                ));
                
                $this->set('idNote', $id);
                $this->set('note', $note);
                $this->set('identitiesNote', $identitiesNote);
                $this->set('fightsNote', $fightsNote);
                $this->set('eventsNote', $eventsNote);
                $this->set('venuesNote', $venuesNote);
                
            }
        }
        
          /**
	 * ibopadmin_saveIdentity method
	 *
	 * @param string $idNote
         * @param string $idIdentity
	 * @return void
	 */
        public function ibopadmin_saveIdentity($idNote = null, $idIdentity = null){
            $this->layout = 'ajax';
            if (($idNote != null) || ($idIdentity != null)){
                $identitiesNote['IdentityNote']['notes_id'] = $idNote;
                $identitiesNote['IdentityNote']['identities_id'] = $idIdentity;
                if($this->IdentityNote->save($identitiesNote)){
                    $identityName = $this->Identity->find('first', array(
                        'conditions' => array(
                            'Identity.id' => $idIdentity
                        ),
                        'fields' => array(
                            'Identity.name',
                            'Identity.last_name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->IdentityNote->getLastInsertId(), 'identity' => $identityName, 'idIdentity' => $idIdentity));
                } else {
                    $this->set('data', 'error');
                }
            }
        }
        
        /**
	* ibopadmin_deleteIdentity method
	*
	* @param string $id
        * @param string $idImage
	* @return void
	*/
        public function ibopadmin_deleteIdentity($id = null, $idNote = null){
            if($id != null){
                if($this->IdentityNote->delete($id)){
                    $this->Session->setFlash(__('The note relationship has been deleted', true), 'alert-success');
                    return $this->redirect(array('action' => 'assign/' .  base64_encode($idNote)));
                } else {
                    $this->Session->setFlash(__('The note relationship could not be deleted. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'assign/' .  base64_encode($idNote))); 
                }
            }
        }
        
         /**
	 * ibopadmin_saveFight method
	 *
	 * @param string $idImage
         * @param string $idFight
	 * @return void
	 */
        public function ibopadmin_saveFight($idNote = null, $idFight = null){
            $this->layout = 'ajax';
            if (($idNote != null) || ($idFight != null)){
                
                $fightsNotes['FightsNote']['fights_id'] = $idFight;
                $fightsNotes['FightsNote']['notes_id'] = $idNote;
                
                if($this->FightsNote->save($fightsNotes)){
                    
                    $fight = $this->Fight->find('first', array(
                        'conditions' => array(
                            'Fight.id' => $idFight 
                        ),
                        'fields' => array(
                            'Fight.id',
                            'Fight.title'
                        ),
                        'recursive' => -1
                    ));
                    
                    $this->set('data', array('id' => $this->FightsNote->getLastInsertId(), 'Fight' => $fight));
                } else {
                    $this->set('data', 'error');
                }
            }
        }
        
        /**
	* ibopadmin_deleteFight method
	*
	* @param string $idFightImage
	*/
        public function ibopadmin_deleteFight($idFightNote = null){
            if($idFightNote != null){
                if($this->FightsNote->delete($idFightNote)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        

/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($id = null) {
		$id = base64_decode($id);
		$this->Note->id = $id;
		if (!$this->Note->exists()) {
			$this->Session->setFlash(__('Invalid note', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->Note->delete($id, true)) {
			$this->Session->setFlash(__('The note has been deleted.', true), 'alert-success');
		} else {
			$this->Session->setFlash(__('The note could not be deleted. Please, try again.', true), 'alert-danger');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
	 * funcion que obtiene los identities y los devuelve en json
	 * @param  string $name 
	 * @return void       
	 */
	public function ibopadmin_getIdentity($name = null){
		$this->layout = 'ajax';
		$identities = $this->Identity->find(
			'all', array(
				'conditions' => array(
					'OR' => array(
						'Identity.name LIKE' 		=> '%' . $name  . '%',
						'Identity.last_name LIKE' 	=> '%' . $name  . '%' 
					)
				),
				'fields' => array(
					'Identity.id',
					'Identity.name',
					'Identity.last_name'
				),
				'recursive' => 0
			)
		);
		foreach ($identities as $identity) {
			$newIdentiy['id'] 		= $identity['Identity']['id'];
			$newIdentiy['label']	= $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'];
			$newIdentities[]		= $newIdentiy; 
		}
		$this->set('identities', $newIdentities);
	}
        
/**
 * ibopadmin_searchNotes method
 *
 * @return void
 */
        public function ibopadmin_searchNotes(){
            if($this->request->is('post')){
                $notes = $this->Note->find(
                    'all', array(
                        'conditions' => array(
                            'or' => array(
                                'Note.title LIKE' => '%' . $this->request->data('searchNotes.keywork') . '%',
                                'Note.note LIKE' => '%' . $this->request->data('searchNotes.keywork') . '%'
                            )
                        )
                    )
                );
                $this->set('notes', $notes);
            }
        }
        
        
        /**
	 * ibopadmin_saveEvent method
	 *
	 * @param string $idNote
         * @param string $idEvent
	 * @return void
	*/
        public function ibopadmin_saveEvent($idNote = null, $idEvent = null){
            $this->layout = 'ajax';
            if(($idNote != null) || ($idEvent != null)){
                $eventsNote['EventsNote']['events_id'] = $idEvent;
                $eventsNote['EventsNote']['notes_id'] = $idNote;
                if($this->EventsNote->save($eventsNote)){
                    $event = $this->Event->find('first', array(
                        'conditions' => array(
                            'Event.id' => $idEvent
                        ),
                        'fields' => array(
                            'Event.id',
                            'Event.name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->EventsNote->getLastInsertId(), 'Event' => $event));
                }
            }
        }
        
        /**
	 * ibopadmin_deleteEvent method
	 *
	 * @param string $idEventNote
	 */
        public function ibopadmin_deleteEvent($idEventNote = null){
            if($idEventNote != null){
                if($this->EventsNote->delete($idEventNote)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        
        /**
	 * ibopadmin_saveVenues method
	 *
	 * @param string $idNote
         * @param string $idVenue
	 * @return void
	 */
        public function ibopadmin_saveVenue($idNote = null, $idVenue = null){
            $this->layout = 'ajax';
            if(($idNote != null) || ($idVenue != null)){
                $venuesNote['VenuesNote']['venues_id'] = $idVenue;
                $venuesNote['VenuesNote']['notes_id'] = $idNote;
                if($this->VenuesNote->save($venuesNote)){
                    $venues = $this->Venue->find('first', array(
                        'conditions' => array(
                            'Venue.id' => $idVenue
                        ),
                        'fields' => array(
                            'Venue.id',
                            'Venue.name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->VenuesNote->getLastInsertId(), 'Venue' => $venues));
                }
            }
        }
        
        /**
	* ibopadmin_deleteVenue method
	*
	* @param string $idVenueNote
	*/
        public function ibopadmin_deleteVenue($idVenueNote = null){
            if($idVenueNote != null){
                if($this->VenuesNote->delete($idVenueNote)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
}
