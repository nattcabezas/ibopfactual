<?php
App::uses('AppController', 'Controller');
/**
 * Quotes Controller
 *
 * @property Quote $Quote
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */

class QuotesController extends AppController{
    
    /**
 * Components
 *
 * @var array
 */
	public $components 	= array('Paginator', 'Session');
	public $uses 		= array('Quote', 'Identity');
	public $helper		= array('newJsonEncode');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Quote->recursive = 0;
                $this->Paginator->settings = array(
                    'order' => array(
                        'Quote.id' => 'DESC'
                    )
                );
		$this->set('quotes', $this->Paginator->paginate());
	}
        /**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
            
            if ($this->request->is('post')) {
                     
                if( $this->request->data('Quote.img_file.name') != "" ){
                    $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'quotes' . DIRECTORY_SEPARATOR;                            
                    $upload = move_uploaded_file($this->request->data('Quote.img_file.tmp_name'), $hostImages . $this->request->data('Quote.img_file.name'));
                   
                    if(!$upload){
                        $this->Session->setFlash(__('The Quote Image could not be saved. error upload the image', true), 'alert-danger');
                        return $this->redirect(array('action' => 'add'));
                    }
                }
		$this->Quote->create();
		if ($this->Quote->save($this->request->data)) {
                    $this->Session->setFlash(__('The quote has been saved.', true), 'alert-success');
                    return $this->redirect(array('action' => 'index'));
		} else {
                    $this->Session->setFlash(__('The note could not be saved. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'index'));
		}
            }                
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->Quote->exists($id)) {
			$this->Session->setFlash(__('Invalid quote', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}       
                
		if ($this->request->is(array('quote', 'put'))) {
                        if( $this->request->data('Quote.img_file.name') != "" ){                             
                            $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'quotes' . DIRECTORY_SEPARATOR;
                            $upload = move_uploaded_file($this->request->data('Quote.img_file.tmp_name'), $hostImages . $this->request->data('Quote.img_file.name'));
                       
                            if(!$upload){
                                $this->Session->setFlash(__('The Quote Image could not be saved. error upload the image', true), 'alert-danger');
                                return $this->redirect(array('action' => 'add'));
                            }
                        }
			if ($this->Quote->save($this->request->data)) {                            
				$this->Session->setFlash(__('The quote has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The quote could not be saved. Please, try again.', true), 'alert-danger');
                                return $this->redirect(array('action' => 'index'));
			}
		} else {
			$options 	= array('conditions' => array('Quote.' . $this->Quote->primaryKey => $id));
			$quoteData	= $this->Quote->find('first', $options);
			$this->request->data = $quoteData;
			$this->set('quote', $quoteData);
                        
                       
		}
	}

        public function ibopadmin_delete($id = null) {
		$id = base64_decode($id);
		$this->Quote->id = $id;
		if (!$this->Quote->exists()) {
			$this->Session->setFlash(__('Invalid quote', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->Quote->delete($id, true)) {
			$this->Session->setFlash(__('The quote has been deleted.', true), 'alert-success');
		} else {
			$this->Session->setFlash(__('The quote could not be deleted. Please, try again.', true), 'alert-danger');
		}
		return $this->redirect(array('action' => 'index'));
	}
        public function ibopadmin_getIdentity($name = null){
		$this->layout = 'ajax';
		$identities = $this->Identity->find(
			'all', array(
				'conditions' => array(
					'OR' => array(
						'Identity.name LIKE' 		=> '%' . $name  . '%',
						'Identity.last_name LIKE' 	=> '%' . $name  . '%' 
					)
				),
				'fields' => array(
					'Identity.id',
					'Identity.name',
					'Identity.last_name'
				),
				'recursive' => 0
			)
		);
		foreach ($identities as $identity) {
			$newIdentiy['id'] 		= $identity['Identity']['id'];
			$newIdentiy['label']	= $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'];
			$newIdentities[]		= $newIdentiy; 
		}
		$this->set('identities', $newIdentities);
	}
        public function ibopadmin_searchQuotes(){
            if($this->request->is('post')){
                $quotes = $this->Quote->find(
                    'all', array(
                        'conditions' => array(
                            'or' => array(
                                'Quote.quote LIKE' => '%' . $this->request->data('searchQuotes.keywork') . '%'
                                
                            )
                        )
                    )
                );
                $this->set('quotes', $quotes);
            }
        }
        
        /*
        Widget con los quotes para TSS
        * 
        */
        public function widgetQuotes()
        {
            $this->layout = false;//disable the default layouts
            
            $this->Quote->unbindModel(array(
                   'belongsTo' => array(
                   'Identity'
                )
            ));
            $quotes = $this->Quote->find(
                  'all'
            );
            
            /*
            Escaped quotes in the quote and note text to avoid problems on the widget
             *              */
            $escapedQuoteArray = array();
            foreach ($quotes as $quote)
            {
                $quote['Quote']['quote'] = str_replace('"','\"', $quote['Quote']['quote']);
                $quote['Quote']['note'] = str_replace('"','\"', $quote['Quote']['note']);               
                array_push($escapedQuoteArray,$quote);  
            }
            $this->set('quotes', $escapedQuoteArray);
        }
        
        
}

