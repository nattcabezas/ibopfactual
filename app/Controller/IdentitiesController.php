<?php
App::uses('AppController', 'Controller');
/**
 * Identities Controller
 *
 * @property Identity $Identity
 * @author rudysibaja <neo.nou@gmail.com>
 */
class IdentitiesController extends AppController {

	public $name 	= "Identities";
	
	public $uses 	= array(
            'Identity', 
            'Country', 
            'State', 
            'City', 
            'IdentityBirth', 
            'IdentityResidence', 
            'IdentityDeath', 
            'IdentityNickname', 
            'IdentityBiography',
            'IdentityAlias',
            'IdentityContact',
            'IdentityId',
            'IdentityBiography',
            'Image',
            'IdentitiesImage',
            'Video',
            'IdentitiesVideo',
            'IdsSource',
            'IdentityRelationship',
            'Note',
            'IdentityNote',
            'FightIdentity',
            'Fight',
            'Event',
            'FighterJob',
            'FightDatum',
            'EventsJob',
            'Source',
            'Sponsor'
	);

	public $helper	= array('newJsonEncode', 'utf8EncodeArray','getUrl');
	
	public $paginate = array(
		'limit' => 20,
		'order' => array(
                    'Identity.id' => 'DESC'
		)
	);

	/**
	 * index de la seccion de personas (identities)
	 * @return void 
	 */
	public function ibopadmin_index(){
		$this->Identity->unbindModel(
			array(
				'hasMany' => array(
					'IdentitiesImage',
					'IdentitiesVideo',
					'IdentityAlias',
					'IdentityContact',
					'IdentityNickname',
					'IdentityNote'
                                        
				),
				'hasOne' => array(
					'IdentityBiography',
					'IdentityBirth',
					'IdentityDeath',
					'IdentityResidence'
				)
			)
		);
		$this->set('identities', $this->paginate('Identity'));
	}

	/**
	 * agrega las identities 
	 * @return void
	 */
	public function ibopadmin_add(){
		if ($this->request->is('post')) {
			$this->Identity->create();
			if ($this->Identity->save($this->request->data)) {
				$this->Session->setFlash(__('The Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->Identity->getLastInsertId())));
			} else {
				$this->Session->setFlash(__('The Person could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
                $this->set('source', $this->Source->find('list'));
	}

	/**
	 * edicion de la identidad
	 * @param  intiger $id 
	 * @return void     
	 */
	public function ibopadmin_edit($id = null){
		$id = base64_decode($id);
		if (!$this->Identity->exists($id)) {
			$this->Session->setFlash(__('Invalid Person', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Identity->save($this->request->data)) {
				$this->Session->setFlash(__('The Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($id)));
			} else {
				$this->Session->setFlash(__('The Person could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$idtypes = $this->IdsSource->find('list');
			$newIdTypes = array();
			foreach ($idtypes as $idtype) {
				$newIdTypes[$idtype] = $idtype;
			}
			$options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $id));
                        $this->set('source', $this->Source->find('list'));
			$this->set('identity', $this->Identity->find('first', $options));
			$this->set('idtypes', $newIdTypes);
                        $this->set('idIdentity', $id);
                        $this->set('identityName', $this->ibopadmin_getName($id));
                        $this->set('sponsors', $this->ibopadmin_getSponsor($id));
		}
	}

	/**
	 * obtiene los estados del pais
	 * @param  string $type tipo para devolver los estados
	 * @param intiger default option
	 * @return void       
	 */
	public function ibopadmin_getStates($type = null, $default = 0){
		$this->layout = 'ajax';
		if ($this->request->is('post')) {
			$states = $this->State->find('list', array(
				'conditions' => array(
					'State.countries_id' => $this->request->data('countries_id')
				), 'order'=> array(
                                    'State.name' => 'ASC'
                                )
			));
			$this->set('states', $states);
			$this->set('type', $type);
			$this->set('default', $default);
		}
	}

	/**
	 * obtiene las ciudades del estado
	 * @param  string $type tipo para devolver las ciudades
	 * @param intiger default option
	 * @return void       
	 */
	public function ibopadmin_getCities($type = null, $default = 0){
		$this->layout = 'ajax';
		if ($this->request->is('post')) {
			$cities = $this->City->find('list', array(
				'conditions' => array(
					'City.states_id' => $this->request->data('states_id')
				), 'order'=> array(
                                    'City.name' => 'ASC'
                                )
			));
			$this->set('cities', $cities);
			$this->set('type', $type);
			$this->set('default', $default);
		}
	}

	/**
	 * funcion que guarda los datos de nacimiento
	 * @return void 
	 */
	public function ibopadmin_saveBirth(){
		if ($this->request->is('post')) {
			if ($this->IdentityBirth->save($this->request->data)) {
				$this->Session->setFlash(__('The Birth Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityBirth.identities_id'))));
			} else {
				$this->Session->setFlash(__('The Birth Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityBirth.identities_id'))));
			}
		}
	}

	/**
	 * funcion que guarda el lugar de residencia IdentityResidence
	 * @return void
	 */
	public function ibopadmin_saveRecidence(){
		if($this->request->is('post')){
			if ($this->IdentityResidence->save($this->request->data)) {
				$this->Session->setFlash(__('The Residence Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityResidence.identities_id'))));
			} else {
				$this->Session->setFlash(__('The Residence Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityResidence.identities_id'))));
			}
		}
	}

	/**
	 * funcion que guarda la fecha de muerte
	 * @return void
	 */
	public function ibopadmin_saveDeath(){
		if($this->request->is('post')){
			if ($this->IdentityDeath->save($this->request->data)) {
				$this->Session->setFlash(__('The Death Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityDeath.identities_id'))));
			} else {
				$this->Session->setFlash(__('The Death Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityDeath.identities_id'))));
			}
		}
	}

	/**
	 * function que guarda los Nicknames
	 * @return void
	 */
	public function ibopadmin_saveNicknames(){
		if($this->request->is('post')){
			if ($this->IdentityNickname->save($this->request->data)) {
				$this->Session->setFlash(__('The Nickname Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityNickname.identities_id'))));
			} else {
				$this->Session->setFlash(__('The Nickname Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityNickname.identities_id'))));
			}
		}
	}

        public function ibopadmin_lastUnknownId()
        {
            $this->layout = 'ajax';
            $lastUnknownIDDB= $this->IdentityId->find(
		'all', array(
                    'conditions' => array(
                        'IdentityId.type' => "UNKNOWN"
                    )
                )
            );
            $lastUnknownID = sizeof($lastUnknownIDDB);
                        
            $this->set('lastUnknownID', $lastUnknownID);
        }
        
	/**
	 * funcion que obtiene y devuelve para json el nickname por el id
	 * @param  intiger $id 
	 * @return void     
	 */
	public function ibopadmin_getNickname($id = null){
		$this->layout = 'ajax';
		$nicknameData = $this->IdentityNickname->find(
			'first', array(
				'conditions' => array(
					'IdentityNickname.id' => $id
				),
				'recursive' => -1
			)
		);
		$this->set('nicknameData', Set::extract('/IdentityNickname/.', $nicknameData));
	}

	/**
	 * funcion que elimina el nickname
	 * @param  intiger $id         
	 * @param  intiger $idIdentity 
	 * @return void             
	 */
	public function ibopadmin_deleteNickname($id = null, $idIdentity = null){
		if ($this->IdentityNickname->delete($id)) {
			$this->Session->setFlash(__('The Nickname Person has been delete.', true), 'alert-success');
			return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
		} else {
			$this->Session->setFlash(__('The Nickname Person could not be delete. Please, try again.', true), 'alert-danger');
			return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
		}
	}

	/**
	 * funcion que guarda los alias
	 * @return void 
	 */
	public function ibopadmin_saveAlias(){
		if($this->request->is('post')){
			if ($this->IdentityAlias->save($this->request->data)) {
				$this->Session->setFlash(__('The Alias Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityAlias.identities_id'))));
			} else {
				$this->Session->setFlash(__('The Alias Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityAlias.identities_id'))));
			}
		}
	}

	/**
	 * funcion que obtiene y devuelve pasa json el alias por el id
	 * @param  intiger $id 
	 * @return void 
	 */
	public function ibopadmin_getAlias($id = null){
		$this->layout = 'ajax';
		$aliasData = $this->IdentityAlias->find(
			'first', array(
				'conditions' => array(
					'IdentityAlias.id' => $id
				),
				'recursive' => -1
			)
		);
		$this->set('aliasData', Set::extract('/IdentityAlias/.', $aliasData));
	}

	/**
	 * funcion que elimina el alias
	 * @param  intiger $id         
	 * @param  intiger $idIdentity 
	 * @return void             
	 */
	public function ibopadmin_deleteAlias($id = null, $idIdentity = null){
		if ($this->IdentityAlias->delete($id)) {
			$this->Session->setFlash(__('The Alias Person has been delete.', true), 'alert-success');
			return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
		} else {
			$this->Session->setFlash(__('The Alias Person could not be delete. Please, try again.', true), 'alert-danger');
			return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
		}
	}

	/**
	 * funcion que guarda el contacto
	 * @return void
	 */
	public function ibopadmin_saveContact(){
		if($this->request->is('post')){
			if ($this->IdentityContact->save($this->request->data)) {
				$this->Session->setFlash(__('The contact Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityContact.identities_id'))));
			} else {
				$this->Session->setFlash(__('The contact Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityContact.identities_id'))));
			}
		}
	}
        
	/**
	 * funcion que devuelve en json la informacion de contacto por el ID
	 * @param  intiger $id 
	 * @return void     
	 */
	public function ibopadmin_getContact($id = null){
		$this->layout = 'ajax';
		$contactData = $this->IdentityContact->find(
			'first', array(
				'conditions' => array(
					'IdentityContact.id' => $id
				),
				'recursive' => -1
			)
		);
		$this->set('contactData', Set::extract('/IdentityContact/.', $contactData));
	}

	/**
	 * funcion que borra el contact
	 * @param  intiger $id         
	 * @param  intiger $idIdentity 
	 * @return void             
	 */
	public function ibopadmin_deleteContact($id = null, $idIdentity = null){
		if ($this->IdentityContact->delete($id)) {
			$this->Session->setFlash(__('The Contact Person has been delete.', true), 'alert-success');
			return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
		} else {
			$this->Session->setFlash(__('The Contact Person could not be delete. Please, try again.', true), 'alert-danger');
			return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
		}
	}

	/**
	 * funcion que guarda el ID del identity
	 * @return void
	 */
	public function ibopadmin_saveId(){
		if($this->request->is('post')){
			if ($this->IdentityId->save($this->request->data)) {
				$this->Session->setFlash(__('The ID Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityId.identities_id'))));
			} else {
				$this->Session->setFlash(__('The ID Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityId.identities_id'))));
			}
		}
	}	
        
        public function ibopadmin_saveUnknownId($identitiesId = null, $typeId = null, $comments = null)
        {
            //echo "PUM";
            if($this->request->is('post'))
            { 
                $this->autoRender = false;
                $this->IdentityId->set(array(
                    'identities_id' => $identitiesId,
                    'type' => "UNKNOWN",
                    'type_id'  => $typeId,
                    'comments' => $comments,
                    'is_principal' => 0
                ));
                $this->IdentityId->save();
                $this->Session->setFlash(__('Unknown IDs saved successfully', true), 'alert-success');
                return;
                //return $this->redirect(array('action' => 'edit/' . base64_encode($identitiesId)));
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($identitiesId)));
            //$this->autoRender = false;
         }

	/**
	 * funcion que borra los ID
	 * @param  integer $id         
	 * @param  integer $idIdentity 
	 * @return void             
	 */
	public function ibopadmin_deleteIds($id = null, $idIdentity = null){
		if($this->IdentityId->delete($id)){
			$this->Session->setFlash(__('The ID Person has been delete.', true), 'alert-success');
		} else {
			$this->Session->setFlash(__('The ID Person could not be delete. Please, try again.', true), 'alert-danger');
		}
		return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));

	}

	/**
	 * function que devuelve en json la informacion del ID 
	 * @param  intiger $id 
	 * @return void
	 */
	public function ibopadmin_getIds($id = null){
		$this->layout = 'ajax';
		$idsData = $this->IdentityId->find(
			'first', array(
				'conditions' => array(
					'IdentityId.id' => $id
				),
				'recursive' => -1
			)
		);
		$this->set('idsData', Set::extract('/IdentityId/.', $idsData));
	}

	/**
	 * funcion que guarda la biografia
	 * @return void
	 */
	public function ibopadmin_saveBiography(){
		if($this->request->is('post')){
			if ($this->IdentityBiography->save($this->request->data)) {
				$this->Session->setFlash(__('The Biography Person has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityBiography.identities_id'))));
			} else {
				$this->Session->setFlash(__('The Biography Person could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('IdentityBiography.identities_id'))));
			}
		}
	}

	/**
	 * funcion que carga las imagenes del sistema
	 * @param  integer $idIdentity
	 * @param  integer $page  
	 * @return void             
	 */
	public function ibopadmin_images($idIdentity = null, $page = 1){
		$idIdentity = base64_decode($idIdentity);

		$images = $this->Image->find(
			'all', array(
				'fields' => array(
					'Image.id',
					'Image.url'
				),
				'order' 	=> array(
					'Image.id' => 'DESC'
				),
				'recursive' => -1,
				'limit' 	=> 9,
				'page' 		=> $page
			)
		);

		if($this->request->is('post')){
			$identityImageExist = $this->IdentitiesImage->find(
				'count', array(
					'conditions' => array(
						'IdentitiesImage.identities_id' => $this->request->data('IdentitiesImage.identities_id'),
						'IdentitiesImage.images_id' => $this->request->data('IdentitiesImage.images_id'), 
					)
				)
			);
			
			if( $identityImageExist > 0 ){
				$this->Session->setFlash(__('The image is already added', true), 'alert-danger');
			} else {
				if( $this->IdentitiesImage->save($this->request->data) ){
					$this->Session->setFlash(__('The image has been saved in the event.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}
                $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
                $this->set('identity', $this->Identity->find('first', $options));
		$this->set('images', $images);
		$this->set('idIdentity', $idIdentity);
		$this->set('page', $page);
                $this->set('identityName', $this->ibopadmin_getName($idIdentity));
	}

	/**
	 * funcion que carga las imagenes del identity
	 * @param  integer $idIdentity 
	 * @param  integer $page       
	 * @return void              
	 */
	public function ibopadmin_identityImages($idIdentity = null, $page = 1){
		$idIdentity = base64_decode($idIdentity);

		if($this->request->is('post')){
			if($this->IdentitiesImage->delete($this->request->data('IdentitiesImage.id'))){
                            $this->Session->setFlash(__('The Image has been deleted', true), 'alert-success');
                            return $this->redirect(array('action' => 'identityImages/' .  base64_encode($idIdentity). '/' . $page));    
			} else {
                            $this->Session->setFlash(__('The Image could not be deleted. Please, try again.', true), 'alert-danger');
                            return $this->redirect(array('action' => 'identityImages/' .  base64_encode($idIdentity). '/' . $page)); 
			}
		}

		$images = $this->IdentitiesImage->find(
			'all', array(
				'conditions' => array(
					'IdentitiesImage.identities_id' => $idIdentity
				),
				'fields' => array(
					'IdentitiesImage.id',
                                        'IdentitiesImage.principal',
					'Images.id',
					'Images.url'
				),
				'order' 	=> array(
                                    'IdentitiesImage.principal' => 'DESC',
                                    'IdentitiesImage.id' => 'ASC'
				),
				'limit' => 9,
				'page'	=> $page
			)
		);
                $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
                $this->set('identity', $this->Identity->find('first', $options));
		$this->set('images', $images);
		$this->set('idIdentity', $idIdentity);
		$this->set('page', $page);
                $this->set('identityName', $this->ibopadmin_getName($idIdentity));
	}

	/**
	 * funcion que busca las imagenes del sistama
	 * @param  integer $idIdentity 
	 * @param  string  $keyword    
	 * @param  integer $page       
	 * @return void              
	 */
	public function ibopadmin_searchImages($idIdentity = null, $keyword = null, $page = 1){
		$idIdentity = base64_decode($idIdentity);
		$keyword    = base64_decode($keyword);

		if($this->request->is('post')){
			$identityImageExist = $this->IdentitiesImage->find(
				'count', array(
					'conditions' => array(
                                            'IdentitiesImage.identities_id' => $this->request->data('IdentitiesImage.identities_id'),
                                            'IdentitiesImage.images_id' => $this->request->data('IdentitiesImage.images_id'), 
					)
				)
			);
			
			if( $identityImageExist > 0 ){
				$this->Session->setFlash(__('The image is already added', true), 'alert-danger');
			} else {
				if( $this->IdentitiesImage->save($this->request->data) ){
					$this->Session->setFlash(__('The image has been saved', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}

		if($keyword != null){
			$images = $this->Image->find(
				'all', array(
					'conditions' => array(
                                            'or' => array(
                                                'Image.title LIKE' => '%' . $keyword . '%',
                                                'Image.description LIKE' => '%' . $keyword . '%',
                                                'Image.url LIKE' => '%' . $keyword . '%'
                                            )
					),
					'fields' => array(
						'Image.id',
						'Image.url' 	
					),
					'order' 	=> array(
						'Image.id' => 'DESC'
					),
					'recursive' => -1,
					'limit' 	=> 9,
					'page' 		=> $page
				)
			);
		} else {
			$images = array();
		}
                $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
                $this->set('identity', $this->Identity->find('first', $options));
		$this->set('idIdentity', $idIdentity);
		$this->set('images', $images);
		$this->set('page', $page);
		$this->set('keywordData', $keyword);
                $this->set('identityName', $this->ibopadmin_getName($idIdentity));

	}
        
        /**
	 * ibopadmin_imagePrincipal
	 * @return void
	 */
        public function ibopadmin_imagePrincipal(){
            if($this->request->is('post')){
                if($this->request->data('ImagePrincipal.make_principal') == 1){
                    $countPrincipal = $this->IdentitiesImage->find(
                        'count', array(
                            'conditions' => array(
                                'IdentitiesImage.identities_id' => $this->request->data('ImagePrincipal.idIdentity'),
                                'IdentitiesImage.principal' => 1
                            )
                        )
                    );
                    if($countPrincipal == 0){
                        $IdentitiesImage['IdentitiesImage']['id']           = $this->request->data('ImagePrincipal.id');
                        $IdentitiesImage['IdentitiesImage']['principal']    = 1;
                        if($this->IdentitiesImage->save($IdentitiesImage)){
                            $this->Session->setFlash(__('The image has been selected.', true), 'alert-success');
                        } else {
                            $this->Session->setFlash(__('The Image could not be selected. Please, try again.', true), 'alert-danger');
                        }
                    } else {
                            $this->Session->setFlash(__('The person already has a main image.', true), 'alert-danger');
                    }
                } else {
                    $IdentitiesImage['IdentitiesImage']['id']           = $this->request->data('ImagePrincipal.id');
                    $IdentitiesImage['IdentitiesImage']['principal']    = null;
                    if($this->IdentitiesImage->save($IdentitiesImage)){
                        $this->Session->setFlash(__('The image has been unselected.', true), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Image could not be unselected. Please, try again.', true), 'alert-danger');
                    }
                }
                return $this->redirect(array('action' => 'identityImages/' . base64_encode($this->request->data('ImagePrincipal.idIdentity')) . '/'. $this->request->data('ImagePrincipal.page')));
            } else {
                return $this->redirect(array('action' => 'index'));
            }
            
        }

	/**
	 * funcion que carga los videos
	 * @param  integer $idIdentity
	 * @param  intiger $page  
	 * @return void             
	 */
	public function ibopadmin_videos($idIdentity = null, $page = 1){
		$idIdentity = base64_decode($idIdentity);

		$videos = $this->Video->find(
			'all', array(
				'fields' => array(
					'Video.id',
					'Video.title',
					'Video.mp4'
				),
				'order' => array(
					'Video.id' => 'DESC'
				),
				'recursive' => -1,
				'limit' 	=> 10,
				'page'		=> $page
			)
		);

		if($this->request->is('post')){
			$identitiesVideoExist  = $this->IdentitiesVideo->find(
				'count', array(
					'conditions' => array(
						'IdentitiesVideo.identities_id' => $this->request->data('IdentitiesVideo.identities_id'),
						'IdentitiesVideo.videos_id' 	=> $this->request->data('IdentitiesVideo.videos_id')
					)
				)
			);
			if($identitiesVideoExist > 0){
				$this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
			} else {
				if($this->IdentitiesVideo->save($this->request->data)){
					$this->Session->setFlash(__('The Identities video has been saved in.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The Identities video could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}
                $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
                $this->set('identity', $this->Identity->find('first', $options));
		$this->set('videos', $videos);
		$this->set('page', $page);
		$this->set('idIdentity', $idIdentity);
                $this->set('identityName', $this->ibopadmin_getName($idIdentity));
	}

	/**
	 * funcion que carga los videos de una identidad
	 * @param  integer $idIdentity 
	 * @param  integer $page       
	 * @return void              
	 */
	public function ibopadmin_identityVideos($idIdentity = null, $page = 1, $keyword = null){
		$idIdentity = base64_decode($idIdentity);

		if($this->request->is('post')){
			if($this->IdentitiesVideo->delete($this->request->data('IdentitiesVideo.id'))){
                            $this->Session->setFlash(__('The video has been deleted', true), 'alert-success');
                            return $this->redirect(array('action' => 'identityVideos/'. base64_encode($idIdentity) . '/' . $page));
			} else {
                            $this->Session->setFlash(__('The video could not be deleted. Please, try again.', true), 'alert-danger');
                            return $this->redirect(array('action' => 'identityVideos/'. base64_encode($idIdentity) . '/' . $page));
			}
		}

		$videos = $this->IdentitiesVideo->find(
			'all', array(
				'conditions' => array(
					'IdentitiesVideo.identities_id' => $idIdentity 
				),
				'fields' => array(
					'IdentitiesVideo.id',
                                        'IdentitiesVideo.position',
					'Videos.id',
					'Videos.title',
					'Videos.mp4'
				),
				'order' => array(
					'IdentitiesVideo.position' => 'ASC',
				),
				'limit' 	=> 10,
				'page'		=> $page
			)
		);
                
                $totalVideos = $this->IdentitiesVideo->find(
                    'all', array(
                        'conditions' => array(
                            'and' => array(
                                'IdentitiesVideo.identities_id' => $idIdentity
                            )
                        )
                    )
                );
                                                
                $countVideos = $this->IdentitiesVideo->find('count', array(
                    'conditions' => array('IdentitiesVideo.identities_id' => $idIdentity )
                ));
                
                $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
                $this->set('identity', $this->Identity->find('first', $options));
		$this->set('videos', $videos);
		$this->set('page', $page);
                $this->set('countVideos', $countVideos);
                $this->set('totalVideos', $totalVideos);
		$this->set('idIdentity', $idIdentity);
                $this->set('identityName', $this->ibopadmin_getName($idIdentity));
	}
        
        public function ibopadmin_searchIdentityVideos($idIdentity = null, $keyword = null)
        {
            $idIdentity = base64_decode($idIdentity);
            $totalVideos = $this->IdentitiesVideo->find(
                'all', array(
                    'conditions' => array(
                        'and' => array(
                            'IdentitiesVideo.identities_id' => $idIdentity
                        )
                    )
                )
            );
           
            $videosArray = array();
            foreach ($totalVideos as $theVideos)
            {
                if (strpos($theVideos['Videos']['title'], $keyword)==true)
                {
                    array_push($videosArray, $theVideos);
                }
            }            
            
            $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
            $this->set('keyword', $keyword);
            $this->set('identity', $this->Identity->find('first', $options));
            $this->set('videosArray', $videosArray);
        }
        

	/**
	 * funcion que busca en los videos
	 * @param  integer $idIdentity 
	 * @param  string  $keyword 
	 * @param  integer $page    
	 * @return void           
	 */
	public function ibopadmin_searchVideos($idIdentity = null, $keyword = null, $page = 1){
		$idIdentity 	= base64_decode($idIdentity);
		$keyword 		= base64_decode($keyword);

		if($this->request->is('post')){
			$eventVideoExist  = $this->IdentitiesVideo->find(
				'count', array(
					'conditions' => array(
						'IdentitiesVideo.identities_id' => $this->request->data('IdentitiesVideo.identities_id'),
						'IdentitiesVideo.videos_id' 	=> $this->request->data('IdentitiesVideo.videos_id')
					)
				)
			);
			if($eventVideoExist > 0){
				$this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
			} else {
				if($this->IdentitiesVideo->save($this->request->data)){
					$this->Session->setFlash(__('The video has been saved.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The video could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}
		
		if($keyword != null){
			$videos = $this->Video->find(
				'all', array(
					'conditions' => array(
						'Video.title LIKE' => '%' . $keyword . '%'
					),
					'fields' => array(
						'Video.id',
						'Video.title',
						'Video.mp4'
					),
					'order' 	=> array(
						'Video.id' => 'DESC'
					),
					'recursive' => -1,
					'limit' 	=> 10,
					'page' 		=> $page
				)
			);
		} else {
			$videos = array();
		}
                $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
                $this->set('identity', $this->Identity->find('first', $options));
		$this->set('videos', $videos);
		$this->set('idIdentity', $idIdentity);
		$this->set('page', $page);
		$this->set('keywordData', $keyword);
                $this->set('identityName', $this->ibopadmin_getName($idIdentity));
	}

	/**
	 * funcion que carga y guarda la familia
         * Tambien guarda automaticamente un array con la relacion en una nueva tupla en la tabla
	 * @param  integer $idIdentity 
	 * @return void
	 */
	public function ibopadmin_family($idIdentity = null){
		$idIdentity = base64_decode($idIdentity);

		if($this->request->is('post')){
                    $this->IdentityRelationship->create();
                    if($this->IdentityRelationship->save($this->request->data)){
                           
                        $relation = $this->request->data('IdentityRelationship.relationship');
                        $related = "";

                        switch ($relation){               
                            case "parent":
                                $related = "offspring";
                                break;
                            case "offspring";
                                $related = "parent";
                                break;
                            case "sibling";
                                $related = "sibling";
                                break;
                             case "cousin";
                                $related = "cousin";
                                break;
                            case "uncle";
                                $related = "nephew";
                                break;
                            case "spouse";
                                $related = "spouse";
                                break;
                            case "nephew";
                                $related = "uncle";
                                break;
                            case "grandchild";
                                $related = "grandparent";
                                break;
                            case "grandparent";
                                $related = "grandchild";
                                break;
                        }

                        $family['IdentityRelationship']['identities_id']    = $this->request->data('IdentityRelationship.family');
                        $family['IdentityRelationship']['family']           = $this->request->data('IdentityRelationship.identities_id');
                        $family['IdentityRelationship']['relationship']     = $related;
                        $this->IdentityRelationship->create();
                        if($this->IdentityRelationship->save($family)){
                            $this->Session->setFlash(__('The familiar has been saved.', true), 'alert-success');
                        }else{
                            $this->Session->setFlash(__('The familiar relation could not be saved. Please, try again.', true), 'alert-danger');
                        }
                    } else {
                            $this->Session->setFlash(__('The familiar could not be saved. Please, try again.', true), 'alert-danger');
                    }
		}
                
		$familiars = $this->IdentityRelationship->find(
			'all', array(
				'conditions' => array(
					'IdentityRelationship.identities_id' => $idIdentity
				)
			)
		); 
                $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
                $this->set('identity', $this->Identity->find('first', $options));
		$this->set('familiars', $familiars);
		$this->set('idIdentity', $idIdentity);
                $this->set('identityName', $this->ibopadmin_getName($idIdentity));
	}

	/**
	 * funcion que borra la relacion 
	 * @param  integer $idIdentity 
	 * @param  integer $idfamiliar 
	 * @return void             
	 */        
	public function ibopadmin_deleteFamily($idIdentity = null, $id = null, $idFamily = null){
		$id = base64_decode($id);
		$this->IdentityRelationship->id = $id;
                
		if (!$this->IdentityRelationship->exists()) {
			$this->Session->setFlash(__('Invalid familiar', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
                
		if ($this->IdentityRelationship->delete()) {

                    $variable = $this->IdentityRelationship->deleteAll(array(
                        'IdentityRelationship.identities_id' => base64_decode($idFamily),
                        'IdentityRelationship.family' => base64_decode($idIdentity) 
                    ),true);
                    
                    $this->Session->setFlash(__('The familiar has been deleted.', true), 'alert-success');
                    
                } else {
			$this->Session->setFlash(__('The familiar could not be deleted. Please, try again.', true), 'alert-danger');
		}
		return $this->redirect(array('action' => 'family/' . $idIdentity));
	}
        
        /**
	 * funcion que carga las notas
	 * @param  integer $idIdentity
	 * @param  intiger $page  
	 * @return void             
	 */
        public function ibopadmin_notes($idIdentity = null, $page = 1){
            $idIdentity = base64_decode($idIdentity);
            
            $notes = $this->Note->find(
                'all', array(
                    'fields' => array(
                        'Note.id',
                        'Note.title',
                        'Note.note'
                    ),
                    'order' => array(
                        'Note.id' => 'DESC' 
                   ),
                    'recursive' => -1,
                    'limit' 	=> 10,
                    'page'	=> $page
                )
            );
            
            if($this->request->is('post')){
                $identityNoteExist = $this->IdentityNote->find(
                    'count', array(
                        'conditions' => array(
                            'IdentityNote.identities_id'    => $this->request->data('IdentityNote.identities_id'),
                            'IdentityNote.notes_id'         => $this->request->data('IdentityNote.notes_id')
                        )
                    )
                );
                if($identityNoteExist > 0){
                    $this->Session->setFlash(__('The Note are already added', true), 'alert-danger');
                } else {
                    if($this->IdentityNote->save($this->request->data)){
                        $this->Session->setFlash(__('The Note has been saved in.', true), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Note could not be saved. Please, try again.', true), 'alert-danger');
                    }
                }
            }
            $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
            $this->set('identity', $this->Identity->find('first', $options));
            $this->set('notes', $notes);
            $this->set('idIdentity', $idIdentity);
            $this->set('page', $page);
            $this->set('identityName', $this->ibopadmin_getName($idIdentity));
        }
        
        /**
	 * funcion que carga las notas del identity
	 * @param  integer $idIdentity
	 * @param  intiger $page  
	 * @return void             
	 */
        public function ibopadmin_identityNotes($idIdentity = null, $page = 1){
            $idIdentity = base64_decode($idIdentity);
            
            if($this->request->is('post')){
                if($this->IdentityNote->delete($this->request->data('IdentityNote.id'))){
                    $this->Session->setFlash(__('The note has been deleted', true), 'alert-success');
                    return $this->redirect(array('action' => 'identityNotes/' . base64_encode($idIdentity). '/' . $page));
                } else {
                    $this->Session->setFlash(__('The note could not be deleted. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'identityNotes/' . base64_encode($idIdentity). '/' . $page));
                }
            }
            
            $notes = $this->IdentityNote->find(
                'all', array(
                    'conditions' => array(
                        'IdentityNote.identities_id' => $idIdentity
                    ),
                    'fields' => array(
                        'IdentityNote.id',
                        'Notes.id',
                        'Notes.title',
                        'Notes.note'
                    ),
                    'order' => array(
                        'IdentityNote.id' => 'DESC'
                    ),
                    'limit' 	=> 10,
                    'page'	=> $page
                )
            );
            $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
            $this->set('identity', $this->Identity->find('first', $options));
            $this->set('idIdentity', $idIdentity);
            $this->set('page', $page);
            $this->set('notes', $notes);
            $this->set('identityName', $this->ibopadmin_getName($idIdentity));
        }
        
        /**
	 * funcion que busca en las notas
	 * @param  integer $idIdentity 
	 * @param  string  $keyword 
	 * @param  integer $page    
	 * @return void           
	 */
        public function ibopadmin_searchNotes($idIdentity = null, $keyword = null, $page = 1){
            $idIdentity = base64_decode($idIdentity);
            $keyword    = base64_decode($keyword);
            
            if($keyword != null){
                $notes = $this->Note->find(
                    'all', array(
                        'conditions' => array(
                            'Note.title LIKE' => '%' . $keyword . '%'
                        ),
                        'fields' => array(
                            'Note.id',
                            'Note.title',
                            'Note.note'
                        ),
                        'order' 	=> array(
                            'Note.id' => 'DESC'
                        ),
                        'recursive' => -1,
                        'limit'     => 10,
                        'page'      => $page
                    )
                );
            } else {
                $notes = array();
            }
            
            if($this->request->is('post')){
                $identityNoteExist = $this->IdentityNote->find(
                    'count', array(
                        'conditions' => array(
                            'IdentityNote.identities_id'    => $this->request->data('IdentityNote.identities_id'),
                            'IdentityNote.notes_id'         => $this->request->data('IdentityNote.notes_id')
                        )
                    )
                );
                if($identityNoteExist > 0){
                    $this->Session->setFlash(__('The Note are already added', true), 'alert-danger');
                } else {
                    if($this->IdentityNote->save($this->request->data)){
                        $this->Session->setFlash(__('The Note has been saved in.', true), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Note could not be saved. Please, try again.', true), 'alert-danger');
                    }
                }
            }
            $options = array('conditions' => array('Identity.' . $this->Identity->primaryKey => $idIdentity));                        
            $this->set('identity', $this->Identity->find('first', $options));
            $this->set('notes', $notes);
            $this->set('idIdentity', $idIdentity);
            $this->set('page', $page);
            $this->set('keywordData', $keyword);
            $this->set('identityName', $this->ibopadmin_getName($idIdentity));
        }
        
        /**
	 * ibopadmin_list method
	 * @param  integer $idIdentity 
	 * @return void
	 */
        public function ibopadmin_list($idIdentity = null){
            $idIdentity = base64_decode($idIdentity);
            
            $this->Fight->unbindModel(
                array(
                    'belongsTo' => array(
                        'Sources',
                        'FinalFigths',
                        'Rules'
                    ),
                    'hasAndBelongsToMany' => array(
                        'Image',
                        'Note',
                        'Title',
                        'Video'
                    )
                )
            );
            
            $this->Event->unbindModel(
                array(
                    'hasMany' => array(
                        'EventsJob',
                        'EventsImage'
                    ),
                    'hasAndBelongsToMany' => array(
                        'Fight',
                        'Image',
                        'PromotionalCompany',
                        'Video'
                    )
                )
            );
            
            $fights = $this->FightIdentity->find(
                'all', array(
                    'conditions' => array(
                        'FightIdentity.identities_id' => $idIdentity
                    ),
                    'recursive' => 4
                )
            );
            
            $this->set('fights', $fights);
            $this->set('idIdentity', $idIdentity);
            $this->set('identityName', $this->ibopadmin_getName($idIdentity));
           
        }
        
        public function ibopadmin_list2($idIdentity = null){
            
            if($idIdentity != null){
                $idIdentity = base64_decode($idIdentity);
                
                $newFights = array();
                $fights = $this->FightIdentity->find(
                    'all', array(
                        'conditions' => array(
                            'FightIdentity.identities_id' => $idIdentity
                        ),
                        'fields' => array(
                            'FightIdentity.id',
                            'FightIdentity.fights_id',
                            'FightIdentity.identities_id',
                            'FightIdentity.corner'
                        ),
                        'recursive' => -1
                    )
                );
                
                foreach ($fights as $fight){
                    $fight['Fight'] = $this->ibopadmin_getListFight($fight['FightIdentity']['fights_id']);
                    $newFights[] = $fight;
                }
                
                
                
            }
            
        }
        
        public function ibopadmin_getListFight($idFights = null){
            if($idFights != null){
                $fight = $this->Fight->find('first', array(
                    'conditions' => array(
                        'Fight.id' => $idFights
                    )
                ));
                return $fight;
            }
        }


        /**
	 * ibopadmin_fightJobs method
	 * @param  integer $idIdentity 
	 * @return void
	 */
        public function ibopadmin_fightJobs($idIdentity = null){
            $idIdentity = base64_decode($idIdentity);
            
            $fighterJobs = $this->FighterJob->find(
                'all', array(
                    'conditions' => array(
                        'FighterJob.identities_id' => $idIdentity
                    ),
                    'fields' => array(
                        'Fights.id',
                        'Fights.title',
                        'Jobs.id',
                        'Jobs.name'
                    )
                )
            );
            
            $referee = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.referee' => $idIdentity
                    )
                )
            );
            
            $ringAnnouncer = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.ring_announcer' => $idIdentity
                    )
                )
            );
            
            $judge = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'OR' => array(
                            'judge_1' => $idIdentity,
                            'judge_2' => $idIdentity,
                            'judge_3' => $idIdentity
                        )
                    )
                )
            );
            
            $inspector = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.inspector' => $idIdentity
                    )
                )
            );
            
            $timekeeper = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.timekeeper' => $idIdentity
                    )
                )
            );
            
            $this->set('fighterJobs', $fighterJobs);
            $this->set('idIdentity', $idIdentity);
            $this->set('referee', $referee);
            $this->set('ringAnnouncer', $ringAnnouncer);
            $this->set('judge', $judge);
            $this->set('inspector', $inspector);
            $this->set('timekeeper', $timekeeper);
            $this->set('identityName', $this->ibopadmin_getName($idIdentity));
        }
        
        /**
	 * ibopadmin_eventsJobs method
	 * @param  integer $idIdentity 
	 * @return void
	 */
        public function ibopadmin_eventsJobs($idIdentity = null){
            $idIdentity = base64_decode($idIdentity);
            
            $eventsJobs = $this->EventsJob->find(
                'all', array(
                    'conditions' => array(
                        'EventsJob.identities_id' => $idIdentity
                    ),
                    'fields' => array(
                        'EventsJob.type',
                        'Events.id',
                        'Events.name',
                        'Events.date'
                    )
                )
            );
            
            $this->set('eventsJobs', $eventsJobs);
            $this->set('idIdentity', $idIdentity);
            $this->set('identityName', $this->ibopadmin_getName($idIdentity));
        }
        
        /*
        Gets an identity as an auxiliar method for other controllers
         */
        public function ibopadmin_getIdentityExternal($name, $id)
        {
            $theName = explode(" ", $name);//theName[0] have first name and theName[1] have the last name
            $firstName = "";
            $lastName = "";
            
            if ( count($theName) == 2 )//this means that is normal firstName lastName: Mike Tyson
            {
                $firstName = $theName[0];
                $lastName = $theName[1];
            }
            else if ( count($theName) == 3 ) //cases with two names and a last name: Juan Jose Perez
            {
                $firstName = $theName[0] . " " . $theName[1];
                $lastName = $theName[2];
            }
            else if ( count($theName) == 4 ) //cases with two names and two last names: Jose Manuel Lopez Clavero
            {
                $firstName = $theName[0] . " " . $theName[1];
                $lastName =  $theName[2] . " " . $theName[3];
            }
            else if ( count($theName) == 5 ) //Mainly portuguese names: Edson Roberto Dos Santos Borges,  Ronaldo Luís Nazário de Lima 
            {
                $firstName = $theName[0] . " " . $theName[1];
                $lastName =  $theName[2] . " " . $theName[3] . " " . $theName[4];
            }
                
            $this->Identity->unbindModel(
			array(
				'hasMany' => array(
					'IdentitiesImage',
					'IdentitiesVideo',
					'IdentityAlias',
					'IdentityContact',
					'IdentityNickname',
					'IdentityNote'
				),
				'hasOne' => array(
					'IdentityBiography',
					'IdentityBirth',
					'IdentityDeath',
					'IdentityResidence'
				)
			)
            );
            
            $identity = $this->Identity->find(
			'all', array(
				'conditions' => array(
                                        'Identity.name' => $firstName,
                                        'Identity.last_name' => $lastName,
                                        'Identity.id' => $id
				)
			)
            );
            return $identity;
        }
        
        /**
	 * funcion que busca los identiies por nombre
	 * @param  string $name 
	 * @return void       
	 */
	public function ibopadmin_getIdentity($name = null, $numberOfResults = null){
		
                 //If the parameter number of results come with 1,
                 // then we will limit the query result to 100000
                 
                 $limit =20;
                 if (isset($numberOfResults))
                 {
                     $limit = 15000;
                     
                 }
                             
                $this->layout = 'ajax';
                
                $name = base64_decode($name);
		$this->Identity->unbindModel(
			array(
				'hasMany' => array(
					'IdentitiesImage',
					'IdentitiesVideo',
					'IdentityAlias',
					'IdentityContact',
					'IdentityNickname',
					'IdentityNote'
				),
				'hasOne' => array(
					'IdentityBiography',
					'IdentityBirth',
					'IdentityDeath',
					'IdentityResidence'
				)
			)
		);
                
                $newIdentities = array();
                
                $this->Identity->virtualFields = array(
                    'full_name' => "CONCAT(Identity.name, ' ', Identity.last_name)"
                );
                
		$identities = $this->Identity->find(
			'all', array(
				'conditions' => array(
                                        'Identity.full_name LIKE' => '%' . $name  . '%',
				),
				'fields' => array(
					'Identity.id',
					'Identity.name',
					'Identity.last_name'
				),
                                'limit' => $limit
			)
		);
                                
		foreach ($identities as $identity) {
                    
                        if(isset($identity['IdentityId'][0]['type'])){
                            $typeName	= ' (' . $identity['IdentityId'][0]['type'] . ': ';
                            $typeId	= $identity['IdentityId'][0]['type_id'] .')';
                        } else {
                            $typeName	= "";
                            $typeId	= "";
                        }
                    
			$newIdentiy['id']       = $identity['Identity']['id'];
			$newIdentiy['label']    = $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'] . $typeName . $typeId;
			$newIdentities[]	= $newIdentiy; 
		}

		if(!$identities){

			$identities = $this->IdentityId->find(
				'all', array(
					'conditions' => array(
						'IdentityId.type_id LIKE' 	=> '%' . $name  . '%',
						'IdentityId.is_principal'	=> 1
					),
					'fields' => array(
						'IdentityId.type',
						'IdentityId.type_id',
						'IdentityId.is_principal',
						'Identities.id',
						'Identities.name',
						'Identities.last_name'
					),
                                        'limit' => $limit
				)
			);

			foreach ($identities as $identity) {
				
                            if( isset($identity['IdentityId']['type']) ){
                                $typeName   = ' (' . $identity['IdentityId']['type'] . ': ';
                                $typeId     = $identity['IdentityId']['type_id'] . ')';
                            } else {
                                $typeName   = "";
                                $typeId     = "";
                            }
                            
                            $newIdentiy['id']       = $identity['Identities']['id'];
                            $newIdentiy['label']    = $identity['Identities']['name'] . ' ' . $identity['Identities']['last_name'] . $typeName . $typeId;
                            $newIdentities[]        = $newIdentiy; 
			}

		}
                
                
                $this->set('identities', $newIdentities);
	}
        
        public function ibopadmin_seeAllResults($name = null)
        {
            $name = base64_decode($name);
            $this->Identity->unbindModel(
                array(
			'hasMany' => array(
				'IdentitiesImage',
				'IdentitiesVideo',
				'IdentityAlias',
				'IdentityContact',
				'IdentityNickname',
				'IdentityNote'
			),
			'hasOne' => array(
				'IdentityBiography',
				'IdentityBirth',
				'IdentityDeath',
                        	'IdentityResidence'
                    	)
		)
            );
                
            $newIdentities = array();
                
            $this->Identity->virtualFields = array(
                'full_name' => "CONCAT(Identity.name, ' ', Identity.last_name)"
            );
                
            $identities = $this->Identity->find(
            	'all', array(
            		'conditions' => array(
                            'Identity.full_name LIKE' => '%' . $name  . '%',
			),
                    	'fields' => array(
				'Identity.id',
				'Identity.name',
				'Identity.last_name'
			),
                               'limit' => 15000
		)
            );
                
                
            foreach ($identities as $identity) {      
                if(isset($identity['IdentityId'][0]['type'])){
                    $typeName	= ' (' . $identity['IdentityId'][0]['type'] . ': ';
                    $typeId	= $identity['IdentityId'][0]['type_id'] .')';
                } else {
                    $typeName	= "";
                    $typeId	= "";
                }   
                    $newIdentiy['id']       = $identity['Identity']['id'];
                    $newIdentiy['label']    = $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'] . $typeName . $typeId;
                    $newIdentities[]	= $newIdentiy; 
		}

		if(!$identities){

			$identities = $this->IdentityId->find(
				'all', array(
					'conditions' => array(
						'IdentityId.type_id LIKE' 	=> '%' . $name  . '%',
						'IdentityId.is_principal'	=> 1
					),
					'fields' => array(
						'IdentityId.type',
						'IdentityId.type_id',
						'IdentityId.is_principal',
						'Identities.id',
						'Identities.name',
						'Identities.last_name'
					),
                                        'limit' => 15000
				)
			);

			foreach ($identities as $identity) {
				
                            if( isset($identity['IdentityId']['type']) ){
                                $typeName   = ' (' . $identity['IdentityId']['type'] . ': ';
                                $typeId     = $identity['IdentityId']['type_id'] . ')';
                            } else {
                                $typeName   = "";
                                $typeId     = "";
                            }
                            
                            $newIdentiy['id']       = $identity['Identities']['id'];
                            $newIdentiy['label']    = $identity['Identities']['name'] . ' ' . $identity['Identities']['last_name'] . $typeName . $typeId;
                            $newIdentities[]        = $newIdentiy; 
			}

		}
            $this->set('searchCriteria',$name);
            $this->set('identities', $newIdentities);
                 
        }
        
        /**
	 * ibopadmin_getName method
	 * @param  string $id 
	 * @return string       
	 */
        public function ibopadmin_getName($id = null){
            if($id != null){
                $identity = $this->Identity->find(
                    'first', array(
                        'conditions' => array(
                            'Identity.id' => $id
                        ),
                        'fields' => array(
                            'Identity.name',
                            'Identity.last_name',
                        ),
                        'recursive' => -1
                    )
                );
                return $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'];
            }
        }
         /**
	 * ibopadmin_getSponsor method
	 * @param  string $id 
	 * @return string  $sponsor for populate list     
	 */
        public function ibopadmin_getSponsor($idIdentity = null){
                
		$sponsor = $this->Sponsor->find('all', array(
                    'conditions' => array(
                        'Sponsor.identities_id' =>$idIdentity
                    )
                ));
		return $sponsor;
	}
         /**
	 * ibopadmin_saveSponsor method
	 * @param  string $id 
	 *   save sponsor name, its kind of nickname given for brands    
	 */
        public function ibopadmin_saveSponsor($id = null){
           if($this->request->is('post')){
			if ($this->Sponsor->save($this->request->data)) {
				$this->Session->setFlash(__('The sponsor name has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('Sponsor.identities_id'))));
			} else {
				$this->Session->setFlash(__('The sponsor name could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('Sponsor.identities_id'))));
			}
		}
        }
         /**
	 * ibopadmin_deleteSponsor method
	 * @param  string $id 
	 *   Delete sponsor record,    
	 */
        public function ibopadmin_deleteSponsor($id = null,$idIdentity = null){
          
          
            if($this->Sponsor->delete($id)){
                $this->Session->setFlash(__('The Sponsor has been deleted.', true), 'alert-success');
                return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
            }else{
                $this->Session->setFlash(__('The Sponsor could not be deleted,try again.', true), 'alert-danger');
                return $this->redirect(array('action' => 'edit/' . base64_encode($idIdentity)));
                
            }
        }
        public function ibopadmin_lock($id = null, $lock = null){
            $lockPerson['Identity']['id']       = base64_decode($id);
            $lockPerson['Identity']['locked']   = $lock;          
            if($this->Identity->save($lockPerson)){
                if ($lock == 1){
                    $this->Session->setFlash(__('The register has been locked.', true), 'alert-success');
                }else{
                    $this->Session->setFlash(__('The register has been unlocked.', true), 'alert-success');
                }                
            } else {
                $this->Session->setFlash(__('The status could not be changed.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' .$id));    
        }
        
        //test git nueva cuenta
        
        /**
        * ibopadmin_fightOrder method
        * @return void 
        */
        public function ibopadmin_videoOrder(){
            if($this->request->is('post')){
                $IdentitiesVideo['IdentitiesVideo']['id']         = $this->request->data('id');
                $IdentitiesVideo['IdentitiesVideo']['position']   = $this->request->data('newOrder');
                $this->IdentitiesVideo->save($IdentitiesVideo);
            }
            return die();
        }
}