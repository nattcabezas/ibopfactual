<?php
App::uses('AppController', 'Controller');
/**
 * Identities Controller
 *
 * @property Identity $Identity
 * @author rudysibaja <neo.nou@gmail.com>
 */

class PersonsController extends AppController {
    
    public $name = "Persons";
    
    public $components = array('Paginator', 'RequestHandler');
    
    public $uses = array(
        'Article',
        'Identity', 
        'Image', 
        'Country', 
        'State', 
        'City', 
        'Fight', 
        'Event', 
        'FightIdentity', 
        'Location', 
        'IdentitiesImage', 
        'IdentitiesVideo',
        'IdentityNote',
        'IdentityContact',
        'Venue', 
        'FightsTitle', 
        'Title',
        'IdentityRelationship',
        'FighterJob',
        'FightDatum',
        'EventsJob'
    );
       
    public $helper = array('getUrl', 'QrCode', 'getage', 'Js');
    
    /*
     * index method
     * @param string $id
     * @return void
     */
    public function index($url = null) {
        if($url != null){
            list($id, $name) = explode('-', $url);
            
            $this->Identity->unbindModel(
                    array(
                        'hasMany' => array(
                                'IdentitiesVideo',
                                'IdentitiesImage',
                                'IdentityContact',
                                'IdentityNote',
                                'IdentityId'
                        )
                    )
            );
            
            $identity = $this->Identity->find('first', array(
                'conditions' => array(
                    'Identity.id' => $id
                ),
                'recursive' => 3
            ));
            
            $birthCountry = $this->Country->find('first', array(
                'conditions' => array(
                    'Country.id' => $identity['IdentityBirth']['countries_id']
                ),
                'fields' => array(
                    'Country.name'
                )
            ));
            
            $birthState = $this->State->find('first', array(
                'conditions' => array(
                    'State.id' => $identity['IdentityBirth']['states_id']
                ),
                'fields' => array(
                    'State.name'
                )
            ));
            
            $birthCity = $this->City->find('first', array(
                'conditions' => array(
                    'City.id' => $identity['IdentityBirth']['cities_id']
                ),
                'fields' => array(
                    'City.name'
                )
            ));
            
            $image = $this->IdentitiesImage->find('first', array(
                'conditions' => array(
                    'IdentitiesImage.identities_id' => $identity['Identity']['id'],
                    'IdentitiesImage.principal'     => 1
                )
            ));
            
            $countVideos = $this->IdentitiesVideo->find('count', array(
                'conditions' => array(
                    'IdentitiesVideo.identities_id' => $id,
                    'Videos.private' => 1
                )
            ));
            
            $countArticles = $this->Article->find(
                'count', array(
                    'conditions' => array(
                       'Article.identities_id' => $id
                   )
                )
            );
            
            if( ($this->Session->read('UserPage') != null)){
                
                if($this->Session->read('UserPage.PageUser.type') == '3'){//v.i.p type
                    if($countVideos == 0){
                        $countVideos = $this->IdentitiesVideo->find('count', array(
                            'conditions' => array(
                                'IdentitiesVideo.identities_id' => $id,
                                'Videos.private' => 3
                            )
                        ));
                        
                    }
                } else if($this->Session->read('UserPage.PageUser.type') == '2'){//premium videos
                    if($countVideos == 0){
                        $countVideos = $this->IdentitiesVideo->find('count', array(
                            'conditions' => array(
                                'IdentitiesVideo.identities_id' => $id
                            )
                        ));
                    }
                    
                }
            }
            
            $countImages = $this->IdentitiesImage->find('count', array(
                'conditions' => array(
                    'IdentitiesImage.identities_id' => $id
                )
            ));
            
            $countNotes = $this->IdentityNote->find('count', array(
                'conditions' => array(
                    'IdentityNote.identities_id' => $id
                )
            ));
            
            $countFamily = $this->IdentityRelationship->find('count', array(
                'conditions' => array(
                    'IdentityRelationship.identities_id' => $id
                )
            ));
            
            $countFights = $this->FightIdentity->find('count', array(
                'conditions' => array(
                    'FightIdentity.identities_id' => $id
                ),
            ));
            $countContacts = $this->IdentityContact->find('count', array(
                'conditions' => array(
                    'IdentityContact.identities_id' => $id
                ),
            ));
            
            
            
            $identity['IdentityBirth']['Country']   = isset($birthCountry['Country']['name']) ? $birthCountry['Country']['name'] : '';
            $identity['IdentityBirth']['State']     = isset($birthState['State']['name']) ? $birthState['State']['name'] : '';
            $identity['IdentityBirth']['City']      = isset($birthCity['City']['name']) ? $birthCity['City']['name'] : '';
            $identity['IdentitiesImage']            = $image;
            
            $this->set('identity', $identity);
            $this->set('countImages', $countImages);
            $this->set('countVideos', $countVideos);
            $this->set('countNotes', $countNotes);
            $this->set('countFamily', $countFamily);
            $this->set('countFights', $countFights);
            $this->set('countContacts', $countContacts);
            $this->set('countArticles', $countArticles);
            $this->set('record', $this->getRecord($id));
            $this->set('countJobs', $this->getCountJobs($id));
            $this->set('id', $id);
            
            if($identity['Identity']['description'] != ""){
                if(strpos($identity['Identity']['description'], '/')){
                    list($pageTitle, $pageDescription) = explode('/', $identity['Identity']['description']);
                } else {
                    $pageTitle = $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'];
                    $pageDescription = $identity['Identity']['description'];
                }
                
            } else {
                $pageTitle = $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name'];
                $pageDescription = $identity['Identity']['name'] . ' ' . $identity['Identity']['last_name']. ' is ';
                $pageDescription .= 'an ' . $identity['Identity']['stance']. ' ';
                $pageDescription .=  $identity['Identity']['gender'] == 'M' ? 'male fighter' : 'female fighter';
            }
            
            if( (isset($identity['IdentitiesImage']['Images']['url'])) && ($identity['IdentitiesImage']['Images']['url'] != null)){
                $this->set('pageImage', $identity['IdentitiesImage']['Images']['url']);
            } else {
                $this->set('pageImage', 0);
            }
            
            $this->set('pageTitle', trim($pageTitle));
            $this->set('pageDescription', trim($pageDescription));
            
        }
    }
    
    /*
     * search method
     * @param string $keyword
     * @param string $page
     * @return void
     */
    public function search($keyword = null, $page = 1){
        
        if($this->request->is('post')){
            $keywordPost = $this->request->data('name') . '_' . $this->request->data('last-name');
            $keywordPost = trim($keywordPost);
            $keywordPost = str_replace(" ", "--", $keywordPost);
            $keywordPost = $this->deleteAccent($keywordPost);
            return $this->redirect(array('action' => 'search/' . $keywordPost));
        }
        
        if($keyword != null){
            
            $keywordSearch = $keyword;
            $keyword = str_replace("--", " ", $keyword);
            list($name, $lastName) = explode('_', $keyword);
            $this->set('name', $name);
            $this->set('lastName', $lastName);
            $this->set('keywordSearch', $keywordSearch);
            
            $this->Identity->unbindModel(
                    array(
                            'hasMany' => array(
                                    'IdentitiesVideo',
                                    'IdentitiesImage',
                                    'IdentityContact',
                                    'IdentityNote',
                                    'IdentityId'
                            ),
                            'hasOne' => array(
                                    'IdentityBiography',
                                    'IdentityBirth',
                                    'IdentityDeath',
                            )
                    )
            );
            
            $this->Identity->bindModel(
                array(
                    'hasMany' => array(
                        'IdentitiesImage' => array(
                            'className' 	=> 'IdentitiesImage',
                            'foreignKey' 	=> 'identities_id',
                            'order' => array(
                                'IdentitiesImage.principal' => 'DESC'
                            ),
                            'limit' => 1
                        )
                    )
                )
            );
            
            $identities = $this->Identity->find(
                    'all', array(
                        'conditions' => array(
                            'Identity.fullname LIKE' => '%' . $name . ' ' . $lastName . '%' 
                        ),
                        'order' => array(
                            'Identity.fullname' => 'ASC'
                        ),
                        'limit' => 20,
                        'page' => $page
                    )
            );
            
            if(count($identities) == 0){
                
                $search         = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
                $replace        = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
                $cleanName      = addslashes(str_replace($search, $replace, $name));
                $cleanLastName  = addslashes(str_replace($search, $replace, $lastName));
                
                $identities = $this->Identity->find(
                        'all', array(
                            'conditions' => array(
                                'or' => array(
                                    '"' . $cleanName . '"' . ' LIKE CONCAT("%",`Identity.name`,"%")',
                                    '"' . $cleanLastName . '"' . ' LIKE CONCAT("%",`Identity.last_name`,"%")'
                                )
                            ),
                            'limit' => 20,
                            'page' => $page
                        )
                );
                
                $this->set('secondQuery', true);
                
            } else {
                $this->set('secondQuery', false);
            }
            
            
            $newIdentities = array();
            foreach ($identities as $identity){
                if(count($identity['IdentitiesImage']) > 0){
                    $identity['IdentitiesImage']    = $this->getImageSearch($identity['IdentitiesImage'][0]['images_id']);
                }
                $identity['IdentityResidence']  = $this->getResidenceSearch($identity['IdentityResidence']);
                $identity['Career']             = $this->getCareer($identity['Identity']['id']);
                $newIdentities[] = $identity;
            }
            $this->set('identities', $newIdentities);
            
            
        }
        
    }
    
    /*
     * searchPage method
     * @param string $keyword
     * @param string $page
     * @return void;
     */
    public function searchPage($keyword = null, $page = 1, $secondQuery = false){
        
        $this->layout = 'ajax';
        if($keyword != null){
            
            
            $keywordSearch = $keyword;
            $keyword = str_replace("--", " ", $keyword);
            list($name, $lastName) = explode('_', $keyword);

            
            $this->Identity->unbindModel(
                    array(
                            'hasMany' => array(
                                    'IdentitiesVideo',
                                    'IdentitiesImage',
                                    'IdentityContact',
                                    'IdentityNote',
                                    'IdentityId'
                            ),
                            'hasOne' => array(
                                    'IdentityBiography',
                                    'IdentityBirth',
                                    'IdentityDeath',
                            )
                    )
            );
            
            $this->Identity->bindModel(
                array(
                    'hasMany' => array(
                        'IdentitiesImage' => array(
                            'className' 	=> 'IdentitiesImage',
                            'foreignKey' 	=> 'identities_id',
                            'order' => array(
                                'IdentitiesImage.principal' => 'DESC'
                            ),
                            'limit' => 1
                        )
                    )
                )
            );
            
            if(!$secondQuery){
                $identities = $this->Identity->find(
                        'all', array(
                            'conditions' => array(
                                    'Identity.fullname LIKE' => '%' . $name . ' ' . $lastName . '%'
                            ),
                            'order' => array(
                                'Identity.fullname' => 'ASC'
                            ),
                            'limit' => 20,
                            'page' => $page
                        )
                );
            } else {
                
                $search         = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
                $replace        = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
                $cleanName      = addslashes(str_replace($search, $replace, $name));
                $cleanLastName  = addslashes(str_replace($search, $replace, $lastName));
                
                $identities = $this->Identity->find(
                        'all', array(
                            'conditions' => array(
                                'or' => array(
                                    '"' . $cleanName . '"' . ' LIKE CONCAT("%",`Identity.name`,"%")',
                                    '"' . $cleanLastName . '"' . ' LIKE CONCAT("%",`Identity.last_name`,"%")'
                                )
                            ),
                            'limit' => 20,
                            'page' => $page
                        )
                );
                
            }
            
            
            $newIdentities = array();
            foreach ($identities as $identity){
                if(count($identity['IdentitiesImage']) > 0){
                    $identity['IdentitiesImage']    = $this->getImageSearch($identity['IdentitiesImage'][0]['images_id']);    
                }
                $identity['IdentityResidence']  = $this->getResidenceSearch($identity['IdentityResidence']);
                $identity['Career']             = $this->getCareer($identity['Identity']['id']);
                $newIdentities[]                = $identity;
            }
            
            $this->set('identities', $newIdentities);
            
            
        }
        
    }
    
    /*
     * getImageSearch method
     * @param string $idImage
     * @return array
     */
    public function getImageSearch($idImage = null){
        $imageData = $this->Image->find('first', array(
            'conditions' => array(
                'Image.id' => $idImage
            ),
            'recursive' => -1
        ));
        return $imageData;
    }
    
    /*
     * getResidenceSearch method
     * @param array $IdentityResidence
     * @return array
     */
    public function getResidenceSearch($IdentityResidence = array()){
        $location = array();
        if(isset($IdentityResidence['countries_id'])){
            $country = $this->Country->find('first', array(
                'conditions' => array(
                    'Country.id' => $IdentityResidence['countries_id']
                ),
                'fields' => array(
                    'Country.name',
                    'Country.flag'
                ),
                'recursive' => -1
            ));
            $location['Country']    = $country['Country']['name'];
            $location['Flag']       = $country['Country']['flag'];
        }
        
        if(isset($IdentityResidence['states_id'])){
            $state = $this->State->find('first', array(
                'conditions' => array(
                    'State.id' => $IdentityResidence['states_id']
                ),
                'fields' => array(
                    'State.name',
                ),
                'recursive' => -1
            ));
            if(isset($state['State']['name'])){
                $location['State'] = $state['State']['name'];
            }
        }
        
        if(isset($IdentityResidence['cities_id'])){
            $city = $this->City->find('first', array(
                'conditions' => array(
                    'City.id' => $IdentityResidence['cities_id']
                ),
                'fields' => array(
                    'City.name',
                ),
                'recursive' => -1
            ));
            if(isset($city['City']['name'])){
                $location['City'] = $city['City']['name'];
            }
        }
        
        return $location;
    }
    
    /*
     * getCareer method
     * @param string $idIdentity
     * @return array
     */
    public function getCareer($idIdentity = null){
  
        $fights = $this->FightIdentity->find(
            'all', array(
                'conditions' => array(
                    'FightIdentity.identities_id' => $idIdentity
                ),
                'fields' => array(
                    'FightIdentity.fights_id',
                    'FightIdentity.Corner'
                ),
                'recursive' => -1
            )
        );
        
        $totalFights    = count($fights);
        $winFights      = 0;
        $lostFights     = 0;
        $drawFights     = 0;
        $firtsFights    = null;
        $lastFights     = null;
        $career         = array();
        
        foreach ($fights as $fight){
            
            $this->Fight->unbindModel(array(
                'hasAndBelongsToMany' => array(
                    'Image',
                    'Note',
                    'Title',
                    'Video'
                ),
                'hasMany' => array(
                    'FightIdentity'
                )
            ));
            
            $thisFight = $this->Fight->find('first', array(
                'conditions' => array(
                    'Fight.id' => $fight['FightIdentity']['fights_id']
                ),
                'fields' => array(
                    'Fight.winner',
                    'Event.date'
                ),
                'oder' => array(
                    'Event.date' => 'ASC'
                )
            ));

            if($thisFight['Fight']['winner'] != 4){
                if($thisFight['Fight']['winner'] == 3){
                    $drawFights++;
                } else if($fight['FightIdentity']['Corner'] == $thisFight['Fight']['winner']) {
                    $winFights++;
                } else {
                    $lostFights++;
                }
            }
            
            
            
            if(isset($thisFight['Event'])){
                
                if($firtsFights == null){
                    $firtsFights = $thisFight['Event']['date'];
                } 
                if($lastFights == null){
                    $lastFights = $thisFight['Event']['date'];
                }

                if(strtotime($firtsFights) > strtotime($thisFight['Event']['date'])){
                    $firtsFights = $thisFight['Event']['date'];
                }

                if(strtotime($lastFights) < strtotime($thisFight['Event']['date'])){
                    $lastFights = $thisFight['Event']['date'];
                }
            }
        }
        
        
        
        $career['win']      = $winFights;
        $career['lost']     = $lostFights;
        $career['draw']     = $drawFights;
        $career['firts']    = $firtsFights;
        $career['last']     = $lastFights;
        
        return $career;
    }
    
    /*
     * getRecord method
     * @param string $idIdentity
     * @return array
     */
    public function getRecord($idIdentity = null){
        
        $record = array();
        
        $boxingData = $this->FightIdentity->find('all', array(
            'conditions' => array(
                'FightIdentity.identities_id' => $idIdentity,
                'Fight.types_id' => 1
            ),
            'fields' => array(
                'FightIdentity.corner',
                'Fight.winner',
                'Fight.via',
                'Fight.boxed_roundas',
                'Fight.rules_id'
            )
        ));
        
        $winBoxing      = 0;
        $lostBoxing     = 0;
        $drawBoxing     = 0;
        $roundsBoxing   = 0;
        $totalFights    = 0;
        
        $winKOBoxing    = 0;
        $lostKOBoxing   = 0;
        
        $winNWSBoxing   = 0;
        $lostNWSBoxing  = 0;
        $drawNWSBoxing  = 0;
        
        $winPerBoxing   = 0;
                
        foreach($boxingData as $boxing){
            if($boxing['Fight']['winner'] != 4 ){
                if( ($boxing['Fight']['winner'] == 3) && ( ($boxing['Fight']['rules_id'] != 4) &&  ($boxing['Fight']['rules_id'] != 14) ) ){
                    $drawBoxing++;
                    if( ($boxing['Fight']['via'] == 'D-NWS') && ( ($boxing['Fight']['rules_id'] != 4) &&  ($boxing['Fight']['rules_id'] != 14) ) ){
                        $drawNWSBoxing++;
                    }
                } else if( ($boxing['FightIdentity']['corner'] == $boxing['Fight']['winner']) && ( ($boxing['Fight']['rules_id'] != 4) &&  ($boxing['Fight']['rules_id'] != 14) ) ) {
                    $winBoxing++;
                    if(($boxing['Fight']['via'] == 'KO') || ($boxing['Fight']['via'] == 'TKO') || ($boxing['Fight']['via'] == 'RTD')){
                        $winKOBoxing++;
                    } else if($boxing['Fight']['via'] == 'NWS'){
                        $winNWSBoxing++;
                    }
                    if( ($boxing['Fight']['via'] == 'KO')  || ($boxing['Fight']['via'] == 'TKO') || ($boxing['Fight']['via'] == 'RTD') ){
                        $winPerBoxing++;
                    }
                } else {
                    
                    if ( ($boxing['Fight']['rules_id'] != 4) &&  ($boxing['Fight']['rules_id'] != 14) )
                    {
                        $lostBoxing++;
                        if(($boxing['Fight']['via'] == 'KO')  || ($boxing['Fight']['via'] == 'TKO') || ($boxing['Fight']['via'] == 'RTD')){
                            $lostKOBoxing++;
                        } else if($boxing['Fight']['via'] == 'NWS'){
                            $lostNWSBoxing++;
                        }
                    }
                }
                
                /*if($boxing['Fight']['via'] != "NC"){
                    $totalFights++;
                }*/
                
            }
            if (($boxing['Fight']['rules_id'] != 4) &&  ($boxing['Fight']['rules_id'] != 14))
            {
                $totalFights++;
                $roundsBoxing = $roundsBoxing + $boxing['Fight']['boxed_roundas'];     
            }
            
        }
        
        $record['boxing']['is']     = count($boxingData);
        $record['boxing']['won']    = $winBoxing;
        $record['boxing']['lost']   = $lostBoxing;
        $record['boxing']['draw']   = $drawBoxing;
        $record['boxing']['rounds'] = $roundsBoxing;
        $record['boxing']['total']  = $totalFights;
        
        $record['boxing']['win_ko']     = $winKOBoxing;
        $record['boxing']['lost_ko']    = $lostKOBoxing;
        
        $record['boxing']['won_nws']    = $winNWSBoxing;
        $record['boxing']['lost_nws']   = $lostNWSBoxing;
        $record['boxing']['draw_nws']   = $drawNWSBoxing;
        
        $record['boxing']['win_perc']   = $winPerBoxing;
        
        $mmaData = $this->FightIdentity->find('all', array(
            'conditions' => array(
                'FightIdentity.identities_id' => $idIdentity,
                'Fight.types_id' => 2
            ),
            'fields' => array(
                'FightIdentity.corner',
                'Fight.winner',
                'Fight.via',
                'Fight.submission_comments',
                'Fight.boxed_roundas',
                'Fight.rules_id'
            )
        ));
        
        $winMMA      = 0;
        $lostMMA     = 0;
        $drawMMA     = 0;
        $roundsMMA   = 0;
        $totalMMA    = 0;
        
        $winKoMMA    = 0;
        $lostKoMMA   = 0;
        
        //debug($mmaData);die();
        
        foreach($mmaData as $mma){
            if($mma['Fight']['winner'] != 4 ){
                if( ($mma['Fight']['winner'] == 3) && ( ($mma['Fight']['rules_id'] != 4) &&  ($mma['Fight']['rules_id'] != 14) )  ){
                    $drawMMA++;
                } else if( ($mma['FightIdentity']['corner'] == $mma['Fight']['winner']) && ( ($mma['Fight']['rules_id'] != 4) &&  ($mma['Fight']['rules_id'] != 14) )  ) {
                    $winMMA++;
                    if( ( ($mma['Fight']['via'] == 'STOPPAGE') || ($mma['Fight']['via'] == 'SUBMISSION')) && ( ($mma['Fight']['rules_id'] != 4) &&  ($mma['Fight']['rules_id'] != 14) ) ){
                        $winKoMMA++;
                    }
                } else {
                    
                    if (($mma['Fight']['rules_id'] != 4) &&  ($mma['Fight']['rules_id'] != 14))
                    {
                        $lostMMA++;
                        if(($mma['Fight']['via'] == 'STOPPAGE') || ($mma['Fight']['via'] == 'SUBMISSION')){
                            $lostKoMMA++;
                        }                        
                    }
                    
                }
                if($mma['Fight']['via'] != "NC" && (($mma['Fight']['rules_id'] != 4) &&  ($mma['Fight']['rules_id'] != 14))){
                    $totalMMA++;
                }   
            }
            if (($mma['Fight']['rules_id'] != 4) &&  ($mma['Fight']['rules_id'] != 14))
            {
                $roundsMMA = $roundsMMA + $mma['Fight']['boxed_roundas']; 
            }            
        }
        
        $record['mma']['is']     = count($mmaData);
        $record['mma']['won']    = $winMMA;
        $record['mma']['lost']   = $lostMMA;
        $record['mma']['draw']   = $drawMMA;
        $record['mma']['rounds'] = $roundsMMA;
        $record['mma']['total']  = $totalMMA;
        
        $record['mma']['win_ko']  = $winKoMMA;
        $record['mma']['lost_ko'] = $lostKoMMA;
        
        return $record;
    }
    
    /*
     * getImagesPerson method
     * @param string $idIdentity
     * @return void
     */
    public function getImagesPerson($idIdentity = null){
        
        $this->layout = 'ajax';
        
        $images = $this->IdentitiesImage->find('all', array(
            'conditions' => array(
                'IdentitiesImage.identities_id' => $idIdentity
            )
        ));
        
        $this->set('images', $images);
        
    }
    
    /*
     * getVideosPerson method
     * @param string $idIdentity
     * @return void
     */
    public function getVideosPerson($idIdentity = null, $videoType = null){
        
        $this->layout = 'ajax';
        
        $videos = $this->IdentitiesVideo->find('all', array(
            'conditions' => array(
                'IdentitiesVideo.identities_id' => $idIdentity,
                'Videos.private' => 1
            )
            
        ));
        
        if( ($this->Session->read('UserPage') != null)){
            if($this->Session->read('UserPage.PageUser.type') == '3'){//v.i.p type
                
                $videosVIP = $this->IdentitiesVideo->find('all', array(
                    'conditions' => array(
                        'IdentitiesVideo.identities_id' => $idIdentity,
                        'Videos.private' => 3
                    )

                ));
                $this->set('videosVIP', $videosVIP);
                
            } else if ($this->Session->read('UserPage.PageUser.type') == '2'){//Premium type
                
                $videosVIP = $this->IdentitiesVideo->find('all', array(
                    'conditions' => array(
                        'IdentitiesVideo.identities_id' => $idIdentity,
                        'Videos.private' => 3
                    )

                ));
                
                $this->set('videosVIP', $videosVIP);
                
                $videosPre = $this->IdentitiesVideo->find('all', array(
                    'conditions' => array(
                        'IdentitiesVideo.identities_id' => $idIdentity,
                        'Videos.private' => 2
                    )

                ));
                $this->set('videosPre', $videosPre);
                
            }
        }
        $this->set('id', $idIdentity);
        $this->set('videos', $videos);
        $this->set('videoType', $videoType);
    }
    
    
    /*
    * Filter videos return an array with the requested video type on the front end
    * @param string $videos: The list of all videos for a person
    * @param int $requestedType: The list of all videos for a person
    * @return void
    */
    //PUT ALL THE VIDEO TYPES!!!!!!!!!!!!!!!!
    public function filterVideos($idIdentity, $requestedType)
    {
            $this->layout = 'ajax';
            /*
             Retrieve videos of identity   
             **/
             /**********/
            $videos = $this->IdentitiesVideo->find('all', array(
                'conditions' => array(
                    'IdentitiesVideo.identities_id' => $idIdentity,
                    'Videos.private' => 1
                )
            ));
            /**********/
        
        
            $arrayFullFight = array();
            $arrayPartialFightsAndHighlights = array();
            $arraySparring = array();
            $arrayWeighIns = array();
            $arrayInterview = array();
            
            $arrayMisc = array();
            
            foreach ($videos as $video)
            {
                if ($video['Videos']['image_types_id']==20)
                {
                    array_push($arrayFullFight,$video);
                }
                if ($video['Videos']['image_types_id']==21)
                {
                    array_push($arrayPartialFightsAndHighlights,$video);
                }
                if ($video['Videos']['image_types_id']==22)
                {
                    array_push($arraySparring,$video);
                }
                if ($video['Videos']['image_types_id']==23)
                {
                    array_push($arrayWeighIns,$video);
                }
                if ($video['Videos']['image_types_id']==24)
                {
                    array_push($arrayInterview,$video);
                }
                if ($video['Videos']['image_types_id']==29)
                {
                    array_push($arrayMisc,$video);
                }
                
                
            }
            
            $finalArray = array();
            
            if ($requestedType == 20)
            {
                $finalArray = $arrayFullFight;
            }
            if ($requestedType == 21)
            {
                $finalArray = $arrayPartialFightsAndHighlights;
            }
            if ($requestedType == 22)
            {
                $finalArray = $arraySparring;
            }
            if ($requestedType == 23)
            {
                $finalArray = $arrayWeighIns;
            }
            if ($requestedType == 24)
            {
                $finalArray = $arrayInterview;
            }
            if ($requestedType == 29)
            {
                $finalArray = $arrayMisc;
            }
            $this->set('videoArray', $finalArray);
            //return $finalArray;
    }
    
    /*
     * showRecord method
     * @param string $idIdentity
     * @return void
     */
    public function showRecord($idIdentity = null){
        
        $this->layout = 'ajax';
        
        $countBoxing = $this->FightIdentity->find('count', array(
            'conditions' => array(
                'FightIdentity.identities_id' => $idIdentity,
                'Fight.types_id' => 1
            )
        ));
        
        $countMMA = $this->FightIdentity->find('count', array(
            'conditions' => array(
                'FightIdentity.identities_id' => $idIdentity,
                'Fight.types_id' => 2
            )
        ));
        
        $this->Identity->unbindModel(
            array(
                'hasMany' => array(
                    'IdentitiesVideo',
                    'IdentitiesImage',
                    'IdentityContact',
                    'IdentityNote',
                    'IdentityAlias',
                    'IdentityNickname',
                    'IdentityId'
                ),
                'hasOne' => array(
                    'IdentityBiography',
                    'IdentityBirth',
                    'IdentityDeath',
                )
            )
        );
        $identity = $this->Identity->find('first', array(
            'conditions' => array(
                'Identity.id' => $idIdentity
            ),
            'fields' => array(
                'Identity.id',
                'Identity.gender'
            )
        ));
        
        
        if($countBoxing > 0){
            $this->set('boxingRecord', $this->getFightRecord($idIdentity, 1));
        }
        
        if($countMMA > 0){
            $this->set('mmaRecord', $this->getFightRecord($idIdentity, 2));
        }
        
        $this->set('identity', $identity);
        $this->set('countBoxing', $countBoxing);
        $this->set('countMMA', $countMMA);
    }
    
 
    /*
     * getBoxingRecord method
     * @param string $idIdentity
     * @return array
     */
    public function getFightRecord($idIdentity = null, $type = null){
        
        $fightsData = $this->FightIdentity->find('all', array(
            'conditions' => array(
                'FightIdentity.identities_id' => $idIdentity,
                'Fight.types_id' => $type
            ),
            'recursive' => 2
        ));
        
        $fights = array();
        foreach ($fightsData as $fightData) {
            //debug($fightData);
            $fight['Fight']['id']               = $fightData['Fight']['id'];
            $fight['Fight']['title']            = $fightData['Fight']['title'];
            if(isset($fightData['Fight']['Event']['date'])){
                $fight['Fight']['date']         = $fightData['Fight']['Event']['date'];
            } else {
                $fight['Fight']['date']         = null;
            }
            if(isset($fightData['Fight']['Event']['venues_id'])){
                $fight['Fight']['venue']        = $this->getVenueRecord($fightData['Fight']['Event']['venues_id']);
            } else {
                $fight['Fight']['venue'] = null;
            }
            
            if(isset($fightData['Fight']['Event']['locations_id'])){
                $fight['Fight']['locations']    = $this->getLocation($fightData['Fight']['Event']['locations_id']);
            } else if ($this->getLocationByEvent($fightData['Fight']['events_id']!=null)) {
                $theLocation = $this->getLocationByEvent($fightData['Fight']['events_id']);
                $fight['Fight']['locations'] = $theLocation['Event']['locations_id'];
            }
            else
            {
                $fight['Fight']['locations']    = null;
            }
            
            $fight['Fight']['corner']           = $fightData['FightIdentity']['corner'];
            $fight['Fight']['winner']           = $fightData['Fight']['winner'];
            $fight['Fight']['opponent']         = $this->getOpponentRecord($fightData['FightIdentity']['corner'], $fightData['Fight']['id']);
            $fight['Fight']['via']              = $fightData['Fight']['via'];
            $fight['Fight']['boxed_roundas']    = $fightData['Fight']['boxed_roundas'];
            $fight['Fight']['schedule_rounds']  = $fightData['Fight']['schedule_rounds'];
            $fight['Fight']['time_per_round']   = $fightData['Fight']['time_per_round'];
            $fight['Fight']['time']             = $fightData['Fight']['time'];
            $fight['Fight']['Title']            = $fightData['Fight']['Title'];
            if($type == 2){
               $fight['Fight']['submission_comments'] = $fightData['Fight']['submission_comments']; 
            }
            $fights[] = $fight;
        }
        
        /*
        Ordena las peleas por numero de fecha
         **/
        if ($type==1)
        {
            function datescompareBoxing($a, $b) 
            {
                return (string)$a['Fight']['date'] <  (string)$b['Fight']['date'];
            }   
            usort($fights, 'datescompareBoxing');
        }
        else if ($type==2)
        {
            function datescompareMMA($a, $b) 
            {
                return (string)$a['Fight']['date'] <  (string)$b['Fight']['date'];
            }   
            usort($fights, 'datescompareMMA');
            
            
        }
        return $fights;
    }
    
    /*
     * Gets a venue that belong to a certain Event
     */
    public function getLocationByEvent($idEvent)
    {
        $event = $this->Event->find('first', array(
            'conditions' => array(
                'Event.id' => $idEvent
            ),
            'fields' => array(
                'Event.locations_id'
            )
        ));
        return $event;
    }
    
    /*
     * getVenueRecord method
     * @param string $idVenue
     * @return array
     */
    public function getVenueRecord($idVenue){
        
        $returnData = array();
        
        $this->Venue->unbindModel(array(
            'hasAndBelongsToMany' => array(
                'Image',
                'video'
            )
        ));
        
        $venue = $this->Venue->find('first', array(
            'conditions' => array(
                'Venue.id' => $idVenue
            ),
            'fields' => array(
                'Venue.id',
                'Venue.name',
                'Locations.id'
            )
        ));
        
        if(isset($venue['Venue'])){
            $returnData['id']   = $venue['Venue']['id'];
            $returnData['name'] = $venue['Venue']['name'];
        } else {
            $returnData['id']   = null;
            $returnData['name'] = null;
        }
        
        if($venue){
            $location = $this->Location->find('first', array(
                'conditions' => array(
                    'Location.id' => $venue['Locations']['id']
                )
            ));
            $returnData['flag']     = $location['Countries']['flag'];
            $returnData['Country']  = $location['Countries']['name'];
            $returnData['State']    = $location['States']['name'];
            $returnData['Citiy']    = $location['Cities']['name'];
        }
        return $returnData;
        
    }
    
    /*
     * getOpponentRecord method
     * @param string $corner
     * @param string $idFight
     * @return array
     */
    public function getOpponentRecord($corner, $idFight){
        
        if($corner == 1){
            $corner = 2;
        } else {
            $corner = 1;
        }
        
        $identity = $this->FightIdentity->find('first', array(
            'conditions' => array(
                'FightIdentity.fights_id' => $idFight,
                'FightIdentity.corner' => $corner
            )
        ));
        
        $returnData['id']           = $identity['Identities']['id'];
        $returnData['name']         = $identity['Identities']['name'];
        $returnData['last_name']    = $identity['Identities']['last_name'];
        
        return $returnData;
        
    }
    
    /*
     * showTitles method
     * @param string $idFight
     * @return void
     */
    public function showTitles($idFight = null){
        
        $this->layout = 'ajax';
        
        $titlesDetails = array();
        
        $titles = $this->FightsTitle->find('all', array(
            'conditions' => array(
                'FightsTitle.fights_id' => $idFight
            ),
            'fields' => array(
                'Titles.id',
                'Titles.name',
                'Titles.description'
            )
        ));
        
        foreach ($titles as $title){

            $titlesHistory = $this->FightsTitle->find('all', array(
                'conditions' => array(
                    'Titles.id' => $title['Titles']['id']
                )
            ));
            
            $newTitlesHistory = array();
            foreach ($titlesHistory as $titleHistory){
                $dateEvent = $this->getDateEvent($titleHistory['Fights']['events_id']);
                $titleHistory['Event'] = isset($dateEvent['Event']) ? $dateEvent['Event'] : null;
                $newTitlesHistory[] = $titleHistory;
            }
            
            $titlesDetails[] = $newTitlesHistory;
        }
        
        $this->set('titles', $titles);
        $this->set('titlesDetails', $titlesDetails);
    }
    
    /*
     * showNotes method
     * @param string $idIdentity
     * @return void
     */
    public function showNotes($idIdentity = null , $page = 1){
        $this->layout = 'ajax';
        if($idIdentity != null){
            $countNotes = $this->IdentityNote->find('count', array(
                'conditions' => array('IdentityNote.identities_id' => $idIdentity)
            ));
                        
            $notes = $this->IdentityNote->find('all', array(
                'conditions' => array(
                    'IdentityNote.identities_id' => $idIdentity
                )
            ));
            
            /************/
            $this->Paginator->settings = array(
                
                'IdentityNote' => array(
                    'conditions' => array('IdentityNote.identities_id' => $idIdentity),
                    'order' => array('IdentityNote.identities_id DESC'),
                    'order' => 'IdentityNote.identities_id DESC',
                    'limit' => 10
                )
            );
            /************/
            
            $this->set('countNotes', $countNotes);
            $this->set('idIdentity', $idIdentity);
            $this->set('notes', $notes);
            $this->set('page', 1);
            
            $data = $this->paginate('IdentityNote');
            $this->set(compact('data'));
        }
    }
    
    /*
     * showContacts method
     * @param string $idIdentity
     * @return void
     */
    public function showContacts($idIdentity = null){
        $this->layout = 'ajax';
        if($idIdentity != null){
            $contacts = $this->IdentityContact->find('all', array(
                'conditions' => array(
                    'IdentityContact.identities_id' => $idIdentity
                )
            ));
            $this->set('contacts', $contacts);
        }
    }
    
    public function showArticles($idIdentity = null){
        $this->layout = 'ajax';
        if($idIdentity != null){
            $articles = $this->Article->find(
                'all', array(
                    'conditions' => array(
                       'Article.identities_id' => $idIdentity
                    )
                )
            ); 
            //return $articles;
            $this->set('articles', $articles);
        }
    }
    
    /*
     * showRelationships method
     * @param string $idIdentity
     * @return void
     */
    public function showRelationships($idIdentity = null){
        $this->layout = 'ajax';
        if($idIdentity != null){
            $relationships = $this->IdentityRelationship->find('all', array(
                'conditions' => array(
                    'IdentityRelationship.identities_id' => $idIdentity
                )
            ));
            $this->set('relationships', $relationships);
        }
    }
    
    /*
     * getDateEvent method
     * @param string $idEvent
     * @return void
     */
    function getDateEvent($idEvent){
        $event = $this->Event->find('first', array(
            'conditions' => array(
                'Event.id' => $idEvent
            ),
            'fields' => array(
                'Event.date'
            ),
            'recursive' => -1
        ));
        return $event;
    }
    
    /*
     * countJobs method
     * @param string $idFight
     * @return bool
     */
    function getCountJobs($idIdentity){
        
        $countFightJob = $this->FightDatum->find('count', array(
            'conditions' => array(
                'OR' => array(
                    'FightDatum.referee'        => $idIdentity,
                    'FightDatum.ring_announcer' => $idIdentity,
                    'FightDatum.judge_1'        => $idIdentity,
                    'FightDatum.judge_2'        => $idIdentity,
                    'FightDatum.judge_3'        => $idIdentity,
                    'FightDatum.inspector'      => $idIdentity,
                    'FightDatum.timekeeper'     => $idIdentity,
                )
            )
        ));
        
        $countEventJobs = $this->FighterJob->find('count', array(
            'conditions' => array(
                'FighterJob.identities_id' => $idIdentity
            )
        ));
        
        $countEventsJobs = $this->EventsJob->find('count', array(
            'conditions' => array(
                'EventsJob.identities_id' => $idIdentity
            )
        ));
        
        if( ($countFightJob) || ($countEventJobs) || ($countEventsJobs) ){
            return true;
        } else {
            return false;
        }

    }
    
    /*
     * showJobs method
     * @param string $idIdentity
     * @return void
     */
    function showJobs($idIdentity){
        $this->layout = 'ajax';

        
        $fighterJobs = $this->FighterJob->find(
                'all', array(
                    'conditions' => array(
                        'FighterJob.identities_id' => $idIdentity
                    ),
                    'fields' => array(
                        'Fights.id',
                        'Fights.title',
                        'FighterJob.corner',
                        'Jobs.id',
                        'Jobs.name'                       
                    )
                )
            );
                    
            $referee = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.referee' => $idIdentity
                    ),
                    'fields' => array(
                        'Fights.id',
                        'Fights.title'
                    )
                )
            );
            
            $ringAnnouncer = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.ring_announcer' => $idIdentity
                    ),
                    'fields' => array(
                        'Fights.id',
                        'Fights.title'
                    )
                )
            );
            
            $judge = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'OR' => array(
                            'judge_1' => $idIdentity,
                            'judge_2' => $idIdentity,
                            'judge_3' => $idIdentity
                        )
                    ),
                    'fields' => array(
                        'Fights.id',
                        'Fights.title'
                    )
                )
            );
            
            $inspector = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.inspector' => $idIdentity
                    ),
                    'fields' => array(
                        'Fights.id',
                        'Fights.title'
                    )
                )
            );
            
            $timekeeper = $this->FightDatum->find(
                'all', array(
                    'conditions' => array(
                        'FightDatum.timekeeper' => $idIdentity
                    ),
                    'fields' => array(
                        'Fights.id',
                        'Fights.title'
                    )
                )
            );
            
            $eventJobs = $this->EventsJob->find('all', array(
                'conditions' => array(
                    'EventsJob.identities_id' => $idIdentity
                ),
                'fields' => array(
                    'EventsJob.type',
                    'Events.id',
                    'Events.name',
                    'Events.date'
                )
            ));

            /*
             Sort by event so the jobs with newer events show up first
            */
            function compareDatesEvents($a, $b) 
            {
                return (string)$a['Events']['date'] <  (string)$b['Events']['date'];
            }   
            usort($eventJobs, 'compareDatesEvents');
                        
            $this->set('fighterJobs', $fighterJobs);
            $this->set('referee', $referee);
            $this->set('ringAnnouncer', $ringAnnouncer);
            $this->set('judge', $judge);
            $this->set('inspector', $inspector);
            $this->set('timekeeper', $timekeeper);
            $this->set('eventJobs', $eventJobs);
        
    }
    
    public function getConerIdentity($idIdentity){
        
        $fight = $this->Identity->find('first', array(
            'conditions' => array(
                'Identity.id' => $idIdentity
            ),
            'fields'=> array(
                'Identity.name'
            )
        ));
        
        return $fight;
    }


    /*
     * getLocation method
     * @param string $idLocation
     * @return array
     */
    function getLocation($idLocation = null){
        $location = $this->Location->find('first', array(
            'conditions' => array(
                'Location.id' => $idLocation
            )
        ));
        return $location;
    }
    
    /*
     * deleteAccent method
     * @param string $keyword
     * @return string
     */
    public function deleteAccent($keyword = null){
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$keyword);
        return $texto;
    }
    
}
