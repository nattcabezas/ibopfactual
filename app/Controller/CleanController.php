<?php
App::uses('AppController','Controller');
/**
 * Clean Controller
 *
 
 *
 * @property SessionComponent $Session
 */
class CleanController extends AppController{
      /**
 * Components
 *
 * @var array
 */
    public $components= array('Session');
    public $uses = array( 'Identity','Fight','Venue','Event');       
    public function ibopadmin_index(){
        
        
    }
     /**
 * ibopadmin_clean method
      * @param $type establece el valor nulo a la funcion
 * actualiza campos nulos en las tablas de identidades para identificarlos para su posterior edicion
 * @return void
 */
    public function ibopadmin_clean($type = null){
        if($type != null){
            switch ($type) {
                case 1:                   
                    if($this->Identity->query('UPDATE `identities` SET `name` = "AAA", `last_name` = "AAA" WHERE `name` = "" AND `last_name` = ""')){
                        $this->Session->setFlash(__('The Clean of Persons has been executed.'), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Clean of Persons could not be executed.'), 'alert-danger');
                    }
                    break;                    
                case 2:
                   if($this->Fight->query('UPDATE `fights` SET `title` = "AAA" WHERE `title` = ""')){
                       $this->Session->setFlash(__('The Clean of Fights has been executed.'), 'alert-success');
                   }else{
                       $this->Session->setFlash(__('The Clean of Fights could not be executed.'), 'alert-danger');
                   }
                    break;
                case 3:
                   if( $this->Venue->query('UPDATE `venues` SET `name` = "AAA" WHERE `name` = ""')){
                       $this->Session->setFlash(__('The Clean of Venues has been executed.'), 'alert-success');
                   }else{
                       $this->Session->setFlash(__('The Clean of Venues could not be executed.'), 'alert-danger');
                   }
                    break;
                case 4: 
                    if($this->Event->query('UPDATE `events` SET `name` = "AAA" WHERE `name` = ""')){
                        $this->Session->setFlash(__('The Clean of Events has been executed.'), 'alert-success');
                    }else{
                       $this->Session->setFlash(__('The Clean of Events could not be executed.'), 'alert-danger'); 
                    }
                    break;
            }
        }
        return $this->redirect(array('action' => 'index'));
    }
}
