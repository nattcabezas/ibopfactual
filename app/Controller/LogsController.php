<?php
App::uses('AppController', 'Controller');
/**
 * Logs Controller
 *
 * @property Log $Log
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
        public $uses = array('Log', 'User');
        
/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Log->recursive = 0;
                $this->Paginator->settings = array(
                    'order' => array(
                        'Log.date' => 'DESC'
                    )
                );
		$this->set('logs', $this->Paginator->paginate());
	}
        
        public function ibopadmin_search($keywork = null, $page = 1){
            if($keywork != null){
                $keywork = base64_decode($keywork);
                
                $logs = $this->Log->find('all', array(
                    'conditions' => array(
                        'Log.description LIKE' => '%' . $keywork . '%' 
                    ),
                    'order' => array(
                        'Log.date' => 'DESC'
                    ),
                    'limit' => 50,
                    'page' => $page
                ));
                
                $this->set('keywork', $keywork);
                $this->set('logs', $logs);
                $this->set('page', $page);
            }
        }
        
        public function ibopadmin_userActivity()
        {
            $users = $this->User->find('all', array(
                'order' => array(
                    'User.name' => 'ASC'
                )
            ));
            $this->set('users', $users);
        }
        
        public function ibopadmin_individualActivity($userID = null, $name = null)
        {
            $id = base64_decode($userID);
            
            date_default_timezone_set('America/New_York');
            $start_date = date("Y-m-d 00:00:00");
            $end_date = date("Y-m-d 23:59:59");
            
            $dailyLogsCreate = $this->Log->find('all', array(
                'conditions' => array(
                    'Log.users_id ' => $id,
                    'Log.date between ? and ?' => array($start_date, $end_date),
                    'Log.modification_type ' => "CREATE"
                ),            
                'fields' => array(
                    'Log.table_name'
                )
            ));
            
            $dailyLogsUpdate = $this->Log->find('all', array(
                'conditions' => array(
                    'Log.users_id ' => $id,
                    'Log.date between ? and ?' => array($start_date, $end_date),
                    'Log.modification_type ' => "UPDATE"
                ),            
                'fields' => array(
                    'Log.table_name'
                )
            ));
            
            
            $this->Paginator->settings = array(
                'conditions' => array('Log.users_id ' => $id ),
                'order' => array('Log.date' => 'DESC'),
                'order' => 'Log.date DESC',
                'limit' => 10
            );
            
            $this->set('dailyLogs', $dailyLogsCreate);
            $this->set('dailyLogsUpdate', $dailyLogsUpdate);
            $this->set('name', $name);
            $this->set('logs', $this->Paginator->paginate());
            $this->set('userID', $id);
        }
        
        public function ibopadmin_queryDateRange($userID = null, $startDate = null, $endDate = null, $modification = null)
        {
            $this->layout = 'ajax';
            //date_default_timezone_set('America/New_York');
            
            /*if (strcmp($startDate, $endDate) == 0 )
            {*/
                //date_default_timezone_set('America/New_York');
                $newStartDate = new DateTime($startDate, new DateTimeZone('America/New_York'));
                $newStartDate->setTime(00, 00, 00);
                $startDate = $newStartDate->format('Y-m-d H:i:s');
                                
                //date_default_timezone_set('America/New_York');
                $newEndDate = new DateTime($endDate, new DateTimeZone('America/New_York'));
                $newEndDate->setTime(23, 59, 59);
                $endDate = $newEndDate->format('Y-m-d H:i:s');
                
             //}
            $rangedLogs = "";
            if (strcmp($modification, "CREATE") == 0 )
            {
                $rangedLogs = $this->Log->find('all', array(
                    'conditions' => array(
                        'Log.users_id ' => $userID,
                        'Log.date between ? and ?' => array($startDate, $endDate),
                        'Log.modification_type ' => "CREATE"
                    ),            
                    'fields' => array(
                        'Log.table_name'
                    )
                ));
            }
            else if (strcmp($modification, "UPDATE") == 0 )
            {
                $rangedLogs = $this->Log->find('all', array(
                    'conditions' => array(
                        'Log.users_id ' => $userID,
                        'Log.date between ? and ?' => array($startDate, $endDate),
                        'Log.modification_type ' => "UPDATE"
                    ),            
                    'fields' => array(
                        'Log.table_name'
                    )
                ));  
            }
                
            $this->set('data', $rangedLogs);
        }
        
        public function ibopadmin_completeReport($userID = null, $startDate = null, $endDate = null, $modification = null)
        {
            
        }
}
