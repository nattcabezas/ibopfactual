<?php
App::uses('AppController', 'Controller');
/**
 * Venues Controller
 *
 * @property Venue $Venue
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class VenuesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components 	= array('Paginator', 'Session');
	public $uses		= array('Venue', 'Source', 'Location', 'Country', 'State', 'City', 'Image', 'VenuesImage', 'Video', 'VenuesVideo', 'VenueContact', 'Event', 'Note', 'VenuesNote'); 
        
/**
 * index method
 *
 * @return void
 */
        public function index($url = null){
            if($url != null){
                list($id, $name) = explode('-', $url);
                if(is_numeric($id)){
                    
                    $this->Venue->unbindModel(array(
                        'belongsTo' => array(
                            'Sources'
                        ),
                        'hasAndBelongsToMany' => array(
                            'Image',
                            'Video'
                        )
                    ));
                    
                    $this->Venue->bindModel(array(
                        'hasMany' => array(
                            'VenuesImage' => array(
                                'className'     => 'VenuesImage',
                                'foreignKey'    => 'venues_id',
                                'order'         => array(
                                    'VenuesImage.principal' => 'DESC'
                                ),
                                'limit' => 1
                            )
                        )
                    ));
                    
                    $venue = $this->Venue->find('first', array(
                        'conditions' => array(
                            'Venue.id' => $id
                        ),
                        'recursive' => 2
                    ));
                    
                    $location = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $venue['Locations']['id']
                        )
                    ));
                    
                    $countImages = $this->VenuesImage->find('count', array(
                        'conditions' => array(
                            'VenuesImage.venues_id' => $id
                        )
                    ));
                    
                    
                    
                    $this->set('id', $id);
                    $this->set('venue', $venue);
                    $this->set('location', $location);
                    $this->set('countImages', $countImages);
                    
                }
            }
        }
        
/**
 * showImages method
 *
 * @param stream $idVenue
 * @return void
 */
        public function getImagesVenues($idVenue = null){
            $this->layout = 'ajax';
            $images = $this->VenuesImage->find('all', array(
                    'conditions' => array(
                        'VenuesImage.venues_id' => $idVenue
                    )
                ));
                $this->set('images', $images);
            }
        
        
        
       
        
/**
 * showEvents method
 *
 * @param stream $idVenue
 * @return void
 */
        public function showEvents($idVenue = null){
            $this->layout = 'ajax';
            if($idVenue != null){
                $events = $this->Event->find('all', array(
                    'conditions' => array(
                        'Event.venues_id' => $idVenue
                    ),
                    'fields' => array(
                        'Event.id',
                        'Event.name',
                        'Event.date'
                    ),
                    'recursive' => -1
                ));
                $this->set('events', $events);
            }
        }
        
/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Venue->recursive = 0;
                $this->Paginator->settings = array(
                    'order' => array(
                        'Venue.id' => 'DESC'
                    )
                );
		$this->set('venues', $this->Paginator->paginate());
                
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->Venue->create();
			if ($this->Venue->save($this->request->data)) {
				$this->Session->setFlash(__('The venue has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'edit/' . base64_encode($this->Venue->getLastInsertId())));
			} else {
				$this->Session->setFlash(__('The venue could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
		$sources = $this->Source->find('list');
		$this->set(compact('sources'));
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->Venue->exists($id)) {
                    $this->Session->setFlash(__('Invalid venue', true), 'alert-danger');
                    return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Venue->save($this->request->data)) {
                            $this->Session->setFlash(__('The venue has been saved.', true), 'alert-success');
                            return $this->redirect(array('action' => 'edit/' . base64_encode($id)));
			} else {
                            $this->Session->setFlash(__('The venue could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
                    $options = array('conditions' => array('Venue.' . $this->Venue->primaryKey => $id));
                    $this->request->data = $this->Venue->find('first', $options);
		}
                
                $venueContact = $this->VenueContact->find(
                    'all', array(
                        'conditions' => array(
                            'VenueContact.venues_id' => $id
                        )
                    )
                );
                 $options = array(
                    'conditions' => array(
                        'Venue.' . $this->Event->primaryKey => $id
                    )
                );
		$sources = $this->Source->find('list');
		$this->set(compact('sources'));
                $this->set('idVenue', $id);
                $this->set('venueContact', $venueContact);
                $this->set('venueName', $this->getName($id));
		$this->set('venue', $this->Venue->find('first', $options));
	}

/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($id = null) {
		$this->Venue->id = $id;
		if (!$this->Venue->exists()) {
			throw new NotFoundException(__('Invalid venue'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Venue->delete()) {
			$this->Session->setFlash(__('The venue has been deleted.'));
		} else {
			$this->Session->setFlash(__('The venue could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * ibopadmin_saveContact method
 * 
 * @return void
 */
        public function ibopadmin_saveContact(){
            if($this->request->is('post')){
                if($this->VenueContact->save($this->request->data)){
                    $this->Session->setFlash(__('The Contact has been saved.', true), 'alert-success');
                    return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('VenueContact.venues_id'))));
                } else {
                    $this->Session->setFlash(__('The venue could not be saved. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('VenueContact.venues_id'))));
                }
            }
            return $this->redirect(array('action' => 'index'));
        }
        
        /*
         This method show a list of venues based
         * on its geographical location to be assigned
         * to an event.
        */
        
        public function ibopadmin_assign_to_event($location= null, $page = 1)
        {            
            if( isset($this->request->data['Location']['cities_id'])){
                $venues = $this->Location->find('all', array(
                    'conditions' => array(
                        'Countries.id' => $this->request->data['Location']['countries_id'],
                        'States.id' => $this->request->data['Location']['states_id'],
                        'Cities.id' => $this->request->data['Location']['cities_id'],
                        'Venue.id !=' => null
                    )
                ));                    
            }elseif( isset($this->request->data['Location']['states_id']) ){
                $venues = $this->Location->find('all', array(
                    'conditions' => array(
                        'Countries.id' => $this->request->data['Location']['countries_id'],
                        'States.id' => $this->request->data['Location']['states_id'],
                        'Venue.id !=' => null
                    )
                ));                   
            } else{
                $venues = $this->Location->find('all', array(
                    'conditions' => array(
                        'Countries.id' => $this->request->data['Location']['countries_id'],
                        'Venue.id !=' => null 
                    )
                ));
           }
           $this->set('idEvent', $this->request->data['Location']['idEvent']);
           $this->set('venues', $venues);
           $this->set('page', $page);
        }
        
/**
 * ibopadmin_search method
 * 
 * @return void
 */
        public function ibopadmin_search( $location= null, $page = 1){
            
            if(($location == null) && ($this->request->is('post'))){
                $postData = $this->request->data; 
                $locationData = "";
                if((isset($postData['Location']['cities_id'])) && ($postData['Location']['cities_id'] != null) ){
                    $locationData = $postData['Location']['cities_id'].'-'.$postData['Location']['states_id'].'-'.$postData['Location']['countries_id'];
                    return $this->redirect(array('action' => 'search/' . base64_encode($locationData)));
                } elseif((isset($postData['Location']['states_id'])) && ($postData['Location']['states_id'] != null) ){
                    $locationData = '0-'.$postData['Location']['states_id'].'-'.$postData['Location']['countries_id'];
                    return $this->redirect(array('action' => 'search/' . base64_encode($locationData)));
                } elseif((isset($postData['Location']['countries_id'])) && ($postData['Location']['countries_id'] != null) ){
                    $locationData = '0-0-'.$postData['Location']['countries_id'];
                    return $this->redirect(array('action' => 'search/' . base64_encode($locationData)));
                } else {
                    $this->Session->setFlash(__('Please write search parameters. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'index/'));
                }
                
            } else {
               
                list($cityId, $stateId, $countryId) = explode('-', base64_decode($location));                
                if( $cityId != 0 ){
                    $venues = $this->Location->find('all', array(
                        'conditions' => array(
                            'Countries.id' => $countryId,
                            'States.id' => $stateId,        
                            'Cities.id' => $cityId,
                            'Venue.id !=' => null
                        ),
                        'page' => $page,
                        'limit' => 20
                    ));                    
                }elseif( $stateId != 0){
                    $venues = $this->Location->find('all', array(
                        'conditions' => array(
                            'Countries.id' => $countryId,
                            'States.id' => $stateId,
                            'Venue.id !=' => null
                        ),
                        'page' => $page,
                        'limit' => 20
                    ));                   
                } else{
                    $venues = $this->Location->find('all', array(
                        'conditions' => array(
                            'Countries.id' => $countryId,
                            'Venue.id !=' => null 
                        ),
                        'page' => $page,
                        'limit' => 20
                    ));
                }  
                
                $this->set('venues', $venues);
                $this->set('page', $page);
                $this->set('location', $location);
                
            }
        }
        
/**
 * ibopadmin_deleteContact method
 * 
 * @return void
 */
        public function ibopadmin_deleteContact(){
            if($this->request->is('post')){
                $this->VenueContact->id = $this->request->data('VenueContact.id');
                if (!$this->VenueContact->exists()) {
                    $this->Session->setFlash(__('Invalid Contact.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'index'));
		}
                if ($this->VenueContact->delete()) {
                    $this->Session->setFlash(__('The Contact has been deleted.', true), 'alert-success');
                    return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('VenueContact.venues_id'))));
                } else {
                    $this->Session->setFlash(__('The Contact could not be deleted. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('VenueContact.venues_id'))));
                }
            }
            return $this->redirect(array('action' => 'index'));
        }
        
	/**
	 * funcion para administrar las locaciones 
	 * @param  intiger $idVenue
	 * @return void
	 */
	public function ibopadmin_location($idVenue = null){
		$idVenue 	= base64_decode($idVenue);
		$location 	= $this->Venue->find(
			'first', array(
				'conditions' => array(
					'Venue.id' => $idVenue
				),
				'fields' => array(
					'Locations.id',
					'Locations.countries_id',
					'Locations.states_id',
					'Locations.cities_id'
				)
			)
		);
		if($this->request->is('post')){
			if( $this->Location->save($this->request->data) ){
				if($location['Locations']['id'] == null){
					$venueUpdate['Venue']['id'] 		= $idVenue;
					$venueUpdate['Venue']['locations_id']	= $this->Location->getLastInsertId();
					if($this->Venue->save($venueUpdate)){
						$this->Session->setFlash(__('The location has been saved.', true), 'alert-success');
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('The location could not be saved in the venue. Please, try again.', true), 'alert-danger');
					}
				}
				$this->Session->setFlash(__('The location has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The location could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));
                $this->set('idVenue', $idVenue);
		$this->set('location', $location);
                $this->set('venueName', $this->getName($idVenue));
	}
        
	/**
	 * obtiene los estados del pais
	 * @param intiger default option
	 * @return void       
	 */
	public function ibopadmin_getStates($default = 0){
		$this->layout = 'ajax';
		if ($this->request->is('post')) {
			$states = $this->State->find('list', array(
				'conditions' => array(
					'State.countries_id' => $this->request->data('countries_id')
				),'order' => array(
                                    'State.name' =>'ASC'
                                )
			));
			$this->set('states', $states);
			$this->set('default', $default);
		}
	}

	/**
	 * obtiene las ciudades del estado
	 * @param intiger default option
	 * @return void       
	 */
	public function ibopadmin_getCities($default = 0){
		$this->layout = 'ajax';
		if ($this->request->is('post')) {
			$cities = $this->City->find('list', array(
				'conditions' => array(
					'City.states_id' => $this->request->data('states_id')
				), 'order'=>array(
                                    'City.name'=>'ASC'
                                )
			));
			$this->set('cities', $cities);
			$this->set('default', $default);
		}
	}

	/**
	 * lista de imagenes 
	 * @param  intiger $idVenue 
	 * @return void
	 */
	public function ibopadmin_images($idVenue = null, $page = 1){
		$idVenue 	= base64_decode($idVenue);
		$images = $this->Image->find(
			'all', array(
				'fields' => array(
					'Image.id',
					'Image.url'
				),
				'order' 	=> array(
					'Image.id' => 'DESC'
				),
				'recursive' => -1,
				'limit' 	=> 9,
				'page' 		=> $page
			)
		);
		if($this->request->is('post')){
			$venusImageExist = $this->VenuesImage->find(
				'count', array(
					'conditions' => array(
						'VenuesImage.venues_id' => $this->request->data('VenuesImage.venues_id'),
						'VenuesImage.images_id' => $this->request->data('VenuesImage.images_id'), 
					)
				)
			);
			if( $venusImageExist > 0 ){
				$this->Session->setFlash(__('The image is already added', true), 'alert-danger');
			} else {
				if( $this->VenuesImage->save($this->request->data) ){
					$this->Session->setFlash(__('The image has been saved in the Venue.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));
		$this->set('images', $images);
		$this->set('idVenue', $idVenue);
		$this->set('page', $page);
                $this->set('venueName', $this->getName($idVenue));
	}

	/**
	 * listado de las imagenes del venues
	 * @param  integer $idVenue 
	 * @param  integer $page    
	 * @return void     
	 */
	public function ibopadmin_venuesImages($idVenue = null, $page = 1){
		$idVenue 	= base64_decode($idVenue);
		if($this->request->is('post')){
                    if($this->VenuesImage->delete($this->request->data('VenuesImage.id'))){
                        $this->Session->setFlash(__('The Image has been deleted', true), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Image could not be deleted. Please, try again.', true), 'alert-danger');
                    }
                    $this->redirect(array('action' => 'venuesImages/' . base64_encode($idVenue) . '/' . $page));
		}
		$images = $this->VenuesImage->find(
			'all', array(
				'conditions' => array(
					'VenuesImage.venues_id' => $idVenue
				),
				'fields' => array(
                                    'VenuesImage.id',
                                    'VenuesImage.principal',
                                    'Images.id',
                                    'Images.url'
				),
				'order' 	=> array(
                                    'VenuesImage.principal' => 'DESC',
                                    'VenuesImage.id' => 'ASC'
				),
				'limit' => 9,
				'page'	=> $page
			)
		);
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));
		$this->set('idVenue', $idVenue);
		$this->set('images', $images);
		$this->set('page', $page);
                $this->set('venueName', $this->getName($idVenue));
	}

	/**
	 * funcion para buscar imagenes
	 * @param  integer $idVenue 
	 * @param  string  $keyword 
	 * @param  integer $page  
	 * @return void        
	 */
	public function ibopadmin_searchImages($idVenue = null, $keyword = null, $page = 1){
		$idVenue 	= base64_decode($idVenue);
		$keyword 	= base64_decode($keyword);
		if($keyword != null){
			$images = $this->Image->find(
				'all', array(
					'conditions' => array(
						'Image.title LIKE' => '%' . $keyword . '%'
					),
					'fields' => array(
						'Image.id',
						'Image.url'
					),
					'order' 	=> array(
						'Image.id' => 'DESC'
					),
					'recursive' => -1,
					'limit' 	=> 9,
					'page' 		=> $page
				)
			);
		} else {
			$images = array();
		}

		if($this->request->is('post')){
			$venusImageExist = $this->VenuesImage->find(
				'count', array(
					'conditions' => array(
						'VenuesImage.venues_id' => $this->request->data('VenuesImage.venues_id'),
						'VenuesImage.images_id' => $this->request->data('VenuesImage.images_id'), 
					)
				)
			);
			if( $venusImageExist > 0 ){
				$this->Session->setFlash(__('The image is already added', true), 'alert-danger');
			} else {
				if( $this->VenuesImage->save($this->request->data) ){
					$this->Session->setFlash(__('The image has been saved in the Venue.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));    
		$this->set('idVenue', $idVenue);
		$this->set('images', $images);
		$this->set('page', $page);
		$this->set('keywordData', $keyword);
                $this->set('venueName', $this->getName($idVenue));
	}
        
        
        /**
        * ibopadmin_imagePrincipal
        * @return void
        */
        public function ibopadmin_imagePrincipal(){
            if($this->request->is('post')){
                if($this->request->data('ImagePrincipal.make_principal') == 1){
                    $countPrincipal = $this->VenuesImage->find(
                        'count', array(
                            'conditions' => array(
                                'VenuesImage.venues_id' => $this->request->data('ImagePrincipal.idVenue'),
                                'VenuesImage.principal' => 1
                            )
                        )
                    );
                    if($countPrincipal == 0){
                        $venuesImage['VenuesImage']['id']           = $this->request->data('ImagePrincipal.id');
                        $venuesImage['VenuesImage']['principal']    = 1;
                        if($this->VenuesImage->save($venuesImage)){
                            $this->Session->setFlash(__('The image has been selected.', true), 'alert-success');
                        } else {
                            $this->Session->setFlash(__('The Image could not be selected. Please, try again.', true), 'alert-danger');
                        }
                    } else {
                        $this->Session->setFlash(__('The Venue already has a main image.', true), 'alert-danger');
                    }
                } else {
                    $venuesImage['VenuesImage']['id']           = $this->request->data('ImagePrincipal.id');
                    $venuesImage['VenuesImage']['principal']    = null;
                    if($this->VenuesImage->save($venuesImage)){
                        $this->Session->setFlash(__('The image has been unselected.', true), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Image could not be unselected. Please, try again.', true), 'alert-danger');
                    }
                }
                return $this->redirect(array('action' => 'venuesImages/' . base64_encode($this->request->data('ImagePrincipal.idVenue')) . '/'. $this->request->data('ImagePrincipal.page')));
            } else {
                return $this->redirect(array('action' => 'index'));
            }
        }
        
          /**
	 * funcion que lista las notas del evento
	 * @param  integer $idVenue 
	 * @param  integer $page    
	 * @return void           
	 */
	public function ibopadmin_venuesNotes($idVenue = null, $page = 1){
		$idVenue 	= base64_decode($idVenue);

		if($this->request->is('post')){
			if($this->VenuesNote->delete($this->request->data('VenuesNote.id'))){
                            $this->Session->setFlash(__('The note has been deleted', true), 'alert-success');
                            $this->redirect(array('action' => 'venuesNotes/' . base64_encode($idVenue). '/' . $page));
			} else {
                            $this->Session->setFlash(__('The note could not be deleted. Please, try again.', true), 'alert-danger');
                            $this->redirect(array('action' => 'venuesNotes/' . base64_encode($idVenue). '/' . $page));
			}
		}

		$notes = $this->VenuesNote->find(
			'all', array(
				'conditions' => array(
					'VenuesNote.venues_id' => $idVenue 
				),
				'fields' => array(
					'VenuesNote.id',
					'Notes.id',
                                        'Notes.title',
                                        'Notes.note'
				),
				'order' => array(
					'VenuesNote.id' => 'DESC',
				),
				'limit' 	=> 10,
				'page'		=> $page
			)
		);
                                
                $options = array('conditions' => array('Venue.' . $this->Venue->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));
                $this->set('venueName', $this->getName($idVenue));
		$this->set('notes', $notes);
		$this->set('idVenue', $idVenue);
		$this->set('page', $page);
	}
        
        /**
	 * funcion que busca los notes
	 * @param  integer $idVenue
	 * @param  string  $keyword 
	 * @param  integer $page    
	 * @return void           
	 */
	public function ibopadmin_searchNotes($idVenue = null, $keyword = null, $page = 1){
		$idVenue 	= base64_decode($idVenue);
		$keyword 	= base64_decode($keyword);

		if($this->request->is('post')){
			$venueNoteExist  = $this->VenuesNote->find(
				'count', array(
					'conditions' => array(
						'VenuesNote.venues_id' => $this->request->data('VenuesNote.venues_id'),
						'VenuesNote.notes_id' => $this->request->data('VenuesNote.notes_id')
					)
				)
			);
			if($venueNoteExist > 0){
				$this->Session->setFlash(__('The note is already added', true), 'alert-danger');
			} else {
				if($this->VenuesNote->save($this->request->data)){
					$this->Session->setFlash(__('The event note has been saved in.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The event note could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}
		
		if($keyword != null){
			$notes = $this->Note->find(
				'all', array(
					'conditions' => array(
						'Note.title LIKE' => '%' . $keyword . '%'
					),
					'fields' => array(
						'Note.id',
						'Note.title',
						'Note.note'
					),
					'order' 	=> array(
						'Note.id' => 'DESC'
					),
					'recursive' => -1,
					'limit' 	=> 10,
					'page' 		=> $page
				)
			);
		} else {
			$notes = array();
		}
                                
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));
                $this->set('venueName', $this->getName($idVenue));                
		$this->set('notes', $notes);
		$this->set('idVenue', $idVenue);
		$this->set('page', $page);
		$this->set('keywordData', $keyword);
	}
        
        
        /**
	 * lista de videos para agregar al venues
	 * @param  integer $idVenue 
	 * @param  integer $page    
	 * @return void           
	 */
	public function ibopadmin_notes($idVenue = null, $page = 1) {
		$idVenue 	= base64_decode($idVenue);

		if($this->request->is('post')){
			$venueNoteExist  = $this->VenuesNote->find(
				'count', array(
					'conditions' => array(
						'VenuesNote.venues_id' => $this->request->data('VenuesNote.venues_id'),
						'VenuesNote.notes_id' => $this->request->data('VenuesNote.notes_id')
					)
				)
			);
			if($venueNoteExist > 0){
				$this->Session->setFlash(__('The note is already added', true), 'alert-danger');
			} else {
				if($this->VenuesNote->save($this->request->data)){
					$this->Session->setFlash(__('The note has been saved in the Venue.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The note could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}

		$notes = $this->Note->find(
			'all', array(
				'fields' => array(
					'Note.id',
					'Note.title',
					'Note.note'
				),
				'order' => array(
					'Note.id' => 'DESC'
				),
				'recursive' => -1,
				'limit' 	=> 10,
				'page'		=> $page
			)
		);
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));    
		$this->set('notes', $notes);
		$this->set('idVenue', $idVenue);
		$this->set('page', $page);
                $this->set('venueName', $this->getName($idVenue));
	} 	
        
        
	/**
	 * lista de videos para agregar al venues
	 * @param  integer $idVenue 
	 * @param  integer $page    
	 * @return void           
	 */
	public function ibopadmin_videos($idVenue = null, $page = 1) {
		$idVenue 	= base64_decode($idVenue);

		if($this->request->is('post')){
			$venueVideoExist  = $this->VenuesVideo->find(
				'count', array(
					'conditions' => array(
						'VenuesVideo.venues_id' => $this->request->data('VenuesVideo.venues_id'),
						'VenuesVideo.videos_id' => $this->request->data('VenuesVideo.videos_id')
					)
				)
			);
			if($venueVideoExist > 0){
				$this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
			} else {
				if($this->VenuesVideo->save($this->request->data)){
					$this->Session->setFlash(__('The video has been saved in the Venue.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The video could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}

		$videos = $this->Video->find(
			'all', array(
				'fields' => array(
					'Video.id',
					'Video.title',
					'Video.mp4'
				),
				'order' => array(
					'Video.id' => 'DESC'
				),
				'recursive' => -1,
				'limit' 	=> 10,
				'page'		=> $page
			)
		);
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options));    
		$this->set('videos', $videos);
		$this->set('idVenue', $idVenue);
		$this->set('page', $page);
                $this->set('venueName', $this->getName($idVenue));
	} 	

	/**
	 * lista de videos del venue
	 * @param  integer $idVenue 
	 * @param  integer $page    
	 * @return void           
	 */
	public function ibopadmin_venuesVideos($idVenue = null, $page = 1){
		$idVenue 	= base64_decode($idVenue);

		if($this->request->is('post')){
			if($this->VenuesVideo->delete($this->request->data('VenuesVideo.id'))){
                            $this->Session->setFlash(__('The video has been deleted', true), 'alert-success');
			} else {
                            $this->Session->setFlash(__('The video could not be deleted. Please, try again.', true), 'alert-danger');
			}
                        $this->redirect(array('action' => 'venuesVideos/' . base64_encode($idVenue) . '/' . $page));
		}

		$videos = $this->VenuesVideo->find(
			'all', array(
				'conditions' => array(
					'VenuesVideo.venues_id' => $idVenue 
				),
				'fields' => array(
					'VenuesVideo.id',
					'Videos.id',
					'Videos.title',
					'Videos.mp4'
				),
				'order' => array(
					'VenuesVideo.id' => 'DESC',
				),
				'limit' 	=> 10,
				'page'		=> $page
			)
		);
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options)); 
		$this->set('videos', $videos);
		$this->set('idVenue', $idVenue);
		$this->set('page', $page);
                $this->set('venueName', $this->getName($idVenue));
	}

	/**
	 * funcion que busca los videos
	 * @param  integer $idVenue 
	 * @param  string  $keyword 
	 * @param  integer $page    
	 * @return void           
	 */
	public function ibopadmin_searchVideos($idVenue = null, $keyword = null, $page = 1){
		$idVenue 	= base64_decode($idVenue);
		$keyword 	= base64_decode($keyword);

		if($this->request->is('post')){
			$venueVideoExist  = $this->VenuesVideo->find(
				'count', array(
					'conditions' => array(
						'VenuesVideo.venues_id' => $this->request->data('VenuesVideo.venues_id'),
						'VenuesVideo.videos_id' => $this->request->data('VenuesVideo.videos_id')
					)
				)
			);
			if($venueVideoExist > 0){
				$this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
			} else {
				if($this->VenuesVideo->save($this->request->data)){
					$this->Session->setFlash(__('The video has been saved in the Venue.', true), 'alert-success');
				} else {
					$this->Session->setFlash(__('The video could not be saved. Please, try again.', true), 'alert-danger');
				}
			}
		}

		if($keyword != null){
			$videos = $this->Video->find(
				'all', array(
					'conditions' => array(
						'Video.title LIKE' => '%' . $keyword . '%'
					),
					'fields' => array(
						'Video.id',
						'Video.title',
						'Video.mp4'
					),
					'order' 	=> array(
						'Video.id' => 'DESC'
					),
					'recursive' => -1,
					'limit' 	=> 10,
					'page' 		=> $page
				)
			);
		} else {
			$videos = array();
		}
                $options = array('conditions' => array('Venue.' . $this->Event->primaryKey => $idVenue));
                $this->set('venue', $this->Venue->find('first', $options)); 
		$this->set('videos', $videos);
		$this->set('idVenue', $idVenue);
		$this->set('page', $page);
		$this->set('keywordData', $keyword);
                $this->set('venueName', $this->getName($idVenue));
	}
        
        /**
	 * funcion que busca los videos
	 * @param  string  $name    
	 * @return void           
	 */
        public function ibopadmin_getVenue($name = null){
            $this->layout = 'ajax';
            
            $this->Venue->unbindModel(
                array(
                    'belongsTo' => array(
                        'Sources'
                    ),
                    'hasAndBelongsToMany' => array(
                        'Image',
                        'Video'
                    )
                )
            );
            
            $this->Location->virtualFields = array(
                'full_name' => "CONCAT(Venue.name, ' ', Venue.complete_name)"
            );
            
            $venues = $this->Location->find(
                'all', array(
                    'conditions' => array(
                        'Location.full_name LIKE' => '%' . $name . '%'
                    ),
                    'fields' => array(
                        'Venue.id',
                        'Venue.name',
                        'Venue.complete_name',
                        'Countries.name',
                        'States.name',
                        'States.id',
                        'Cities.name',
                        'Cities.id'
                    ),
                    'limit' => 50
                )
            );
            
            $venueNullFlag = 0;//if venue is null, value is one, otherwise 0
            if ($venues==null)
            {
                //debug("Es nulo");
                $nullVenues = $this->Venue->find(
                    'all', array(
                        'conditions' => array(
                            'Venue.name LIKE' => '%' . $name . '%'
                        ),
                        'fields' => array(
                            'Venue.id',
                            'Venue.name'
                        ),
                        'limit' => 50
                    )
                );
                $venues = $nullVenues;
                $venueNullFlag = 1;
            }
            
            
            //debug($venues);
            //debug($nullVenues);
            $newVenues = array();
            
            if ($venueNullFlag==0)
            {
                foreach ($venues as $venue) {
                    $newVenue['id']         = $venue['Venue']['id'];
                    $newVenue['label']      = str_replace('"', '\"', $venue['Venue']['name']) . ' ' . str_replace('"', '\"', $venue['Venue']['complete_name']);
                    $newVenue['Country']    = utf8_encode($venue['Countries']['name']);
                    $newVenue['State']      = utf8_encode($venue['States']['name']);
                    $newVenue['City']       = utf8_encode($venue['Cities']['name']);
                    $newVenue['CityID']       = $venue['Cities']['id'];
                    $newVenue['StateID']       = $venue['States']['id'];
                    
                    $newVenues[]            = $newVenue;
                }
            }
            else
            {
                //debug('Es nulo');
                foreach ($nullVenues as $venue) {
                    $newVenue['id']         = $venue['Venue']['id'];
                    $newVenue['label']      = $venue['Venue']['name'];
                    $newVenue['Country']    = 'No country';
                    $newVenue['State']      = 'No state';
                    $newVenue['City']       = 'No city';
                    $newVenues[]            = $newVenue;
                }
                
            }
            $this->set('venues', $newVenues);
            
        }
        
        
        /**
	 * ibopadmin_listEvents method
	 * @param  string  $id    
	 * @return void           
	 */
        public function ibopadmin_listEvents($id = null){
            if($id != null){
                $id = base64_decode($id);
                
                $events = $this->Event->find('all', array(
                    'conditions' => array(
                        'Event.venues_id' => $id
                    ),
                    'fields' => array(
                        'Event.name',
                        'Event.id'
                    ),
                    'order' => array(
                        'Event.id'  => 'DESC'
                    ),
                    'recursive' => -1
                ));
                
                $this->set('venueName', $this->getName($id));
                $this->set('idVenue', $id);
                $this->set('events', $events);
            }
        }
        
        /**
	 * getName method
	 * @param  string  $id    
	 * @return string           
	 */
        public function getName($id = null){
            if($id != null){
                $venue = $this->Venue->find(
                    'first', array(
                        'conditions' => array(
                            'Venue.id' => $id
                        ),
                        'fields' => array(
                            'Venue.name'
                        ),
                        'recursive' => -1
                    )
                );
                return $venue['Venue']['name'];
            }
        }
        public function ibopadmin_lock($id = null, $lock = null){
            $lockPerson['Venue']['id']       = base64_decode($id);
            $lockPerson['Venue']['locked']   = $lock;                        
         if($this->Venue->save($lockPerson)){
                if ($lock == 1){
                    $this->Session->setFlash(__('The register has been locked.', true), 'alert-success');
                }else{
                    $this->Session->setFlash(__('The register has been unlocked.', true), 'alert-success');
                }                
            } else {
                $this->Session->setFlash(__('The status could not be changed.', true), 'alert-danger');
            }            
            return $this->redirect(array('action' => 'edit/' .$id));    
        }
        
}
