<?php

App::uses('AppController', 'Controller');

/**
 * CakePHP CountryController
 * @author rudysibaja <neo.nou@gmail.com>
 */
class CountryController extends AppController {

    public $name = "Country";
    
    public $uses = array('Country');
    
    public $helper	= array('newJsonEncode', 'utf8EncodeArray');
    
    public function ibopadmin_getCountry($name = ""){
        $this->layout = 'ajax';
        $name = base64_decode($name);
        $countries = $this->Country->find(
            'all', array(
                'conditions' => array(
                    'Country.name LIKE' => '%' . $name . '%'
                )
            )
        );
        $newCountries = array();
        foreach($countries as $country) {
              $newCountry['id']        = $country['Country']['id'];
              $newCountry['label']     = utf8_encode($country['Country']['name']);
              $newCountry['flag']      = $country['Country']['flag'];
              $newCountries[]          = $newCountry;
        }
        
        $this->set('countries', $newCountries);
                        
        
    }
    
    public function ibopadmin_getCountryData($id = null){
        $this->layout = 'ajax';
        $country = $this->Country->find(
            'first', array(
                'conditions' => array(
                    'Country.id' => $id
                )
            ) 
        );
        $newCountry['id']        = $country['Country']['id'];
        $newCountry['label']     = utf8_encode($country['Country']['name']);
        $newCountry['flag']      = $country['Country']['flag'];
        $this->set('country', $newCountry);

    }

}   
