<?php
App::uses('AppController', 'Controller');

/**
 * CakePHP backupController
 * @author RudySibaja <neo.nou@gmail.com>
 */
class BackupController extends AppController {

    public $uses = array('User');
    
    public function ibopadmin_index(){
        $this->set('tables', $this->User->query('SHOW TABLES'));
    }

}
