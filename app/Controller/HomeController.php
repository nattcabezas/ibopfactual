<?php
App::uses('AppController', 'Controller');
/**
 * Home Controller
 * pueba
 *
 * @property HomeController 
 * @author rudysibaja <neo.nou@gmail.com>
 * contolador de usuarios
 */
class HomeController extends AppController  {
        
        public $name 	= "Home";
        
        public $uses 	= array(
            'Article', 
            'Event', 
            'Image', 
            'EventsImage', 
            'Locations', 
            'State',
            'Country',
            'FeaturedFight', 
            'FightIdentity', 
            'IdentitiesImage', 
            'FeaturedEvent', 
            'Venue',
            'Location',
            'Quote',
            'Identity'
        );
        
        public $helper	= array('Image');
        
	public function index(){
            $articles = $this->Article->find(
                'all', array(
                    'conditions' => array(
                        'Article.publish' => 1,
                        'Article.date <=' => date('Y-m-d H:i:s')
                    ),
                    'order' => array(
                        'Article.date' => 'DESC'
                    ),
                    'limit' => 5
                )
            );
            
            $this->set('upcomingEvents', $this->getEvents('>', 'ASC'));
            $this->set('pastEvents', $this->getEvents('<', 'DESC'));
            $this->set('featuredFights', $this->getFeaturedFights());
            $this->set('featuredEvent', $this->getFeaturedEvent());
            $this->set('articles', $articles);
            $this->set('randomQuotes', $this->getQuotes());
        }
        
        public function getEvents($mayor = null, $order = null){
            $this->Event->unbindModel(
                array(
                    'hasAndBelongsToMany' => array(
                        'Fight',
                        'PromotionalCompany',
                        'Video',
                        'Image'
                    ),
                    'hasMany' => array(
                        'EventsJob'
                    )
                )
            );
            
            $this->Image->unbindModel(
                array(
                    'hasMany' => array(
                        'FightsImages',
                        'IdentitiesImages',
                        'VenuesImage'
                    )
                )
            );
            
            $this->EventsImage->unbindModel(
                array(
                    'belongsTo' => array(
                        'Events'
                    )
                )
            );
            
            $events = $this->Event->find(
                'all', array(
                    'conditions' => array(
                        'Event.date '.$mayor => date('Y-m-d')
                    ),
                    'order' => array(
                        'Event.date' => $order
                    ),
                    'fields' => array(
                        'Event.id',
                        'Event.name',
                        'Event.description',
                        'Event.locations_id',
                        'Venues.id',
                        'Venues.locations_id',
                        'Venues.name',
                        'Locations.countries_id',
                        'Locations.states_id'
                    ),
                    'recursive' => 2,
                    'limit' => 6
                )
            );
            $newEvents = array();
            foreach ($events as $event){
                if( ($event['Venues']['locations_id'] != "") && ($event['Venues']['name'] != "unknown") ){
                    $venueLocation = $this->Locations->find(
                        'first', array(
                            'conditions' => array(
                                'Locations.id' => $event['Venues']['locations_id']
                            )
                        )
                    );
                    //array_push($event['Venues'], $venueLocation);
                    $state = $this->State->find(
                        'first', array(
                            'conditions' => array(
                                'State.id' => $venueLocation['Locations']['states_id']
                            )
                        )
                    );
                    $event['Venues']['Locations'] = $state;
                    
                 } else if($event['Event']['locations_id'] != ""){
                     
                     $location = $this->Location->find('first', array(
                         'conditions' => array(
                             'Location.id' => $event['Event']['locations_id']
                         )
                     ));
                     
                     $state = $this->State->find(
                        'first', array(
                            'conditions' => array(
                                'State.id' => $location['Location']['states_id']
                            )
                        )
                    );
                    $event['Venues']['Locations'] = $state;
                     
                 } else {
                     if($event['Locations']['states_id'] != null){
                         $state = $this->State->find(
                            'first', array(
                                'conditions' => array(
                                    'State.id' => $event['Locations']['states_id']
                                )
                            )
                        );
                         $event['Locations']['Location'] = $state;
                     }
                 }
                 $newEvents[] = $event;
            }
            
            return $newEvents;
        }

        public function getFeaturedFights(){
            
            $fights = array();
            $featuredFights = $this->FeaturedFight->find('all', array(
                'order' => array(
                    'FeaturedFight.position' => 'ASC'
                ),
                'fields' => array(
                    'Fights.id',
                    'Fights.title',
                    'Fights.events_id',
                    'Fights.description'
                )
            ));
            
            foreach ($featuredFights as $fight) {
                $fight['Event'] = $this->getEvent($fight['Fights']['events_id']);
                $fight['Corners'] = $this->getCorners($fight['Fights']['id']);
                $fights[] = $fight;
            }
            
            return $fights;
        }
        
        public function getCorners($idFight = null){
            
            $this->FightIdentity->unbindModel(array(
                'belongsTo' => array(
                    'Fight'
                )
            ));
            
            $corners = $this->FightIdentity->find('all', array(
                'conditions' => array(
                    'FightIdentity.fights_id' => $idFight
                ),
                'fields' => array(
                    'Identities.id',
                    'Identities.name',
                    'Identities.last_name'
                ),
                'order' => array(
                    'FightIdentity.corner'
                ),
            ));
            
            $newCorners = array();
            foreach ($corners as $corners) {
                
                $image = $this->IdentitiesImage->find('first', array(
                    'conditions' => array(
                        'IdentitiesImage.identities_id' => $corners['Identities']['id']
                    ),
                    'order' => array(
                        'IdentitiesImage.principal' => 'DESC'
                    ),
                    'fields' => array(
                        'Images.id',
                        'Images.title',
                        'Images.url'
                    )
                ));
                if(isset($image['Images'])){
                    $corners['Image'] = $image['Images'];
                } else {
                    $corners['Image'] = null;
                }
                $newCorners[] = $corners;
                
            }
            
            return $newCorners;
            
        }
        
        public function getEvent($idEvent = null){
            
            $event = $this->Event->find('first', array(
                'conditions' => array(
                    'Event.id' => $idEvent
                ),
                'fields' => array(
                    'Event.id',
                    'Event.name',
                    'Event.date'
                )
            ));
            
            return $event['Event'];
            
        }
        
        public function getFeaturedEvent(){
            
            $events = array();
            $featuredEvent = $this->FeaturedEvent->find('all', array(
                'order' => array(
                    'FeaturedEvent.position' => 'ASC'
                ),
                'fields' => array(
                    'Events.id',
                    'Events.name',
                    'Events.description',
                    'Events.date',
                    'Events.venues_id',
                    'Events.locations_id'
                )
            ));
            
            foreach ($featuredEvent as $event) {
                $event['Image'] = $this->getEventImages($event['Events']['id']);
                $event['Location'] = $this->getLocation($event['Events']['venues_id'], $event['Events']['locations_id']);
                $events[] = $event;
            }
            
            return $events;
            
        }
        
        public function getEventImages($idEvent = null){
            $image = $this->EventsImage->find('first', array(
                'conditions' => array(
                    'EventsImage.events_id' => $idEvent
                ),
                'fields' => array(
                    'Images.title',
                    'Images.url'
                )
            ));
            
            if(isset($image['Images'])){
                return $image['Images'];
            } else {
                return $image;
            }
        }
        
        public function getLocation($idVenue = null, $idLocation = null){
            $returnLocation = array();
            if($idVenue != null){
                $location = $this->Venue->find('first', array(
                    'conditions' => array(
                        'Venue.id' => $idVenue
                    )
                ));
                
                $country = $this->Country->find('first', array(
                    'conditions' => array(
                        'Country.id' => $location['Locations']['countries_id']
                    ),
                    'fields' => array(
                        'Country.name',
                        'Country.flag'
                    )
                ));
                
                $state = $this->State->find('first', array(
                    'conditions' => array(
                        'State.id' => $location['Locations']['states_id']
                    ),
                    'fields' => array(
                        'State.name'
                    )
                ));
                
                $returnLocation['country']   = $country['Country']['name'];
                $returnLocation['flag']      = $country['Country']['flag'];
                $returnLocation['State']     = $state['State']['name'];
            } else if($idLocation != null){
                
                $location = $this->Location->find('first', array(
                    'conditions' => array(
                        'Location.id' => $idLocation
                    )
                ));
                
                $returnLocation['country']   = $location['Countries']['name'];
                $returnLocation['flag']      = $location['Countries']['flag'];
                $returnLocation['State']     = $location['States']['name'];
                
            }
            
            return $returnLocation;
        }
        
        public function getQuotes(){
            
            $newQuotes = array();
            $quotes = $this->Quote->find('all', array(
                'order' => 'rand()',
                'limit' => 5
            ));
            
            foreach ($quotes as $quote) {
                
                $newQuote['Quote']['quote']         = $quote['Quote']['quote'];
                $newQuote['Quote']['quotes_by']     = $quote['Quote']['quotes_by'];
                $newQuote['Quote']['note']          = $quote['Quote']['note'];
                $newQuote['Quote']['img']           = $quote['Quote']['img'];
                $newQuote['Identity']['name']       = $quote['Identity']['name'];
                $newQuote['Identity']['last_name']  = $quote['Identity']['last_name'];
                
                if($quote['Identity']['id'] != null){
                    $image = $this->IdentitiesImage->find('first', array(
                        'conditions' => array(
                            'IdentitiesImage.identities_id' => $quote['Identity']['id'],
                            'IdentitiesImage.principal' => 1
                        )
                    ));
                    if($image){
                        $newQuote['Identity']['Images'] = $image['Images']['url']; 
                    } else {
                        $newQuote['Identity']['Images'] = null;
                    }
                } else {
                    $newQuote['Identity']['Images'] = null;
                }
                $newQuotes[] = $newQuote;
            }
            
            return $newQuotes;
        }
        


}