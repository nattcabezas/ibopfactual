<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP CompareController
 * @author rudysibaja
 */
class ComparisonsController extends AppController {
    
    public $name = "Comparisons";
    
    public $uses = array(
        'Identity',
        'IdentitiesImage',
        'Image',
        'Country',
        'State',
        'City',
        'FightIdentity',
        'Fight'
    );
    
    /*
     * index method
     * @param string $firstFighter
     * @param string $secondFighter
     * @return view
     */
    public function index($firstFighter = null, $secondFighter = null) {
        list($firstId, $firstName) = explode('-', $firstFighter);
        
        $this->set('firstFighter', $firstFighter);
        $this->set('secondFighter', $secondFighter);
        
        /** first Fighter data  **/
        $this->set('firstFighterData', $this->getFighter($firstId));
        $this->set('firstFighterPhoto', $this->getPhoto($firstId));
        $this->set('firstFighterRecord', $this->getRecord($firstId));
        
        /** Second Fighter data  **/
        if($secondFighter != null){
            list($secondId, $secondName) = explode('-', $secondFighter);
            
            $this->set('secondFighterData', $this->getFighter($secondId));
            $this->set('secondFighterPhoto', $this->getPhoto($secondId));
            $this->set('secondFighterRecord', $this->getRecord($secondId));
        }
        
        
    }
    
    /*
     * getFighter method
     * @param intiger $id
     * @return array
     */
    public function getFighter($id){
        
        $fighter = $this->Identity->find('first', array(
            'conditions' => array(
                'Identity.id' => $id
            ),
            'fields' => array(
                'Identity.id',
                'Identity.name',
                'Identity.last_name',
                'Identity.height',
                'Identity.gender',
                'Identity.reach',
                'Identity.stance'
            ),
            'recursive' => -1
        ));
        
        return $fighter;
        
    }
    
    /*
     * getPhoto method
     * @param intiger $id
     * @return array
     */
    public function getPhoto($id){
        
        $photo = $this->IdentitiesImage->find('first', array(
            'conditions' => array(
                'IdentitiesImage.identities_id' => $id,
                'IdentitiesImage.principal' => 1
            ),
            'fields' => array(
                'Images.url',
                'Images.title',
                'Images.description'
            )
        ));
        
        return $photo;
    }
    
    /*
     * getRecord method
     * @param string $idIdentity
     * @return array
     */
    public function getRecord($idIdentity = null){
        
        $record = array();
        
        $boxingData = $this->FightIdentity->find('all', array(
            'conditions' => array(
                'FightIdentity.identities_id' => $idIdentity,
                'Fight.types_id' => 1
            ),
            'fields' => array(
                'FightIdentity.corner',
                'Fight.winner',
                'Fight.via',
                'Fight.boxed_roundas'
            )
        ));
        
        $winBoxing      = 0;
        $lostBoxing     = 0;
        $drawBoxing     = 0;
        $roundsBoxing   = 0;
        $totalFights    = 0;
        
        $winKOBoxing    = 0;
        $lostKOBoxing   = 0;
        
        $winNWSBoxing   = 0;
        $lostNWSBoxing  = 0;
        $drawNWSBoxing  = 0;
        
        $winPerBoxing   = 0;
        
        foreach($boxingData as $boxing){
            if($boxing['Fight']['winner'] != 4 ){
                if($boxing['Fight']['winner'] == 3){
                    $drawBoxing++;
                    if($boxing['Fight']['via'] == 'D-NWS'){
                        $drawNWSBoxing++;
                    }
                } else if($boxing['FightIdentity']['corner'] == $boxing['Fight']['winner']) {
                    $winBoxing++;
                    if(($boxing['Fight']['via'] == 'KO') || ($boxing['Fight']['via'] == 'TKO') || ($boxing['Fight']['via'] == 'RTD')){
                        $winKOBoxing++;
                    } else if($boxing['Fight']['via'] == 'NWS'){
                        $winNWSBoxing++;
                    }
                    if( ($boxing['Fight']['via'] == 'KO')  || ($boxing['Fight']['via'] == 'TKO') || ($boxing['Fight']['via'] == 'RTD') ){
                        $winPerBoxing++;
                    }
                } else {
                    $lostBoxing++;
                    if(($boxing['Fight']['via'] == 'KO')  || ($boxing['Fight']['via'] == 'TKO') || ($boxing['Fight']['via'] == 'RTD')){
                        $lostKOBoxing++;
                    } else if($boxing['Fight']['via'] == 'NWS'){
                        $lostNWSBoxing++;
                    }
                }
                
                /*if($boxing['Fight']['via'] != "NC"){
                    $totalFights++;
                }*/
                
            }
            $totalFights++;
            $roundsBoxing = $roundsBoxing + $boxing['Fight']['boxed_roundas']; 
        }
        
        $record['boxing']['is']     = count($boxingData);
        $record['boxing']['won']    = $winBoxing;
        $record['boxing']['lost']   = $lostBoxing;
        $record['boxing']['draw']   = $drawBoxing;
        $record['boxing']['rounds'] = $roundsBoxing;
        $record['boxing']['total']  = $totalFights;
        
        $record['boxing']['win_ko']     = $winKOBoxing;
        $record['boxing']['lost_ko']    = $lostKOBoxing;
        
        $record['boxing']['won_nws']    = $winNWSBoxing;
        $record['boxing']['lost_nws']   = $lostNWSBoxing;
        $record['boxing']['draw_nws']   = $drawNWSBoxing;
        
        $record['boxing']['win_perc']   = $winPerBoxing;
        
        $mmaData = $this->FightIdentity->find('all', array(
            'conditions' => array(
                'FightIdentity.identities_id' => $idIdentity,
                'Fight.types_id' => 2
            ),
            'fields' => array(
                'FightIdentity.corner',
                'Fight.winner',
                'Fight.via',
                'Fight.submission_comments',
                'Fight.boxed_roundas'
            )
        ));
        
        $winMMA      = 0;
        $lostMMA     = 0;
        $drawMMA     = 0;
        $roundsMMA   = 0;
        $totalMMA    = 0;
        
        $winKoMMA    = 0;
        $lostKoMMA   = 0;
        
        //debug($mmaData);die();
        
        foreach($mmaData as $mma){
            if($mma['Fight']['winner'] != 4 ){
                if($mma['Fight']['winner'] == 3){
                    $drawMMA++;
                } else if($mma['FightIdentity']['corner'] == $mma['Fight']['winner']) {
                    $winMMA++;
                    if(($mma['Fight']['via'] == 'STOPPAGE') || ($mma['Fight']['via'] == 'SUBMISSION')){
                        $winKoMMA++;
                    }
                } else {
                    $lostMMA++;
                    if(($mma['Fight']['via'] == 'STOPPAGE') || ($mma['Fight']['via'] == 'SUBMISSION')){
                        $lostKoMMA++;
                    } 
                }
                if($mma['Fight']['via'] != "NC"){
                    $totalMMA++;
                }
                
            }
            $roundsMMA = $roundsMMA + $mma['Fight']['boxed_roundas']; 
        }
        
        $record['mma']['is']     = count($mmaData);
        $record['mma']['won']    = $winMMA;
        $record['mma']['lost']   = $lostMMA;
        $record['mma']['draw']   = $drawMMA;
        $record['mma']['rounds'] = $roundsMMA;
        $record['mma']['total']  = $totalMMA;
        
        $record['mma']['win_ko']  = $winKoMMA;
        $record['mma']['lost_ko'] = $lostKoMMA;
        
        return $record;
    }
    
    
    /*
     * searchFighter method
     * @param string $firstFighter
     * @param intiger $page
     * @return view
     */
    function searchFighter($firstFighter = null, $page = 1){
        $this->layout = 'ajax';
        
        
        if($this->request->is('post')){
            $keyword = $this->request->data('name') . '_' . $this->request->data('last-name');
            $keyword = trim($keyword);
            $keyword = str_replace(" ", "--", $keyword);
            $keyword = $this->deleteAccent($keyword);
        }
        
        if($keyword != null){
            
            $keywordSearch = $keyword;
            $keyword = str_replace("--", " ", $keyword);
            list($name, $lastName) = explode('_', $keyword);
            $this->set('name', $name);
            $this->set('lastName', $lastName);
            $this->set('keywordSearch', $keywordSearch);
            
            $this->Identity->unbindModel(
                    array(
                            'hasMany' => array(
                                    'IdentitiesVideo',
                                    'IdentitiesImage',
                                    'IdentityContact',
                                    'IdentityNote',
                                    'IdentityId'
                            ),
                            'hasOne' => array(
                                    'IdentityBiography',
                                    'IdentityBirth',
                                    'IdentityDeath',
                            )
                    )
            );
            
            $this->Identity->bindModel(
                array(
                    'hasMany' => array(
                        'IdentitiesImage' => array(
                            'className' 	=> 'IdentitiesImage',
                            'foreignKey' 	=> 'identities_id',
                            'order' => array(
                                'IdentitiesImage.principal' => 'DESC'
                            ),
                            'limit' => 1
                        )
                    )
                )
            );
            
            $identities = $this->Identity->find(
                    'all', array(
                        'conditions' => array(
                            'Identity.fullname LIKE' => '%' . $name . ' ' . $lastName . '%' 
                        ),
                        'order' => array(
                            'Identity.fullname' => 'ASC'
                        ),
                        'limit' => 20,
                        'page' => $page
                    )
            );
            
            if(count($identities) == 0){
                
                $search         = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
                $replace        = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
                $cleanName      = addslashes(str_replace($search, $replace, $name));
                $cleanLastName  = addslashes(str_replace($search, $replace, $lastName));
                
                $identities = $this->Identity->find(
                        'all', array(
                            'conditions' => array(
                                'or' => array(
                                    '"' . $cleanName . '"' . ' LIKE CONCAT("%",`Identity.name`,"%")',
                                    '"' . $cleanLastName . '"' . ' LIKE CONCAT("%",`Identity.last_name`,"%")'
                                )
                            ),
                            'limit' => 20,
                            'page' => $page
                        )
                );
                
                $this->set('secondQuery', true);
                
            } else {
                $this->set('secondQuery', false);
            }
            
            
            $newIdentities = array();
            foreach ($identities as $identity){
                if(count($identity['IdentitiesImage']) > 0){
                    $identity['IdentitiesImage']    = $this->getImageSearch($identity['IdentitiesImage'][0]['images_id']);
                }
                $identity['IdentityResidence']  = $this->getResidenceSearch($identity['IdentityResidence']);
                $identity['Career']             = $this->getCareer($identity['Identity']['id']);
                $newIdentities[] = $identity;
            }
            $this->set('identities', $newIdentities);
            $this->set('firstFighter', $firstFighter);
            
        }
        
        
    }
    
    /*
     * getImageSearch method
     * @param string $idImage
     * @return array
     */
    public function getImageSearch($idImage = null){
        $imageData = $this->Image->find('first', array(
            'conditions' => array(
                'Image.id' => $idImage
            ),
            'recursive' => -1
        ));
        return $imageData;
    }
    
    /*
     * getResidenceSearch method
     * @param array $IdentityResidence
     * @return array
     */
    public function getResidenceSearch($IdentityResidence = array()){
        $location = array();
        if(isset($IdentityResidence['countries_id'])){
            $country = $this->Country->find('first', array(
                'conditions' => array(
                    'Country.id' => $IdentityResidence['countries_id']
                ),
                'fields' => array(
                    'Country.name',
                    'Country.flag'
                ),
                'recursive' => -1
            ));
            $location['Country']    = $country['Country']['name'];
            $location['Flag']       = $country['Country']['flag'];
        }
        
        if(isset($IdentityResidence['states_id'])){
            $state = $this->State->find('first', array(
                'conditions' => array(
                    'State.id' => $IdentityResidence['states_id']
                ),
                'fields' => array(
                    'State.name',
                ),
                'recursive' => -1
            ));
            if(isset($state['State']['name'])){
                $location['State'] = $state['State']['name'];
            }
        }
        
        if(isset($IdentityResidence['cities_id'])){
            $city = $this->City->find('first', array(
                'conditions' => array(
                    'City.id' => $IdentityResidence['cities_id']
                ),
                'fields' => array(
                    'City.name',
                ),
                'recursive' => -1
            ));
            if(isset($city['City']['name'])){
                $location['City'] = $city['City']['name'];
            }
        }
        
        return $location;
    }
    
     /*
     * getCareer method
     * @param string $idIdentity
     * @return array
     */
    public function getCareer($idIdentity = null){
  
        $fights = $this->FightIdentity->find(
            'all', array(
                'conditions' => array(
                    'FightIdentity.identities_id' => $idIdentity
                ),
                'fields' => array(
                    'FightIdentity.fights_id',
                    'FightIdentity.Corner'
                ),
                'recursive' => -1
            )
        );
        
        $totalFights    = count($fights);
        $winFights      = 0;
        $lostFights     = 0;
        $drawFights     = 0;
        $firtsFights    = null;
        $lastFights     = null;
        $career         = array();
        
        foreach ($fights as $fight){
            
            $this->Fight->unbindModel(array(
                'hasAndBelongsToMany' => array(
                    'Image',
                    'Note',
                    'Title',
                    'Video'
                ),
                'hasMany' => array(
                    'FightIdentity'
                )
            ));
            
            $thisFight = $this->Fight->find('first', array(
                'conditions' => array(
                    'Fight.id' => $fight['FightIdentity']['fights_id']
                ),
                'fields' => array(
                    'Fight.winner',
                    'Event.date'
                ),
                'oder' => array(
                    'Event.date' => 'ASC'
                )
            ));

            if($thisFight['Fight']['winner'] != 4){
                if($thisFight['Fight']['winner'] == 3){
                    $drawFights++;
                } else if($fight['FightIdentity']['Corner'] == $thisFight['Fight']['winner']) {
                    $winFights++;
                } else {
                    $lostFights++;
                }
            }
            
            
            
            if(isset($thisFight['Event'])){
                
                if($firtsFights == null){
                    $firtsFights = $thisFight['Event']['date'];
                } 
                if($lastFights == null){
                    $lastFights = $thisFight['Event']['date'];
                }

                if(strtotime($firtsFights) > strtotime($thisFight['Event']['date'])){
                    $firtsFights = $thisFight['Event']['date'];
                }

                if(strtotime($lastFights) < strtotime($thisFight['Event']['date'])){
                    $lastFights = $thisFight['Event']['date'];
                }
            }
        }
        
        
        
        $career['win']      = $winFights;
        $career['lost']     = $lostFights;
        $career['draw']     = $drawFights;
        $career['firts']    = $firtsFights;
        $career['last']     = $lastFights;
        
        return $career;
    }
    
    
    /*
     * deleteAccent method
     * @param string $keyword
     * @return string
     */
    public function deleteAccent($keyword = null){
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$keyword);
        return $texto;
    }
}
