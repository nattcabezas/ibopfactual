<?php
App::uses('AppController', 'Controller');
/**
 * Jobs Controller
 *
 * @property Job $Job
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class JobsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
            $this->Job->recursive = 0;
            $this->set('jobs', $this->Paginator->paginate());
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
            if ($this->request->is('post')) {
                $this->Job->create();
                if ($this->Job->save($this->request->data)) {
                    $this->Session->setFlash(__('The job has been saved.', true), 'alert-success');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The job could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
            $id = base64_decode($id);
            if (!$this->Job->exists($id)) {
                $this->Session->setFlash(__('Invalid job.', true), 'alert-danger');
                return $this->redirect(array('action' => 'index'));
            }
            if ($this->request->is(array('post', 'put'))) {
                if ($this->Job->save($this->request->data)) {
                    $this->Session->setFlash(__('The job has been saved.', true), 'alert-success');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The job could not be saved. Please, try again.', true), 'alert-danger');
                }
            } else {
                $options = array('conditions' => array('Job.' . $this->Job->primaryKey => $id));
                $this->request->data = $this->Job->find('first', $options);
            }
	}

}
