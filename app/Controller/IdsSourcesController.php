<?php
App::uses('AppController', 'Controller');
/**
 * IdsSources Controller
 *
 * @property IdsSource $IdsSource
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IdsSourcesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->IdsSource->recursive = 0;
		$this->set('idsSources', $this->Paginator->paginate());
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->IdsSource->create();
			if ($this->IdsSource->save($this->request->data)) {
				$this->Session->setFlash(__('The ids source has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ids source could not be saved. Please, try again.'));
			}
		}
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->IdsSource->exists($id)) {
			$this->Session->setFlash(__('Invalid ids source', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->IdsSource->save($this->request->data)) {
				$this->Session->setFlash(__('The ids source has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ids source could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('IdsSource.' . $this->IdsSource->primaryKey => $id));
			$this->request->data = $this->IdsSource->find('first', $options);
		}
	}
}
