<?php
App::uses('AppController', 'Controller');
/**
 * Rules Controller
 *
 * @property Rule $Rule
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RulesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Rule->recursive = 0;
		$this->set('rules', $this->Paginator->paginate());
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->Rule->create();
			if ($this->Rule->save($this->request->data)) {
				$this->Session->setFlash(__('The rule has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rule could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->Rule->exists($id)) {
			$this->Session->setFlash(__('Invalid rule.'), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Rule->save($this->request->data)) {
				$this->Session->setFlash(__('The rule has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rule could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('Rule.' . $this->Rule->primaryKey => $id));
			$this->request->data = $this->Rule->find('first', $options);
		}
	}

}
