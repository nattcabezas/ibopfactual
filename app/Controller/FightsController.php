<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'Identities');


/**
 * Fights Controller
 *
 * @property Fight $Fight
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FightsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * uses
     *
     * @var array
     */
    public $uses = array(
        'Fight',
        'Source',
        'Weight',
        'Country',
        'FinalFigth',
        'Rule',
        'Event',
        'Image',
        'Note',
        'Title',
        'Video',
        'Type',
        'FightIdentity',
        'Job',
        'FighterJob',
        'FightDatum',
        'FightsImage',
        'FightsVideo',
        'Note',
        'FightsNote',
        'FightsTitle',
        'Title',
        'Location',
        'IdentitiesImage',
        'Submission',
        'Venue'
    );

    /**
     * helper
     *
     * @var array
     */
    public $helper = array('getUrl', 'QrCode', 'getage');

    /**
     * index method
     *
     * @return void
     */
    public function index($url = null) {
        if ($url != null) {
            list($id, $name) = explode('-', $url);
            if (is_numeric($id)) {

                $this->Event->unbindModel(array(
                    'hasAndBelongsToMany' => array(
                        'Image',
                        'PromotionalCompany',
                        'Video'
                    ),
                    'hasMany' => array(
                        'EventsJob',
                        'EventsImage'
                    )
                ));

                $this->Fight->unbindModel(array(
                    'hasAndBelongsToMany' => array(
                        'Image',
                        'Note',
                        'Title',
                        'Video'
                    )
                ));

                $fight = $this->Fight->find('first', array(
                    'conditions' => array(
                        'Fight.id' => $id
                    ),
                    'recursive' => 2
                ));

                $fightData = $this->FightDatum->find('first', array(
                    'conditions' => array(
                        'FightDatum.fights_id' => $id
                    )
                ));

                if (isset($fight['Event']['Venues']['locations_id'])) {
                    $this->set('location', $this->getLocation($fight['Event']['Venues']['locations_id']));
                } else if (isset($fight['Event']['Locations']['id'])) {
                    $this->set('location', $this->getLocation($fight['Event']['Locations']['id']));
                } else {
                    $this->set('location', '');
                }

                $countImages = $this->FightsImage->find('count', array(
                    'conditions' => array(
                        'FightsImage.fights_id' => $id
                    )
                ));

                $countVideos = $this->FightsVideo->find('count', array(
                    'conditions' => array(
                        'FightsVideo.fights_id' => $id
                    )
                ));

                $corner1 = isset($fight['FightIdentity'][0]['identities_id']) ? $fight['FightIdentity'][0]['identities_id'] : null;
                $corner2 = isset($fight['FightIdentity'][1]['identities_id']) ? $fight['FightIdentity'][1]['identities_id'] : null;


                $cornerData1 = $this->FightIdentity->find(
                        'first', array(
                    'conditions' => array(
                        'FightIdentity.fights_id' => $id,
                        'FightIdentity.corner' => 1
                    )
                        )
                );
                $cornerData2 = $this->FightIdentity->find(
                        'first', array(
                    'conditions' => array(
                        'FightIdentity.fights_id' => $id,
                        'FightIdentity.corner' => 2
                    )
                        )
                );

                $jobsCorner1 = $this->FighterJob->find(
                        'all', array(
                    'conditions' => array(
                        'FighterJob.fights_id' => $id,
                        'FighterJob.corner' => 1
                    ),
                    'fields' => array(
                        'FighterJob.id',
                        'Identities.id',
                        'Identities.name',
                        'Identities.last_name',
                        'Jobs.name'
                    )
                        )
                );


                $jobsCorner2 = $this->FighterJob->find(
                        'all', array(
                    'conditions' => array(
                        'FighterJob.fights_id' => $id,
                        'FighterJob.corner' => 2
                    ),
                    'fields' => array(
                        'FighterJob.id',
                        'Identities.id',
                        'Identities.name',
                        'Identities.last_name',
                        'Jobs.name'
                    )
                        )
                );
                
                /*
                Prueba commit
                 *                  */

                $fightTitle = $this->FightsTitle->find(
                    'all', array(
                        'conditions' => array(
                            'FightsTitle.fights_id' => $id,
                        ),
                        'fields' => array(
                            'Titles.id',
                            'Titles.name'
                        )
                    )
                );

                $this->set('id', $id);
                $this->set('fight', $fight);
                $this->set('fightData', $fightData);
                $this->set('countImages', $countImages);
                $this->set('countVideos', $countVideos);
                $this->set('images', $this->getImagesIdentities($corner1, $corner2));
                $this->set('cornerData1', $cornerData1);
                $this->set('cornerData2', $cornerData2);
                $this->set('jobsCorner1', $jobsCorner1);
                $this->set('jobsCorner2', $jobsCorner2);
                $this->set('fightTitle', $fightTitle);
            }
        }
    }

    /*
     * search method
     * 
     * @param string $keyword
     * @return void
     */

    public function search($keyword = null) {

        if ($this->request->is('post')) {
            $keywordPost = $this->request->data('Keyword-fight');
            $keywordPost = str_replace(" ", "_", $keywordPost);
            $keywordPost = $this->deleteAccent($keywordPost);
            return $this->redirect(array('action' => 'search/' . $keywordPost));
        }

        if ($keyword != null) {

            $this->set('keywordSearch', $keyword);

            $this->Event->unbindModel(array(
                'hasMany' => array(
                    'EventsJob',
                    'EventsImage'
                ),
                'hasAndBelongsToMany' => array(
                    'Fight',
                    'Image',
                    'PromotionalCompany',
                    'Video'
                )
            ));

            $this->FightIdentity->unbindModel(array(
                'belongsTo' => array(
                    'Fight'
                )
            ));

            $this->Fight->unbindModel(array(
                'hasAndBelongsToMany' => array(
                    'Image',
                    'Note',
                    'Title',
                    'Video'
                )
            ));

            $keyword = str_replace("_", " ", $keyword);
            $this->set('keyword', $keyword);

            $fights = $this->Fight->find('all', array(
                'conditions' => array(
                    'Fight.title LIKE' => '%' . $keyword . '%'
                ),
                'recursive' => 2,
                'limit' => 20
            ));

            if (count($fights) == 0) {

                $keywords = explode(' ', $keyword);
                $conditions = array();

                foreach ($keywords as $word) {
                    $conditions[]['Fight.title LIKE'] = '%' . $word . '%';
                }

                $fights = $this->Fight->find('all', array(
                    'conditions' => array(
                        'or' => $conditions
                    ),
                    'recursive' => 2,
                    'limit' => 20
                ));

                $this->set('secondQuery', 1);
            } else {
                $this->set('secondQuery', 0);
            }

            $newFights = array();
            foreach ($fights as $fight) {

                if ((isset($fight['Event']['Venues']['locations_id'])) && ($fight['Event']['Venues']['locations_id'] != "")) {
                    $locations = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $fight['Event']['Venues']['locations_id']
                        )
                    ));
                    $fight['Locations'] = $locations;
                } else if ((isset($fight['Event']['Locations']['id'])) && ($fight['Event']['Locations']['id'] != "")) {
                    $locations = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $fight['Event']['Locations']['id']
                        )
                    ));
                    $fight['Locations'] = $locations;
                }
                $newFights[] = $fight;
            }

            $this->set('fights', $newFights);
        }
    }

    /*
     * searchPage method
     * 
     * @param string $keyword
     * @param string $page
     * @return void
     */

    public function searchPage($keyword = null, $page = 1, $second = 0) {
        $this->layout = 'ajax';

        if ($keyword != null) {

            $this->Event->unbindModel(array(
                'hasMany' => array(
                    'EventsJob',
                    'EventsImage'
                ),
                'hasAndBelongsToMany' => array(
                    'Fight',
                    'Image',
                    'PromotionalCompany',
                    'Video'
                )
            ));

            $this->FightIdentity->unbindModel(array(
                'belongsTo' => array(
                    'Fight'
                )
            ));

            $this->Fight->unbindModel(array(
                'hasAndBelongsToMany' => array(
                    'Image',
                    'Note',
                    'Title',
                    'Video'
                )
            ));

            $keyword = str_replace("_", " ", $keyword);

            if (!$second) {
                $fights = $this->Fight->find('all', array(
                    'conditions' => array(
                        'Fight.title LIKE' => '%' . $keyword . '%'
                    ),
                    'recursive' => 2,
                    'page' => $page,
                    'limit' => 20
                ));
            } else {

                $keywords = explode(' ', $keyword);
                $conditions = array();

                foreach ($keywords as $word) {
                    $conditions[]['Fight.title LIKE'] = '%' . $word . '%';
                }

                $fights = $this->Fight->find('all', array(
                    'conditions' => array(
                        'or' => $conditions
                    ),
                    'recursive' => 2,
                    'page' => $page,
                    'limit' => 20
                ));
            }

            $newFights = array();
            foreach ($fights as $fight) {

                if ((isset($fight['Event']['Venues']['locations_id'])) && ($fight['Event']['Venues']['locations_id'] != "")) {
                    $locations = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $fight['Event']['Venues']['locations_id']
                        )
                    ));
                    $fight['Locations'] = $locations;
                } else if ((isset($fight['Event']['Locations']['id'])) && ($fight['Event']['Locations']['id'] != "")) {
                    $locations = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $fight['Event']['Locations']['id']
                        )
                    ));
                    $fight['Locations'] = $locations;
                }
                $newFights[] = $fight;
            }

            $this->set('fights', $newFights);
        }
    }

    /**
     * showImages method
     *
     * @param string $idFights
     * @return array
     */
    public function showImages($idFights = null) {

        $this->layout = 'ajax';
        $images = $this->FightsImage->find('all', array(
            'conditions' => array(
                'FightsImage.fights_id' => $idFights
            )
        ));
        $this->set('images', $images);
    }

    /**
     * showVideos method
     *
     * @param string $idFights
     * @return array
     */
    public function showVideos($idFights = null) {

        $this->layout = 'ajax';
        $videos = $this->FightsVideo->find('all', array(
            'conditions' => array(
                'FightsVideo.fights_id' => $idFights
            )
        ));
        $this->set('videos', $videos);
    }

    /**
     * getLocation method
     *
     * @param string $idLocation
     * @return array
     */
    public function getLocation($idLocation) {
        $location = $this->Location->find('first', array(
            'conditions' => array(
                'Location.id' => $idLocation
            )
        ));

        return $location;
    }

    /**
     * getImagesIdentities method
     *
     * @param string $idCorner1
     * @param string $idCorner2
     * @return void
     */
    public function getImagesIdentities($idCorner1 = null, $idCorner2 = null) {

        $images = array();

        $imageCorner1 = $this->IdentitiesImage->find('first', array(
            'conditions' => array(
                'IdentitiesImage.identities_id' => $idCorner1,
                'IdentitiesImage.principal' => 1
            )
        ));

        $imageCorner2 = $this->IdentitiesImage->find('first', array(
            'conditions' => array(
                'IdentitiesImage.identities_id' => $idCorner2,
                'IdentitiesImage.principal' => 1
            )
        ));

        $images['corner1'] = $imageCorner1;
        $images['corner2'] = $imageCorner2;

        return $images;
    }

    /**
     * ibopadmin_index method
     *
     * @return void
     */
    public function ibopadmin_index() {
        $this->Fight->recursive = 0;
        $this->Paginator->settings = array(
            'order' => array(
                'Fight.id' => 'DESC'
            )
        );
        $this->set('fights', $this->Paginator->paginate());
    }

    public function ibopadmin_addMultiple()
    {       
        $sources = $this->Source->find('list');
        $this->set('sources', $sources);
        
        if ($this->request->is('post')) 
        {
            //debug($this->request->data);
            $arrayEventNames = array();
            
            $fightCounter = 0; //determine which field its fetched
            foreach($this->request->data['fighter1'] as $fighter1)
            {
                $fightDetails['Fight']['title'] = substr($fighter1, 0, strpos($fighter1, ' (')) . " vs " . substr($this->request->data['fighter2'][$fightCounter], 0, strpos($this->request->data['fighter2'][$fightCounter], ' ('));
                $fightDetails['Fight']['winner'] = $this->request->data['winner'][$fightCounter];
                $fightDetails['Fight']['via'] = $this->request->data['via'][$fightCounter];
                $fightDetails['Fight']['time'] = $this->request->data['time'][$fightCounter];
                $fightDetails['Fight']['boxed_roundas'] = $this->request->data['boxed_rounds'][$fightCounter];
                $fightDetails['Fight']['schedule_rounds'] = $this->request->data['scheduled_rounds'][$fightCounter];
                $fightDetails['Fight']['sources_id'] = $this->request->data['sources'][$fightCounter];
                $fightDetails['Fight']['types_id'] = 1;
                
                /*
                if the event id is -1, its a new event and needs to be created
                * otherwise, its an existing event and needs to be assigned to the fight
                */
                //create an event with title and date
                                
                $arrayDate = explode("-", $this->request->data['eventDate'][$fightCounter]);
                                
                if ($this->request->data['idEvent'][$fightCounter] == "-1")
                {
                    array_push($arrayEventNames,$this->request->data['event'][$fightCounter]);//used to determine if the event is duplicated
                    $tempArray = array_count_values($arrayEventNames);
                    
                    if ($tempArray[$this->request->data['event'][$fightCounter]]>1)//this means that the event is duplicated, so dont create and assign the existing one
                    {
                        //do the query to find the event using the name
                        $existingEvent = $this->Event->find(
                            'first', array(
                                'conditions' => array(
                                    'Event.name' => $this->request->data['event'][$fightCounter]
                                ),
                                'fields' => array(
                                    'Event.id'
                                )
                            )
                            
                        );
                        $fightDetails['Fight']['events_id'] = $existingEvent['Event']['id'];
                    }
                    else //event not existing, create a new one
                    {                        
                        $eventDetails['Event']['name'] = $this->request->data['event'][$fightCounter];
                        $eventDetails['Event']['date'] = $arrayDate[0] . "-" . $arrayDate[1] . "-" . $arrayDate[2];
                        
                        /*
                         * Do a query and check if location exist
                         */
                        $existingLocation = $this->Location->find(
                            'first', array(
                                'conditions' => array(
                                    'Location.countries_id' => $this->request->data['countryEvent'][$fightCounter],
                                    'Location.states_id' => $this->request->data['stateEvent'][$fightCounter],
                                    'Location.cities_id' => $this->request->data['cityEvent'][$fightCounter]
                                ),
                                'fields' => array(
                                    'Location.id'
                                )
                            )
                            
                        );
                        
                        if (count($existingLocation)==0 )/*If location is not existing create it and save*/
                        {
                            $locationDetails['Location']['countries_id'] = $this->request->data['countryEvent'][$fightCounter];
                            $locationDetails['Location']['states_id'] = $this->request->data['stateEvent'][$fightCounter];
                            $locationDetails['Location']['cities_id'] = $this->request->data['cityEvent'][$fightCounter];
                            $this->Location->save($locationDetails);
                            $this->Location->clear();
                            $eventDetails['Event']['locations_id'] = $this->Location->getLastInsertId();
                        }
                        else
                        {
                            /*
                             * Assign the found location id to the event
                             */
                            $eventDetails['Event']['locations_id'] = $existingLocation['Location']['id'];
                        }
                        $this->Event->save($eventDetails);
                        $this->Event->clear();
                    }
                    $fightDetails['Fight']['events_id'] = $this->Event->getLastInsertId();
                }
                else
                {
                    $fightDetails['Fight']['events_id'] = $this->request->data['idEvent'][$fightCounter];    
                }
                
                $this->Fight->save($fightDetails);
                $this->Fight->clear();
                                
                $fightsIdentityFighter1['FightIdentity']['fights_id'] = $this->Fight->getLastInsertId();
                $fightsIdentityFighter1['FightIdentity']['identities_id'] = $this->request->data['idFighter1'][$fightCounter];
                $fightsIdentityFighter1['FightIdentity']['corner'] = 1;
                $fightsIdentityFighter1['FightIdentity']['weigth'] = $this->request->data['weight1'][$fightCounter];
                
                $this->FightIdentity->save($fightsIdentityFighter1);
                $this->FightIdentity->clear();
                                
                $fightsIdentityFighter2['FightIdentity']['fights_id'] = $this->Fight->getLastInsertId();
                $fightsIdentityFighter2['FightIdentity']['identities_id'] = $this->request->data['idFighter2'][$fightCounter];
                $fightsIdentityFighter2['FightIdentity']['corner'] = 2;
                $fightsIdentityFighter2['FightIdentity']['weigth'] = $this->request->data['weight2'][$fightCounter];
                
                $this->FightIdentity->save($fightsIdentityFighter2);
                $this->FightIdentity->clear();
                
                $fightCounter++;
            }
            $this->Session->setFlash(__('Fights were saved.', true), 'alert-success');
        }
    }

    /**
     * ibopadmin_add method
     *
     * @return void
     */
    public function ibopadmin_add() {
        if ($this->request->is('post')) {
            //debug($this->request->data);
             /*
             * Si el checkbox de round sin tiempo es seleccionado,  asignar el valor de -1
             * al campo time_per_round para indicar que el round no tiene tiempo, luego
             * sacar del arreglo Fight a no_time_check_box para evitar problemas con la insercion
             * a la base de datos. Si el checkbox viene en cero, quiere decir que hubo que meter
             * tiempo, y probablemente venga un valor.
             */
           if ($this->request->data['Fight']['no_time_checkbox'] == 1) {
                $this->request->data['Fight']['time_per_round'] = -1;
            }
            unset($this->request->data['Fight']['no_time_checkbox']);

            $this->Fight->create();
            if ($this->Fight->save($this->request->data)) {
                $id = $this->Fight->getLastInsertID();
                $this->Session->setFlash(__('The fight has been saved.', true), 'alert-success');
                /*****create Fight identity here****/
                $names = explode(" vs ", $this->request->data['Fight']['title']);//names[0] and names[1] have the names of the fighters
                $Identities = new IdentitiesController;
                $theFighter0 = $Identities->ibopadmin_getIdentityExternal(trim($names[0]), $this->request->data['Fight']['corner1_id']);
                $theFighter1 = $Identities->ibopadmin_getIdentityExternal(trim($names[1]), $this->request->data['Fight']['corner2_id']);
                
                $fightsIdentityValue0['FightIdentity']['fights_id'] = $this->Fight->id;
                $fightsIdentityValue0['FightIdentity']['identities_id'] = $theFighter0[0]['Identity']['id'];
                $fightsIdentityValue0['FightIdentity']['corner'] = 1;
                $this->FightIdentity->save($fightsIdentityValue0);
                $this->FightIdentity->clear();
                
                $fightsIdentityValue1['FightIdentity']['fights_id'] = $this->Fight->id;
                $fightsIdentityValue1['FightIdentity']['identities_id'] = $theFighter1[0]['Identity']['id'];
                $fightsIdentityValue1['FightIdentity']['corner'] = 2;
                $this->FightIdentity->save($fightsIdentityValue1);
                /***********************************/
                return $this->redirect(array('action' => 'edit/' . base64_encode($id)));
            } else {
                $this->Session->setFlash(__('The fight could not be saved. Please, try again.', true), 'alert-danger');
            }
        }

        $submissions = $this->Submission->find(
                'all', array(
            'fields' => array(
                'Submission.id',
                'Submission.name'
            )
                )
        );

        $sources = $this->Source->find('list');
        $weights = $this->Weight->find('list');
        $finalFigths = $this->FinalFigth->find('list');
        $rules = $this->Rule->find('list');
        $types = $this->Type->find('list');

        $this->set(compact('sources', 'weights', 'finalFigths', 'rules', 'types'));
        $this->set('submissions', $submissions);
        
    }

    /**
     * ibopadmin_edit method
     *
     * @param string $id
     * @return void
     */
    public function ibopadmin_edit($id = null) {
        $id = base64_decode($id);
        if (!$this->Fight->exists($id)) {
            $this->Session->setFlash(__('Invalid fight.', true), 'alert-danger');
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Fight->save($this->request->data)) {
                $this->Session->setFlash(__('The fight has been saved.', true), 'alert-success');
                return $this->redirect(array('action' => 'edit/' . base64_encode($id)));
            } else {
                $this->Session->setFlash(__('The fight could not be saved. Please, try again.', true), 'alert-danger');
            }
        } else {
            $options = array('conditions' => array('Fight.' . $this->Fight->primaryKey => $id));

            $corner1 = $this->FightIdentity->find(
                    'first', array(
                'conditions' => array(
                    'FightIdentity.fights_id' => $id,
                    'FightIdentity.corner' => 1
                )
                    )
            );

            $corner2 = $this->FightIdentity->find(
                    'first', array(
                'conditions' => array(
                    'FightIdentity.fights_id' => $id,
                    'FightIdentity.corner' => 2
                )
                    )
            );

            $jobs = $this->Job->find('list');

            $jobsCorner1 = $this->FighterJob->find(
                    'all', array(
                'conditions' => array(
                    'FighterJob.fights_id' => $id,
                    'FighterJob.corner' => 1
                ),
                'fields' => array(
                    'FighterJob.id',
                    'Identities.name',
                    'Identities.last_name',
                    'Jobs.name'
                )
                    )
            );

            $jobsCorner2 = $this->FighterJob->find(
                    'all', array(
                'conditions' => array(
                    'FighterJob.fights_id' => $id,
                    'FighterJob.corner' => 2
                ),
                'fields' => array(
                    'FighterJob.id',
                    'Identities.name',
                    'Identities.last_name',
                    'Jobs.name'
                )
                    )
            );

            $this->FightDatum->unbindModel(
                    array(
                        'belongsTo' => array(
                            'Fights'
                        )
                    )
            );

            $otherInformation = $this->FightDatum->find(
                    'first', array(
                'conditions' => array(
                    'FightDatum.fights_id' => $id
                )
                    )
            );

            if (!$corner1) {
                $corner1['FightIdentity']['id'] = "";
                $corner1['FightIdentity']['fights_id'] = "";
                $corner1['FightIdentity']['identities_id'] = "";
                $corner1['FightIdentity']['corner'] = "";
                $corner1['FightIdentity']['corner_color'] = "";
                $corner1['FightIdentity']['weigth'] = "";
                $corner1['FightIdentity']['final_weigth'] = "";
                $corner1['FightIdentity']['shorts'] = "";
                $corner1['FightIdentity']['robe'] = "";
                $corner1['FightIdentity']['robe_says'] = "";
                $corner1['FightIdentity']['purse_amount'] = "";
                $corner1['FightIdentity']['purse_comments'] = "";
                $corner1['FightIdentity']['death_ring'] = "";
                $corner1['FightIdentity']['comments'] = "";
                $corner1['FightIdentity']['winner'] = "";
                $corner1['Identities']['id'] = "";
                $corner1['Identities']['name'] = "";
                $corner1['Identities']['last_name'] = "";
            }

            if (!$corner2) {
                $corner2['FightIdentity']['id'] = "";
                $corner2['FightIdentity']['fights_id'] = "";
                $corner2['FightIdentity']['identities_id'] = "";
                $corner2['FightIdentity']['corner'] = "";
                $corner2['FightIdentity']['corner_color'] = "";
                $corner2['FightIdentity']['weigth'] = "";
                $corner2['FightIdentity']['final_weigth'] = "";
                $corner2['FightIdentity']['shorts'] = "";
                $corner2['FightIdentity']['robe'] = "";
                $corner2['FightIdentity']['robe_says'] = "";
                $corner2['FightIdentity']['purse_amount'] = "";
                $corner2['FightIdentity']['purse_comments'] = "";
                $corner2['FightIdentity']['death_ring'] = "";
                $corner2['FightIdentity']['comments'] = "";
                $corner2['FightIdentity']['winner'] = "";
                $corner2['Identities']['id'] = "";
                $corner2['Identities']['name'] = "";
                $corner2['Identities']['last_name'] = "";
            }

            $this->set('corner1', $corner1);
            $this->set('corner2', $corner2);
            $this->set('jobs', $jobs);
            $this->set('jobsCorner1', $jobsCorner1);
            $this->set('jobsCorner2', $jobsCorner2);
            $this->set('fightsData', $otherInformation);
            $this->request->data = $this->Fight->find('first', $options);
        }

        $submissions = $this->Submission->find(
                'all', array(
            'fields' => array(
                'Submission.id',
                'Submission.name'
            )
                )
        );

        $sources = $this->Source->find('list');
        $weights = $this->Weight->find('list');
        $finalFigths = $this->FinalFigth->find('list');
        $rules = $this->Rule->find('list');
        $types = $this->Type->find('list');
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $id));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('id', $id);
        $this->set('titleName', $this->getTitleName($id));
        $this->set(compact('sources', 'weights', 'finalFigths', 'rules', 'types'));
        $this->set('submissions', $submissions);
    }

    /**
     * ibopadmin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function ibopadmin_delete($id = null) {
        $this->Fight->id = $id;
        if (!$this->Fight->exists()) {
            throw new NotFoundException(__('Invalid fight'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Fight->delete()) {
            $this->Session->setFlash(__('The fight has been deleted.'));
        } else {
            $this->Session->setFlash(__('The fight could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * ibopadmin_saveCorner method
     *
     * @return void
     */
    public function ibopadmin_saveCorner() {
        if ($this->request->is('post')) {
            if ($this->request->data('FightIdentity.id') == "") {
                $isRepit = $this->FightIdentity->find('first', array(
                    'conditions' => array(
                        'FightIdentity.fights_id' => $this->request->data('FightIdentity.fights_id'),
                        'FightIdentity.corner' => $this->request->data('FightIdentity.corner')
                    ),
                    'fields' => array(
                        'FightIdentity.id'
                    )
                ));
                if ($isRepit) {
                    if (!$this->FightIdentity->delete($this->request->data('FightIdentity.id'))) {
                        $this->Session->setFlash(__('The Fighter could not be saved. Please, try again.', true), 'alert-danger');
                        return $this->redirect(array('action' => 'index'));
                    }
                }
            }
            if ($this->FightIdentity->save($this->request->data)) {
                $this->Session->setFlash(__('The Fighter has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Fighter could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('FightIdentity.fights_id'))));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * ibopadmin_saveJobs method
     *
     * @return void
     */
    public function ibopadmin_saveJobs() {
        if ($this->request->is('post')) {
            if ($this->FighterJob->save($this->request->data)) {
                $this->Session->setFlash(__('The Job has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Job could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('FighterJob.fights_id'))));
        } else {
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * ibopadmin_deleteJobs method
     *
     * @return void
     */
    public function ibopadmin_deleteJobs() {
        if ($this->request->is('post')) {
            $this->FighterJob->id = $this->request->data('FighterJob.id');
            if (!$this->FighterJob->exists()) {
                $this->Session->setFlash(__('Invalid Job.', true), 'alert-danger');
                $this->redirect(array('action' => 'index'));
            }
            if ($this->FighterJob->delete()) {
                $this->Session->setFlash(__('The Job has been delete.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Job could not be delete. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('FighterJob.idFight'))));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * ibopadmin_saveFightDatum method
     *
     * @return void
     */
    public function ibopadmin_saveFightDatum() {
        if ($this->request->is('post')) {
            if ($this->FightDatum->save($this->request->data)) {
                $this->Session->setFlash(__('The Fight Information has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Fight Information could not be saved. Please, try again.', true), 'alert-danger');
            }
            $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('FightDatum.fights_id'))));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * funcion que muestra las imagenes del sistama para agregarlas al evento
     * @param  integer $idFights 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_images($idFights = null, $page = 1) {
        $idFights = base64_decode($idFights);

        $images = $this->Image->find(
                'all', array(
            'fields' => array(
                'Image.id',
                'Image.url'
            ),
            'order' => array(
                'Image.id' => 'DESC'
            ),
            'recursive' => -1,
            'limit' => 9,
            'page' => $page
                )
        );

        if ($this->request->is('post')) {
            $figthImageExist = $this->FightsImage->find(
                    'count', array(
                'conditions' => array(
                    'FightsImage.fights_id' => $this->request->data('FightsImage.fights_id'),
                    'FightsImage.images_id' => $this->request->data('FightsImage.images_id')
                )
                    )
            );

            if ($figthImageExist > 0) {
                $this->Session->setFlash(__('The image is already added', true), 'alert-danger');
            } else {
                if ($this->FightsImage->save($this->request->data)) {
                    $this->Session->setFlash(__('The image has been saved in the fight.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('images', $images);
        $this->set('idFights', $idFights);
        $this->set('page', $page);
    }

    /**
     * lista de imagenes de la pelea
     * @param  integer $idFights 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_fightsImages($idFights = null, $page = 1) {
        $idFights = base64_decode($idFights);

        if ($this->request->is('post')) {
            if ($this->FightsImage->delete($this->request->data('FightsImage.id'))) {
                $this->Session->setFlash(__('The Image has been deleted', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Image could not be deleted. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'fightsImages/' . base64_encode($idFights) . '/' . $page));
        }

        $images = $this->FightsImage->find(
                'all', array(
            'conditions' => array(
                'FightsImage.fights_id' => $idFights
            ),
            'fields' => array(
                'FightsImage.id',
                'FightsImage.principal',
                'Images.id',
                'Images.url'
            ),
            'order' => array(
                'FightsImage.principal' => 'DESC',
                'FightsImage.id' => 'ASC'
            ),
            'limit' => 9,
            'page' => $page
                )
        );
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('images', $images);
        $this->set('idFights', $idFights);
        $this->set('page', $page);
    }

    /**
     * funcion que busca las imagenes
     * @param  integer $idFights 
     * @param  string  $keyword 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_searchImages($idFights = null, $keyword = null, $page = 1) {
        $idFights = base64_decode($idFights);
        $keyword = base64_decode($keyword);

        if ($keyword != null) {
            $images = $this->Image->find(
                    'all', array(
                'conditions' => array(
                    'Image.title LIKE' => '%' . $keyword . '%'
                ),
                'fields' => array(
                    'Image.id',
                    'Image.url'
                ),
                'order' => array(
                    'Image.id' => 'DESC'
                ),
                'recursive' => -1,
                'limit' => 9,
                'page' => $page
                    )
            );
        } else {
            $images = array();
        }

        if ($this->request->is('post')) {
            $figthImageExist = $this->FightsImage->find(
                    'count', array(
                'conditions' => array(
                    'FightsImage.fights_id' => $this->request->data('FightsImage.fights_id'),
                    'FightsImage.images_id' => $this->request->data('FightsImage.images_id')
                )
                    )
            );

            if ($figthImageExist > 0) {
                $this->Session->setFlash(__('The image is already added', true), 'alert-danger');
            } else {
                if ($this->FightsImage->save($this->request->data)) {
                    $this->Session->setFlash(__('The image has been saved in the fight.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('idFights', $idFights);
        $this->set('images', $images);
        $this->set('page', $page);
        $this->set('keywordData', $keyword);
    }

    /**
     * ibopadmin_imagePrincipal
     * @return void
     */
    public function ibopadmin_imagePrincipal() {
        if ($this->request->is('post')) {
            if ($this->request->data('ImagePrincipal.make_principal') == 1) {
                $countPrincipal = $this->FightsImage->find(
                        'count', array(
                    'conditions' => array(
                        'FightsImage.fights_id' => $this->request->data('ImagePrincipal.idFights'),
                        'FightsImage.principal' => 1
                    )
                        )
                );
                if ($countPrincipal == 0) {
                    $fightsImage['FightsImage']['id'] = $this->request->data('ImagePrincipal.id');
                    $fightsImage['FightsImage']['principal'] = 1;
                    if ($this->FightsImage->save($fightsImage)) {
                        $this->Session->setFlash(__('The image has been selected.', true), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Image could not be selected. Please, try again.', true), 'alert-danger');
                    }
                } else {
                    $this->Session->setFlash(__('The Fight already has a main image.', true), 'alert-danger');
                }
            } else {
                $fightsImage['FightsImage']['id'] = $this->request->data('ImagePrincipal.id');
                $fightsImage['FightsImage']['principal'] = null;
                if ($this->FightsImage->save($fightsImage)) {
                    $this->Session->setFlash(__('The image has been unselected.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Image could not be unselected. Please, try again.', true), 'alert-danger');
                }
            }
            return $this->redirect(array('action' => 'fightsImages/' . base64_encode($this->request->data('ImagePrincipal.idFights')) . '/' . $this->request->data('ImagePrincipal.page')));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * funcion que carga todos los videos para guardarlos en la pelea
     * @param  integer $idFights 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_videos($idFights = null, $page = 1) {
        $idFights = base64_decode($idFights);

        $videos = $this->Video->find(
                'all', array(
            'fields' => array(
                'Video.id',
                'Video.title',
                'Video.mp4'
            ),
            'order' => array(
                'Video.id' => 'DESC'
            ),
            'recursive' => -1,
            'limit' => 10,
            'page' => $page
                )
        );

        if ($this->request->is('post')) {
            $fightsVideoExist = $this->FightsVideo->find(
                    'count', array(
                'conditions' => array(
                    'FightsVideo.fights_id' => $this->request->data('FightsVideo.fights_id'),
                    'FightsVideo.videos_id' => $this->request->data('FightsVideo.videos_id')
                )
                    )
            );
            if ($fightsVideoExist > 0) {
                $this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
            } else {
                if ($this->FightsVideo->save($this->request->data)) {
                    $this->Session->setFlash(__('The Fights video has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Fights video could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('videos', $videos);
        $this->set('idFights', $idFights);
        $this->set('page', $page);
    }

    /**
     * funcion que lista los videos de la pelea
     * @param  integer $idFights 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_fightsVideos($idFights = null, $page = 1) {
        $idFights = base64_decode($idFights);

        if ($this->request->is('post')) {
            if ($this->FightsVideo->delete($this->request->data('FightsVideo.id'))) {
                $this->Session->setFlash(__('The video has been deleted', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The video could not be deleted. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'fightsVideos/' . base64_encode($idFights) . '/' . $page));
        }

        $videos = $this->FightsVideo->find(
                'all', array(
            'conditions' => array(
                'FightsVideo.fights_id' => $idFights
            ),
            'fields' => array(
                'FightsVideo.id',
                'Videos.id',
                'Videos.title',
                'Videos.mp4'
            ),
            'order' => array(
                'FightsVideo.id' => 'DESC',
            ),
            'limit' => 10,
            'page' => $page
                )
        );
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('videos', $videos);
        $this->set('idFights', $idFights);
        $this->set('page', $page);
    }

    /**
     * funcion que busca los videos
     * @param  integer $idFights 
     * @param  string  $keyword 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_searchVideos($idFights = null, $keyword = null, $page = 1) {
        $idFights = base64_decode($idFights);
        $keyword = base64_decode($keyword);

        if ($keyword != null) {
            $videos = $this->Video->find(
                    'all', array(
                'conditions' => array(
                    'Video.title LIKE' => '%' . $keyword . '%'
                ),
                'fields' => array(
                    'Video.id',
                    'Video.title',
                    'Video.mp4'
                ),
                'order' => array(
                    'Video.id' => 'DESC'
                ),
                'recursive' => -1,
                'limit' => 10,
                'page' => $page
                    )
            );
        } else {
            $videos = array();
        }

        if ($this->request->is('post')) {
            $fightsVideoExist = $this->FightsVideo->find(
                    'count', array(
                'conditions' => array(
                    'FightsVideo.fights_id' => $this->request->data('FightsVideo.fights_id'),
                    'FightsVideo.videos_id' => $this->request->data('FightsVideo.videos_id')
                )
                    )
            );
            if ($fightsVideoExist > 0) {
                $this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
            } else {
                if ($this->FightsVideo->save($this->request->data)) {
                    $this->Session->setFlash(__('The Fights video has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Fights video could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('videos', $videos);
        $this->set('idFights', $idFights);
        $this->set('page', $page);
        $this->set('keywordData', $keyword);
    }

    /**
     * funcion que carga las notas
     * @param  integer $idFights
     * @param  intiger $page  
     * @return void             
     */
    public function ibopadmin_notes($idFights = null, $page = 1) {
        $idFights = base64_decode($idFights);

        $notes = $this->Note->find(
                'all', array(
            'fields' => array(
                'Note.id',
                'Note.title',
                'Note.note'
            ),
            'order' => array(
                'Note.id' => 'DESC'
            ),
            'recursive' => -1,
            'limit' => 10,
            'page' => $page
                )
        );

        if ($this->request->is('post')) {
            $fightsNoteExist = $this->FightsNote->find(
                    'count', array(
                'conditions' => array(
                    'FightsNote.fights_id' => $this->request->data('FightsNote.fights_id'),
                    'FightsNote.notes_id' => $this->request->data('FightsNote.notes_id')
                )
                    )
            );
            if ($fightsNoteExist > 0) {
                $this->Session->setFlash(__('The Note are already added', true), 'alert-danger');
            } else {
                if ($this->FightsNote->save($this->request->data)) {
                    $this->Session->setFlash(__('The Note has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Note could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('notes', $notes);
        $this->set('idFights', $idFights);
        $this->set('page', $page);
    }

    /**
     * funcion que carga las notas del identity
     * @param  integer $idFights
     * @param  intiger $page  
     * @return void             
     */
    public function ibopadmin_fightNotes($idFights = null, $page = 1) {
        $idFights = base64_decode($idFights);

        if ($this->request->is('post')) {
            if ($this->FightsNote->delete($this->request->data('FightsNote.id'))) {
                $this->Session->setFlash(__('The note has been deleted', true), 'alert-success');
                return $this->redirect(array('action' => 'fightNotes/' . base64_encode($idFights) . '/' . $page));
            } else {
                $this->Session->setFlash(__('The note could not be deleted. Please, try again.', true), 'alert-danger');
                return $this->redirect(array('action' => 'fightNotes/' . base64_encode($idFights) . '/' . $page));
            }
        }

        $notes = $this->FightsNote->find(
                'all', array(
            'conditions' => array(
                'FightsNote.fights_id' => $idFights
            ),
            'fields' => array(
                'FightsNote.id',
                'Notes.id',
                'Notes.title',
                'Notes.note'
            ),
            'order' => array(
                'FightsNote.id' => 'DESC'
            ),
            'limit' => 10,
            'page' => $page
                )
        );
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('idFights', $idFights);
        $this->set('page', $page);
        $this->set('notes', $notes);
    }

    /**
     * funcion que busca en las notas
     * @param  integer $idFights 
     * @param  string  $keyword 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_searchNotes($idFights = null, $keyword = null, $page = 1) {
        $idFights = base64_decode($idFights);
        $keyword = base64_decode($keyword);

        if ($keyword != null) {
            $notes = $this->Note->find(
                    'all', array(
                'conditions' => array(
                    'Note.title LIKE' => '%' . $keyword . '%'
                ),
                'fields' => array(
                    'Note.id',
                    'Note.title',
                    'Note.note'
                ),
                'order' => array(
                    'Note.id' => 'DESC'
                ),
                'recursive' => -1,
                'limit' => 10,
                'page' => $page
                    )
            );
        } else {
            $notes = array();
        }

        if ($this->request->is('post')) {
            $fightsNoteExist = $this->FightsNote->find(
                    'count', array(
                'conditions' => array(
                    'FightsNote.fights_id' => $this->request->data('FightsNote.fights_id'),
                    'FightsNote.notes_id' => $this->request->data('FightsNote.notes_id')
                )
                    )
            );
            if ($fightsNoteExist > 0) {
                $this->Session->setFlash(__('The Note are already added', true), 'alert-danger');
            } else {
                if ($this->FightsNote->save($this->request->data)) {
                    $this->Session->setFlash(__('The Note has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Note could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('notes', $notes);
        $this->set('idFights', $idFights);
        $this->set('page', $page);
        $this->set('keywordData', $keyword);
    }

    /**
     * ibopadmin_titles method
     *
     * @param string $idFights
     * @return void
     */
    public function ibopadmin_titles($idFights = null) {

        $idFights = base64_decode($idFights);

        if ($this->request->is('post')) {
            $this->FightsTitle->create();
            if ($this->FightsTitle->save($this->request->data)) {
                $this->Session->setFlash(__('The Title has been saved.', true), 'alert-success');
                return $this->redirect(array('action' => 'titles/' . base64_encode($idFights)));
            } else {
                $this->Session->setFlash(__('The Title could not be saved. Please, try again.', true), 'alert-danger');
            }
        }

        $titles = $this->FightsTitle->find(
                'all', array(
            'conditions' => array(
                'FightsTitle.fights_id' => $idFights
            ),
            'fields' => array(
                'FightsTitle.id',
                'FightsTitle.title_issued',
                'FightsTitle.interim',
                'Titles.name',
                'Titles.description',
                'Identity.name',
                'Identity.last_name'
            )
                )
        );

        $fightersData = $this->FightIdentity->find(
                'all', array(
            'conditions' => array(
                'FightIdentity.fights_id' => $idFights
            ),
            'fields' => array(
                'FightIdentity.corner',
                'Identities.id',
                'Identities.name',
                'Identities.last_name'
            ),
            'order' => array(
                'FightIdentity.corner' => 'ASC'
            )
                )
        );

        $fighters = array();
        foreach ($fightersData as $data) {
            $fighters[$data['Identities']['id']] = __('corner') . ' ' . $data['FightIdentity']['corner'] . ' (' . $data['Identities']['name'] . ' ' . $data['Identities']['last_name'] . ')';
        }
        $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
        $this->set('fight', $this->Fight->find('first', $options));
        $this->set('titleName', $this->getTitleName($idFights));
        $this->set('idFights', $idFights);
        $this->set('titles', $titles);
        $this->set('fighters', $fighters);
    }

    /**
     * ibopadmin_titleDelete method
     *
     * @param string $idFights
     * @param string $id
     * @return void
     */
    public function ibopadmin_titleDelete($idFights = null, $id = null) {
        $id = base64_decode($id);

        $this->FightsTitle->id = $id;
        if (!$this->FightsTitle->exists()) {
            $this->Session->setFlash(__('Invalid title', true), 'alert-danger');
            return $this->redirect(array('action' => 'titles/' . $idFights));
        }
        if ($this->FightsTitle->delete()) {
            $this->Session->setFlash(__('The title has been deleted.', true), 'alert-success');
        } else {
            $this->Session->setFlash(__('The title could not be deleted. Please, try again.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'titles/' . $idFights));
    }

    /**
     * ibopadmin_getFights method
     *
     * @param string $title
     * @return void
     */
    public function ibopadmin_getFights($title = null) {
        $this->layout = 'ajax';

        $fights = $this->Fight->find(
                'all', array(
            'conditions' => array(
                'Fight.title LIKE' => '%' . $title . '%'
            ),
            'fields' => array(
                'Fight.id',
                'Fight.title'
            ),
            'recursive' => -1,
            'limit' => 50
                )
        );

        $newFights = array();

        foreach ($fights as $fight) {
            $newFight['id'] = $fight['Fight']['id'];
            $newFight['label'] = $fight['Fight']['title'];
            $newFights[] = $newFight;
        }

        $this->set('fights', $newFights);
    }

    /**
     * getTitleName method
     *
     * @param string $id
     * @return string
     */
    public function getTitleName($id = null) {
        if ($id != null) {
            $fights = $this->Fight->find(
                    'first', array(
                'conditions' => array(
                    'Fight.id' => $id
                ),
                'fields' => array(
                    'Fight.title'
                ),
                'recursive' => -1
                    )
            );
            return $fights['Fight']['title'];
        }
    }

    /**
     * ibopadmin_lock method
     *
     * @param string $id
     * @param string $lock
     * @return void
     */
    public function ibopadmin_lock($id = null, $lock = null) {
        $lockPerson['Fight']['id'] = base64_decode($id);
        $lockPerson['Fight']['locked'] = $lock;
        if ($this->Fight->save($lockPerson)) {
            if ($lock == 1) {
                $this->Session->setFlash(__('The register has been locked.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The register has been unlocked.', true), 'alert-success');
            }
        } else {
            $this->Session->setFlash(__('The status could not be changed.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'edit/' . $id));
    }

    /*
     * deleteAccent method
     * 
     * @param string $keyword
     * @return string
     */

    public function deleteAccent($keyword = null) {
        $no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ", "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹");
        $permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
        $texto = str_replace($no_permitidas, $permitidas, $keyword);
        return $texto;
    }
    
    //ibopadmin_getFights
    /*
    Widget con el resultado de las 100 ultimas peleas
     * 
     */
    public function widgetFightResults()
    {
        $this->layout = false;//disable the default layouts
        
        $finalArray = array();
        
        $this->Fight->unbindModel(array(
            'hasAndBelongsToMany' => array(
                'Image',
                'Note',
                'Title',
                'Video'
            ),
            'belongsTo' => array(
                'Sources',
                'Weights',
                'FinalFigths',
                'Rules',
                'Event'
            )
            
        ),false);
        
        $this->Event->unbindModel(array(
            'hasAndBelongsToMany' => array(
                'Fight',
                'Image',
                'PromotionalCompany',
                'Video'
            ),
            'hasMany' => array(
                'EventsJob',
                'EventsImage'
            ),
            'belongsTo' => array(
                'Venues',
                'Locations'
            )
            
        ),false);
        /*
        Trae los ultimos 100 eventos
         **/
        $events = $this->Event->find(
           'all', array(
                'order' => array(
                    'Event.date' => 'DESC'
                ),
                'limit' => 205,
                'fields' => array(
                    'Event.id',
                    'Event.date',
                    'Event.venues_id',
                    'Event.locations_id',
                ),
            )
         );
        
        $fights = array();
        $fightsArray = array();        
        $dateArray = array();
        $venuesArray = array();
        $locationsArray = array();
        
        foreach ($events as $event)
        {
            $theFight = $this->Fight->find(
                'all', array(
                     'order' => array(
                         'Fight.id' => 'DESC'
                     ),
                    'conditions' => array(
                        'Fight.events_id' => $event['Event']['id']
                    ),
                     'limit' => 100,
                     'fields' => array(
                         'Fight.id',
                         'Fight.winner',
                         'Fight.via',
                         'Fight.boxed_roundas',
                         'Fight.types_id',
                         'Fight.events_id',
                     ),

                 )
            );
                        
            foreach ($theFight as $aFight)
            {
                $element = array();
                $element['id'] = $aFight['Fight']['id'];
                $element['fecha'] = $event['Event']['date'];
                array_push($dateArray,$element);
                
                $venueElement=array();
                $venueElement['id'] = $aFight['Fight']['id'];
                $venueElement['venue'] = $event['Event']['venues_id'];
                if (isset($event['Event']['locations_id']))
                {
                    $venueElement['locationId'] = $event['Event']['locations_id'];    
                }
                else
                {
                    $venueElement['locationId'] = 'UNKNOWN';    
                }
                
                array_push($venuesArray,$venueElement);
            }
            
            if (count($theFight)>0)//if a fight was found in an event
            {   
                array_push($fightsArray,$theFight);
            }
        }
        
        foreach ($fightsArray as $aFight)
        {
            foreach ($aFight as $theFight)
            {
                array_push($fights,$theFight);
            }            
        }
        
        $fighterNames = array();
        $finalArray = array();
        $tempArray = array();
        $numericIndex = 0;
        
        $fightType = 0;
       
        
        foreach ($fights as $fight)
        {
            $numericIndex = 0;
            
            foreach ($fight as $fightElement)//when fighters on a fight are two or more
            {
                if (count($fightElement)==6){
                    $tempArray['fight_id'] = $fight['Fight']['id'];
                    $tempArray['winner'] = $fight['Fight']['winner'];
                    $tempArray['via'] = $fight['Fight']['via'];
                    $tempArray['rounds'] = $fight['Fight']['boxed_roundas'];
                }
                
                $fightType = $fight['Fight']['types_id'];
                
                if ($fightType==1){//just add boxing fights
                    if ((count($fightElement)!=6) && (count($fightElement)!=0))//if the fight element have more than 4 array values, it mean that is the identity of a fight
                    {
                        foreach ($fightElement as $theFighter){
                            $fighterName = $this->Identity->find(
                                'all', array(
                                     'conditions' => array(
                                        'Identity.id' => $theFighter['identities_id']
                                      ),
                                      'fields' => array(
                                        'Identity.name',
                                        'Identity.last_name'
                                      ),
                                         'recursive' => -2
                                      )
                            );

                            foreach ($fighterName as $theName)
                            {
                                array_push($tempArray,implode(" ",$theName['Identity']));
                            }

                            if (isset($tempArray[0]))
                            {
                                $tempArray['fighter1'] = $tempArray[0];
                                unset($tempArray[0]);
                            }
                            if (isset($tempArray[1]))
                            {
                                $tempArray['fighter2'] = $tempArray[1];
                                unset($tempArray[1]);
                            }
                        }                    
                    }
                }
            }
            
            $arrayOrdenado = array(); //array ordered as needed
            if ($tempArray['winner']==1)
            {
                if (isset($tempArray['fighter1']))
                {
                    $arrayOrdenado['peleador1'] = $tempArray['fighter1'];
                }
                if (isset($tempArray['fighter2']))
                {
                    $arrayOrdenado['peleador2'] = $tempArray['fighter2'];
                }
            }
            else if ($tempArray['winner']==2)
            {
                if (isset($tempArray['fighter2']))
                {
                    $arrayOrdenado['peleador1'] = $tempArray['fighter2'];
                }
                if (isset($tempArray['fighter1']))
                {
                    $arrayOrdenado['peleador2'] = $tempArray['fighter1'];
                }
            }
            else
            {
                if (isset($tempArray['fighter1']))
                {
                    $arrayOrdenado['peleador1'] = $tempArray['fighter1'];
                }
                if (isset($tempArray['fighter2']))
                {
                    $arrayOrdenado['peleador2'] = $tempArray['fighter2'];
                }
            }
            $arrayOrdenado['siglaResultado'] = $tempArray['via'];
            $arrayOrdenado['totalRounds'] = $tempArray['rounds'];
            
            
            //If some value is null, or if there is a fighter missing,
            // dont save the figtht
            $addFight = 1;//determine if a fight is added or not
            if ( !isset($arrayOrdenado['peleador1']) || !isset($arrayOrdenado['peleador2']) || !isset($arrayOrdenado['siglaResultado']) || !isset($arrayOrdenado['totalRounds']))
            {
                $addFight = 0;
            }
            
            foreach ($dateArray as $theDate)
            {
                if ($fight['Fight']['id']==$theDate['id'])
                {
                    
                    $arrayOrdenado['fecha'] = $theDate['fecha'];
                }                
            }
            
            foreach ($venuesArray as $theVenue)
            {
                if ($fight['Fight']['id']==$theVenue['id'])
                {
                    $venue = $this->Venue->find(
                        'first', array(
                            'conditions' => array(
                                'Venue.id' => $theVenue['venue']
                            ),
                            'fields' => array(
                                'Venue.name',
                            ),
                        )
                    );
                }
               if (count($venue)>0)
               {
                   $arrayOrdenado['venue'] = $venue['Venue']['name'];
               }
               else if (count($venue)==0)
               {
                   //iterate through the locations array, and search for a match on the fight id
                   if (strcmp($theVenue['locationId'],'UNKNOWN')!=0)
                   {
                        $currentLocation = $this->getLocation($theVenue['locationId']);
                        if (count($currentLocation)>0)
                        {
                             $arrayOrdenado['venue'] = "PUM " . $currentLocation['Countries']['name'] . " " . $currentLocation['Cities']['name'];;    

                        }
                   }
                   $arrayOrdenado['venue'] = 'UNKNOWN ';
               }
            }
            
            if ($addFight==1)
            {
                array_push($finalArray,$arrayOrdenado);    
            }            
            $tempArray = array();
        } 
        $this->set('fights', $finalArray);
    }
    

}
