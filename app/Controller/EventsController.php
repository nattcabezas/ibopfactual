<?php

App::uses('AppController', 'Controller');

/**
 * Events Controller
 *
 * @property Event $Event
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class EventsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array(
        'Event',
        'Identity',
        'Venue',
        'Location',
        'Country',
        'State',
        'City',
        'Image',
        'EventsImage',
        'Video',
        'EventsVideo',
        'EventsJob',
        'EventsNote',
        'PromotionalCompany',
        'EventsPromotionalCompany',
        'EventsSponsor',
        'Source',
        'Fight',
        'Note'
    );
    public $helper = array('getUrl', 'QrCode', 'getage');

    /**
     * index method
     *
     * @param string $url
     * @return void
     */
    public function index($url = null) {
        if ($url != null) {
            list($id, $name) = explode('-', $url);

            $event = $this->Event->find('first', array(
                'conditions' => array(
                    'Event.id' => $id
                ),
                'recursive' => -1
            ));

            /* $fights = $this->Fight->find('all', array(
              'conditions' => array(
              'Fight.events_id' => $id
              ),
              'recursive' => 2
              )); */

            $sponsors = $this->EventsSponsor->find('all', array(
                'conditions' => array(
                    'EventsSponsor.events_id' => $id
                ),
                'recursive' => -1
            ));

            $image = $this->EventsImage->find('first', array(
                'conditions' => array(
                    'EventsImage.events_id' => $id,
                    'EventsImage.principal' => 1
                )
            ));

            if ($event['Event']['venues_id'] != null) {
                $venue = $this->Venue->find('first', array(
                    'conditions' => array(
                        'Venue.id' => $event['Event']['venues_id']
                    ),
                    'recursive' => -1
                ));

                if ($venue['Venue']['locations_id'] != null) {
                    $location = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $venue['Venue']['locations_id']
                        )
                    ));
                } else {
                    $location = array();
                }
            } else {
                $venue = array();
                if ($event['Event']['locations_id'] != null) {
                    $location = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $event['Event']['locations_id']
                        )
                    ));
                } else {
                    $location = array();
                }
            }

            $countImages = $this->EventsImage->find('count', array(
                'conditions' => array(
                    'EventsImage.events_id' => $id
                )
            ));

            $this->set('event', $event);
            $this->set('venue', $venue);
            $this->set('location', $location);
            $this->set('image', $image);
            $this->set('sponsors', $sponsors);
            $this->set('id', $id);
            $this->set('countImages', $countImages);
        
        }
    }

    /**
     * search method
     *
     * @param string $url
     * @return void
     */
    public function search($date = null) {
        if ($this->request->is('post')) {
            $this->redirect(array('action' => 'search/' . $this->request->data('date-event')));
        }

        if ($date != null) {

            $this->Event->unbindModel(array(
                'hasAndBelongsToMany' => array(
                    'Fight',
                    'Image',
                    'PromotionalCompany',
                    'Video'
                ),
                'hasMany' => array(
                    'EventsJob'
                )
            ));

            $events = $this->Event->find('all', array(
                'conditions' => array(
                    'Event.date' => $date
                ),
                'fields' => array(
                    'Event.id',
                    'Event.name',
                    'Event.venues_id',
                    'Event.locations_id'
                ),
                'recursive' => 2,
                'limit' => 20
            ));

            $newEvents = array();
            foreach ($events as $event) {

                if (($event['Event']['venues_id'] != "") && ($event['Event']['venues_id'] != 0)) {
                    $venue = $this->Venue->find('first', array(
                        'conditions' => array(
                            'Venue.id' => $event['Event']['venues_id']
                        ),
                        'fields' => array(
                            'Venue.locations_id'
                        ),
                        'recursive' => -1
                    ));

                    if (($venue['Venue']['locations_id'] != "") && ($venue['Venue']['locations_id'] != 0)) {
                        $location = $this->Location->find('first', array(
                            'conditions' => array(
                                'Location.id' => $venue['Venue']['locations_id']
                            )
                        ));
                    }

                    $event['Countries']['name'] = $location['Countries']['name'];
                    $event['Countries']['flag'] = $location['Countries']['flag'];
                    $event['States']['name'] = $location['States']['name'];
                    $event['Cities']['name'] = $location['Cities']['name'];
                } else if (($event['Event']['locations_id'] != "") && ($event['Event']['locations_id'] != 0)) {

                    $location = $this->Location->find('first', array(
                        'conditions' => array(
                            'Location.id' => $event['Event']['locations_id']
                        )
                    ));

                    $event['Countries']['name'] = $location['Countries']['name'];
                    $event['Countries']['flag'] = $location['Countries']['flag'];
                    $event['States']['name'] = $location['States']['name'];
                    $event['Cities']['name'] = $location['Cities']['name'];
                } else {

                    $event['Countries']['name'] = "";
                    $event['Countries']['flag'] = "";
                    $event['States']['name'] = "";
                    $event['Cities']['name'] = "";
                }

                $newEvents[] = $event;
            }
            $this->set('searchDate', $date);
            $this->set('events', $newEvents);
        }
    }

    /**
     * listFights method
     *
     * @param string $idEvent
     * @return void
     */
    public function listFights($idEvent = null) {
        
        $this->layout = 'ajax';
        if ($idEvent != null) {


            $this->Fight->bindModel(array(
                'hasMany' => array(
                    'FightsTitle' => array(
                        'className' => 'FightsTitle',
                        'foreignKey' => 'fights_id'
                    )
                ),
                'belongsTo' => array(
                    'Type' => array(
                        'className' => 'Type',
                        'foreignKey' => 'types_id',
                        'fields' => array(
                            'id',
                            'name',
                        )
                    )
                )
            ));

            $fights = $this->Fight->find('all', array(
                'conditions' => array(
                    'Fight.events_id' => $idEvent
                ),
                'fields' => array(
                    'Fight.id',
                    'Fight.title',
                    'Fight.weights_id',
                    'Fight.via',
                    'Fight.schedule_rounds',
                    'Fight.time_per_round',
                    'Fight.boxed_roundas',
                    'Fight.time',
                    'Fight.types_id',
                    'Fight.position'
                ),
                'order'=>array('Fight.position ASC'),
                'recursive' => 2
            ));
            
            
            $eventJobs = $this->EventsJob->find(
                'all', array(
                    'conditions' => array(
                        'EventsJob.events_id' => $idEvent
                    ),
                    'fields' => array(
                        'Identities.id',
                        'Identities.name',
                        'Identities.last_name',
                        'EventsJob.type'
                    )
                )
            );
            
            $this->set('eventJobs', $eventJobs);
            $this->set('fights', $fights);
        }
    }
    
    /**
    * ibopadmin_fightOrder method
    * @return void 
    */
    public function ibopadmin_fightOrder(){
        if($this->request->is('post')){
            //debug($this->request->data());
            $newFightOrder['Fight']['id']         = $this->request->data('id');
            $newFightOrder['Fight']['position']   = $this->request->data('newOrder');
            $this->Fight->save($newFightOrder);
        }
        return die();
    }

    /**
     * showImages method
     *
     * @param string $idEvent
     * @return void
     */
    public function showImages($idEvent = null) {
        $this->layout = 'ajax';
        if ($idEvent != null) {
            $images = $eventsImage = $this->EventsImage->find('all', array(
                'conditions' => array(
                    'EventsImage.events_id' => $idEvent
                )
            ));

            $this->set('images', $images);
        }
    }
     public function getImagesEvent($idEvent = null){
        
        $this->layout = 'ajax';
        
        $images = $this->EventsImage->find('all', array(
            'conditions' => array(
                'EventsImage.events_id'  => $idEvent
                  
                       
            )
        ));
       
          
            
        $this->set('images', $images);
         $this->set('idEvent', $idEvent);
        
    }

    /**
     * ibopadmin_index method
     *
     * @return void
     */
    public function ibopadmin_index() {
        $this->Event->recursive = 0;
        $this->Paginator->settings = array(
            'order' => array(
                'Event.id' => 'DESC'
            )
        );
        $this->set('events', $this->Paginator->paginate());
    }

    /**
     * ibopadmin_add method
     *
     * @return void
     */
    public function ibopadmin_add() {
        if ($this->request->is('post')) {
            $this->Event->create();
            if ($this->Event->save($this->request->data)) {
                $this->Session->setFlash(__('The event has been saved.', true), 'alert-success');
                return $this->redirect(array('action' => 'edit/' . base64_encode($this->Event->getLastInsertId())));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.', true), 'alert-danger');
            }
        }
        $this->set('source', $this->Source->find('list'));
    }

    /**
     * ibopadmin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function ibopadmin_edit($id = null) {
        $id = base64_decode($id);
        if (!$this->Event->exists($id)) {
            $this->Session->setFlash(__('Invalid event', true), 'alert-danger');
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Event->save($this->request->data)) {
                $this->Session->setFlash(__('The event has been saved.', true), 'alert-success');
                return $this->redirect(array('action' => 'edit/' . base64_encode($id)));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.', true), 'alert-danger');
            }
        } else {

            $options = array(
                'conditions' => array(
                    'Event.' . $this->Event->primaryKey => $id
                )
            );

            $this->Event->unbindModel(
                    array(
                        'belongsTo' => array(
                            'Locations',
                            'Venues'
                        ),
                        'hasAndBelongsToMany' => array(
                            'Fight',
                            'PromotionalCompany',
                            'Image',
                            'Video'
                        ),
                        'hasMany' => array(
                            'EventsDoctor'
                        )
                    )
            );

            $jobs = $this->EventsJob->find(
                    'all', array(
                'conditions' => array(
                    'EventsJob.events_id' => $id
                ),
                'fields' => array(
                    'EventsJob.id',
                    'EventsJob.type',
                    'Identities.name',
                    'Identities.last_name'
                )
                    )
            );

            $sponsors = $this->EventsSponsor->find(
                    'all', array(
                'conditions' => array(
                    'EventsSponsor.events_id' => $id
                ),
                'fields' => array(
                    'EventsSponsor.id',
                    'EventsSponsor.name'
                )
                    )
            );

            $this->set('idEvent', $id);
            $this->set('eventName', $this->getEventName($id));
            $this->set('jobs', $jobs);
            $this->set('sponsors', $sponsors);
            $this->set('event', $this->Event->find('first', $options));
            $this->set('source', $this->Source->find('list'));
        }
    }

    /**
     * ibopadmin_saveJob method
     *
     * @return void
     */
    public function ibopadmin_saveJob() {
        if ($this->request->is('post')) {
            if ($this->EventsJob->save($this->request->data)) {
                $this->Session->setFlash(__('The job has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The job could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('EventsJob.events_id'))));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * ibopadmin_deleteJob method
     *
     * @return void
     */
    public function ibopadmin_deleteJob() {
        if ($this->request->is('post')) {
            $this->EventsJob->id = $this->request->data('EventsJob.id');
            if ($this->EventsJob->delete()) {
                $this->Session->setFlash(__('The job has been deleted.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The job could not be deleted. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('EventsJob.events_id'))));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * ibopadmin_saveSponsor method
     *
     * @return void
     */
    public function ibopadmin_saveSponsor() {
        if ($this->request->is('post')) {
            if ($this->EventsSponsor->save($this->request->data)) {
                $this->Session->setFlash(__('The Sponsor has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Sponsor could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('EventsSponsor.events_id'))));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * ibopadmin_deleteSponsor method
     *
     * @return void
     */
    public function ibopadmin_deleteSponsor() {
        if ($this->request->is('post')) {
            $this->EventsSponsor->id = $this->request->data('EventsSponsor.id');
            if ($this->EventsSponsor->delete()) {
                $this->Session->setFlash(__('The Sponsor has been deleted.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Sponsor could not be deleted. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'edit/' . base64_encode($this->request->data('EventsSponsor.events_id'))));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function ibopadmin_save_location($idEvent = null, $idVenue = null)
    {
        $idEvent = base64_decode($idEvent);
        
        $this->Session->setFlash(__('Venue added successfully to the Event', true), 'alert-success');
        
        $this->Venue->unbindModel(
            array(
                'hasAndBelongsToMany' => array(
                    'Image',
                    'Video'
                )
            )
        );
        $venue = $this->Venue->find(
            'first', array(
                'conditions' => array(
                    'Venue.id' => $idVenue
                ),
            'fields' => array(
                    'Venue.name'
                )
            )
        );
        
        $this->Event->unbindModel(
            array(
                'belongsTo' => array(
                    'Locations'
                ),
                'hasAndBelongsToMany' => array(
                    'Fight',
                    'PromotionalCompany',
                    'Image',
                    'Video'
                ),
                'hasMany' => array(
                    'EventsJob',
                    'EventsImage'
                )
            )
        );
        $event = $this->Event->find(
            'first', array(
                'conditions' => array(
                    'Event.id' => $idEvent
                ),
            'fields' => array(
                    'Event.id',
                    'Event.name'
                )
            )
        );
        
        $this->set('event', $event);
        $this->set('venue', $venue);
        
        $this->Event->read(null, $idEvent);
        $this->Event->set(array(
              'venues_id' => $idVenue
        ));
        $this->Event->save();
        
    }
    
    /**
     * funcion que muestra la localizacion(venue) del evento
     * @param  integer $idEvent 
     * @return void          
     */
    public function ibopadmin_location($idEvent = null) {
        
        $idEvent = base64_decode($idEvent);
        
        if ($this->request->is('post')) {
            if ($this->Event->save($this->request->data)) {
                $this->Session->setFlash(__('The event has been saved.', true), 'alert-success');
                return $this->redirect(array('action' => 'location/' . base64_encode($idEvent)));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.', true), 'alert-danger');
            }
        }

        $this->Event->unbindModel(
                array(
                    'belongsTo' => array(
                        'Locations'
                    ),
                    'hasAndBelongsToMany' => array(
                        'Fight',
                        'PromotionalCompany',
                        'Image',
                        'Video'
                    ),
                    'hasMany' => array(
                        'EventsDoctor'
                    )
                )
        );

        $venue = $this->Event->find(
                'first', array(
            'conditions' => array(
                'Event.id' => $idEvent
            ),
            'fields' => array(
                'Event.id',
                'Venues.id',
                'Venues.name',
            )
                )
        );

        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('idEvent', $idEvent);
        $this->set('venue', $venue);
    }

    /**
     * funcion que carga la geo localizacion	 
     * @param  integer $idEvent 	
     * @return void
     */
    public function ibopadmin_geoLocation($idEvent = null) {
        $idEvent = base64_decode($idEvent);

        $this->Event->unbindModel(
                array(
                    'belongsTo' => array(
                        'Venues'
                    ),
                    'hasAndBelongsToMany' => array(
                        'Fight',
                        'PromotionalCompany',
                        'Image',
                        'Video'
                    ),
                    'hasMany' => array(
                        'EventsDoctor'
                    )
                )
        );

        $location = $this->Event->find(
                'first', array(
            'conditions' => array(
                'Event.id' => $idEvent
            ),
            'fields' => array(
                'Event.id',
                'Event.venues_id',
                'Locations.id',
                'Locations.countries_id',
                'Locations.states_id',
                'Locations.cities_id'
            )
                )
        );
        
       $setLocation = 1;
        
        if (isset($location['Event']['venues_id'])){
            $physicalLocation = $this->Venue->find(
                'first', array(
                    'conditions' => array(
                        'Venue.id' => $location['Event']['venues_id']
                    ),
                    'fields' => array(
                        'Venue.locations_id'
                    )
                )
            );
        }
        else
        {
            $setLocation = 0;
        }
        
        if (isset($physicalLocation['Venue']['locations_id'])){
            $locationIdentifiers = $this->Location->find(
                'first', array(
                    'conditions' => array(
                        'Location.id' => $physicalLocation['Venue']['locations_id']
                    ),
                    'fields' => array(
                        'Location.countries_id',
                        'Location.states_id',
                        'Location.cities_id'
                    )
                )                 
            );
        }
        else
        {
            $setLocation = 0;
        }
                
        if ($this->request->is('post')) {
            if ($this->Location->save($this->request->data)) {
                if ($location['Locations']['id'] == null) {
                    $event['Event']['id'] = $idEvent;
                    $event['Event']['locations_id'] = $this->Location->getLastInsertId();
                    if ($this->Event->save($event)) {
                        $this->Session->setFlash(__('The event location has been saved.', true), 'alert-success');
                        return $this->redirect(array('action' => 'geoLocation/' . base64_encode($idEvent)));
                    } else {
                        $this->Session->setFlash(__('The event location could not be saved. Please, try again.', true), 'alert-danger');
                        return $this->redirect(array('action' => 'geoLocation/' . base64_encode($idEvent)));
                    }
                } else {
                    $this->Session->setFlash(__('The event location has been saved.', true), 'alert-success');
                    return $this->redirect(array('action' => 'geoLocation/' . base64_encode($idEvent)));
                }
            } else {
                $this->Session->setFlash(__('The event location could not be saved. Please, try again.', true), 'alert-danger');
                return $this->redirect(array('action' => 'geoLocation/' . base64_encode($idEvent)));
            }
        }
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('idEvent', $idEvent);
        $this->set('location', $location);
        if ($setLocation == 1)
        {
            $this->set('locationIdentifiers', $locationIdentifiers);    
        }
        
    }

    /**
     * funcion que muestra las imagenes del sistama para agregarlas al evento
     * @param  integer $idEvent 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_images($idEvent = null, $page = 1) {
        $idEvent = base64_decode($idEvent);

        $images = $this->Image->find(
                'all', array(
            'fields' => array(
                'Image.id',
                'Image.url'
            ),
            'order' => array(
                'Image.id' => 'DESC'
            ),
            'recursive' => -1,
            'limit' => 9,
            'page' => $page
                )
        );

        if ($this->request->is('post')) {
            $eventImageExist = $this->EventsImage->find(
                    'count', array(
                'conditions' => array(
                    'EventsImage.events_id' => $this->request->data('EventsImage.events_id'),
                    'EventsImage.images_id' => $this->request->data('EventsImage.images_id'),
                )
                    )
            );

            if ($eventImageExist > 0) {
                $this->Session->setFlash(__('The image is already added', true), 'alert-danger');
            } else {
                if ($this->EventsImage->save($this->request->data)) {
                    $this->Session->setFlash(__('The image has been saved in the event.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }

        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('images', $images);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
    }

    /**
     * lista de imagenes del evento
     * @param  integer $idEvent 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_eventsImages($idEvent = null, $page = 1) {
        $idEvent = base64_decode($idEvent);

        if ($this->request->is('post')) {
            if ($this->EventsImage->delete($this->request->data('EventsImage.id'))) {
                $this->Session->setFlash(__('The Image has been deleted', true), 'alert-success');
                $this->redirect(array('action' => 'eventsImages/' . base64_encode($idEvent) . '/' . $page));
            } else {
                $this->Session->setFlash(__('The Image could not be deleted. Please, try again.', true), 'alert-danger');
                $this->redirect(array('action' => 'eventsImages/' . base64_encode($idEvent) . '/' . $page));
            }
        }

        $images = $this->EventsImage->find(
                'all', array(
            'conditions' => array(
                'EventsImage.events_id' => $idEvent
            ),
            'fields' => array(
                'EventsImage.id',
                'EventsImage.principal',
                'Images.id',
                'Images.url'
            ),
            'order' => array(
                'EventsImage.principal' => 'DESC',
                'EventsImage.id' => 'ASC'
            ),
            'limit' => 9,
            'page' => $page
                )
        );
        
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('images', $images);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
    }
    
    /*
    This method call the assign all method in 
     * the images controller
     **/
    public function ibopadmin_callAssignAllImages($event, $idImage)
    {
        $this->Images->ibopadmin_assign_all($event, $idImage);
    }
    
    
    /**
     * funcion que busca en las imagenes del sitio
     * @param  integer $idEvent 
     * @param  integer $keyword 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_searchImages($idEvent = null, $keyword = null, $page = 1) {
        $idEvent = base64_decode($idEvent);
        $keyword = base64_decode($keyword);

        if ($keyword != null) {
            $images = $this->Image->find(
                    'all', array(
                'conditions' => array(
                    'Image.title LIKE' => '%' . $keyword . '%'
                ),
                'fields' => array(
                    'Image.id',
                    'Image.url'
                ),
                'order' => array(
                    'Image.id' => 'DESC'
                ),
                'recursive' => -1,
                'limit' => 9,
                'page' => $page
                    )
            );
        } else {
            $images = array();
        }

        if ($this->request->is('post')) {
            $eventImageExist = $this->EventsImage->find(
                    'count', array(
                'conditions' => array(
                    'EventsImage.events_id' => $this->request->data('EventsImage.events_id'),
                    'EventsImage.images_id' => $this->request->data('EventsImage.images_id'),
                )
                    )
            );

            if ($eventImageExist > 0) {
                $this->Session->setFlash(__('The image is already added', true), 'alert-danger');
            } else {
                if ($this->EventsImage->save($this->request->data)) {
                    $this->Session->setFlash(__('The image has been saved in the event.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('idEvent', $idEvent);
        $this->set('images', $images);
        $this->set('page', $page);
        $this->set('keywordData', $keyword);
    }

    /**
     * ibopadmin_imagePrincipal
     * @return void
     */
    public function ibopadmin_imagePrincipal() {
        if ($this->request->is('post')) {
            if ($this->request->data('ImagePrincipal.make_principal') == 1) {
                $countPrincipal = $this->EventsImage->find(
                        'count', array(
                    'conditions' => array(
                        'EventsImage.events_id' => $this->request->data('ImagePrincipal.idEvent'),
                        'EventsImage.principal' => 1
                    )
                        )
                );
                if ($countPrincipal == 0) {
                    $eventsImage['EventsImage']['id'] = $this->request->data('ImagePrincipal.id');
                    $eventsImage['EventsImage']['principal'] = 1;
                    if ($this->EventsImage->save($eventsImage)) {
                        $this->Session->setFlash(__('The image has been selected.', true), 'alert-success');
                    } else {
                        $this->Session->setFlash(__('The Image could not be selected. Please, try again.', true), 'alert-danger');
                    }
                } else {
                    $this->Session->setFlash(__('The Event already has a main image.', true), 'alert-danger');
                }
            } else {
                $eventsImage['EventsImage']['id'] = $this->request->data('ImagePrincipal.id');
                $eventsImage['EventsImage']['principal'] = null;
                if ($this->EventsImage->save($eventsImage)) {
                    $this->Session->setFlash(__('The image has been unselected.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The Image could not be unselected. Please, try again.', true), 'alert-danger');
                }
            }
            return $this->redirect(array('action' => 'eventsImages/' . base64_encode($this->request->data('ImagePrincipal.idEvent')) . '/' . $this->request->data('ImagePrincipal.page')));
        } else {
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * funcion que carga todos los videos para guardarlos en el evento
     * @param  integer $idEvent 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_videos($idEvent = null, $page = 1) {
        $idEvent = base64_decode($idEvent);

        if ($this->request->is('post')) {
            $eventVideoExist = $this->EventsVideo->find(
                    'count', array(
                'conditions' => array(
                    'EventsVideo.events_id' => $this->request->data('EventsVideo.events_id'),
                    'EventsVideo.videos_id' => $this->request->data('EventsVideo.videos_id')
                )
                    )
            );
            if ($eventVideoExist > 0) {
                $this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
            } else {
                if ($this->EventsVideo->save($this->request->data)) {
                    $this->Session->setFlash(__('The event video has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The event video could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }

        $videos = $this->Video->find(
                'all', array(
            'fields' => array(
                'Video.id',
                'Video.title',
                'Video.mp4'
            ),
            'order' => array(
                'Video.id' => 'DESC'
            ),
            'recursive' => -1,
            'limit' => 10,
            'page' => $page
                )
        );
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('videos', $videos);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
    }

    /**
     * funcion que lista los videos del evento
     * @param  integer $idEvent 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_eventsVideos($idEvent = null, $page = 1) {
        $idEvent = base64_decode($idEvent);

        if ($this->request->is('post')) {
            if ($this->EventsVideo->delete($this->request->data('EventsVideo.id'))) {
                $this->Session->setFlash(__('The video has been deleted', true), 'alert-success');
                $this->redirect(array('action' => 'eventsVideos/' . base64_encode($idEvent) . '/' . $page));
            } else {
                $this->Session->setFlash(__('The video could not be deleted. Please, try again.', true), 'alert-danger');
                $this->redirect(array('action' => 'eventsVideos/' . base64_encode($idEvent) . '/' . $page));
            }
        }

        $videos = $this->EventsVideo->find(
                'all', array(
            'conditions' => array(
                'EventsVideo.events_id' => $idEvent
            ),
            'fields' => array(
                'EventsVideo.id',
                'Videos.id',
                'Videos.title',
                'Videos.mp4'
            ),
            'order' => array(
                'EventsVideo.id' => 'DESC',
            ),
            'limit' => 10,
            'page' => $page
                )
        );
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('videos', $videos);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
    }

    /**
     * funcion que busca los videos
     * @param  integer $idEvent 
     * @param  string  $keyword 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_searchVideos($idEvent = null, $keyword = null, $page = 1) {
        $idEvent = base64_decode($idEvent);
        $keyword = base64_decode($keyword);

        if ($this->request->is('post')) {
            $eventVideoExist = $this->EventsVideo->find(
                    'count', array(
                'conditions' => array(
                    'EventsVideo.events_id' => $this->request->data('EventsVideo.events_id'),
                    'EventsVideo.videos_id' => $this->request->data('EventsVideo.videos_id')
                )
                    )
            );
            if ($eventVideoExist > 0) {
                $this->Session->setFlash(__('The Video are already added', true), 'alert-danger');
            } else {
                if ($this->EventsVideo->save($this->request->data)) {
                    $this->Session->setFlash(__('The event video has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The event video could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }

        if ($keyword != null) {
            $videos = $this->Video->find(
                    'all', array(
                'conditions' => array(
                    'Video.title LIKE' => '%' . $keyword . '%'
                ),
                'fields' => array(
                    'Video.id',
                    'Video.title',
                    'Video.mp4'
                ),
                'order' => array(
                    'Video.id' => 'DESC'
                ),
                'recursive' => -1,
                'limit' => 10,
                'page' => $page
                    )
            );
        } else {
            $videos = array();
        }
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('videos', $videos);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
        $this->set('keywordData', $keyword);
    }

    /*
     * funcion que carga todos los notes para guardarlos en el evento
     * @param  integer $idEvent 
     * @param  integer $page    
     * @return void           
     */

    public function ibopadmin_notes($idEvent = null, $page = 1) {



        $idEvent = base64_decode($idEvent);

        if ($this->request->is('post')) {
            $eventNoteExist = $this->EventsNote->find(
                    'count', array(
                'conditions' => array(
                    'EventsNote.events_id' => $this->request->data('EventsNote.events_id'),
                    'EventsNote.notes_id' => $this->request->data('EventsNote.notes_id')
                )
                    )
            );

            if ($eventNoteExist > 0) {
                $this->Session->setFlash(__('The note is already added', true), 'alert-danger');
            } else {
                if ($this->EventsNote->save($this->request->data)) {
                    $this->Session->setFlash(__('The event note has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The event note could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }
        $notes = $this->Note->find(
                'all', array(
            'fields' => array(
                'Note.id',
                'Note.title',
                'Note.note'
            ),
            'order' => array(
                'Note.id' => 'DESC'
            ),
            'recursive' => -1,
            'limit' => 10,
            'page' => $page
                )
        );
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('notes', $notes);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
    }

    /**
     * funcion que busca los notes
     * @param  integer $idEvent 
     * @param  string  $keyword 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_searchNotes($idEvent = null, $keyword = null, $page = 1) {
        $idEvent = base64_decode($idEvent);
        $keyword = base64_decode($keyword);

        if ($this->request->is('post')) {
            $eventNoteExist = $this->EventsNote->find(
                    'count', array(
                'conditions' => array(
                    'EventsNote.events_id' => $this->request->data('EventsNote.events_id'),
                    'EventsNote.notes_id' => $this->request->data('EventsNote.notes_id')
                )
                    )
            );
            if ($eventNoteExist > 0) {
                $this->Session->setFlash(__('The note is already added', true), 'alert-danger');
            } else {
                if ($this->EventsNote->save($this->request->data)) {
                    $this->Session->setFlash(__('The event note has been saved in.', true), 'alert-success');
                } else {
                    $this->Session->setFlash(__('The event note could not be saved. Please, try again.', true), 'alert-danger');
                }
            }
        }

        if ($keyword != null) {
            $notes = $this->Note->find(
                    'all', array(
                'conditions' => array(
                    'Note.title LIKE' => '%' . $keyword . '%'
                ),
                'fields' => array(
                    'Note.id',
                    'Note.title',
                    'Note.note'
                ),
                'order' => array(
                    'Note.id' => 'DESC'
                ),
                'recursive' => -1,
                'limit' => 10,
                'page' => $page
                    )
            );
        } else {
            $notes = array();
        }

        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('notes', $notes);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
        $this->set('keywordData', $keyword);
    }

    /**
     * funcion que lista las notas del evento
     * @param  integer $idEvent 
     * @param  integer $page    
     * @return void           
     */
    public function ibopadmin_eventsNotes($idEvent = null, $page = 1) {
        $idEvent = base64_decode($idEvent);

        if ($this->request->is('post')) {
            if ($this->EventsNote->delete($this->request->data('EventsNote.id'))) {
                $this->Session->setFlash(__('The note has been deleted', true), 'alert-success');
                $this->redirect(array('action' => 'eventsNotes/' . base64_encode($idEvent) . '/' . $page));
            } else {
                $this->Session->setFlash(__('The note could not be deleted. Please, try again.', true), 'alert-danger');
                $this->redirect(array('action' => 'eventsNotes/' . base64_encode($idEvent) . '/' . $page));
            }
        }

        $notes = $this->EventsNote->find(
                'all', array(
            'conditions' => array(
                'EventsNote.events_id' => $idEvent
            ),
            'fields' => array(
                'EventsNote.id',
                'Notes.id',
                'Notes.title',
                'Notes.note'
            ),
            'order' => array(
                'EventsNote.id' => 'DESC',
            ),
            'limit' => 10,
            'page' => $page
                )
        );

        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $idEvent));
        $this->set('event', $this->Event->find('first', $options));
        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('notes', $notes);
        $this->set('idEvent', $idEvent);
        $this->set('page', $page);
    }

    /**
     * ibopadmin_listFights function 
     * @param  integer $idEvent   
     * @return void           
     */
    public function ibopadmin_listFights($idEvent = null) {
        $idEvent = base64_decode($idEvent);

        $fights = $this->Fight->find('all', array(
            'conditions' => array(
                'Fight.events_id' => $idEvent
            ),
            'fields' => array(
                'Fight.id',
                'Fight.position',
                'Fight.title'
            ),
            'order' => array(
                'Fight.position' => 'ASC'
             ),
            'recursive' => -1
        ));

        $this->set('eventName', $this->getEventName($idEvent));
        $this->set('fights', $fights);
        $this->set('idEvent', $idEvent);
    }

    /**
     * obtiene los estados del pais
     * @param intiger default option
     * @return void       
     */
    public function ibopadmin_getStates($default = 0) {
        $this->layout = 'ajax';
        if ($this->request->is('post')) {
            $states = $this->State->find('list', array(
                'conditions' => array(
                    'State.countries_id' => $this->request->data('countries_id')
                ), 'order' => array(
                    'State.name' => 'ASC')
            ));
            $this->set('states', $states);
            $this->set('default', $default);
        }
    }

    /**
     * obtiene las ciudades del estado
     * @param intiger default option
     * @return void       
     */
    public function ibopadmin_getCities($default = 0) {
        $this->layout = 'ajax';
        if ($this->request->is('post')) {
            $cities = $this->City->find('list', array(
                'conditions' => array(
                    'City.states_id' => $this->request->data('states_id')
                ), 'order' => array(
                    'City.name' => 'ASC')
            ));
            $this->set('cities', $cities);
            $this->set('default', $default);
        }
    }

    /**
     * ibopadmin_getEvents method
     * @param string $name
     * @return void       
     */
    public function ibopadmin_getEvents($name = null) {
        $this->layout = 'ajax';

        $events = $this->Event->find(
                'all', array(
            'conditions' => array(
                'Event.name LIKE' => '%' . $name . '%'
            ),
            'fields' => array(
                'Event.id',
                'Event.name'
            ),
            'recursive' => -1,
            'limit' => 50
                )
        );

        $newEvents = array();
        foreach ($events as $event) {
            $newEvent['id'] = $event['Event']['id'];
            $newEvent['label'] = $event['Event']['name'];
            $newEvents[] = $newEvent;
        }
        $this->set('events', $newEvents);
    }

    /**
     * ibopadmin_SearchEvent method
     * @return void       
     */
    public function ibopadmin_searchEvent() {
        if ($this->request->is('post')) {
            if ($this->request->data('SearchEvent.toDate') != "") {
                $events = $this->Event->find(
                        'all', array(
                    'conditions' => array(
                        'Event.date BETWEEN ? AND ?' => array(
                            $this->request->data('SearchEvent.fromDate'),
                            $this->request->data('SearchEvent.toDate')
                        )
                    ),
                    'recursive' => -1
                        )
                );
            } else {
                $events = $this->Event->find(
                        'all', array(
                    'conditions' => array(
                        'Event.date' => $this->request->data('SearchEvent.fromDate')
                    ),
                    'recursive' => -1
                        )
                );
            }
            $this->set('events', $events);
        }
    }

    public function getEventName($id = null) {
        if ($id != null) {
            $event = $this->Event->find(
                    'first', array(
                'conditions' => array(
                    'Event.id' => $id
                ),
                'fields' => array(
                    'Event.name'
                ),
                'recursive' => -1
                    )
            );
            return $event['Event']['name'];
        }
    }

    public function ibopadmin_lock($id = null, $lock = null) {
        $lockPerson['Event']['id'] = base64_decode($id);
        $lockPerson['Event']['locked'] = $lock;
        if ($this->Event->save($lockPerson)) {
            if ($lock == 1) {
                $this->Session->setFlash(__('The register has been locked.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The register has been unlocked.', true), 'alert-success');
            }
        } else {
            $this->Session->setFlash(__('The status could not be changed.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'edit/' . $id));
    }
    

    /*
     * Remove a venue from an Event by assigning it
     * to the Unknown venue. Note: Add a unknown 
     * venue and an empty location to the database
     * otherwise it will not work
    **/
    public function ibopadmin_deleteVenueFromEvent($idEvent = null){
        $this->Event->set(array(
            'id' => $idEvent,
            'venues_id' => "",
            'locations_id' => ""
        ));
        if ($this->Event->save())
        {
            echo '1';
        }
        else
        {
            echo '0';
        }
        die();
    }
    

}
