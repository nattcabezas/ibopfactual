<?php
App::uses('AppController', 'Controller');
/**
 * Articles Controller
 *
 * @property Article $Article
 * @author rudysibaja <neo.nou@gmail.com>
 */
class ArticlesController extends AppController {

        public $name 	= "Articles";
        public $uses    = array('Article','Category');
        public $helper  = array('getUrl', 'QrCode', 'getage');
/**
 * Paginate
 *
 * @var array
 */
        public $paginate = array(
		'limit' => 20,
		'order' => array(
                    'Article.date' => 'DESC'
		)
	);

/**
 * index method
 *
 * @return void
 */
        public function index($page = 1){
            $articles = $this->Article->find('all', array(
                'conditions' => array(
                    'Article.publish' => 1,
                    'Article.date <=' => date('Y-m-d H:i:s')
                ),
                'order' => array(
                    'Article.date' => 'DESC'
                ),
                'limit' => 6,
                'page' => $page
            ));

            $categories = $this->Category->find('all', array(
                'fields' => array(
                    'Category.id',
                    'Category.name'
                ),
                'recursive' => -1
            ));

            $this->set('articles', $articles);
            $this->set('categories', $categories);
            $this->set('page', $page);
        }
        
/**
 * single method
 *
 * @param string $url
 * @return void
 */
        public function single($category = null, $url = null){
            if($url != null){
                list($id, $title) = explode('-', $url);
                $article = $this->Article->find('first', array(
                    'conditions' => array(
                        'Article.id' => $id
                    )
                ));
                
                $lastArticles = $this->Article->find('all', array(
                    'conditions' => array(
                        'Article.publish' => 1,
                        'Article.date <=' => date('Y-m-d')
                    ),
                    'order' => array(
                        'Article.date' => 'DESC'
                    ),
                    'limit' => 3
                ));
                
                $relatedArticles = $this->Article->find('all', array(
                    'conditions' => array(
                        'Category.id' => $article['Category']['id'],
                        'Article.publish' => 1,
                        'Article.date <=' => date('Y-m-d')
                    ),
                    'order' => 'rand()',
                    'limit' => 3
                ));
                
                $this->set('article', $article);
                $this->set('lastArticles', $lastArticles);
                $this->set('relatedArticles', $relatedArticles);
            }
        }

/**
 * category method
 *
 * @param string $category
 * @return void
 */
        public function category($category = null, $page = 1){
            if($category != null){
                $articles = $this->Article->find('all', array(
                    'conditions' => array(
                        'Article.publish' => 1,
                        'Article.date <=' => date('Y-m-d'),
                        'Category.name' => $category
                    ),
                    'order' => array(
                        'Article.date' => 'DESC'
                    ),
                    'limit' => 6,
                    'page' => $page
                ));

                $categories = $this->Category->find('all', array(
                    'fields' => array(
                        'Category.id',
                        'Category.name'
                    ),
                    'recursive' => -1
                ));

                $this->set('articles', $articles);
                $this->set('categories', $categories);
                $this->set('page', $page);
                $this->set('categoryName', $category);
            } else {
                return $this->redirect(array('action' => 'index'));
            }
        }
        
/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
           
		$this->Article->recursive = 0;
		$this->set('articles', $this->paginate('Article'));
                $this->set('category', $this->Category->find('list'));
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
                //debug($this->request->data);
		if ($this->request->is('post')) {
                        
                        if( $this->request->data('Article.img_file.name') != "" ){
                            $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'articles' . DIRECTORY_SEPARATOR;
                            $upload = move_uploaded_file($this->request->data('Article.img_file.tmp_name'), $hostImages . $this->request->data('Article.img_file.name'));
                            if(!$upload){
                                $this->Session->setFlash(__('The Article Image could not be saved. error upload the image', true), 'alert-danger');
                                return $this->redirect(array('action' => 'add'));
                            }
                        }
			$this->Article->create();
			if ($this->Article->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
                $this->set('category', $this->Category->find('list'));
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->Article->exists($id)) {
			$this->Session->setFlash(__('Invalid article', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
                    
                        if( $this->request->data('Article.img_file.name') != "" ){
                            $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'articles' . DIRECTORY_SEPARATOR;
                            $upload = move_uploaded_file($this->request->data('Article.img_file.tmp_name'), $hostImages . $this->request->data('Article.img_file.name'));
                            if(!$upload){
                                $this->Session->setFlash(__('The Article Image could not be saved. error upload the image', true), 'alert-danger');
                                return $this->redirect(array('action' => 'add'));
                            }
                        }
                    
			if ($this->Article->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
			$this->request->data = $this->Article->find('first', $options);
		}
                 $this->set('category', $this->Category->find('list'));
	}
        
        public function ibopadmin_delete($id = null){
            $id = base64_decode($id);
            $this->Article->id = $id;
            if(!$this->Article->exists()){
                $this->Session->setFlash(__('Invalid Article', true), 'alert-danger');
                return $this->redirect(array('action' => 'index'));
            }
            if ($this->Article->delete()) {
                    $this->Session->setFlash(__('The Article has been deleted.'), 'alert-success');
            } else {
                    $this->Session->setFlash(__('The Article could not be deleted. Please, try again.'), 'alert-danger');
            }
            return $this->redirect(array('action' => 'index'));
        }
        
        /**
	 * ibopadmin_searchArticle
	 * @return void
	*/
        public function searchArticle($keywork = null, $page = 1){
            if($keywork != null){
                $keywork = base64_decode($keywork);
                //$this->set('types', $this->ImageType->find('list'));
                $articles = $this->Article->find(
                    'all', array(
                        'conditions' => array(
                            'or' => array(
                                'Article.title LIKE'         => '%' . $keywork . '%',
                                'Article.tags LIKE'   => '%' . $keywork . '%'
                            )
                        ),
                        //'recursive' => -1,
                        'page'      => $page,
                        'limit'     => 21
                    )
                );
                
                $categories = $this->Category->find('all', array(
                    'fields' => array(
                        'Category.id',
                        'Category.name'
                    ),
                    'recursive' => -1
                ));
                
                $this->set('categories', $categories);
                $this->set('keywork', $keywork);
                $this->set('page', $page);
                $this->set('articles', $articles);
            } else {
                if($this->request->is('post')){
                    $this->redirect(array('action' => 'searchArticle/' . $this->request->data('Article.keyworkData') . '/' . $this->request->data('Article.pageData')));
		}
                return $this->redirect(array('action' => 'index'));
            }
            
        }
        
        /**
	 * ibopadmin_searchArticle
	 * @return void
	*/
        public function ibopadmin_searchArticle($keywork = null, $page = 1){
            if($keywork != null){
                $keywork = base64_decode($keywork);
                //$this->set('types', $this->ImageType->find('list'));
                $articles = $this->Article->find(
                    'all', array(
                        'conditions' => array(
                            'or' => array(
                                'Article.title LIKE'         => '%' . $keywork . '%',
                                'Article.tags LIKE'   => '%' . $keywork . '%'
                            )
                        ),
                        //'recursive' => -1,
                        'page'      => $page,
                        'limit'     => 21
                    )
                );
                
                $categories = $this->Category->find('all', array(
                    'fields' => array(
                        'Category.id',
                        'Category.name'
                    ),
                    'recursive' => -1
                ));
                
                $this->set('categories', $categories);
                $this->set('keywork', $keywork);
                $this->set('page', $page);
                $this->set('articles', $articles);
                $this->set('searchCriteria', $keywork);
            } else {
                return $this->redirect(array('action' => 'index'));
            }
            
        }
}
