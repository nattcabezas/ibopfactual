<?php

App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');//permite usar el security->hash encriptar las contraseñas
App::uses('CakeEmail', 'Network/Email');//usar la clase email

/**
 * CakePHP RegisterController
 * @author Rudy Sibaja <neo.nou@gmail.com>
 */
class RegisterController extends AppController {
    
    public $uses        = array('PageUser');
    public $components  = array('Cookie');
    
    public function beforeFilter() {
        parent::beforeFilter();
        if( ($this->Session->read('UserPage') != null)){
            if($this->params['action'] != 'logout'){
                $this->redirect('/');
            }
        }
        //debug($this->Session->read('UserPage'));
    }
    
    public function index() {
        if($this->request->is('post')){
            $postData = $this->request->data;
            if( base64_encode($postData['PageUser']['capcha']) ==  $postData['PageUser']['capchaResult']){
                if($postData['PageUser']['password'] == $postData['PageUser']['confirm_password']){
                    
                    $existMail = $this->PageUser->find('count', array(
                        'conditions' => array(
                            'PageUser.email' => $postData['PageUser']['email']
                        )
                    ));
                    if(!$existMail){                    
                        $postData['PageUser']['password']   = Security::hash($postData['PageUser']['password'], 'sha1', true);
                        $postData['PageUser']['token']      = md5(uniqid(mt_rand(), true));
                        $postData['PageUser']['status']     = 0;
                        if($this->PageUser->save($postData)){
                            $email = new CakeEmail();
                            $email->template('register')
                                    ->emailFormat('html')
                                    ->from(array('no-replay@ibopfactual.com' => 'ibopfactual.com'))
                                    ->to($postData['PageUser']['email'])
                                    ->subject('ibopfactual register')
                                    ->viewVars(array('username' => '', 'token' => $postData['PageUser']['token'], 'id' => base64_encode($this->PageUser->getLastInsertID())));
                            $emailResult = $email->send();
                            $this->redirect(array('controller' => 'Register', 'action' => 'thanks'));
                        }
                    } else {
                        $this->set('emailError', 1);
                        $this->Session->setFlash(__('<strong>Error.</strong> the email is already registered', true), 'alert-danger');
                    }
                    
                } else {
                    $this->set('passwordError', 1);
                    $this->Session->setFlash(__('<strong>Error.</strong> passwords do not match', true), 'alert-danger');
                }
                //debug($postData);
                //die();
            } else {
                $this->set('capchaError', 1);
                $this->Session->setFlash(__('<strong>Error.</strong> invalid result, please try again', true), 'alert-danger');
            }
            
        }
        
        $firstNumber    = rand(0, 9);
        $secondNumber   = rand(0, 9);
        $operador       = rand(0, 1);
        if($operador == 0){
            $result = $firstNumber + $secondNumber;
        } else if ($operador == 1){
            $result = $firstNumber - $secondNumber;
        }
        $this->set(array('firstNumber' => $firstNumber, 'secondNumber' => $secondNumber, 'operador' => $operador, 'result' => base64_encode($result)));
        
    }
    
    public function thanks(){}
    
    public function token($idUser = null, $token = null){
        
        $user = $this->PageUser->find('count', array(
            'conditions' => array(
                'PageUser.id'       => base64_decode($idUser),
                'PageUser.token'    => $token
            )
        )); 
        
        if($user > 0){
            $userData['PageUser']['id']     = base64_decode($idUser);
            $userData['PageUser']['token']  = "";
            $userData['PageUser']['status'] = 1;
            $this->PageUser->save($userData);
            
        } 
        $this->redirect('/');
    }
 
    public function login(){
        if($this->request->is('post')){
            $loginData = $this->request->data;
            $user = $this->PageUser->find('first', array(
                'conditions' => array(
                    'PageUser.email'    => $loginData['PageUser']['email'],
                    'PageUser.password' => Security::hash($loginData['PageUser']['password'], 'sha1', true),
                    'PageUser.status'   => 1
                )
            ));
            if($user){
                $this->Session->write('UserPage', $user);
                $this->redirect('/');
            } else {
                $this->Session->setFlash(__('<strong>Error.</strong> Email or Password invalid', true), 'alert-danger');
            }
        }
 
    }
    
    public function forgotPassword(){
        
        
        $firstNumber    = rand(0, 9);
        $secondNumber   = rand(0, 9);
        $operador       = rand(0, 1);
        if($operador == 0){
            $result = $firstNumber + $secondNumber;
        } else if ($operador == 1){
            $result = $firstNumber - $secondNumber;
        }
        $this->set(array('firstNumber' => $firstNumber, 'secondNumber' => $secondNumber, 'operador' => $operador, 'result' => base64_encode($result)));
    }
    
    public function logout(){
        $this->Session->write('UserPage', null);
        $this->Session->delete('UserPage');
        $this->redirect('/');
    }
    
}
