<?php
App::uses('AppController', 'Controller');
/**
 * Dash Controller
 *
 * @property Dash $Dash
 * @author rudysibaja <neo.nou@gmail.com>
 * panel de control del sistema
 */
class DashController extends AppController {
    
        public $uses = array('Identity', 'Fight', 'Event', 'Venue', 'Type', 'User', 'IdentityBirth', 'IdentityDeath');
    
	public function ibopadmin_index() {
            //$this->set('user', $this->Session->read('User'));
            
            $maleIdentities = $this->Identity->find('count', array(
                'conditions' => array(
                    'Identity.gender' => 'M'
                )
            ));
            
            $femaleIdentities = $this->Identity->find('count', array(
                'conditions' => array(
                    'Identity.gender' => 'F'
                )
            ));
            
            $typesOfFights = $this->Type->find('all', array(
                'fields' => array(
                    'Type.id',
                    'Type.name'
                ),
                'recursive' => -1
            ));
            $typesData = array();
            foreach ($typesOfFights as $types) {
                $typeData['name'] = $types['Type']['name'];
                $typeData['count'] = $this->Fight->find('count', array(
                    'conditions' => array(
                        'Fight.types_id' => $types['Type']['id']
                    )
                ));
                $typesData[] = $typeData;
            }
            
            $bornToday  = $this->Identity->query('SELECT * FROM  `Boxers born a day like today` ORDER BY birth_date DESC');
            
            
            $this->set('countIdentity', $this->Identity->find('count'));
            $this->set('countFight', $this->Fight->find('count'));
            $this->set('countEvent', $this->Event->find('count'));
            $this->set('countVenue', $this->Venue->find('count'));
            //
            $this->set('maleIdentities', $maleIdentities);
            $this->set('femaleIdentities', $femaleIdentities);
            //
            $this->set('typesData', $typesData);
            $this->set('bornToday', $bornToday);
            
            
        }        
        public function ibopadmin_showDebut(){
            $this->layout = 'ajax';
            $this->set('debut', $this->Identity->query('SELECT * FROM `Boxers debut a day like today` ORDER BY debut_date DESC'));
        }
        
        public function ibopadmin_showDie(){
            $this->layout = 'ajax';
            $this->set('die', $this->Identity->query('SELECT * FROM  `Boxers die a day like today` ORDER BY death_day DESC'));
        }
        
        public function ibopadmin_showFights(){
            $this->layout = 'ajax';
            $this->set('fights', $this->Identity->query('SELECT * FROM `Fights a day like today` ORDER BY date DESC'));
        }
        
        public function ibopadmin_getBorn(){
            $this->layout = 'ajax';
            if($this->request->is('post')){
                list($year, $month, $day) = explode('-', $this->request->data('date'));
                
                $bornToday = $this->IdentityBirth->find('all', array(
                    'conditions' => array(
                        'MONTH(IdentityBirth.date)' => $month,
                        'DAY(IdentityBirth.date)'   => $day
                    ), 'order' => array(
                        'IdentityBirth.date' => 'DESC'
                    )
                ));
                
                $this->set('bornToday', $bornToday);
            }
        }
        public function ibopadmin_getDie(){
            $this->layout = 'ajax';
            if($this->request->is('post')){
                list($year, $month, $day) = explode('-', $this->request->data('date'));
                
                $dieToday = $this->IdentityDeath->find('all', array(
                    'conditions' => array(
                        'MONTH(IdentityDeath.date)' => $month,
                        'DAY(IdentityDeath.date)'   => $day
                    ),'order' => array(
                        'IdentityDeath.date' => 'DESC'
                    )
                ));
                
                $this->set('dieToday', $dieToday);
            }
        }
        
        public function ibopadmin_fightToday(){
            $this->layout = 'ajax';
            if($this->request->is('post')){
                list($year, $month, $day) = explode('-', $this->request->data('date'));
                $fightToday = $this->Fight->find('all',
                        array(
                           'conditions' => array(
                                'MONTH(Event.date)' => $month,
                                'DAY(Event.date)'   => $day
                           ), 'recursive' => -2,
                            'order' => array(
                                'Event.date' => 'DESC'
                            )
                        ));
                $this->set('fightToday', $fightToday);
            }
            
        }
        public function ibopadmin_debutToday(){
           
            $this->layout = 'ajax';
            if($this->request->is('post')){
                list($year, $month, $day) = explode('-', $this->request->data('date'));
                
                $query = "select "
                        . "`identities`.id as identity_id,`identity_ids`."
                        . "`type` as source, "
                        . "`identity_ids`.`type_id` as source_id, "
                        . "`identities`.`name`, "
                        . "`identities`.`last_name`, "
                        . "MIN(STR_TO_DATE(`events`.`date`, '%Y-%m-%d')) as debut_date "
                        . "from `fight_identities` "
                        . "inner join `identities` on `identities`.`id`= `fight_identities`.`identities_id` "
                        . "inner join `fights` on `fights`.id = `fight_identities`.`fights_id` "
                        . "inner join `events` on  `events`.`id` = `fights`.`events_id` "
                        . "inner join `identity_ids`  on `identities`.id = `identity_ids`.`identities_id` "
                        . "group by `identities`.id Having date_format(MIN(STR_TO_DATE(`events`.`date`, '%Y-%m-%d')),'%m-%d') = "
                        . "'" . $month."-".$day . "'"
                        . "order by debut_date Desc";
                $debutToday = $this->Identity->query($query);                 
                $this->set('debutToday', $debutToday);
            }
            
        }
        
        public function ibopadmin_profile(){
                $this->set('userData', $this->User->find(
			'first', array(
				'conditions' => array(
					'User.id' => $this->Session->read('User.User.id')
				)
			)
		));
		if($this->request->is('post')){
			$postData = $this->request->data;
			if($postData['User']['password'] == ""){
				$userPost['User']['id'] 		= $postData['User']['id'];
				$userPost['User']['username']           = $postData['User']['username'];
				$userPost['User']['name'] 		= $postData['User']['name'];
				$userPost['User']['email'] 		= $postData['User']['email'];
				if($this->User->save($userPost)){
					$this->Session->setFlash(__('user saved successfully', true), 'alert-success');
					return $this->redirect(array('action' => 'profile'));
				} else {
					$this->Session->setFlash(__('<strong>Error!</strong> the user could not be saved, please try again', true), 'alert-danger');
					return $this->redirect(array('action' => 'profile'));
				}
			} else if($postData['User']['confirm-password'] == $postData['User']['password']){
				$postData['User']['password'] = Security::hash($postData['User']['password'], 'sha1', true);
				if($this->User->save($postData)){
					$this->Session->setFlash(__('user saved successfully', true), 'alert-success');
					return $this->redirect(array('action' => 'profile'));
				} else {
					$this->Session->setFlash(__('<strong>Error!</strong> the user could not be saved, please try again', true), 'alert-danger');
					return $this->redirect(array('action' => 'profile'));
				}
			}
		}
        }
        
        public function ibopadmin_changeLanguage($language = null){            
            $UserData['User']['id']         = $this->Session->read('User.User.id');
            $UserData['User']['language']   = $language;
            if($this->User->save($UserData)){
               $this->Session->write('Config.language', $language); 
            }
            return $this->redirect(array('controller' => 'Dash', 'action' => 'index'));
        }
}