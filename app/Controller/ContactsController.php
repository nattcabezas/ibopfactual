<?php
App::uses('AppController', 'Controller');
/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContactsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
        public $helper     = array('newJsonEncode');

/**
 * index method
 *
 * @return void
 */
	public function index() {
	    $this->set('firstNumber', rand(0, 9));
            $this->set('secondNumber', rand(0, 9));
            $this->set('typeMath', rand(0, 1));
	}
        
/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Contact->recursive = 0;
                $this->Paginator->settings = array(
                    'order' => array(
                        'Contact.id' => 'DESC'
                    )
                );
		$this->set('contacts', $this->Paginator->paginate());
	}

/**
 * ibopadmin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_view($id = null) {
                $id = base64_decode($id);
		if (!$this->Contact->exists($id)) {
                        $this->Session->setFlash(__('The contact could not be loaded. Please, try again.'), 'error-message');
			return $this->redirect(array('Controller' => 'Contacts', 'action' => 'index'));
		}
                                                                   
		$options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
		$this->set('contact', $this->Contact->find('first', $options));
                $data= array(
                    'Contact' => array (
                        'id' => $id,
                        'view' => 1
                    ));
                $this->Contact->save( $data, false, array('view') );
	} 

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function add() {
            if ($this->request->is('post')) {
                if($this->request->data('Contact.captcha') == base64_decode($this->request->data('Contact.captcha_result'))){
                    $this->Contact->create();
                    if ($this->Contact->save($this->request->data)) {
                        $this->Session->setFlash(__('The contact has been saved.'), 'success-message');
                    } else {
                        $this->Session->setFlash(__('The contact could not be saved. Please, try again.'), 'error-message');
                    }
                } else {
                    $this->Session->setFlash(__('The contact could not be saved error in the math result. Please, try again.'), 'error-message');
                }
            }
            return $this->redirect(array('Controller' => 'Contacts', 'action' => 'index'));
	}
/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($id = null) {
		$id = base64_decode($id);
                $this->Contact->id = $id;
                
		if (!$this->Contact->exists()) {
			$this->Session->setFlash(__('Invalid contact', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		
		if ($this->Contact->delete($id,true)) {
			$this->Session->setFlash(__('The contact has been deleted.'),'success-message' );
		} else {
			$this->Session->setFlash(__('The contact could not be deleted. Please, try again.'),'alert-danger');
		}
		return $this->redirect(array('action' => 'index'));
	}
        public function ibopadmin_searchContacts(){
            if($this->request->is('post')){
                $contacts = $this->Contact->find(
                    'all', array(
                        'conditions' => array(
                            'or' => array(
                                'Contact.name LIKE' => '%' . $this->request->data('searchContacts.keywork') . '%'                                
                            )
                        )
                    )
                );
                $this->set('contacts', $contacts);
            }
        }
}
