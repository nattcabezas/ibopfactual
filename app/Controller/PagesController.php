<?php
App::uses('AppController', 'Controller');
/**
 * Titles Controller
 *
 * @property Pages $page
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * 
 */
class PagesController extends AppController {
        
 /**
 * Components
 *
 * @var array
 */
	public $components = array ('Paginator', 'Session');
        
        public function index($title = null){
            if($title != null){
                $title = str_replace("_", " ", $title);
                $page = $this->Page->find('first', array(
                    'conditions' => array(
                        'Page.title' => $title
                    )
                ));
                $this->set('page', $page);
            }
        }
        
/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
            $this->Paginator->setings = array(
                'order' => array(
                    'Page.id' => 'DESC'
                    
                )
            );            
            $this->set('pages', $this->Paginator->paginate());            
        }
        /**
 * ibopadmin_add method
 *
 * @return void
 */
        public function ibopadmin_add() {
            if ($this->request->is('post')){
                $this->Page->create();
                    if($this->Page->save($this->request->data)){
                        $this->Session->setFlash(__('The page has been saved', true), 'alert-success');
                        return $this->redirect(array('action' => 'index'));
                    }else{
                        $this->Session->setflash(__('The page could not be saved. Please try again.', true), 'alert-danger');

                    }
                }
                
            }
 /**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
        public function ibopadmin_edit($id = null) {
            $id = base64_decode($id);
            if (!$this->Page->exists($id)) {
                    $this->Session->setFlash(__('Invalid page', true), 'alert-danger');
                    return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
                    $options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
                    $this->request->data = $this->Page->find('first', $options);
                    $this->set('idPage', $id);      
		}
            
        }
        public function ibopadmin_delete($id = null) {
            
            $id = base64_decode($id);
            $this->Page->id =$id;
            if (!$this->Page->exists()){
                $this->Session->setFlash(__('Invalid Page', true), 'alert-danger');
                return $this->redirect(array('action' => 'index'));
            }
            if ($this->Page->delete($id,true)){
                $this->Session->setFlash(__('The page has been deleted.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The page could not be deleted. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'index'));
            
        }
        public function ibopadmin_searchPages(){
            if($this->request->is('post')){
                $pages = $this->Page->find(
                       'all', array(
                           'conditions' => array(
                               'or' => array(
                                    'Page.title LIKE' => '%'. $this->request->data('searchPages.keywork') . '%'                          
                                )
                            )
                        ) 
                    );
                $this->set('pages', $pages);
            }
            
        }        
}
