<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

      public $uses = array('Country', 'Identity', 'Banner');
      public $components  = array('Session');
     
	/**
	 * funcion que carga antes de todos los controladores
	 * @return void 
	 */
	public function beforeFilter(){
            if( (isset($this->params['ibopadmin'])) && ($this->params['ibopadmin']) ){
		  $this->layout = 'ibopadmin';
                    if( ($this->Session->read('User') == null) && ($this->params['controller'] != 'Validation') ){
                        if(isset($this->params->url)){
                            $url = base64_encode($this->params->url);
                            return $this->redirect(array('controller' => 'Validation', 'action' => 'index/' . $url));
                        } else { 
                            return $this->redirect('/ibopadmin');
                        }
                    }
                    if( ($this->Session->read('User') != null) && ( $this->params['controller'] == 'Validation' )  ){
                        if( $this->params['action'] != 'ibopadmin_logout' ){
                            return $this->redirect(array('controller' => 'Dash', 'action'=>'index'));
                        }
                    }
                    if($this->Session->read('User') != null){
                        $this->set('user', $this->Session->read('User'));
                        Configure::write('Config.language', $this->Session->read('Config.language'));
                        
                        $this->set('thisController', $this->params['controller']);
                        if(($this->params['controller'] == 'User') && ($this->Session->read('User.Role.users') != 1)){
                                return $this->redirect('/ibopadmin');
                        }
                        if(($this->params['controller'] == 'Weights') && ($this->Session->read('User.Role.manage') != 1) ){
                                return $this->redirect('/ibopadmin');
                        }
                        if(($this->params['controller'] == 'Fights') && ($this->Session->read('User.Role.figth') != 1) ){
                                return $this->redirect('/ibopadmin');
                        }
                        if(($this->params['controller'] == 'Events') && ($this->Session->read('User.Role.events') != 1) ){
                                return $this->redirect('/ibopadmin');
                        }
                        if(($this->params['controller'] == 'Venues') && ($this->Session->read('User.Role.venues') != 1) ){
                                return $this->redirect('/ibopadmin');
                        }
                        if(($this->params['controller'] == 'Images') && ($this->Session->read('User.Role.images') != 1) ){
                                return $this->redirect('/ibopadmin');
                        }
                        if(($this->params['controller'] == 'Videos') && ($this->Session->read('User.Role.videos') != 1) ){
                                return $this->redirect('/ibopadmin');
                        }
                        if(($this->params['controller'] == 'Notes') && ($this->Session->read('User.Role.notes') != 1) ){
                                return $this->redirect('/ibopadmin');
                        }
                    }

            }
	
            if($this->params['controller'] == 'home'){                
                $this->set('topBanner', $this->getBanner('1'));
                $this->set('footerBanner', $this->getBanner('2'));
            }
            if($this->params['controller'] == 'Persons'){
                if($this->params['action'] == 'search'){
                    $this->set('searchPersons', $this->getBanner('3'));
                } 
                if($this->params['action'] == 'index'){
                    $this->set('detailPersons', $this->getBanner('4'));
                }
            }
            if($this->params['controller'] == 'Fights'){
                if($this->params['action'] == 'search'){
                    $this->set('detailBanner', $this->getBanner('5'));
                } 
                if($this->params['action'] == 'index'){
                    $this->set('detailBanner', $this->getBanner('6'));
                }
            }
            if($this->params['controller'] == 'Events'){
                if($this->params['action'] == 'search'){
                    $this->set('detailBanner', $this->getBanner('7'));
                }
                if($this->params['action'] == 'index'){
                    $this->set('detailBanner', $this->getBanner('8'));
                }
            }
            
            $this->set('thisController', $this->params['controller']);
            $this->set('thisAction', $this->params['action']);
            
            if( ($this->Session->read('UserPage') != null)){
                $this->set('userPage', $this->Session->read('UserPage'));
            }

        }

      /**
       * funcion que carga antes de renderizar las vistas
       * @return void
       */
      public function beforeRender(){
            /*if($this->name == 'CakeError'){
                if( (isset($this->params['ibopadmin'])) && ($this->params['ibopadmin']) ){
                    $this->layout = 'ibopadmin';
                } else {
                    $this->layout = '404';
                }
                 //$this->layout = 'ajax';
            } */       
      }
      
      public function getBanner($type){
          
          $banner = $this->Banner->find('first', array(
              'conditions' => array(
                  'Banner.type' => $type,
                  'Banner.status' => 1
              )
          ));
          
          return $banner;
          /* $this->set('quotes', $quotes);*/
      }
      


}
