<?php
App::uses('AppController', 'Controller');
/**
 * Sources Controller
 *
 * @property Source $Source
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SourcesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Source->recursive = 0;
		$this->set('sources', $this->Paginator->paginate());
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->Source->create();
			if ($this->Source->save($this->request->data)) {
				$this->Session->setFlash(__('The source has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The source could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->Source->exists($id)) {
			$this->Session->setFlash(__('Invalid source', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Source->save($this->request->data)) {
				$this->Session->setFlash(__('The source has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The source could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('Source.' . $this->Source->primaryKey => $id));
			$this->request->data = $this->Source->find('first', $options);
		}
	}

/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($id = null) {
		$this->Source->id = $id;
		if (!$this->Source->exists()) {
			$this->Session->setFlash(__('Invalid source', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Source->delete()) {
			$this->Session->setFlash(__('The source has been deleted.'));
		} else {
			$this->Session->setFlash(__('The source could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
