<?php
App::uses('AppController', 'Controller');
/**
 * Image Controller
 *
 * @property Images $Images
 * @author rudysibaja <neo.nou@gmail.com>
 */
class ImagesController extends AppController {

	public $name 		= "Images";
	public $uses 		= array('Image', 'ImageType', 'IdentitiesImage', 'FightsImage', 'EventsImage', 'VenuesImage', 'Identity', 'Fight', 'Event', 'Venue','FightIdentity');
	public $paginate	= array(
		'limit' 	=> 18,
		'recursive' => -1,
		'order' 	=> array(
			'Image.id' => 'DESC'
		)
	);

	/**
	 * index de la seccion 
	 * @return void
	 */
	public function ibopadmin_index(){
		$this->set('images', $this->paginate('Image'));
		$this->set('types', $this->ImageType->find('list'));
		if($this->request->is('post')){
                    if($this->Image->save($this->request->data)){
                            $this->Session->setFlash(__('The Image has been saved.', true), 'alert-success');
                    } else {
                            $this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
                    }
                    $this->redirect('/' . $this->request->data('Image.pageurl'));
		}
	}

	/**
	 * funcion donde se agregan las imagenes
	 * @return void
	 */
	public function ibopadmin_add(){
		if ($this->request->is('post')) {
			$errorUpload	= false;

			//$hostConnect = ftp_connect($hostName, 21);
			//ftp_login($hostConnect, $hostUser, $hostpassword);
			
			$hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;

			for ($i = 0; $i < count($_FILES['ImageUrl']['name']); $i++) { 
				
				$nameImage 	= uniqid().'_'.$_FILES['ImageUrl']['name'][$i];
				//$upload 	= ftp_put($hostConnect, $hostPath.$nameImage, $_FILES['ImageUrl']['tmp_name'][$i], FTP_ASCII);
				$upload = move_uploaded_file($_FILES['ImageUrl']['tmp_name'][$i], $hostImages . $nameImage);

				if($upload){
					$imagesData['Image']['url'] = $nameImage;
					$this->Image->create();
					if(!$this->Image->save($imagesData)){
						$errorUpload = true;
					}
				} else {
					$errorUpload = true;
				}
			}
			//ftp_close($hostConnect);
			if(!$errorUpload){
				$this->Session->setFlash(__('The Images has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Some image could not be saved. Please, try again.', true), 'alert-danger');
				return $this->redirect(array('action' => 'index'));
			}
		}
	}

	/**
	 * ibopadmin_delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function ibopadmin_delete($id = null) {
		$this->Image->id = $id;
                $url = $this->Image->find('first',array(
                        'conditions' => array(
                            'Image.id' => $id
                        ),
                    'fields' => array(
                        'Image.url'
                    ),
                        'recursive' => -1
                    )
                );
               
                $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $url['Image']['url'];
		if (!$this->Image->exists()) {
			$this->Session->setFlash(__('Invalid image.', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->Image->delete()) {
                        if (!unlink($hostImages))
                            {
                                $this->Session->setFlash(__('The image could not be deleted. Please, try again.', true), 'alert-danger');
                            }
			$this->Session->setFlash(__('The image has been deleted.', true), 'alert-success');
		} else {
			$this->Session->setFlash(__('The image could not be deleted. Please, try again.', true), 'alert-danger');
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        /**
	 * ibopadmin_searchImage
	 * @return void
	 */
        public function ibopadmin_searchImage($keywork = null, $page = 1){
            if($keywork != null){
                $keywork = base64_decode($keywork);
                $this->set('types', $this->ImageType->find('list'));
                $images = $this->Image->find(
                    'all', array(
                        'conditions' => array(
                            'or' => array(
                                'Image.title LIKE'         => '%' . $keywork . '%',
                                'Image.description LIKE'   => '%' . $keywork . '%',
                                'Image.url LIKE'           => '%' . $keywork . '%'
                            )
                        ),
                        'recursive' => -1,
                        'page'      => $page,
                        'limit'     => 21
                    )
                );
                $this->set('keywork', $keywork);
                $this->set('page', $page);
                $this->set('images', $images);
            } else {
                if($this->request->is('post')){
                    if($this->Image->save($this->request->data)){
                            $this->Session->setFlash(__('The Image has been saved.', true), 'alert-success');
                    } else {
                            $this->Session->setFlash(__('The Image could not be saved. Please, try again.', true), 'alert-danger');
                    }
                    $this->redirect(array('action' => 'searchImage/' . $this->request->data('Image.keyworkData') . '/' . $this->request->data('Image.pageData')));
		}
                return $this->redirect(array('action' => 'index'));
            }
            
        }
        
        /**
	 * ibopadmin_assign method
	 *
	 * @param string $id
	 * @return void
	 */
        public function ibopadmin_assign($id = null){
            if($id != null){
                $id = base64_decode($id);
                $image = $this->Image->find('first', array(
                    'conditions' => array(
                        'Image.id' => $id
                    ),
                    'recursive' => -1
                ));
                
                $identitiesImage = $this->IdentitiesImage->find('all', array(
                    'conditions' => array(
                        'IdentitiesImage.images_id' => $id
                    )
                ));
                
                $fightsImage = $this->FightsImage->find('all', array(
                    'conditions' => array(
                        'FightsImage.images_id' => $id
                    )
                ));
                
                $eventsImage = $this->EventsImage->find('all', array(
                    'conditions' => array(
                        'EventsImage.images_id' => $id
                    )
                ));
                
                $venuesImage = $this->VenuesImage->find('all', array(
                    'conditions' => array(
                        'VenuesImage.images_id' => $id
                    )
                ));
                
                $this->set('idImage', $id);
                $this->set('image', $image);
                $this->set('identitiesImage', $identitiesImage);
                $this->set('fightsImage', $fightsImage);
                $this->set('eventsImage', $eventsImage);
                $this->set('venuesImage', $venuesImage);
            }
        }
        
        /**
	 * ibopadmin_saveIdentity method
	 *
	 * @param string $idImage
         * @param string $idIdentity
	 * @return void
	 */
        public function ibopadmin_saveIdentity($idImage = null, $idIdentity = null){
            $this->layout = 'ajax';
            if (($idImage != null) || ($idIdentity != null)){
                $identitiesImage['IdentitiesImage']['images_id'] = $idImage;
                $identitiesImage['IdentitiesImage']['identities_id'] = $idIdentity;
                if($this->IdentitiesImage->save($identitiesImage)){
                    $identityName = $this->Identity->find('first', array(
                        'conditions' => array(
                            'Identity.id' => $idIdentity
                        ),
                        'fields' => array(
                            'Identity.name',
                            'Identity.last_name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->IdentitiesImage->getLastInsertId(), 'identity' => $identityName, 'idIdentity' => $idIdentity));
                } else {
                    $this->set('data', 'error');
                }
            }
        }
        
        /**
	 * ibopadmin_saveIdentity method
	 *
	 * @param string $idImage
         * @param string $idFight
	 * @return void
	 */
        public function ibopadmin_saveFight($idImage = null, $idFight = null){
            $this->layout = 'ajax';
            if (($idImage != null) || ($idFight != null)){
                
                $fightsImage['FightsImage']['fights_id'] = $idFight;
                $fightsImage['FightsImage']['images_id'] = $idImage;
                
                if($this->FightsImage->save($fightsImage)){
                    
                    $fight = $this->Fight->find('first', array(
                        'conditions' => array(
                            'Fight.id' => $idFight 
                        ),
                        'fields' => array(
                            'Fight.id',
                            'Fight.title'
                        ),
                        'recursive' => -1
                    ));
                    
                    $this->set('data', array('id' => $this->FightsImage->getLastInsertId(), 'Fight' => $fight));
                } else {
                    $this->set('data', 'error');
                }
            }
        }
        
        /**
	 * ibopadmin_saveIdentity method
	 *
	 * @param string $id
         * @param string $idImage
	 * @return void
	 */
        public function ibopadmin_deleteIdentity($id = null, $idImage = null){
            if($id != null){
                if($this->IdentitiesImage->delete($id)){
                    $this->Session->setFlash(__('The Image relationship has been deleted', true), 'alert-success');
                    return $this->redirect(array('action' => 'assign/' .  base64_encode($idImage)));    
                } else {
                    $this->Session->setFlash(__('The Image relationship could not be deleted. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'assign/' .  base64_encode($idImage))); 
                }
            }
        }
        
        /**
	 * ibopadmin_deleteFight method
	 *
	 * @param string $idFightImage
	 */
        public function ibopadmin_deleteFight($idFightImage = null){
            if($idFightImage != null){
                if($this->FightsImage->delete($idFightImage)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        /**
	 * ibopadmin_saveEvent method
	 *
	 * @param string $idImage
         * @param string $idEvent
	 * @return void
	 */
        public function ibopadmin_saveEvent($idImage = null, $idEvent = null){
            $this->layout = 'ajax';
            if(($idImage != null) || ($idEvent != null)){
                $eventsImage['EventsImage']['events_id'] = $idEvent;
                $eventsImage['EventsImage']['images_id'] = $idImage;
                if($this->EventsImage->save($eventsImage)){
                    $event = $this->Event->find('first', array(
                        'conditions' => array(
                            'Event.id' => $idEvent
                        ),
                        'fields' => array(
                            'Event.id',
                            'Event.name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->EventsImage->getLastInsertId(), 'Event' => $event));
                }
            }
        }
        
        /**
	 * ibopadmin_deleteEvent method
	 *
	 * @param string $idEventImage
	 */
        public function ibopadmin_deleteEvent($idEventImage = null){
            if($idEventImage != null){
                if($this->EventsImage->delete($idEventImage)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        /**
	 * ibopadmin_saveVenues method
	 *
	 * @param string $idImage
         * @param string $idVenue
	 * @return void
	 */
        public function ibopadmin_saveVenues($idImage = null, $idVenue = null){
            $this->layout = 'ajax';
            if(($idImage != null) || ($idVenue != null)){
                $venuesImage['VenuesImage']['venues_id'] = $idVenue;
                $venuesImage['VenuesImage']['images_id'] = $idImage;
                if($this->VenuesImage->save($venuesImage)){
                    $venues = $this->Venue->find('first', array(
                        'conditions' => array(
                            'Venue.id' => $idVenue
                        ),
                        'fields' => array(
                            'Venue.id',
                            'Venue.name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->VenuesImage->getLastInsertId(), 'Venue' => $venues));
                }
            }
        }
        
        /**
	 * ibopadmin_deleteVenue method
	 *
	 * @param string $idVenueImage
	 */
        public function ibopadmin_deleteVenue($idVenueImage = null){
            if($idVenueImage != null){
                if($this->VenuesImage->delete($idVenueImage)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        
        public function ibopadmin_saveImage(){
            if($this->request->is('post')){
                if($this->Image->save($this->request->data)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        /**
	 * funcion to show image form
	 *
	 * @param integer $idImage
         * @return void
	 */
        public function ibopadmin_formImage($idImage = null){
            $this->layout = 'ajax';
            $image = $this->Image->find('first', array(
                'conditions' => array(
                    'Image.id' => $idImage
                ),
                'recursive' => -1
            ));
            $this->set('types', $this->ImageType->find('list'));
            $this->set('image', $image);
        }
        
        /**
	 * funcion to save image form
	 *
         * @return void
	 */
        public function ibopadmin_saveForm(){
            $this->layout = 'ajax';
            if($this->request->is('post')){
                if($this->Image->save($this->request->data)){
                    echo '1';
                } else {
                    echo '0';
                }
            }
            die();
        }
        
        /**
	 * funcion to take all the persons, fights
         * and venues of an event to assign it
         *  to an image of type: poster
	 * with a single button
         * @return void
	*/
        public function ibopadmin_assign_all($events = null, $idImage = null)
        {
            $eventArray = explode("-",$events);
            array_pop($eventArray); /*Last position of the array is a "-", so it should not be used*/
            
            //Events id already in array, extract information using those id's
            
            /*
            Fetch the fights of each event
            **********************************/                       
            foreach($eventArray as $event)
            {
                $fights = $this->Fight->find('all', array(
                    'conditions' => array(
                        'Fight.events_id' => $event
                    ),
                    'recursive' => -1
                ));
                /*
                 Fetch a fight, and assign each fight to the image 
                 **/
                foreach($fights as $fight)
                {
                    $this->ibopadmin_saveFightMultiple($idImage,$fight['Fight']['id']);
                }
            }
            /***********************************/
            
            /*
            Fetch the venues of each event and assign the image to it
            **********************************/
            foreach($eventArray as $event)
            {
                $eventObject = $this->Event->find('first', array(
                        'conditions' => array(
                            'Event.id' => $event
                        ),
                        'fields' => array(
                            'Event.id',
                            'Event.name',
                            'Event.venues_id'
                        ),
                        'recursive' => -1
                ));
                $this->ibopadmin_saveVenuesMultiple($idImage,$eventObject['Event']['venues_id']);
            }
            /***********************************/
            
            /*
             Fetch the identities of a fight an assign it to an image   
            **********************************/
            foreach($fights as $fight)
            {
                $fightIdentities = $this->FightIdentity->find('all', array(
                        'conditions' => array(
                            'FightIdentity.fights_id' => $fight['Fight']['id']
                        ),
                        'fields' => array(
                            'FightIdentity.identities_id'
                        ),
                        'recursive' => -1
                ));
                foreach ($fightIdentities as $identity)
                {
                    $this->ibopadmin_saveIdentityMultiple($idImage, $identity['FightIdentity']['identities_id']);                    
                }
            }
            /**********************************/
            $this->Session->setFlash(__('Fights, persons and venues of the event assigned to poster', true), 'alert-success');
            return $this->redirect(array('action' => 'assign/' .  base64_encode($idImage)));

        }
        
        /**
	 * This method assign an image to a fight, it have some slight
         * modifications respect to saveFight because data need to be
         * saved using a loop, due to the need of assigning all the 
         * fights of an event to an image. Return a false to avoid the call
         * to a view, which is the standard behavior on cakephp.
         * This works the same for saveFightMultiple, saveVenuesMultiple
         * and saveIdentityMultiple
	 *
	 * @param string $idImage
         * @param string $idFight
	 * @return false
	 */
        public function ibopadmin_saveFightMultiple($idImage = null, $idFight = null){
            $this->layout = 'ajax';
            if (($idImage != null) || ($idFight != null)){
                
                $fightsImage['FightsImage']['fights_id'] = $idFight;
                $fightsImage['FightsImage']['images_id'] = $idImage;
                $this->FightsImage->create();//this is used when data needs to be saved in loops
                if($this->FightsImage->save($fightsImage)){
                    
                    $fight = $this->Fight->find('first', array(
                        'conditions' => array(
                            'Fight.id' => $idFight 
                        ),
                        'fields' => array(
                            'Fight.id',
                            'Fight.title'
                        ),
                        'recursive' => -1
                    ));
                    
                    $this->set('data', array('id' => $this->FightsImage->getLastInsertId(), 'Fight' => $fight));
                } else {
                    $this->set('data', 'error');
                }
            }
            return false;
        }
        
        /**
	 * ibopadmin_saveVenuesMultiple method
	 *
	 * @param string $idImage
         * @param string $idVenue
	 * @return false
	 */
        public function ibopadmin_saveVenuesMultiple($idImage = null, $idVenue = null){
            
            $this->layout = 'ajax';
            if(($idImage != null) || ($idVenue != null)){
                $venuesImage['VenuesImage']['venues_id'] = $idVenue;
                $venuesImage['VenuesImage']['images_id'] = $idImage;
                $this->VenuesImage->create();//this is used when data needs to be saved in loops
                if($this->VenuesImage->save($venuesImage)){
                    $venues = $this->Venue->find('first', array(
                        'conditions' => array(
                            'Venue.id' => $idVenue
                        ),
                        'fields' => array(
                            'Venue.id',
                            'Venue.name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->VenuesImage->getLastInsertId(), 'Venue' => $venues));
                }
            }
            return false;
        }
        
        /**
	 * ibopadmin_saveIdentityMultiple method
	 *
	 * @param string $idImage
         * @param string $idIdentity
	 * @return false
	 */
        public function ibopadmin_saveIdentityMultiple($idImage = null, $idIdentity = null){
            $this->layout = 'ajax';
            if (($idImage != null) || ($idIdentity != null)){
                $identitiesImage['IdentitiesImage']['images_id'] = $idImage;
                $identitiesImage['IdentitiesImage']['identities_id'] = $idIdentity;
                $this->IdentitiesImage->create();//this is used when data needs to be saved in loops
                if($this->IdentitiesImage->save($identitiesImage)){
                    $identityName = $this->Identity->find('first', array(
                        'conditions' => array(
                            'Identity.id' => $idIdentity
                        ),
                        'fields' => array(
                            'Identity.name',
                            'Identity.last_name'
                        ),
                        'recursive' => -1
                    ));
                    $this->set('data', array('id' => $this->IdentitiesImage->getLastInsertId(), 'identity' => $identityName, 'idIdentity' => $idIdentity));
                } else {
                    $this->set('data', 'error');
                }
            }
            return false;
        }
        
}