<?php
App::uses('AppController', 'Controller');


class StatisticsController extends AppController{
    
    public $uses = array(
        'Identity',
        'Fight',
        'Event',
        'Venue',
        'Statistic'
    );
    public function index(){
            $cPersons = $this->Identity->find('count');
            $cFights  = $this->Fight->find('count');
            $cEvents  = $this->Event->find('count');
            $cVenues  = $this->Venue->find('count');
            
            $record['Statistic']['count_persons']   = $cPersons;
            $record['Statistic']['count_fights']    = $cFights;
            $record['Statistic']['count_events']    = $cEvents;
            $record['Statistic']['count_venues']    = $cVenues;
            $record['Statistic']['date']            = date('Y-m-d H:s:i');
            
            $this->Statistic->save($record);
            
            die();
          
    }
    
}
