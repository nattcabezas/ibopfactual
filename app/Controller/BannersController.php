<?php
App::uses('AppController','Controller');
/**
 * Banners Controller
 *
 * @property Banner $Banner
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BannersController extends AppController{
    /**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session');
    public $uses       = array('Banner'); // this an array base on the models
    public $helper     = array('newJsonEncode');
    /**
 * ibopadmin_index method
 *
 * @return void
 */
    public function ibopadmin_index(){
        $this->Paginator->settings = array(
            'order' => array(
                'Banner.id' => 'DESC'
            )
        );
        $this->set('banners', $this->Paginator->paginate());
    }
         /**
 * ibopadmin_add method
 *
 * @return void
 */
    public function ibopadmin_add(){
            if($this->request->is('post')){
                if( $this->request->data('Banner.img_file.name') != "" ){
                            $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'banners' . DIRECTORY_SEPARATOR;                            
                            $upload = move_uploaded_file($this->request->data('Banner.img_file.tmp_name'), $hostImages . $this->request->data('Banner.img_file.name'));
                            
                            if(!$upload){
                                $this->Session->setFlash(__('The Banner Image could not be saved. error upload the image', true), 'alert-danger');
                                return $this->redirect(array('action' => 'add'));
                            }
                        }                           
                $this->Banner->create();             
                if ($this->Banner->save($this->request->data)){
                        $this->Session->setFlash(__('The banner has been saved.',true), 'alert-success');
                        return $this->redirect(array('action'=>'index'));
                }else{
                    $this->Session->setFlash(__('The banner could not be saved. Please try again.',true), 'alert-danger');
                    return $this->redirect(array('action' => 'index'));
                }
            }
    }
    /**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id 
 * @return void
 */
    public function ibopadmin_edit($id = null){
        $id = base64_decode($id);
        if (!$this->Banner->exists($id)) {
			$this->Session->setFlash(__('Invalid banner', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
                
        if ($this->request->is(array('banner', 'put'))){
            if( $this->request->data('Banner.img_file.name') != "" ){                             
                            $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'banners' . DIRECTORY_SEPARATOR;
                            $upload = move_uploaded_file($this->request->data('Banner.img_file.tmp_name'), $hostImages . $this->request->data('Banner.img_file.name'));                       
                            if(!$upload){
                                $this->Session->setFlash(__('The Banner Image could not be saved. error upload the image', true), 'alert-danger');
                                return $this->redirect(array('action' => 'add'));
                            }
                        }
            if ($this->Banner->save($this->request->data)) {                            
                    $this->Session->setFlash(__('The banner has been saved.', true), 'alert-success');
                    return $this->redirect(array('action' => 'index'));
            } else {
                    $this->Session->setFlash(__('The banner could not be saved. Please, try again.', true), 'alert-danger');
                    return $this->redirect(array('action' => 'index'));
            }
            
        }else{
            $options 	= array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
			$bannerData	= $this->Banner->find('first', $options);
			$this->request->data = $bannerData;
			$this->set('banner', $bannerData);
        }        
    } 
    public function ibopadmin_searchBanners(){
        if($this->request->is('post')){
           
            $banners = $this->Banner->find(
                'all', array (
                   'conditions' => array(
                       'or' => array(
                        'Banner.tittle LIKE' => '%' . $this->request->data('searchBanners.keywork') . '%' 
                        )
                    )
                )        
            );
            $this->set('banners', $banners);
        }
    } 
    public function ibopadmin_delete($id = null){
        $id = base64_decode($id);
        $this->Banner->id = $id;
        
        $url = $this->Banner->find('first',array(
                'conditions' => array(
                    'Banner.id' => $id
                ),
            'fields' => array(
                'Banner.img'
            ),
                'recursive' => -1
            )
        );
        if (!$this->Banner->exists()) {
			$this->Session->setFlash(__('Invalid banner', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
        }
        $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'banners' . DIRECTORY_SEPARATOR . $url['Banner']['img'];
        
        if ($this->Banner->delete($id, true)) {
                if (!unlink($hostImages))
                    {
                        $this->Session->setFlash(__('The image could not be deleted. Please, try again.', true), 'alert-danger');
                    }
                $this->Session->setFlash(__('The image has been deleted.', true), 'alert-success');
        } else {
                $this->Session->setFlash(__('The banner could not be deleted. Please, try again.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'index'));
    }
}
