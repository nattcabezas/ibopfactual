<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller 
 *
 * @property Category
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CategoriesController extends AppController {
    //put your code here
    public $components = array('Paginator', 'Session');
     
    /*
     * index method
     * 
     * @return void
     */
    public function ibopadmin_index() { 
         $this->Category->recursive = 0;
	 $this->set('categories', $this->paginate('Category'));
    }
    /*
     * add method
     * 
     * @return true or false
     */
    public function ibopadmin_add() { 
        if ($this->request->is('post')) {
                $this->Category->create();
                if ($this->Category->save($this->request->data)) {
                    $this->Session->setFlash(__('The Category has been saved.', true), 'alert-success');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The Category could not be saved. Please, try again.', true), 'alert-danger');
                }            
        }
    }
    /*
     * edit method
     * @param string $id
     * @return void
     */
    public function ibopadmin_edit($id = null) { 
            $id = base64_decode($id);
            if (!$this->Category->exists($id)) {
                    $this->Session->setFlash(__('Invalid Category', true), 'alert-danger');
                    return $this->redirect(array('action' => 'index'));
                }
            if ($this->request->is(array('post', 'put'))) {            
               if ($this->Category->save($this->request->data)) {               
                        $this->Session->setFlash(__('The Category has been saved.', true), 'alert-success');
                        return $this->redirect(array('action' => 'index'));
                    }else {
                        $this->Session->setFlash(__('The Category could not be saved. Please, try again.', true), 'alert-danger');
                            }
                     } else {
                        $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
                        $this->request->data = $this->Category->find('first', $options);
            }  
        }
    /*
     * delete method
     * @param string $id
     * @return void
     */    
    public function ibopadmin_delete($id = null){
            $id = base64_decode($id);
            $this->Category->id = $id;
            if(!$this->Category->exists()){
                $this->Session->setFlash(__('Invalid Category', true), 'alert-danger');
                return $this->redirect(array('action' => 'index'));
            }
            if ($this->Category->delete()) {
                $this->Session->setFlash(__('The Category has been deleted.'), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Category could not be deleted. Please, try again.'), 'alert-danger');
            }
            return $this->redirect(array('action' => 'index'));
    }
}

