<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');//permite usar el security->hash encriptar las contraseñas
/**
 * Validation Controller
 *
 * @property Validation $Validation
 * @author rudysibaja <neo.nou@gmail.com>
 * controller to manage the user admin validation
 */
class ValidationController extends AppController  {

	public $name = 'Validation';
	public $uses = array('User', 'Role'); //permite usar el modelo usuarios

	/**
	 * index de la seccion administrativa login
	 * @return void
	 */
	public function ibopadmin_index($url = null){
		$this->layout = 'login';
                $this->set('url', $url);
	}

	/**
	 * valida el usuario contra la base de datos
	 * @return void 
	 */
	public function ibopadmin_login(){

		if($this->request->is('post')){
                    
			$postData = $this->request->data;
                        $postData['User']['password'] = Security::hash($postData['User']['password'], 'sha1', true);
			$userData = $this->User->find(
				'first', array(
					'conditions' => array(
						'User.username' => $postData['User']['username'],
						'User.password' => $postData['User']['password'],
					),
					'recursive' => 1
				)
			);
			if($userData){
				$this->Session->write('User', $userData);
                                $this->Session->write('Config.language', $userData['User']['language']);
                                if($postData['User']['url'] == ''){
                                    return $this->redirect(array('controller' => 'Dash', 'action' => 'index'));
                                } else {
                                    $url = base64_decode($postData['User']['url']);
                                    return $this->redirect('/' . $url);
                                }
			} else {
				$this->Session->setFlash('', 'error_login');
				return $this->redirect(array('action' => 'index'));
			}
		} else {
			return $this->redirect(array('action' => 'index'));
		}
	}

	/**
	 * cierra la sesion del usuario activo
	 * @return void 
	 */
	public function ibopadmin_logout(){
		$this->Session->write('User', null);
		$this->Session->delete('User');
		$this->redirect('/ibopadmin');
	}
}