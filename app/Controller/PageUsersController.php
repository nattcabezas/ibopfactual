<?php
App::uses('AppController', 'Controller');
/**
 * PageUsers Controller
 *
 * @property PageUser $PageUser
 * @property PaginatorComponent $Paginator
 */
class PageUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
        
        public function ibopadmin_index(){
            $this->PageUser->recursive = 0;
            $this->Paginator->settings = array(
                'order' => array(
                    'Contact.id' => 'DESC'
                )
            );
            $this->set('pageUser', $this->Paginator->paginate());
        }
        
        public function ibopadmin_saveType($idUser = null){
            echo ($idUser);
            if($this->request->is('post')){
                $this->PageUser->save($this->request->data);
            }
            die();
        }
           
        
         public function ibopadmin_deleteUser($idUser = null ){
          $this->PageUser->id = $idUser;
   
            if ($this->PageUser->delete()) {

                    $this->Session->setFlash(__('The User has been deleted.', true), 'alert-success');
            }else{
           	
                    $this->Session->setFlash(__('The User could not be deleted. Please, try again.', true), 'alert-danger');
            }
		//}
   
            return $this->redirect(array('controller' => 'PageUsers', 'action' => 'index'));
      
          
        }


}
