<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');//permite usar el security->hash encriptar las contraseñas
/**
 * User Controller
 *
 * @property Identity $Identity
 * @author rudysibaja <neo.nou@gmail.com>
 * contolador de usuarios
 */
class UserController extends AppController  {

	public $name 		= 'User';
	public $uses 		= array('User', 'Role');
	public $paginate 	= array(
		'limit' => 10,
		'order' => array(
			'User.id' => 'ASC'
		)
	);

	/**
	 * index de usuarios en la seccion administrativa
	 * @return void 
	 */
	public function ibopadmin_index(){
		$this->set('users', $this->paginate('User'));
	}

	/**
	 * funcion de agregar usuarios
	 * @return void
	 */
	public function ibopadmin_add(){
		if($this->request->is('post')){
			$newUser = $this->request->data;
			if($newUser['User']['confirm-password'] == $newUser['User']['password']){
				$newUser['User']['password'] = Security::hash($newUser['User']['password'], 'sha1', true);
				if($this->User->save($newUser)){
					$roleData['Role']['users_id'] = $this->User->getLastInsertId();
					$this->Role->save($roleData);
					$userId = base64_encode($this->User->getLastInsertId());
					$this->Session->setFlash(__('user saved successfully, now you can assign user permissions', true), 'alert-success');
					return $this->redirect(array('action' => 'roles/' . $userId));
				} else {
					$this->Session->setFlash(__('<strong>Error!</strong> the user could not be saved, please try again', true), 'alert-danger');
					return $this->redirect(array('action' => 'add'));
				}
			} else {
				$this->Session->setFlash('<strong>Error!</strong> passwords do not match, please try again', 'alert-danger');
			}
		}
	}

	/**
	 * funcion donde se asignan los roles a los usuarios
	 * @param  integer $idUser identificador del usuario
	 * @return void       
	 */
	public function ibopadmin_roles($idUser = null){
		$this->set('roles', $this->Role->find(
			'first', array(
				'conditions' => array(
					'Role.users_id' => base64_decode($idUser)
				),
				'recursive' => -1
			)
		));
		if($this->request->is('post')){
			if($this->Role->save($this->request->data)){
				$this->Session->setFlash(__('User Roles, saved successfully', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('<strong>Error!</strong> the user roles could not be saved, please try again', true), 'alert-danger');
				return $this->redirect(array('action' => 'roles/' . $idUser));
			}
		}
	}

	/**
	 * funcion de edicion del usuario
	 * @param  integer $idUser 
	 * @return void
	 */
	public function ibopadmin_edit($idUser = null){
		$this->set('userData', $this->User->find(
			'first', array(
				'conditions' => array(
					'User.id' => base64_decode($idUser)
				)
			)
		));
		if($this->request->is('post')){
			$postData = $this->request->data;
			if($postData['User']['password'] == ""){
				$userPost['User']['id'] 		= $postData['User']['id'];
				$userPost['User']['username']           = $postData['User']['username'];
				$userPost['User']['name'] 		= $postData['User']['name'];
				$userPost['User']['email'] 		= $postData['User']['email'];
				if($this->User->save($userPost)){
					$this->Session->setFlash(__('user saved successfully', true), 'alert-success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('<strong>Error!</strong> the user could not be saved, please try again', true), 'alert-danger');
					return $this->redirect(array('action' => 'index'));
				}
			} else if($postData['User']['confirm-password'] == $postData['User']['password']){
				$postData['User']['password'] = Security::hash($postData['User']['password'], 'sha1', true);
				if($this->User->save($postData)){
					$this->Session->setFlash(__('user saved successfully', true), 'alert-success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('<strong>Error!</strong> the user could not be saved, please try again', true), 'alert-danger');
					return $this->redirect(array('action' => 'add'));
				}
			}
		}
	}
        
        public function ibopadmin_mysqlDump($tables = '*') {
            $return = '';

            $modelName = $this->modelClass;

            $dataSource = $this->{$modelName}->getDataSource();
            $databaseName = $dataSource->getSchemaName();


            // Do a short header
            $return .= '-- Database: `' . $databaseName . '`' . "\n";
            $return .= '-- Generation time: ' . date('D jS M Y H:i:s') . "\n\n\n";


            if ($tables == '*') {
                $tables = array();
                $result = $this->{$modelName}->query('SHOW TABLES');
                foreach($result as $resultKey => $resultValue){
                    $tables[] = current($resultValue['TABLE_NAMES']);
                }
            } else {
                $tables = is_array($tables) ? $tables : explode(',', $tables);
            }

            // Run through all the tables
            foreach ($tables as $table) {
                $tableData = $this->{$modelName}->query('SELECT * FROM ' . $table);

                $return .= 'DROP TABLE IF EXISTS ' . $table . ';';
                $createTableResult = $this->{$modelName}->query('SHOW CREATE TABLE ' . $table);
                $createTableEntry = current(current($createTableResult));
                $return .= "\n\n" . $createTableEntry['Create Table'] . ";\n\n";

                // Output the table data
                foreach($tableData as $tableDataIndex => $tableDataDetails) {

                    $return .= 'INSERT INTO ' . $table . ' VALUES(';

                    foreach($tableDataDetails[$table] as $dataKey => $dataValue) {

                        if(is_null($dataValue)){
                            $escapedDataValue = 'NULL';
                        }
                        else {
                            // Convert the encoding
                            $escapedDataValue = mb_convert_encoding( $dataValue, "UTF-8", "ISO-8859-1" );

                            // Escape any apostrophes using the datasource of the model.
                            $escapedDataValue = $this->{$modelName}->getDataSource()->value($escapedDataValue);
                        }

                        $tableDataDetails[$table][$dataKey] = $escapedDataValue;
                    }
                    $return .= implode(',', $tableDataDetails[$table]);

                    $return .= ");\n";
                }

                $return .= "\n\n\n";
            }

            // Set the default file name
            $fileName = $databaseName . '-backup-' . date('Y-m-d_H-i-s') . '.sql';

            // Serve the file as a download
            $this->autoRender = false;
            $this->response->type('Content-Type: text/x-sql');
            $this->response->download($fileName);
            $this->response->body($return);
        }

}