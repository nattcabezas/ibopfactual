<?php
App::uses('AppController', 'Controller');
/**
 * Weights Controller
 *
 * @property Weight $Weight
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @author rudysibaja <neo.nou@gmail.com>
 */
class WeightsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Weight->recursive = 0;
		$this->set('weights', $this->Paginator->paginate());
	}


/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->Weight->create();
			if ($this->Weight->save($this->request->data)) {
				$this->Session->setFlash(__('The weight has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The weight could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->Weight->exists($id)) {
			$this->Session->setFlash(__('Invalid weight', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Weight->save($this->request->data)) {
				$this->Session->setFlash(__('The weight has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The weight could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('Weight.' . $this->Weight->primaryKey => $id));
			$this->request->data = $this->Weight->find('first', $options);
		}
	}
}
