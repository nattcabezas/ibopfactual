f<?php
App::uses('AppController', 'Controller');
/**
 * SourceOdds Controller
 *
 * @property SourceOdd $SourceOdd
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SourceOddsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->SourceOdd->recursive = 0;
		$this->set('sourceOdds', $this->Paginator->paginate());
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {

			if( $this->request->data('SourceOdd.img_file.name') != "" ){
				$hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'odds_images' . DIRECTORY_SEPARATOR;
				$upload = move_uploaded_file($this->request->data('SourceOdd.img_file.tmp_name'), $hostImages . $this->request->data('SourceOdd.img_file.name'));
				if(!$upload){
					$this->Session->setFlash(__('The sourceodd could not be saved. error upload the image', true), 'alert-danger');
					return $this->redirect(array('action' => 'add'));
				}
			}
			$this->SourceOdd->create();
			if ($this->SourceOdd->save($this->request->data)) {
				$this->Session->setFlash(__('The sourceodd has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The source odd could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->SourceOdd->exists($id)) {
			$this->Session->setFlash(__('Invalid source odd', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if( $this->request->data('SourceOdd.img_file.name') != "" ){
				$hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'odds_images' . DIRECTORY_SEPARATOR;
				$upload = move_uploaded_file($this->request->data('SourceOdd.img_file.tmp_name'), $hostImages . $this->request->data('SourceOdd.img_file.name'));
				if(!$upload){
					$this->Session->setFlash(__('The sourceodd could not be saved. error upload the image', true), 'alert-danger');
					return $this->redirect(array('action' => 'add'));
				}
			}

			if ($this->SourceOdd->save($this->request->data)) {
				$this->Session->setFlash(__('The source odd has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The source odd could not be saved. Please, try again.', true), 'alert-danger');
			}

		} else {
			$options = array('conditions' => array('SourceOdd.' . $this->SourceOdd->primaryKey => $id));
			$this->request->data = $this->SourceOdd->find('first', $options);
		}
	}

/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($id = null) {
		$this->SourceOdd->id = $id;
		if (!$this->SourceOdd->exists()) {
			throw new NotFoundException(__('Invalid source odd'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SourceOdd->delete()) {
			$this->Session->setFlash(__('The source odd has been deleted.'));
		} else {
			$this->Session->setFlash(__('The source odd could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
