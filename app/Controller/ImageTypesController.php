<?php
App::uses('AppController', 'Controller');
/**
 * ImageTypes Controller
 *
 * @property ImageType $ImageType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ImageTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->ImageType->recursive = 0;
		$this->set('imageTypes', $this->Paginator->paginate());
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->ImageType->create();
			if ($this->ImageType->save($this->request->data)) {
				$this->Session->setFlash(__('The image type has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The image type could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
		$id = base64_decode($id);
		if (!$this->ImageType->exists($id)) {
			$this->Session->setFlash(__('Invalid Type Image', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ImageType->save($this->request->data)) {
				$this->Session->setFlash(__('The image type has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The image type could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('ImageType.' . $this->ImageType->primaryKey => $id));
			$this->request->data = $this->ImageType->find('first', $options);
		}
	}

/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($id = null) {
		$this->ImageType->id = $id;
		if (!$this->ImageType->exists()) {
			throw new NotFoundException(__('Invalid image type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ImageType->delete()) {
			$this->Session->setFlash(__('The image type has been deleted.'));
		} else {
			$this->Session->setFlash(__('The image type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
