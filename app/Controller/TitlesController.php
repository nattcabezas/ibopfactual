<?php
App::uses('AppController', 'Controller');
/**
 * Titles Controller
 *
 * @property Title $Title
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @author rudysibaja <neo.nou@gmail.com>
 */
class TitlesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'PaginationRecall');
        public $uses = array('Title', 'FightsTitle', 'TitlesCategory');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index() {
		$this->Title->recursive = 0;
                $this->Paginator->settings = array(
                    'order' => array(
                        'Title.id' => 'DESC'
                    )
                );
		$this->set('titles', $this->Paginator->paginate());
	}
/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add() {
		if ($this->request->is('post')) {
			$this->Title->create();
			if ($this->Title->save($this->request->data)) {
				$this->Session->setFlash(__('The title has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The title could not be saved. Please, try again.', true), 'alert-danger');
			}
		}
                $titleCategory = $this->TitlesCategory->find('list', array('fields' => array('TitlesCategory.id','TitlesCategory.description') ) );
		$this->set('titleCategory',$titleCategory);
                
                
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($id = null) {
                
		$id = base64_decode($id);
		if (!$this->Title->exists($id)) {
			$this->Session->setFlash(__('Invalid title', true), 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Title->save($this->request->data)) {
				$this->Session->setFlash(__('The title has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The title could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
                        /* */
			$options = array('conditions' => array('Title.' . $this->Title->primaryKey => $id));
			$this->request->data = $this->Title->find('first', $options);
                        $this->set('titlesCategory',$this->TitlesCategory->find('list', array('fields' => array('TitlesCategory.id','TitlesCategory.description'))) );
                        $this->set('idTitle', $id);
                        
		}
                
	}

/**
 * ibopadmin_getTitle method
 *
 * @param string $title
 * @return void
 */
        public function ibopadmin_getTitle($title = null) {
            $this->layout = 'ajax';
            $titles = $this->Title->find(
                'all', array(
                    'conditions' => array(
                        'OR' => array(
                            'Title.name LIKE' => '%' . $title . '%',
                            'Title.description LIKE' => '%' . $title . '%'
                        )
                    ),
                    'limit' => 25,
                    'order' => array(
                        'Title.name' => 'ASC'
                    )
                )
            );
            $newTitles = array();
            foreach ($titles as $title) {
                $newTitle['id']             = $title['Title']['id'];
                $newTitle['label']          = $title['Title']['name'];
                $newTitle['description']    = $title['Title']['description'];
                $newTitles[]                = $newTitle;
            }
            $this->set('titles', $newTitles);
        }
        
/**
 * ibopadmin_list method
 * this function return tittles used in n fights
 * @param string $idTitle
 * @return void
 */
        public function ibopadmin_list($idTitle = null) {            
            $idTitle = base64_decode($idTitle);
            $fights = $this->FightsTitle->find('all', array(
                'conditions' => array(
                    'FightsTitle.titles_id' => $idTitle
                )
            ));
            
           $this->set('fights', $fights);
          
	}
        
}