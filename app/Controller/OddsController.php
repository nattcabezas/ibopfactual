<?php
App::uses('AppController', 'Controller');
/**
 * Odds Controller
 *
 * @property Odd $Odd
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class OddsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
        
        public $uses = array('Odd', 'SourceOdd', 'Fight');

/**
 * ibopadmin_index method
 *
 * @return void
 */
	public function ibopadmin_index($idFights = null) {
                $idFights = base64_decode($idFights);
		$this->Odd->recursive = 0;
                $odds = $this->Odd->find(
                    'all', array(
                        'conditions' => array(
                            'Odd.fights_id' => $idFights
                        )
                    )
                );
                $options = array('conditions' => array('Fight.' . $this->Identity->primaryKey => $idFights));
                $this->set('fight', $this->Fight->find('first', $options));
                $this->set('titleName', $this->getTitleName($idFights));
                $this->set('idFights', $idFights);
		$this->set('odds', $odds);
	}

/**
 * ibopadmin_add method
 *
 * @return void
 */
	public function ibopadmin_add($idFights = null) {
                $idFights = base64_decode($idFights);
                if ($this->request->is('post')) {
                        $this->Odd->create();
                        if ($this->Odd->save($this->request->data)) {
                                $this->Session->setFlash(__('The odd has been saved.', true), 'alert-success');
                                return $this->redirect(array('action' => 'index/' . base64_encode($idFights)));
                        } else {
                                $this->Session->setFlash(__('The odd could not be saved. Please, try again.', true), 'alert-danger');
                        }
                }
                $sourceOdds = $this->SourceOdd->find('list');
                $this->set('idFights', $idFights);
                $this->set('titleName', $this->getTitleName($idFights));
                $this->set(compact('sourceOdds'));
	}

/**
 * ibopadmin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_edit($idFights = null, $id = null) {
                $idFights   = base64_decode($idFights);
                $id         = base64_decode($id);
		if (!$this->Odd->exists($id)) {
                        $this->Session->setFlash(__('Invalid odd', true), 'alert-danger');
                        return $this->redirect(array('action' => 'index/' . base64_encode($idFights)));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Odd->save($this->request->data)) {
				$this->Session->setFlash(__('The odd has been saved.', true), 'alert-success');
				return $this->redirect(array('action' => 'index/' . base64_encode($idFights)));
			} else {
				$this->Session->setFlash(__('The odd could not be saved. Please, try again.', true), 'alert-danger');
			}
		} else {
			$options = array('conditions' => array('Odd.' . $this->Odd->primaryKey => $id));
			$this->request->data = $this->Odd->find('first', $options);
		}
		$sourceOdds = $this->SourceOdd->find('list');
		$this->set(compact('sourceOdds'));
                $this->set('titleName', $this->getTitleName($idFights));
                $this->set('idFights', $idFights);
	}

/**
 * ibopadmin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ibopadmin_delete($idFights = null, $id = null) {
                $idFights   = base64_decode($idFights);
                $id         = base64_decode($id);
		
                $this->Odd->id = $id;
		if (!$this->Odd->exists()) {
			$this->Session->setFlash(__('Invalid odd', true), 'alert-danger');
                        return $this->redirect(array('action' => 'index/' . base64_encode($idFights)));
		}
		
		if ($this->Odd->delete()) {
			$this->Session->setFlash(__('The odd has been deleted.', true), 'alert-success');
		} else {
			$this->Session->setFlash(__('The odd could not be deleted. Please, try again.', true), 'alert-danger');
		}
		return $this->redirect(array('action' => 'index/' . base64_encode($idFights)));
	}
        
/**
 * getTitleName method
 *
 * @param string $id
 * @return string
 */
        public function getTitleName($id= null){
             if($id != null){
                $fights = $this->Fight->find(
                    'first', array(
                        'conditions' => array(
                            'Fight.id' => $id
                        ),
                        'fields' => array(
                            'Fight.title'
                        ),
                        'recursive' => -1
                    )
                );
                return $fights['Fight']['title'];
             }  
        }
}
