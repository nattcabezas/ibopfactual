<?php

App::uses('AppController', 'Controller');

/**
 * Featured Controller
 * 
 * CakePHP FeaturedController
 * @author rudysibaja <neo.nou@gmail.com>
 */
class FeaturedController extends AppController {
    
    /**
    * Name
    *
    * @var string
    */
    public $name = "Featured";
    
    /**
    * Uses
    *
    * @var array
    */
    public $uses = array('FeaturedEvent', 'FeaturedFight', 'FeaturedIdentity', 'FeaturedVenue');
    
    /**
    * ibopadmin_featuredPersons method
    * @return void 
    */
    public function ibopadmin_featuredPersons(){
        if($this->request->is('post')){
            $lastPosition = $this->FeaturedIdentity->find(
                'first', array(
                    'order' => array(
                        'FeaturedIdentity.position' => 'DESC'
                    ),
                    'fields' => array(
                        'FeaturedIdentity.position'
                    )
                )
            );
            if($lastPosition){
                $featuredIdentity['FeaturedIdentity']['position'] = $lastPosition['FeaturedIdentity']['position'] + 1;
            } else {
                $featuredIdentity['FeaturedIdentity']['position'] = 1;
            }
            $featuredIdentity['FeaturedIdentity']['identities_id'] = $this->request->data('FeaturedIdentity.identities_id');
            if($this->FeaturedIdentity->save($featuredIdentity)){
                $this->Session->setFlash(__('The Featured Person has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Featured Person could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'featuredPersons'));
        }
        $identities = $this->FeaturedIdentity->find(
            'all', array(
                'order' => array(
                    'FeaturedIdentity.position' => 'ASC'
                )
            )
        );
        $this->set('identities', $identities);
        
    }
    
    /**
    * ibopadmin_personsOrder method
    * @return void 
    */
    public function ibopadmin_personsOrder(){
        if($this->request->is('post')){
            $featuredIdentity['FeaturedIdentity']['id']         = $this->request->data('id');
            $featuredIdentity['FeaturedIdentity']['position']   = $this->request->data('newOrder');
            $this->FeaturedIdentity->save($featuredIdentity);
        }
        return die();
    }
    
    /**
    * ibopadmin_deltePerson method
    * @param  intiger $id
    * @return void 
    */
    public function ibopadmin_deltePerson($id = null){
        $id = base64_decode($id);
        $this->FeaturedIdentity->id = $id;
        if(!$this->FeaturedIdentity->exists()){
            $this->Session->setFlash(__('Invalid Person', true), 'alert-danger');
            return $this->redirect(array('action' => 'featuredPersons'));
        }
        if ($this->FeaturedIdentity->delete()) {
            $this->Session->setFlash(__('The Person has been deleted.', true), 'alert-success');
        } else {
            $this->Session->setFlash(__('The Person could not be deleted. Please, try again.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'featuredPersons'));
    }
    
    /**
    * ibopadmin_featuredFights method
    * @return void 
    */
    public function ibopadmin_featuredFights(){
        if($this->request->is('post')){
            $lastPosition = $this->FeaturedFight->find(
                'first', array(
                    'order' => array(
                        'FeaturedFight.position' => 'DESC'
                    ),
                    'fields' => array(
                        'FeaturedFight.position'
                    )
                )
            );
            if($lastPosition){
                $featuredFights['FeaturedFight']['position'] = $lastPosition['FeaturedFight']['position'] + 1;
            } else {
                $featuredFights['FeaturedFight']['position'] = 1;
            }
            $featuredFights['FeaturedFight']['fights_id'] = $this->request->data('FeaturedFight.fights_id');
            if($this->FeaturedFight->save($featuredFights)){
                $this->Session->setFlash(__('The Featured Fights has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Featured Fights could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'featuredFights'));
        }
        $fights = $this->FeaturedFight->find(
            'all', array(
                'order' => array(
                    'FeaturedFight.position' => 'ASC'
                )
            )
        );
        $this->set('fights', $fights);
    }
    
    /**
    * ibopadmin_fightOrder method
    * @return void 
    */
    public function ibopadmin_fightOrder(){
        if($this->request->is('post')){
            $featuredFight['FeaturedFight']['id']         = $this->request->data('id');
            $featuredFight['FeaturedFight']['position']   = $this->request->data('newOrder');
            $this->FeaturedFight->save($featuredFight);
        }
        return die();
    }
    
    /**
    * ibopadmin_deleteFight method
    * @param  intiger $id
    * @return void 
    */
    public function ibopadmin_deleteFight($id = null){
        $id = base64_decode($id);
        $this->FeaturedFight->id = $id;
        if(!$this->FeaturedFight->exists()){
            $this->Session->setFlash(__('Invalid Fights', true), 'alert-danger');
            return $this->redirect(array('action' => 'featuredFights'));
        }
        if ($this->FeaturedFight->delete()) {
            $this->Session->setFlash(__('The Fights has been deleted.', true), 'alert-success');
        } else {
            $this->Session->setFlash(__('The Fights could not be deleted. Please, try again.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'featuredFights'));
    }
    
    /**
    * ibopadmin_featuredEvents method
    * @return void 
    */
    public function ibopadmin_featuredEvents(){
        if($this->request->is('post')){
            $lastPosition = $this->FeaturedEvent->find(
                'first', array(
                    'order' => array(
                        'FeaturedEvent.position' => 'DESC'
                    ),
                    'fields' => array(
                        'FeaturedEvent.position'
                    )
                )
            );
            if($lastPosition){
                $featuredEvent['FeaturedEvent']['position'] = $lastPosition['FeaturedEvent']['position'] + 1;
            } else {
                $featuredEvent['FeaturedEvent']['position'] = 1;
            }
            $featuredEvent['FeaturedEvent']['events_id'] = $this->request->data('FeaturedEvent.events_id');
            if($this->FeaturedEvent->save($featuredEvent)){
                $this->Session->setFlash(__('The Featured Event has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Featured Event could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'featuredEvents'));
        }
        $events = $this->FeaturedEvent->find(
            'all', array(
                'order' => array(
                    'FeaturedEvent.position' => 'ASC'
                )
            )
        );
        $this->set('events', $events);
    }
    
    /**
    * ibopadmin_eventOrder method
    * @return void 
    */
    public function ibopadmin_eventOrder(){
        if($this->request->is('post')){
            $featuredEvent['FeaturedEvent']['id']         = $this->request->data('id');
            $featuredEvent['FeaturedEvent']['position']   = $this->request->data('newOrder');
            $this->FeaturedEvent->save($featuredEvent);
        }
        return die();
    }
    
    /**
    * ibopadmin_deleteEvent method
    * @param  intiger $id
    * @return void 
    */
    public function ibopadmin_deleteEvent($id = null){
        $id = base64_decode($id);
        $this->FeaturedEvent->id = $id;
        if(!$this->FeaturedEvent->exists()){
            $this->Session->setFlash(__('Invalid Event', true), 'alert-danger');
            return $this->redirect(array('action' => 'featuredEvents'));
        }
        if ($this->FeaturedEvent->delete()) {
            $this->Session->setFlash(__('The Event has been deleted.', true), 'alert-success');
        } else {
            $this->Session->setFlash(__('The Event could not be deleted. Please, try again.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'featuredEvents'));
    }
    
    /**
    * ibopadmin_featuredVenues method
    * @return void 
    */
    public function ibopadmin_featuredVenues(){
        if($this->request->is('post')){
            $lastPosition = $this->FeaturedVenue->find(
                'first', array(
                    'order' => array(
                        'FeaturedVenue.position' => 'DESC'
                    ),
                    'fields' => array(
                        'FeaturedVenue.position'
                    )
                )
            );
            if($lastPosition){
                $featuredVenue['FeaturedVenue']['position'] = $lastPosition['FeaturedVenue']['position'] + 1;
            } else {
                $featuredVenue['FeaturedVenue']['position'] = 1;
            }
            $featuredVenue['FeaturedVenue']['venues_id'] = $this->request->data('FeaturedVenue.venues_id');
            if($this->FeaturedVenue->save($featuredVenue)){
                $this->Session->setFlash(__('The Featured Venue has been saved.', true), 'alert-success');
            } else {
                $this->Session->setFlash(__('The Featured Venue could not be saved. Please, try again.', true), 'alert-danger');
            }
            return $this->redirect(array('action' => 'featuredVenues'));
        }
        
        $venues = $this->FeaturedVenue->find(
            'all', array(
                'order' => array(
                    'FeaturedVenue.position' => 'ASC'
                )
            )
        );
        $this->set('venues', $venues);
    }
    
    /**
    * ibopadmin_venueOrder method
    * @return void 
    */
    public function ibopadmin_venueOrder(){
        if($this->request->is('post')){
            $featuredVenue['FeaturedVenue']['id']         = $this->request->data('id');
            $featuredVenue['FeaturedVenue']['position']   = $this->request->data('newOrder');
            $this->FeaturedVenue->save($featuredVenue);
        }
        return die();
    }
    
    /**
    * ibopadmin_deleteVenue method
    * @param  intiger $id
    * @return void 
    */
    public function ibopadmin_deleteVenue($id = null){
        $id = base64_decode($id);
        $this->FeaturedVenue->id = $id;
        if(!$this->FeaturedVenue->exists()){
            $this->Session->setFlash(__('Invalid Venue', true), 'alert-danger');
            return $this->redirect(array('action' => 'featuredVenues'));
        }
        if ($this->FeaturedVenue->delete()) {
            $this->Session->setFlash(__('The Venue has been deleted.', true), 'alert-success');
        } else {
            $this->Session->setFlash(__('The Venue could not be deleted. Please, try again.', true), 'alert-danger');
        }
        return $this->redirect(array('action' => 'featuredVenues'));
    }
    
}
